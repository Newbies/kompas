<?php

namespace index\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class IndexAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/index/grid12.css',
        'css/index/james_typography.css',
        'css/index/main.css',
        'css/index/color_scheme_light.css',
        'css/index/colors/color_palette_blue.css',
        'css/index/rapid-icons.css',
        'css/index/js_styles/jquery.maximage.min.css',
        'css/index/responsivity.css',
        'css/index/animate.css',
        'css/index/nivo-lightbox.css',
        'css/index/nivo_lightbox_themes/default/default.css',
        'css/index/owl.carousel.css',
        'css/index/owl.theme.css',
        'css/index/owl.transitions.css',
    ];
    public $js = [
        'js/index/modernizr.custom.js',
        'js/index/jquery-2.1.3.min.js',
        'js/index/jquery.easing.min.js',
        'js/index/jquery.scrollTo.js',
        'js/index/jquery.cycle.all.min.js',
        'js/index/jquery.form.js',
        'js/index/jquery.maximage.min.js',
        'js/index/materialize.js',
        'js/index/classie.js',
        'js/index/pathLoader.js',
        'js/index/preloader.js',
        'js/index/count_down.js',
        'js/index/happy.js',
        'js/index/happy.methods.js',
        'js/index/retina.js',
        'js/index/waypoints.min.js',
        'js/index/nivo-lightbox.min.js',
        'js/index/jquery.fitvids.js',
        'js/index/jquery.stellar.js',
        'js/index/owl.carousel.js',
        'js/index/main.js',
        'js/index/script.js',
        'js/index/mobile-nav.js',
        'js/index/show-hide-nav.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'rmrevin\yii\fontawesome\AssetBundle'
    ];
}
