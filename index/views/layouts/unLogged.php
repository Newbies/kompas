<?php

/* @var $this \yii\web\View */

/* @var $content string */

use common\widgets\Alert;
use index\assets\UnLoggedAsset;
use kartik\social\GoogleAnalytics;
use yii\helpers\Html;

UnLoggedAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <link href="https://fonts.googleapis.com/css?family=Lato:400,700,900" rel="stylesheet">
    <link rel="shortcut icon" href="<?php echo Yii::$app->request->baseUrl; ?>/favicon.ico" type="image/x-icon"/>
    <link rel="icon" href="<?php echo Yii::$app->request->baseUrl; ?>/favicon.ico" type="image/x-icon"/>
    <title>Kompas</title>
    <?php $this->head() ?>
    <?= GoogleAnalytics::widget([]); ?>

</head>
<body>
<?php $this->beginBody() ?>

<div class="bg-login">
    <div class="bg-login-blur">
        <svg
        xmlns="http://www.w3.org/2000/svg"
        xmlns:xlink="http://www.w3.org/1999/xlink"
        width="784px" height="1259px">
        <path fill-rule="evenodd"  stroke="rgb(255, 255, 255)" stroke-width="10px" stroke-dasharray="0, 60" stroke-linecap="round" stroke-linejoin="round" fill="none"
        d="M597.000,10.000 C597.000,10.000 371.241,87.646 464.000,190.000 C556.759,292.353 600.659,464.243 320.000,553.000 C39.340,641.756 -111.288,884.712 132.000,1128.000 C375.287,1371.287 679.289,1185.296 774.000,1007.999 "/>
        </svg>
    </div>
    <div class="bg-login-normal">
        <svg
        xmlns="http://www.w3.org/2000/svg"
        xmlns:xlink="http://www.w3.org/1999/xlink"
        width="784px" height="1259px">
        <path fill-rule="evenodd"  stroke="rgb(255, 255, 255)" stroke-width="10px" stroke-dasharray="0, 60" stroke-linecap="round" stroke-linejoin="round" fill="none"
        d="M597.000,10.000 C597.000,10.000 371.241,87.646 464.000,190.000 C556.759,292.353 600.659,464.243 320.000,553.000 C39.340,641.756 -111.288,884.712 132.000,1128.000 C375.287,1371.287 679.289,1185.296 774.000,1007.999 "/>
        </svg>
    </div>
</div>

<div class="head-un-log">
    <div class="container">
        <div class="row">
            <header>
                <div class="">
                    <div id="logo" class="col-xs-12">
                        <a href="<?php echo yii\helpers\Url::to(['/']); ?> "><img class="" src="/img/logo.png"/></a>
                    </div>
                </div>
            </header>
        </div>
    </div>
    <?= Alert::widget() ?>
    <?= $content ?>
</div>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
