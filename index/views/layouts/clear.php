<?php

/* @var $this \yii\web\View */

/* @var $content string */

use common\widgets\Alert;
use index\assets\IndexAsset;
use kartik\social\GoogleAnalytics;
use yii\helpers\Html;

IndexAsset::register($this);

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <link rel="shortcut icon" href="<?php echo Yii::$app->request->baseUrl; ?>/favicon.ico" type="image/x-icon"/>
    <link rel="icon" href="<?php echo Yii::$app->request->baseUrl; ?>/favicon.ico" type="image/x-icon"/>
    <link href="https://fonts.googleapis.com/css?family=Lato:400,500,600,700,900" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,900,700,500,300,100&subset=latin,latin-ext'
          rel='stylesheet' type='text/css'>
    <link rel="stylesheet" type="text/css"
          href="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.0.3/cookieconsent.min.css"/>
    <script src="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.0.3/cookieconsent.min.js"></script>
    <script>
        window.addEventListener("load", function () {
            window.cookieconsent.initialise({
                "palette": {
                    "popup": {
                        "background": "#efefef",
                        "text": "#404040"
                    },
                    "button": {
                        "background": "#53ca33",
                        "text": "#ffffff"
                    }
                },
                "theme": "classic",
                "position": "bottom-left",
                "content": {
                    "message": "Nasz Serwis internetowy używa plików cookies do prawidłowego działania strony.\n" +
                    "Korzystanie z Serwisu bez zmiany ustawień dla plików cookies oznacza, że będą one\n" +
                    "zapisywane w pamięci urządzenia. Ustawienia te można zmieniać w przeglądarce\n" +
                    "internetowej. Więcej informacji udostępniamy w ",
                    "dismiss": "Akceptuje",
                    "link": "Polityce plików cookies",
                    "href": "http://<?= Yii::$app->params['frontDomain'] ?>/site/cookies"
                }
            })
        });
    </script>
    <title>Kompas</title>
    <?php $this->head() ?>
    <?= GoogleAnalytics::widget([]); ?>

</head>
<body>
<?php $this->beginBody() ?>

<div class="head">
    <?= Alert::widget() ?>
    <?= $content ?>
</div>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
