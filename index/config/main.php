<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-index',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'index\controllers',
    'bootstrap' => ['log'],
    'modules' => [
        'social' => [
            'class' => 'kartik\social\Module',
            'googleAnalytics' => [
                'id' => 'UA-106066444-1',
                'domain' => 'kompas.newbies.pl'
            ],
        ],
    ],
    'components' => [
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
            // 'identityCookie' => ['name' => '_identity-backend', 'httpOnly' => true],
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
                'email' => [
                    'class' => 'yii\log\EmailTarget',
                    'except' => ['yii\web\HttpException:404','yii\web\HttpException:401'],
                    'levels' => ['error'],
                    'message' => ['from' => 'kompas@newbies.pl', 'to' => ['newbies@rst.com.pl']],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'urlManager' => [
            'class' => 'yii\web\UrlManager',
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [

                '<controller:\w+>' => '<controller>/index',
                '<controller:\w+>/<id:\d+>' => '<controller>/view',
                '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',

                'game/<gameId:\d+>/<controller:\w+>' => '<controller>/index',
                'game/<gameId:\d+>/<controller:\w+>/<id:\d+>' => '<controller>/view',
                'game/<gameId:\d+>/<controller:\w+>/<action:\w+>' => '<controller>/<action>',
                'game/<gameId:\d+>/<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
            ],
        ],
        'authClientCollection' => [
            'class'=> 'yii\authclient\Collection',
            'clients' => [
                'facebook' => [
                    'class' => 'yii\authclient\clients\Facebook',
                    'authUrl' => 'https://www.facebook.com/dialog/oauth?display=popup',
                    'clientId' => '2131177227108524',
                    'clientSecret' => 'a4257ad5239acdd5eff3ca143f6e4b35',
                    'attributeNames' => ['name', 'email', 'first_name', 'last_name']
                ],
            ],
        ]
    ],
    'params' => $params,
];
