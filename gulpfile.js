var gulp = require('gulp');
    php  = require('gulp-connect-php');
    browserSync = require('browser-sync');
    concat = require('gulp-concat');
    uglify = require('gulp-uglify');
    cleanCSS = require('gulp-clean-css');
    autoprefixer = require('gulp-autoprefixer');

var reload  = browserSync.reload;

gulp.task('php', function() {
    php.server({ base: 'local.frontend-kompas.com', port: 8000, keepalive: true});
});

gulp.task('js', function() {
    return gulp.src(['frontend/web/lib/js/owl.carousel.min.js','frontend/web/lib/js/*.js'])
    .pipe(concat('complete.js'))
    .pipe(uglify())
    .pipe(gulp.dest('frontend/web/dist/js'));
})

gulp.task('css-basic', function() {
    return gulp.src('frontend/web/lib/css/basic/*.css')
        .pipe(concat('basic.css'))
        .pipe(cleanCSS())
        .pipe(autoprefixer({
            browsers: ['last 2 versions'],
            cascade: false
        }))
        .pipe(gulp.dest('frontend/web/dist/css'));
});

gulp.task('css-game', function() {
    return gulp.src('frontend/web/lib/css/game/*.css')
        .pipe(concat('game.css'))
        .pipe(cleanCSS())
        .pipe(autoprefixer({
            browsers: ['last 2 versions'],
            cascade: false
        }))
        .pipe(gulp.dest('frontend/web/dist/css'));
});

gulp.task('css-login', function() {
    return gulp.src('frontend/web/lib/css/login/*.css')
        .pipe(concat('login.css'))
        .pipe(cleanCSS())
        .pipe(autoprefixer({
            browsers: ['last 2 versions'],
            cascade: false
        }))
        .pipe(gulp.dest('frontend/web/dist/css'));
});

gulp.task('browser-sync',['php', 'js', 'css-basic', 'css-game', 'css-login'], function() {
    gulp.src(['frontend/web/lib/fonts/*']).pipe(gulp.dest('frontend/web/dist/fonts'));
    browserSync({
        proxy: 'local.frontend-kompas.com',
        port: 8000,
        open: true,
        notify: false
    });
});

gulp.task('default', ['browser-sync'], function () {
    gulp.watch(['frontend/**/*.css'], [reload]);
    gulp.watch(['frontend/**/*.php'], [reload]);
    gulp.watch(['frontend/web/lib/js/*.js'], [reload]);
});
