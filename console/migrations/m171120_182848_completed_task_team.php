<?php

use yii\db\Migration;

class m171120_182848_completed_task_team extends Migration
{
    public function safeUp()
    {
        $this->createTable('completed_task_team',[
            'id' => $this->primaryKey(),
            'team_id' => $this->integer(),
            'task_id' => $this->integer(),
            'time' => $this->integer(),
        ]);
    }

    public function safeDown()
    {
       $this->dropTable('completed_task_team');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171120_182848_completed_task_team cannot be reverted.\n";

        return false;
    }
    */
}
