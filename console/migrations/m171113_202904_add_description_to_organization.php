<?php

use yii\db\Migration;

class m171113_202904_add_description_to_organization extends Migration
{
    public function safeUp()
    {
        $this->addColumn('organization','description','text');
    }

    public function safeDown()
    {
        $this->dropColumn('organization','description');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171113_202904_add_description_to_organization cannot be reverted.\n";

        return false;
    }
    */
}
