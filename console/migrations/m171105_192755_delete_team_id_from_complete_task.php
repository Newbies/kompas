<?php

use yii\db\Migration;

class m171105_192755_delete_team_id_from_complete_task extends Migration
{
    public function safeUp()
    {
        $this->dropColumn('completed_task','team_id');
    }

    public function safeDown()
    {
        $this->addColumn('completed_task','team_id','integer');
    }
}
