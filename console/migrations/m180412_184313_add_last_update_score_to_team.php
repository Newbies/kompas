<?php

use yii\db\Migration;

/**
 * Class m180412_184313_add_last_update_score_to_team
 */
class m180412_184313_add_last_update_score_to_team extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn("team", "last_update_score", $this->integer()->defaultValue(time()));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn("team", "last_update_score");
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180412_184313_add_last_update_score_to_team cannot be reverted.\n";

        return false;
    }
    */
}
