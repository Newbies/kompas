<?php

use yii\db\Migration;

class m171107_153912_location extends Migration
{
    public function safeUp()
    {
        $this->createTable('location', [
            'id' => $this->primaryKey()->unique(),
            'address' => $this->string(256)->notNull(),
            'voivodeship' => $this->string(256)->notNull(),
            'postal_code' => $this->string(256)->notNull(),
            'lat' => $this->double()->notNull(),
            'lng' => $this->double()->notNull(),
        ]);
    }

    public function safeDown()
    {
        $this->dropTable('location');
    }
}
