<?php

use yii\db\Migration;

class m171109_155621_add_location_to_game extends Migration
{
    public function safeUp()
    {
        $this->addColumn('game','location_id', $this->integer()->notNull());
    }

    public function safeDown()
    {
        $this->dropColumn('game','location_id');
    }
}
