<?php

use yii\db\Migration;

class m171127_143630_change_address_to_city extends Migration
{
    public function safeUp()
    {
        $this->renameTable('location', 'place');
        $this->renameColumn('place','address', 'city');
        $this->renameColumn('game','location_id', 'place_id');
    }

    public function safeDown()
    {
        $this->renameTable('place', 'location');
        $this->renameColumn('location', 'city', 'address');
        $this->renameColumn('game','place_id', 'location_id');
    }
}
