<?php

use console\migrations\script\change_datetime_to_timestamp_task;
use yii\db\Migration;
use console\migrations\script\change_timestamp_to_datetime;

/**
 * Warning! migration is created only for CR yii migrate/create don't work :(
 */

class  m181003_142618_change_timestamp_to_datetime extends Migration
{

    public function safeUp()
    {
        $script = new change_timestamp_to_datetime();
        $script->get();
        $this->alterColumn('task', 'start_time', $this->dateTime());
        $this->alterColumn('task', 'end_time', $this->dateTime());
        $script->insert();
    }

    public function safeDown()
    {
        $script = new change_datetime_to_timestamp_task();
        $script->get();
        $this->alterColumn('task', 'start_time', $this->integer(11));
        $this->alterColumn('task', 'end_time', $this->integer(11));
        $script->insert();
    }
}
