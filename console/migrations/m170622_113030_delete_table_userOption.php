<?php

use yii\db\Migration;

class m170622_113030_delete_table_userOption extends Migration
{
    public function up()
    {
        $this->execute('DROP TABLE user_option');
    }

    public function down()
    {
        echo "m170622_113030_delete_table_userOption cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
