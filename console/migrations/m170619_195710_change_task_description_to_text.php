<?php

use yii\db\Migration;

class m170619_195710_change_task_description_to_text extends Migration
{
    public function up()
    {
        $this->execute('ALTER TABLE `task` CHANGE `description` `description` TEXT CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL;');
    }

    public function down()
    {
        $this->execute('ALTER TABLE `task` CHANGE `description` `description` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL;');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
