<?php

use yii\db\Migration;

/**
 * Class m180324_123828_delete_tag_from_user
 */
class m180324_123828_delete_tag_from_user extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('user', 'tag');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->addColumn('user', 'tag', $this->text());
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180324_123828_delete_tag_from_user cannot be reverted.\n";

        return false;
    }
    */
}
