<?php

use yii\db\Migration;

class m170915_204943_remane_owner_user_id_to_owner_organization_id extends Migration
{
    public function safeUp()
    {
        $this->renameColumn('game', 'owner_user_id', 'owner_organization_id');
    }

    public function safeDown()
    {
        $this->renameColumn('game', 'owner_organization_id', 'owner_user_id');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170915_204943_remane_owner_user_id_to_owner_organization_id cannot be reverted.\n";

        return false;
    }
    */
}
