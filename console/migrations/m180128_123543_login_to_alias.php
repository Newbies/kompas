<?php

use yii\db\Migration;

/**
 * Class m180128_123543_login_to_alias
 */
class m180128_123543_login_to_alias extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->renameColumn('user', 'login', 'alias');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->renameColumn('user', 'alias', 'login');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180128_123543_login_to_alias cannot be reverted.\n";

        return false;
    }
    */
}
