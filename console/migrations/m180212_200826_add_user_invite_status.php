<?php

use yii\db\Migration;

/**
 * Class m180212_200826_add_user_invite_status
 */
class m180212_200826_add_user_invite_status extends Migration
{

    public function safeUp()
    {
        $this->addColumn('user_game', 'invite_status', "integer");
        $this->addCommentOnColumn('user_game', 'invite_status', '0 - add, 1 - invited, 2 - active ');
    }

    public function safeDown()
    {
        $this->dropColumn("user_game", "invite_status");
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180212_200826_add_user_invite_status cannot be reverted.\n";

        return false;
    }
    */
}
