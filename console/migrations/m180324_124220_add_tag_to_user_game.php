<?php

use yii\db\Migration;

/**
 * Class m180324_124220_add_tag_to_user_game
 */
class m180324_124220_add_tag_to_user_game extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('user_game', 'tag', $this->text());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('user_game', 'tag');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180324_124220_add_tag_to_user_game cannot be reverted.\n";

        return false;
    }
    */
}
