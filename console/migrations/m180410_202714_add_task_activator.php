<?php

use yii\db\Migration;

/**
 * Class m180410_202714_add_task_activator
 */
class m180410_202714_add_task_activator extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('task_activator', [
            'id' => $this->primaryKey()->unique(),
            'activator_id' => $this->integer(11),
            'activator_team_id' => $this->integer(11),
            'task_id' => $this->integer(11),
            'start_time' => $this->integer(11),
            'end_time' => $this->integer(11),
            'game_id' => $this->integer(11),
        ]);

        $this->addColumn('task', 'time', $this->integer(11)->defaultValue(0));
        $this->renameColumn('task', 'premium', 'type');
        $this->addCommentOnColumn('task', 'type', '0 - standard, 1 - premium, 2 - timed');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('task_activator');
        $this->dropColumn('task', 'time');
        $this->renameColumn('task', 'type', 'premium');
        $this->dropCommentFromColumn('task', 'premium');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180410_202714_add_task_activator cannot be reverted.\n";

        return false;
    }
    */
}
