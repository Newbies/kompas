<?php

use yii\db\Migration;

/**
 * Class m180707_092620_add_parent_email
 */
class m180707_092620_add_parent_email extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('user', 'parent_email', $this->char(45));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('user', 'parent_email');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180707_092620_add_parent_email cannot be reverted.\n";

        return false;
    }
    */
}
