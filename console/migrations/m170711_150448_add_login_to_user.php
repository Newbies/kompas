<?php

use yii\db\Migration;

class m170711_150448_add_login_to_user extends Migration
{
    public function safeUp()
    {
        $this->execute("ALTER TABLE `user` ADD `login` VARCHAR(255) NOT NULL;");
    }

    public function safeDown()
    {
        $this->execute("ALTER TABLE `user` DROP `login`");
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170711_150448_add_login_to_user cannot be reverted.\n";

        return false;
    }
    */
}
