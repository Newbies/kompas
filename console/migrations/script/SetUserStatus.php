<?php

namespace console\migrations\script;

/**
 * Created by PhpStorm.
 * User: tycjan
 * Date: 29.03.18
 * Time: 13:24
 */

use common\models\fields\UserFields;
use common\models\User;

/**
 * Class SetUserStatus
 */

class SetUserStatus
{
    private $users;

    /**
     * @params null
     * construct class
     */

    public function __construct()
    {
        $this->users = User::findAll([UserFields::STATUS => null]);
    }

    /**
     *  @params null
     *  setting status ACTIVE to existing users and save to DB
     */

    public function set()
    {
        foreach ($this->users as $user){
            $user->status = UserFields::STATUS_ACTIVE;
            $user->save();
        }
    }

}