<?php

namespace console\migrations\script;

use common\models\fields\UserGameFields;
use common\models\UserGame;
use common\models\Team;

class set_team_score
{
    private $teams;

    public function __construct()
    {
        $this->teams = Team::find()->all();
    }

    public function setFromUserGame(){

        foreach ($this->teams as $team){
            $userGame = UserGame::findOne([UserGameFields::TEAM_ID => $team->id]);
            if(isset($userGame->score)){
                $team->score = $userGame->score;
            }
        }
    }

    public function save(){
        foreach ($this->teams as $team){
            $team->save();
        }
    }

}