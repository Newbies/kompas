<?php

namespace console\migrations\script;

use common\models\Task;
use DateTime;

class change_timestamp_to_datetime
{
    private $startTime;
    private $endTime;
    private $tasks;

    /**
     * change_timestamp_to_datetime constructor.
     */
    public function __construct()
    {
        $this->tasks = Task::find()->all();
        $this->startTime = array();
        $this->endTime = array();
    }

    public function get()
    {
        foreach ($this->tasks as $task) {
            array_push($this->startTime, date('Y-m-d H:i:s', $task->start_time));
            array_push($this->endTime, date('Y-m-d H:i:s', $task->end_time));
        }
    }

    public function insert()
    {
        $i = 0;
        foreach ($this->tasks as $task) {
            $task->start_time = $this->startTime[$i];
            $task->end_time = $this->endTime[$i];
            $task->save(false);
            $i++;
        }
    }
}
