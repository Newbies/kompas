<?php
/**
 * Created by PhpStorm.
 * User: mBoiler
 * Date: 01.06.2018
 * Time: 20:34
 */

namespace console\migrations\script;


use backend\helpers\ArrayHelper;
use common\models\fields\GameFields;
use common\models\Game;
use common\models\Task;

class set_show_number
{

    public function setShowNumber()
    {

        $gamesId = ArrayHelper::map(Game::find()->all(), GameFields::ID);

        foreach ($gamesId as $gameId) {

            $tasks = Task::findByGameId($gameId);
            $x = 0;

            foreach ($tasks as $task) {
                $task->setShowNumber($x);
                $x++;
            }
        }

    }

}