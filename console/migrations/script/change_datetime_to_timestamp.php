<?php

namespace console\migrations\script;

use common\models\Game;
use DateTime;

class change_datetime_to_timestamp
{
    private $startDate;
    private $endDate;
    private $games;

    /**
     * change_datetime_to_timestamp constructor.
     */
    public function __construct()
    {
        $this->games = Game::find()->all();
        $this->startDate = array();
        $this->endDate = array();
    }

    public function get()
    {
        foreach ($this->games as $game) {
            $dateStart = new DateTime($game->start_date);
            $dateEnd = new DateTime($game->end_date);
            array_push($this->startDate, $dateStart->getTimestamp());
            array_push($this->endDate, $dateEnd->getTimestamp());
        }
    }

    public function insert()
    {
        $x = 0;
        foreach ($this->games as $game) {
            $game->start_date = $this->startDate[$x];
            $game->end_date = $this->endDate[$x];
            $game->save(false);
            $x++;
        }
    }
}
