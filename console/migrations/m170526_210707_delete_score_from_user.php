<?php

use yii\db\Migration;

class m170526_210707_delete_score_from_user extends Migration
{
    public function up()
    {
        $this->execute('ALTER TABLE `user` DROP `score`');
    }

    public function down()
    {
        echo "m170526_210707_delete_score_from_user cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
