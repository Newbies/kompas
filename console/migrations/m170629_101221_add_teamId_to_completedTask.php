<?php

use yii\db\Migration;

class m170629_101221_add_teamId_to_completedTask extends Migration
{
    public function up()
    {
        $this->execute("ALTER TABLE `completed_task` ADD `team_id` INT NOT NULL");
    }

    public function down()
    {
        $this->execute("ALTER TABLE `completed_task` DROP `team_id`");
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
