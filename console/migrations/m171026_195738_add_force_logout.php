<?php

use yii\db\Migration;

class m171026_195738_add_force_logout extends Migration
{
    public function safeUp()
    {
        $this->createTable('force_logout',[
            'id'=>$this->primaryKey()->unique(),
            'user_id'=>$this->integer(),
        ]);
    }

    public function safeDown()
    {
        $this->dropTable('force_logout');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171026_195738_add_force_logout cannot be reverted.\n";

        return false;
    }
    */
}
