<?php

use yii\db\Migration;

class m170515_192801_score_board extends Migration
{
    public function up()
    {
        $this->execute("ALTER TABLE `team` ADD `points` INT NOT NULL DEFAULT '0' AFTER `color`, ADD `score` INT NOT NULL DEFAULT '0' AFTER `points`, ADD `members` INT NOT NULL DEFAULT '0' AFTER `score`;");
    }

    public function down()
    {
        echo "m170515_192801_score_board cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
