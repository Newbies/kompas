<?php

use yii\db\Migration;

/**
 * Class m180324_131757_delete_password_columne_from_user
 */
class m180324_131757_delete_password_columne_from_user extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('user', 'password');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->addColumn('user', 'password', $this->text());
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180324_131757_delete_password_columne_from_user cannot be reverted.\n";

        return false;
    }
    */
}
