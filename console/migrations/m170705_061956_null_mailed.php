<?php

use yii\db\Migration;

class m170705_061956_null_mailed extends Migration
{
    public function safeUp()
    {
        $this->execute("ALTER TABLE `task` CHANGE `mailed` `mailed` TINYINT(1) NULL;");
    }

    public function safeDown()
    {
        echo "m170705_061956_null_mailed cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170705_061956_null_mailed cannot be reverted.\n";

        return false;
    }
    */
}
