<?php

use yii\db\Migration;
use console\migrations\script\change_datetime_to_timestamp;

class m171202_134521_change_datetime_to_timestamp extends Migration
{
    public function safeUp()
    {
        $script = new change_datetime_to_timestamp();
        $script->get();
        $this->alterColumn('game', 'start_date', $this->integer(11));
        $this->alterColumn('game', 'end_date', $this->integer(11));
        $script->insert();
    }

    public function safeDown()
    {
        $this->alterColumn('game', 'start_date', 'datetime');
        $this->alterColumn('game', 'end_date', 'datetime');
    }
}
