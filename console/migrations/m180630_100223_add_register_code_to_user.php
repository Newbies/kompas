<?php

use yii\db\Migration;

/**
 * Class m180630_100223_add_register_code_to_user
 */
class m180630_100223_add_register_code_to_user extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('user', 'register_code', 'integer');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('user', 'register_code');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180630_100223_add_register_code_to_user cannot be reverted.\n";

        return false;
    }
    */
}
