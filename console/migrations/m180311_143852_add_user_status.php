<?php

use yii\db\Migration;

/**
 * Class m180311_143852_add_user_status
 */
class m180311_143852_add_user_status extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('user', 'status', 'integer');
        $this->addCommentOnColumn('user', 'status', '0-Inactive, 1-Active');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropCommentFromColumn('user', 'status');
        $this->dropColumn('user', 'status');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180311_143852_add_user_status cannot be reverted.\n";

        return false;
    }
    */
}
