<?php

use yii\db\Migration;

class m170501_120339_game_status extends Migration
{
    public function up()
    {
        $this->execute("ALTER TABLE `game` ADD `status` INT NOT NULL AFTER `end_date`;");
    }

    public function down()
    {
        echo "m170501_120339_game_status cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
