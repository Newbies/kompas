<?php

use yii\db\Migration;

class m170517_195832_reader_use_time_create_table extends Migration
{
    public function up()
    {
        $this->execute("CREATE TABLE `reader_use_time` ( `id` INT NOT NULL AUTO_INCREMENT , `user_id` INT NOT NULL , `timestamp` INT NOT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;");
    }

    public function down()
    {
        $this->execute("DROP TABLE reader_use_time");
        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
