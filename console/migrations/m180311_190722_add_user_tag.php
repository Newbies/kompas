<?php

use yii\db\Migration;

/**
 * Class m180311_190722_add_user_tag
 */
class m180311_190722_add_user_tag extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('user', 'tag', $this->text());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('user', 'tag');
    }
    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180311_190722_add_user_tag cannot be reverted.\n";

        return false;
    }
    */
}
