<?php

use console\migrations\script\set_show_number;
use yii\db\Migration;

/**
 * Class m180601_165931_add_show_number_to_task
 */
class m180601_165931_add_show_number_to_task extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $script = new set_show_number();
        $this->addColumn('task', 'show_number', 'integer');
        $script->setShowNumber();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('task', 'show_number');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180601_165931_add_show_number_to_task cannot be reverted.\n";

        return false;
    }
    */
}
