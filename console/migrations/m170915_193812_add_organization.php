<?php

use yii\db\Migration;

class m170915_193812_add_organization extends Migration
{
    public function safeUp()
    {
        $this->createTable('organization',[
            'id' => $this->primaryKey()->unique(),
            'name' => $this->string()->notNull(),
            'contact_email' => $this->string(),
            'owner_user_id' => $this->integer()->notNull(),
            'logo' => $this->string()
        ]);
    }

    public function safeDown()
    {
        $this->dropTable('organization');
    }
}
