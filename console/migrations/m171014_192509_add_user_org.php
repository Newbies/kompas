<?php

use yii\db\Migration;

class m171014_192509_add_user_org extends Migration
{
    public function safeUp()
    {
        $this->createTable('user_org',[
            'id' =>$this->primaryKey()->unique(),
            'user_id' => $this->integer()->notNull(),
            'org_id' => $this->integer()->notNull(),
        ]);
    }

    public function safeDown()
    {
        $this->dropTable('user_org');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171014_192509_add_user_org cannot be reverted.\n";

        return false;
    }
    */
}
