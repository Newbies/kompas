<?php

use yii\db\Migration;

class m170628_192350_add_scoreCounter_to_game extends Migration
{
    public function up()
    {
        $this->execute("ALTER TABLE `game` ADD `scoring_mode` INT NOT NULL COMMENT '0 - Average 1 - All'");
    }

    public function down()
    {
        $this->execute("ALTER TABLE `game` DROP `scoring_mode`");
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
