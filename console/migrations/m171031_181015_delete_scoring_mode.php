<?php

use yii\db\Migration;

class m171031_181015_delete_scoring_mode extends Migration
{
    public function safeUp()
    {
        $this->dropColumn('game','scoring_mode');
    }

    public function safeDown()
    {
        $this->addColumn('game','scoring_mode','integer');
    }
}
