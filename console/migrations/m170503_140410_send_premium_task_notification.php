<?php

use yii\db\Migration;

class m170503_140410_send_premium_task_notification extends Migration
{
    public function up()
    {
        $this->execute("ALTER TABLE `task` ADD `mailed` BOOLEAN NOT NULL DEFAULT FALSE AFTER `premium`;");
    }

    public function down()
    {
        echo "m170503_140410_send_premium_task_notification cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
