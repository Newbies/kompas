<?php

use yii\db\Migration;
use console\migrations\script\set_team_score;
/**
 * Class m180210_101628_team_drop_points_members
 */
class m180210_101628_team_drop_points_members extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $script = new set_team_score();
        $script->setFromUserGame();
        $script->save();
        $this->dropColumn('team', 'points');
        $this->dropColumn('team', 'members');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->addColumn('team', 'points', $this->integer(11));
        $this->addColumn('team', 'members',  $this->integer(11));
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180210_101628_team_drop_points_members cannot be reverted.\n";

        return false;
    }
    */
}
