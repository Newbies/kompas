<?php

use yii\db\Migration;
use yii\db\mysql\Schema;

class m171118_145906_add_time_to_complete_task extends Migration
{
    public function safeUp()
    {
        $this->addColumn('completed_task', 'time', Schema::TYPE_INTEGER."(11) DEFAULT 1");
    }

    public function safeDown()
    {
        $this->dropColumn('completed_task', 'time');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171118_145906_add_time_to_complete_task cannot be reverted.\n";

        return false;
    }
    */
}
