<?php

use yii\db\Migration;

class m170503_130219_special_task extends Migration
{
    public function up()
    {
        $this->execute("ALTER TABLE `task` ADD `premium` BOOLEAN NOT NULL DEFAULT FALSE AFTER `task_owner`;");
    }

    public function down()
    {
        echo "m170503_130219_special_task cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
