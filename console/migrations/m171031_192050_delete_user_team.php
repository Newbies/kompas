<?php

use yii\db\Migration;

class m171031_192050_delete_user_team extends Migration
{
    public function safeUp()
    {
        $this->dropTable('user_team');
    }

    public function safeDown()
    {
        $this->createTable('user_team', [
            'id' => $this->primaryKey()->unique(),
            'user_id' => $this->integer(),
            'team_id' => $this->integer(),
            'game_id' => $this->integer(),
        ]);
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171031_192050_delete_user_team cannot be reverted.\n";

        return false;
    }
    */
}
