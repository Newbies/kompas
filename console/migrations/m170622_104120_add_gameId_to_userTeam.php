<?php

use yii\db\Migration;

class m170622_104120_add_gameId_to_userTeam extends Migration
{
    public function up()
    {
        $this->execute('ALTER TABLE `user_team` ADD `game_id` INT NOT NULL;');
    }

    public function down()
    {
       $this->execute('ALTER TABLE `user_team` DROP `game_id`;');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
