<?php

use yii\db\Migration;

class m171127_161017_premium_task_end_time extends Migration
{
    public function safeUp()
    {
        $this->addColumn('task','end_time', $this->integer(11));
        $this->addColumn('task','start_time', $this->integer(11));
        $this->dropColumn('task', 'time');
    }

    public function safeDown()
    {
        $this->addColumn('task', 'time', $this->dateTime());
        $this->dropColumn('task', 'start_time');
    }
}
