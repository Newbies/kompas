<?php

use yii\db\Migration;

class m170622_112904_delete_table_image extends Migration
{
    public function up()
    {
        $this->execute('DROP TABLE image');
    }

    public function down()
    {
        echo "m170622_112904_delete_table_image cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
