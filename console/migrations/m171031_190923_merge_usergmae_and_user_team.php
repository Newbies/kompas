<?php

use yii\db\Migration;

class m171031_190923_merge_usergmae_and_user_team extends Migration
{
    public function safeUp()
    {
        $this->addColumn('user_game','team_id','integer');
    }

    public function safeDown()
    {
        $this->dropColumn('user_game','team_id');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171031_190923_merge_usergmae_and_user_team cannot be reverted.\n";

        return false;
    }
    */
}
