<?php

use yii\db\Migration;

class m170725_183612_delete_color extends Migration
{
    public function safeUp()
    {
        $this->dropColumn('team','color');
    }

    public function safeDown()
    {
        $this->addColumn('team','color','varchar(255) not null');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170725_183612_delete_color cannot be reverted.\n";

        return false;
    }
    */
}
