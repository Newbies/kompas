<?php

use yii\db\Migration;

class m170510_195117_premium_task_time extends Migration
{
    public function up()
    {
        $this->execute("ALTER TABLE `task` ADD `time` DATETIME NOT NULL AFTER `mailed`;");
    }

    public function down()
    {
        echo "m170510_195117_premium_task_time cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
