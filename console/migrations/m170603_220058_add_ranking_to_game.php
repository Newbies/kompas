<?php

use yii\db\Migration;

class m170603_220058_add_ranking_to_game extends Migration
{
    public function up()
    {
        $this->execute('ALTER TABLE `game` ADD `ranking` INT NOT NULL DEFAULT \'0\'');
    }

    public function down()
    {
        $this->execute('ALTER TABLE `game`  DROP `ranking`;');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
