<?php

use yii\db\Migration;

class m171121_192556_add_activator_id_to_completed_task_team extends Migration
{
    public function safeUp()
    {
        $this->addColumn('completed_task_team', 'activator_id', 'integer');
    }

    public function safeDown()
    {
        $this->dropColumn('completed_task_team', 'activator_id');
    }
}
