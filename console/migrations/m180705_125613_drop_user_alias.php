<?php

use yii\db\Migration;

/**
 * Class m180705_125613_drop_user_alias
 */
class m180705_125613_drop_user_alias extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('user', 'alias');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->addColumn('user', 'alias', $this->char(255));
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180705_125613_drop_user_alias cannot be reverted.\n";

        return false;
    }
    */
}
