<?php

use yii\db\Migration;

/**
 * Class m180412_190703_add_last_update_score_to_user_game
 */
class m180412_190703_add_last_update_score_to_user_game extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn("user_game", "last_update_score", $this->integer()->defaultValue(time()));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn("user_game", "last_update_score");
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180412_190703_add_last_update_score_to_user_game cannot be reverted.\n";

        return false;
    }
    */
}
