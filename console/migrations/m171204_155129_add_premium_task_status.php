<?php

use yii\db\Migration;

class m171204_155129_add_premium_task_status extends Migration
{
    public function safeUp()
    {
        $this->addColumn('task', 'status', 'integer');
        $this->addCommentOnColumn('task', 'status', '0 - ended, 1 - running, 2 - coming ');
    }


    public function safeDown()
    {
        $this->dropCommentFromColumn('task', 'status');
        $this->dropColumn('task', 'status');
    }
}
