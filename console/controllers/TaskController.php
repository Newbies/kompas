<?php

namespace console\controllers;

use backend\helpers\ArrayHelper;
use common\models\fields\TaskFields;
use common\models\fields\UserFields;
use common\models\Task;
use common\models\User;
use common\models\UserGame;
use Yii;
use yii\console\Controller;
use yii\db\Exception;

class TaskController extends Controller
{

    public function actionSendMail()
    {
        $tasks = Task::findAllPremium();

        foreach ($tasks as $task) {

            $task->setStatus();

            if ($task->isAttributeChanged('status', false)) {
                $task->save();
            }

            if ($task->status == Task::STATUS_CURRENT && $task->mailed == Task::NOT_MAILED) {
                $userId = ArrayHelper::map(UserGame::findAll([TaskFields::GAME_ID => $task->game_id]), 'user_id');

                $users = User::findAll([UserFields::ID => $userId]);
                $task->setAttribute('mailed', Task::MAILED);

                $task->save();

                foreach ($users as $user) {
                    $this->sendMail($user->email, $task->name, $task->end_time, $task->score);
                }
            }
        }

    }

    private function sendMail($email, $taskName, $time, $points)
    {
        try {
            Yii::$app->mailer
                ->compose(
                    ['html' => 'specialTask-html'],
                    [
                        'email' => $email,
                        'taskName' => $taskName,
                        'time' => $time,
                        'points' => $points
                    ]
                )
                ->setFrom([Yii::$app->params['email'] => 'Kompas'])
                ->setTo($email)
                ->setSubject('Zadanie spcjalne aktywowane')
                ->send();
        } catch (Exception $e) {
            return false;
        }
        return true;
    }

}
