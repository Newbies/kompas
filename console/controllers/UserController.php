<?php


namespace console\controllers;

use common\models\User;
use yii\console\Controller;

class UserController extends Controller
{
    /**
     * Deletes unactivated users
     */
    public function actionDeleteUnactivated()
    {
        $users = User::findUnactivated();
        foreach ($users as $user) {
            $currentDate = time();
            $registrationDelayDate = $currentDate - $user->created_at;
            if ($registrationDelayDate >= User::DELETE_TIME) {
                $user->delete();
            }
        }
    }
}