<?php

namespace console\controllers;

use yii\console\Controller;
use common\models\Game;

class GameController extends Controller
{
    public function actionChangeStatus()
    {
        $games = Game::find()->all();

        foreach ($games as $game) {
            $game->setStatus();

            if($game->isAttributeChanged('status', false)){
                $game->save();
            }
        }
    }
}