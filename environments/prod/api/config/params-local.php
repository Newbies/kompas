<?php
return [
    'adminEmail' => 'admin@example.com',
    'frontDomain' => 'kompas.newbies.pl',
    'apiDomain' => 'api.kompas.newbies.pl',
    'backDomain' => 'admin.kompas.newbies.pl',
];
