<?php

namespace frontend\helpers;

use common\models\User;
use Yii;
/**
 * Class MailHelper
 */
class MailHelper
{
    /**
     * Sends register code email
     * @param User $user
     * @return bool
     */
    public static function sendRegisterCode(User $user): bool
    {
        return Yii::$app->mailer
            ->compose(
                ['html' => 'emailVerification-html'],
                ['user' => $user]
            )
            ->setFrom([Yii::$app->params['email'] => 'Kompas'])
            ->setTo($user->email)
            ->setSubject(Yii::t('app', 'Potwierdź swój email w aplikacji Kompas'))
            ->send();
    }

    /**
     * Sends request to parent email
     * @param User $user
     * @param $parentEmail
     * @return bool
     */
    public static function sendRequestToParent(User $user, $parentEmail): bool
    {
        $signupLink = 'http://' . Yii::$app->params['frontDomain'] . '/site/signup-with-parent' . '?email=' . $user->email;
        return Yii::$app->mailer
            ->compose(
                ['html' => 'parentRequest-html'],
                [
                    'user' => $user,
                    'parentEmail' => $parentEmail,
                    'signupLink' => $signupLink
                ]
            )
            ->setFrom([Yii::$app->params['email'] => 'Kompas'])
            ->setTo($parentEmail)
            ->setSubject(Yii::t('app', 'Pomóż swojemu dziecku w utworzeniu konta w aplikacji Kompas'))
            ->send();
    }

    public static function sendResponseToChild(User $user): bool
    {
        $link = 'http://' . Yii::$app->params['frontDomain'] . '/site/login';
        return Yii::$app->mailer
            ->compose(
                ['html' => 'childResponse-html'],
                [
                    'user' => $user,
                    'link' => $link
                ]
            )
            ->setFrom([Yii::$app->params['email'] => 'Kompas'])
            ->setTo($user->email)
            ->setSubject(Yii::t('app', 'Aktywowano konto w aplikacji Kompas'))
            ->send();
    }
}