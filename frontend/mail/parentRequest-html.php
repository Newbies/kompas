<?php
use yii\helpers\Html;
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title><?= Yii::t('app', 'Witaj w Kompasie,') ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>
    <body style="background-color: #f3f3f3; margin: 0 auto; padding: 0;">
        <table border="0" cellpadding="0" cellspacing="0" width="100%" align="center"bgcolor="#f3f3f3">
            <tr>
                <td align=""center>
                    <table border="0" cellpadding="0" cellspacing="0" width="100%" height="30" align="center" bgcolor="#f3f3f3">
                        <tr><td></td></tr>
                    </table>
                    <table border="0" cellpadding="0" cellspacing="0" width="600" height="30" align="center" bgcolor="#ffffff">
                        <tr><td></td></tr>
                    </table>
                    <!-- logo start -->
                    <table border="0" cellpadding="0" cellspacing="0" width="600" align="center" bgcolor="#ffffff">
                        <tr>
                            <td align="center">
                                <img src="http://kompas.newbies.pl/img/logo_kompas.png" width="199" height="52" border="0"/>
                            </td>
                        </tr>
                    </table>
                    <!-- logo end -->
                    <table border="0" cellpadding="0" cellspacing="0" width="600" height="30" align="center" bgcolor="#ffffff">
                        <tr><td></td></tr>
                    </table>
                    <!-- title content start -->
                    <table border="0" cellpadding="0" cellspacing="0" width="600" align="center" bgcolor="#ffffff">
                        <tr>
                            <td width="30"></td>
                            <td style="font-family: Helvetica, Arial, sans-serif; font-weight: normal; font-size: 24px;" align="center">
                                <?= Yii::t('app', 'Witaj ') ?><?= Html::encode($parentEmail) ?>
                            </td>
                            <td width="30"></td>
                        </tr>
                    </table>
                    <!-- title content end -->
                    <table border="0" cellpadding="0" cellspacing="0" width="600" height="30" align="center" bgcolor="#ffffff">
                        <tr><td></td></tr>
                    </table>
                    <!-- Main text content start -->
                    <table border="0" cellpadding="0" cellspacing="0" width="600" align="center" bgcolor="#ffffff">
                        <tr>
                            <td width="30"></td>
                            <td style="font-family: Helvetica, Arial, sans-serif; font-weight: normal; font-size: 16px; line-height: 1.4; word-wrap: break-word; overflow-wrap: break-word;" align="center">
                                <?= Yii::t('app', 'Wygląda na to, że ') . Html::encode($user->email) .Yii::t('app', ' prosił Ciebie o pomoc w założeniu konta w aplikacji Kompas. Ponieważ twoje dziecko nie ma jeszcze 16 lat, potrzebuje Twojej zgody i pomocy aby utworzyć swoje konto w aplikacji Kompas. Aby rozpocząć, kliknij w poniższy przycisk') ?>
                            </td>
                            <td width="30"></td>
                        </tr>
                    </table>
                    <!-- Main text content start -->
                    <table border="0" cellpadding="0" cellspacing="0" width="600" height="30" align="center" bgcolor="#ffffff">
                        <tr><td></td></tr>
                    </table>
                    <!-- button content start -->
                    <table border="0" cellpadding="0" cellspacing="0" width="600" align="center" bgcolor="#ffffff">
                        <tr>
                            <td width="120"></td>
                            <td height="50" align="center" bgcolor="#2199e8">
                                <a href="<?= Html::encode($signupLink) ?>" style="font-family: Helvetica, Arial, sans-serif; font-size: 20px; font-weight: bold; text-decoration: none; padding: 12px 18px; color: #ffffff; display: block;" width="100%" align="center" border="0" color="#ffffff">
                                    <?= Yii::t('app', 'Załóż konto dziecka') ?>
                                </a>
                            </td>
                            <td width="120"></td>
                        </tr>
                    </table>
                    <!-- button content end -->
                    <table border="0" cellpadding="0" cellspacing="0" width="600" height="30" align="center" bgcolor="#ffffff">
                        <tr><td></td></tr>
                    </table>
                    <!-- Additional info content start -->
                    <table border="0" cellpadding="0" cellspacing="0" width="600" align="center" bgcolor="#ffffff">
                        <tr>
                            <td width="30"></td>
                            <td style="font-family: Helvetica, Arial, sans-serif; font-weight: normal; font-size: 16px; line-height: 1.4; word-wrap: break-word; overflow-wrap: break-word;" align="center">
                                <?= Yii::t('app', 'Jeśli nie założysz konta do ') . "<b>" . date('Y-m-d', strtotime("+10 day")) . "." . "</b>" . Yii::t('app',  ' Dane twojego dziecka zostaną trwale usuniętę.')?>
                            </td>
                            <td width="30"></td>
                        </tr>
                    </table>
                    <!-- Additional info content start -->
                    <table border="0" cellpadding="0" cellspacing="0" width="600" height="30" align="center" bgcolor="#ffffff">
                        <tr><td></td></tr>
                    </table>
                    <table border="0" cellpadding="0" cellspacing="0" width="600" height="30" align="center" bgcolor="#ffffff">
                        <tr><td></td></tr>
                    </table>
                    <table border="0" cellpadding="0" cellspacing="0" width="100%" height="30" align="center" bgcolor="#f3f3f3">
                        <tr><td></td></tr>
                    </table>
                    <!-- button page start -->
                    <table border="0" cellpadding="0" cellspacing="0" width="100%" align="center" bgcolor="#f3f3f3">
                        <tr>
                            <td style="font-family: Helvetica, Arial, sans-serif; font-weight: normal; font-size: 16px;" width="600" align="center">
                                <a href="kompas.newbies.pl" style="text-decoration: none;" align="center" border="0">
                                    <?= Yii::t('app', 'kompas.newbies.pl') ?>
                                </a>
                            </td>
                        </tr>
                    </table>
                    <!-- button page end -->
                    <table border="0" cellpadding="0" cellspacing="0" width="100%" height="30" align="center" bgcolor="#f3f3f3">
                        <tr><td></td></tr>
                    </table
                </td>
            </tr>
        </table>
    </body>
</html>