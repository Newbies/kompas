<?php

namespace frontend\models\readerForms;

use backend\helpers\ArrayHelper;
use common\models\CompletedTask;
use common\models\Team;
use common\models\CompletedTaskTeam;
use common\models\fields\UserGameFields;
use common\models\ReaderUseTime;
use common\models\Task;
use common\models\UserGame;
use Yii;
use yii\base\Model;


class ReaderForm extends Model
{
    public $code;

    public function checkCompleteTask($userId, $taskId)
    {
        if (CompletedTask::findByUserIdAndTaskId($userId, $taskId)) {
            return false;
        }
        return true;
    }

    public function addScore($userId, $gameId, $score)
    {
        $userGame = UserGame::getByUserIdAndGame($userId, $gameId);
        if ($userGame->hasTeam()) {
            $teamId = $userGame->team_id;
            $teammadesId = ArrayHelper::map(UserGame::findByTeamId($teamId), UserGameFields::USER_ID);
            foreach ($teammadesId as $teammadeId) {
                UserGame::getByUserIdAndGame($teammadeId, $gameId)->upScore($score);
            }
        } else {
            $userGame->upScore($score);
        }
    }

    public function rules()
    {
        return [
            [['code'], 'required'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'code' => 'Kod',
        ];
    }

    public function checkReaderUseTime($userId)
    {
        $readerUseTime = ReaderUseTime::findByUserId($userId);


        if ($readerUseTime && $readerUseTime->timestamp >= time()) {
            $readerUseTime->updateTime();
            return false;
        }

        if ($readerUseTime) {
            $readerUseTime->updateTime();
        } else {
            ReaderUseTime::addTimeTouser($userId);
        }

        return true;
    }

    public function checkCode($gameId)
    {
        return Task::findByCodeToGame(trim($this->code), $gameId);
    }

    public function addCompleteTask($userId, $taskId, $gameId)
    {
        $userGame = UserGame::getByUserIdAndGame($userId, $gameId);

        if ($userGame->hasTeam()) {
            $teamId = $userGame->team_id;
            $teammadesId = ArrayHelper::map(UserGame::findByTeamId($teamId), UserGameFields::USER_ID);
            foreach ($teammadesId as $teammadeId) {
                CompletedTask::add($teammadeId, $taskId);
            }
            CompletedTaskTeam::add($teamId, $taskId,Yii::$app->user->id);
        } else {
            CompletedTask::add($userId, $taskId);
        }
    }
}