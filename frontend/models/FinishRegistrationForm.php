<?php

namespace frontend\models;

use common\models\User;
use Yii;
use yii\base\Model;

/**
 * Finish registration form
 * @property \common\models\User $toActive
 */
class FinishRegistrationForm extends Model
{
    public $regulations;
    public $acceptations = true;
    public $password;
    public $passwordRepeat;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['password', 'required', 'message' => Yii::t('app', 'To pole nie może być puste')],
            ['password', 'string', 'min' => 6, 'message' => Yii::t('app', 'Hasło jest za krótkie.')],

            ['passwordRepeat', 'required', 'message' => Yii::t('app', 'To pole nie może być puste')],
            ['passwordRepeat', 'compare', 'compareAttribute' => 'password', 'message' => Yii::t('app', 'Hasła muszą być identyczne.')],

            ['regulations', 'boolean'],
            ['regulations', 'required', 'requiredValue' => 1, 'message' => Yii::t('app', 'Musisz zaakceptować regulamin')],

            ['acceptations', 'boolean'],
            ['acceptations', 'required', 'requiredValue' => 1, 'message' => Yii::t('app', 'Musisz zaakceptować regulamin')],
            //PURIFIER FILTER
            [['password','passwordRepeat', 'regulations'],
                'filter','filter'=>'\yii\helpers\HtmlPurifier::process']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'password' => 'Hasło',
            'passwordRepeat' => 'Powtórz hasło',
        ];
    }

    /**
     * Set new password to user
     * @param User $user
     * @return boolean
     */
    public function setToActive(User $user): bool
    {
        $user->setActive();
        $user->setPassword($this->password);
        return $user->save();
    }
}
