<?php

namespace frontend\models;

use common\models\User;
use frontend\helpers\MailHelper;
use Yii;
use yii\base\Model;

/**
 * Parent Help form
 */
class ParentHelpForm extends Model
{
    public $parentEmail;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['parentEmail', 'trim'],
            ['parentEmail', 'required'],
            ['parentEmail', 'email', 'message' => Yii::t('app', 'To nie jest email')],
            ['parentEmail', 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'parentEmail' => Yii::t('app', 'Email rodzica/opiekuna'),
        ];
    }

    /**
     * send email to parent
     * @param User $user
     * @return bool
     */
    public function send(User $user): bool
    {
        return MailHelper::sendRequestToParent($user, $this->parentEmail);
    }
}
