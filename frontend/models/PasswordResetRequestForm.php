<?php
namespace frontend\models;

use Yii;
use yii\base\Model;
use common\models\User;
use himiklab\yii2\recaptcha\ReCaptchaValidator;

/**
 * Password reset request form
 */
class PasswordResetRequestForm extends Model
{
    public $email;
    public $reCaptcha;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'exist',
                'targetClass' => '\common\models\User',
                'message' => Yii::t('app','Nie ma takiego użytkownika z tym emailem')],

            [['reCaptcha'], ReCaptchaValidator::className(), 'secret' => '6LdadUsUAAAAACbUzIaFI0J2LtbEhe1b2SBHpxLw', 'uncheckedMessage' => 'Potwierdź że jesteś człowiekiem.'],

                //PURIFIER FILTER
            [['email'],
                'filter','filter'=>'\yii\helpers\HtmlPurifier::process']
        ];
    }

    public function attributeLabels()
    {
        return [
            'reCaptcha' => Yii::t('app', 'Potwierdź że jesteś człowiekiem'),
        ];
    }

    /**
     * Sends an email with a link, for resetting the password.
     *
     * @return boolean whether the email was send
     */
    public function sendEmail()
    {
        /* @var $user User */
        $user = User::findOne([
            'email' => $this->email,
        ]);

        if (!$user) {
            return false;
        }

        if (!User::isPasswordResetTokenValid($user->password_reset_token)) {
            $user->generatePasswordResetToken();
            if (!$user->save()){
                return false;
            }
        }

        return Yii::$app->mailer
            ->compose(
                ['html' => 'passwordResetToken-html', 'text' => 'passwordResetToken-text'],
                ['user' => $user]
            )
            ->setFrom([Yii::$app->params['email'] => 'Kompas'])
            ->setTo($this->email)
            ->setSubject(Yii::t('app','Zmiana hasła w aplikacji Kompas'))
            ->send();
    }
}
