<?php
namespace frontend\models;

use yii\base\Model;
use yii\base\InvalidParamException;
use common\models\User;
use Yii;

/**
 * Password reset form
 */
class ResetPasswordForm extends Model
{
    public $password;

    /**
     * @var \common\models\User
     */
    private $_user;

    /**
     * Creates a form model given a token.
     *
     * @param string $token
     * @param array $config name-value pairs that will be used to initialize the object properties
     * @throws \yii\base\InvalidParamException if token is empty or not valid
     */
    public function __construct($token, $config = [])
    {
        if (empty($token) || !is_string($token)) {
            throw new InvalidParamException('Password reset token cannot be blank.');
        }
        $this->_user = User::findByPasswordResetToken($token);
        if (!$this->_user) {
            throw new InvalidParamException('Coś poszło nie tak. Prawdopodobnie upłynął czas na zmianę hasła. Zresetuj hasło jeszcze raz, jeśli chcesz je zmienić.');
        }
        parent::__construct($config);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['password', 'required'],
            ['password', 'string', 'min' => 6, 'tooShort' => Yii::t('app', 'Hasło musi zawierać co najmniej 6 znaków.')],

            //PURIFIER FILTER
            [['password'],
                'filter', 'filter' => '\yii\helpers\HtmlPurifier::process']
        ];
    }
    /**
     * Resets password.
     * @return bool if password was reset.
     */
    public function resetPassword(): bool
    {
        $user = $this->_user;
        $user->setPassword($this->password);
        $user->removePasswordResetToken();
        $user->generatePasswordResetToken();
        return $user->save(false);
    }
}
