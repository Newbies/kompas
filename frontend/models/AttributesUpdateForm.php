<?php

namespace frontend\models;

use common\models\User;
use Yii;
use yii\base\Model;
use \common\components\CoreApi;

class AttributesUpdateForm extends Model
{
    public $id;
    public $name;
    public $last_name;
    public $alias;
    public $email;
    public $password;
    public $password_repeat;
    public $password_old;
    public $password_old_hash;

    public function rules()
    {
        return [
            ['id', 'integer'],
            ['id', 'required'],

            ['name', 'trim'],
            ['name', 'string', 'max' => 255],
            ['name',  'match', 'pattern' => '/^[\p{L},]+$/u', 'message' =>Yii::t('app', 'Imię może zawierać tylko litery')],

            ['last_name', 'trim'],
            ['last_name', 'string', 'max' => 255],
            ['last_name',  'match', 'pattern' => '/^[\p{L},]+$/u', 'message' =>Yii::t('app', 'Nazwisko może zawierać tylko litery')],

            ['email', 'trim'],
            ['email', 'email', 'message' => Yii::t('app', 'To nie jest email')],
            ['email', 'string', 'max' => 255],
            ['email', 'unique',
                'targetClass' => '\common\models\User',
                'message' => Yii::t('app', 'Ten email jest już zajęty')],

            ['alias', 'trim'],
            ['alias', 'match', 'pattern' => '/^[a-zA-Z0-9\._-]+$/',  'message' => Yii::t('app', 'Pseudonim może zawierać tylko znaki alfabetu angielskiego od a do z i znaki ". - _"')],
            ['alias', 'string', 'max' => 255],
            ['alias', 'unique',
                'targetClass' => '\common\models\User',
                'message' => Yii::t('app', 'Ten pseudonim jest już zajęty')],

            ['password', 'string', 'min' => 6, 'tooShort' => Yii::t('app', 'Hasło musi zawierać co najmniej 6 znaków.')],
            ['password_repeat', 'required', 'when' => function($model){
                return $model->password != null;
            }, 'message' => Yii::t('app', 'To pole nie może być puste')],
            ['password_repeat', 'compare', 'compareAttribute' => 'password', 'message' => Yii::t('app', 'Hasła muszą być identyczne.')],
            ['password_old', 'string'],
            ['password_old', 'required', 'when' => function($model){
                return $model->password != null;
            }],
            ['password_old_hash', 'string'],
            //PURIFIER FILTER
            [['last_name','name','email','alias','password','password_repeat','password_old'],
                'filter','filter'=>'\yii\helpers\HtmlPurifier::process']
        ];
    }

    public function update()
    {
        if($this->password_old != null && !Yii::$app->security->validatePassword($this->password_old, $this->password_old_hash)){
            Yii::$app->session->setFlash('error', Yii::t('app','Musisz podać poprzednie hasło aby ustawić nowe. Jeśli go nie pamiętasz przejdź do odzyskiwania hasła.'));
            return false;
        }
        $params = array_filter($this->getAttributes());
        $user = User::findById($params['id']);
        return $user->updateFields($params);
    }
}
