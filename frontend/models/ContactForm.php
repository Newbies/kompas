<?php

namespace frontend\models;

use common\models\User;
use himiklab\yii2\recaptcha\ReCaptchaValidator;
use Yii;
use yii\base\Model;

/**
 * ContactForm is the model behind the contact form.
 */
class ContactForm extends Model
{

    public $subject;
    public $body;
    public $reCaptcha;
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            //subject and body are required
            ['subject', 'required', 'message' => Yii::t('app', 'To pole nie może być puste')],
            ['body', 'required', 'message' => Yii::t('app', 'To pole nie może być puste')],

            // email has to be a valid email address
            ['email', 'email', 'message' => Yii::t('app', 'To nie jest email')],

            [['reCaptcha'], ReCaptchaValidator::className(), 'secret' => '6LdadUsUAAAAACbUzIaFI0J2LtbEhe1b2SBHpxLw', 'uncheckedMessage' => 'Potwierdź że jesteś człowiekiem.'],

            //PURIFIER FILTER
            [['email', 'subject', 'body', 'name'],
                'filter', 'filter' => '\yii\helpers\HtmlPurifier::process']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'name' => 'Twoje Imię',
            'email' => 'Email',
            'subject' => 'Temat',
            'body' => 'Treść',
            'reCaptcha' => Yii::t('app', 'Potwierdź że jesteś człowiekiem'),
        ];
    }

    /**
     * Sends an email to the specified email address using the information collected by this model.
     *
     * @param string $email the target email address
     * @return boolean whether the email was sent
     */
    public function sendEmail($email)
    {
        return Yii::$app->mailer->compose()
            ->setTo($email)
            ->setFrom($email)
            ->setSubject($this->mailSubjectBuilder())
            ->setTextBody($this->mailBodyBuilder())
            ->send();
    }

    private function mailBodyBuilder()
    {
        $user = User::findOne(['id' => Yii::$app->user->getId()]);
        $bodyPrefix = "Mail from ";

        return $bodyPrefix . $user->email . "\r\n" . $this->body;
    }

    private function mailSubjectBuilder(){
        $subjectPrefix = "Contact form: ";
        return $subjectPrefix.$this->subject;
    }
}
