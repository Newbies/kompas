<?php

namespace frontend\models;

use common\models\User;
use yii\base\Model;
use Yii;

/*
 * Email verification form
 */
class EmailVerificationForm extends Model
{
    public $registerCode;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [

            [['registerCode'], 'integer'],
            [['registerCode'], 'required'],
            [['registerCode'],
                'filter', 'filter' => '\yii\helpers\HtmlPurifier::process']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'registerCode' => Yii::t('app', 'Kod potwierdzający email'),
        ];
    }

    /**
     * * Verifies User by registration code
     * @param User $user
     * @return bool
     */
    public function registerByCode(User $user): bool
    {
        if ($user->checkRegisterCode($this->registerCode))
        {
            $user->register_code = null;
            $user->save();
            return true;
        }
        $this->addError('registerCode', Yii::t('app', 'Kod weryfikacyjny jest nie poprawny'));
        return false;
    }
}