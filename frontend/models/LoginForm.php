<?php

namespace frontend\models;

use common\models\fields\UserFields;
use common\models\User;
use Yii;
use yii\base\Model;

/**
 * Login form
 */
class LoginForm extends Model
{
    public $email;
    public $password;
    private $_user;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['email'], 'email', 'message' => Yii::t('app', 'To nie jest poprawny email')],
            [['email', 'password'], 'required', 'message' => Yii::t('app', 'To pole nie może być puste')],
            ['password', 'validatePassword', 'message' => Yii::t('app', 'Złe hasło lub email.')],

            //PURIFIER FILTER
            [['email', 'password'],
                'filter', 'filter' => '\yii\helpers\HtmlPurifier::process']
        ];
    }

    public function attributeLabels()
    {
        return [
            'email' => 'Email',
            'password' => 'Hasło',
            'rememberMe' => 'Zapamiętaj mnie',
        ];
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();
            if (!$user || !$user->validatePassword($this->password)) {
                $this->addError($attribute, Yii::t('app','Złe hasło lub email.'));
            }
            else if ($user->status == User::STATUS_INACTIVE){
                $this->addError($attribute, Yii::t('app', 'To konto nie jest aktywne.'));
            }
        }
    }

    /**
     * Finds user by [[mail]]
     *
     * @return User|null
     */
    protected function getUser()
    {
        return User::findOne([UserFields::EMAIL => $this->email]);
    }

    /**
     * Logs in a user using the provided username and password.
     *
     * @return boolean whether the user is logged in successfully
     */
    public function login()
    {
        if ($this->validate()) {
            return Yii::$app->user->login($this->getUser(),
                3600 * 24 * 30);
        }
        return false;
    }
}
