<?php

namespace frontend\controllers;

use common\models\ReaderUseTime;
use common\models\Task;
use frontend\models\readerForms\ReaderForm;
use Yii;


class CodeController extends AccessController
{

    public function actionReader($gameId)
    {
        $this->checkAccessToGame($gameId, Yii::$app->user->id);
        $this->checkAccessToReader($gameId);

        $userId = Yii::$app->user->id;

        $model = new ReaderForm();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {

            if (!$model->checkReaderUseTime($userId)) {
                $model->addError('code', Yii::t('app', "Nie tak szybko, zwolnij trochę. Poczekaj " . ReaderUseTime::COOLDOWN . " sekund zanim wprowadzisz ponownie kod."));
                return $this->renderAjax('reader', [
                    'model' => $model,
                    'gameId' => $gameId,
                ]);
            }

            $task = $model->checkCode($gameId);
            if (!$task) {
                return $this->renderAjax('/code/bad', [
                    'gameId' => $gameId,
                ]);
            }

            if (!$model->checkCompleteTask($userId, $task->id)) {
                return $this->renderAjax('/code/completed', [
                    'gameId' => $gameId,
                ]);
            }

            if ($task->isPremium() && $task->status != Task::STATUS_CURRENT) {
                return $this->renderAjax('/code/badTimePremium', [
                    'gameId' => $gameId,
                ]);
            }

            if (!$task->isActivated($userId)) {
                return $this->renderAjax('/code/bad', [
                    'gameId' => $gameId,
                ]);
            }

            if ($task->isTimedTaskExpired($userId)) {
                return $this->renderAjax('/code/badTimeTimedTask', [
                    'gameId' => $gameId,
                ]);
            }

            $model->addCompleteTask($userId, $task->id, $gameId);

            $model->addScore($userId, $gameId, $task->score);

            return $this->renderAjax('/code/good', [
                'gameId' => $gameId,
                'score' => $task->score,

            ]);
        }

        return $this->renderAjax('reader', [
            'model' => $model,
            'gameId' => $gameId,
        ]);
    }

    public function actionModal()
    {

    }
}