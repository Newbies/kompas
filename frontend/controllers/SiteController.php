<?php

namespace frontend\controllers;


use common\models\User;
use DateTime;
use frontend\assets\IndexAsset;
use frontend\helpers\MailHelper;
use frontend\models\AttributesUpdateForm;
use frontend\models\ContactForm;
use frontend\models\EmailVerificationForm;
use frontend\models\FinishRegistrationForm;
use frontend\models\LoginForm;
use frontend\models\ParentHelpForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SetPasswordInviteForm;
use frontend\models\SignupForm;
use Yii;
use yii\base\InvalidParamException;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\BadRequestHttpException;
use yii\web\Controller;

/**
 * Site controller
 */
class SiteController extends Controller
{

    public $successUrl = 'Success';

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup', 'invite', 'contact', 'about', 'howplay', 'regulations', 'settings', "privacypolicy", 'cookies'],
                'rules' => [
                    [
                        'actions' => ['signup', 'invite', 'regulations', 'privacypolicy','cookies'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout', 'invite', 'contact', 'howplay', 'regulations', 'settings','privacypolicy','cookies'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function beforeAction($action)
    {
        if ($action->id == 'error')
            $this->layout = 'unLogged';

        return parent::beforeAction($action);
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
            'auth' => [
                'class' => 'yii\authclient\AuthAction',
                'successCallback' => [$this, 'successLoginByFacebookCallback'],
            ],
        ];
    }

    /*public function successLoginByFacebookCallback($client)
    {
        $attributes = $client->getUserAttributes();

        if (!$attributes['email']) {
            Yii::$app->session->setFlash('error', Yii::t('app', 'Błędny lub brak email\'u'));
            return $this->redirect('/site/login');
        }

        $user = User::findOne(['email' => $attributes['email']]);

        if (!$user) {
            $params = [
                'email' => $attributes['email'],
                'password' => $attributes['id'],
                'login' => "fb" . $attributes['id'],
                'name' => $attributes['first_name'],
                'last_name' => $attributes['last_name']
            ];
            CoreApi::user()->post('', $params);
        }

        $user = User::findOne(['email' => $attributes['email']]);
        Yii::$app->user->login($user, 3600 * 24 * 30);
        return $this->redirect('/');
    }*/

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        if (Yii::$app->user->isGuest) {
            IndexAsset::register($this->view);
            $this->layout = 'clear';
            return $this->redirect('/site/login');
        }
        return $this->redirect('/game/index');
    }


    /**
     * Logs in a user.
     *
     * @return mixed
     */
    public function actionLogin()
    {
        $this->layout = 'unLogged';

        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->redirect('index');
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();
        return $this->redirect('/site/login');
    }

    /**
     * Displays contact page.
     *
     * @return mixed
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($model->sendEmail(Yii::$app->params['email'])) {
                Yii::$app->session->setFlash('success', Yii::t('app', 'Dziękujemy za skontaktowanie się z nami.'));
            } else {
                Yii::$app->session->setFlash('error', Yii::t('app', 'Coś poszło nie tak, spróbuj ponownie.'));
            }
            return $this->refresh();
        } else {
            return $this->render('contact', [
                'model' => $model,
            ]);
        }
    }


    /**
     * Sign user up to app
     *
     * @return string|\yii\web\Response
     */
    public function actionSignup()
    {
        $this->layout = 'unLogged';
        $model = new SignupForm();

        if($email = Yii::$app->request->get('email'))
        {
            $user = User::findByEmail($email);
            $model->id = $user->id;
            $model->email = $user->email;
            $model->name = $user->name;
            $model->lastName = $user->last_name;
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $user = $model->signup())
        {
            return $this->redirect(['site/email-verification/', 'id' => $user->id]);
        }
        return $this->render('signup', [
            'model' => $model
        ]);
    }

    /**
     * Verifies new user email
     *
     * @param $id
     * @return string|\yii\web\Response
     * @throws BadRequestHttpException
     */
    public function actionEmailVerification($id)
    {
        $this->layout = 'unLogged';
        $user = User::findById($id);
        $userBirthDate = Yii::$app->session->get('userBirthDate');

        if($user == null)
        {
            throw new BadRequestHttpException(Yii::t('app', 'Błędne ID użytkownika'));
        }

        $model = new EmailVerificationForm();
        if($model->load(Yii::$app->request->post()) && $model->validate() && $model->registerByCode($user))
        {
            if(User::isUserIndependent($userBirthDate)){
                return $this->redirect(['site/finish-registration/', 'id' => $user->id]);
            }
            return $this->redirect(['site/parent-help/', 'id' => $user->id]);
        }
        return $this->render('emailVerification', [
            'model' => $model
        ]);
    }

    /**
     * Parent - user email request to finish registration
     *
     * @param $id
     * @return string
     * @throws BadRequestHttpException
     */
    public function actionParentHelp($id)
    {
        $this->layout = 'unLogged';
        $user = User::findById($id);

        if($user == null) {
            throw new BadRequestHttpException(Yii::t('app', 'Błędne ID użytkownika'));
        }

        if($user->register_code) {
            throw new BadRequestHttpException(Yii::t('app', 'Konto nie zostało aktywowane'));
        }

        $model = new ParentHelpForm();
        if($model->load(Yii::$app->request->post()) && $model->validate() && $model->send($user))
        {
            Yii::$app->session->setFlash('success', Yii::t('app', 'Prośba została wyłana, poproś opiekuna o pomoc'));
            return $this->redirect(['/site/login/']);
        }
        return $this->render('parentHelp', [
            'model' => $model
        ]);
    }

    /**
     * Finish registration process
     *
     * @param $id
     * @return string|\yii\web\Response
     * @throws BadRequestHttpException
     */
    public function actionFinishRegistration($id)
    {
        $this->layout = 'unLogged';
        $model = new FinishRegistrationForm();
        $user = User::findById($id);

        if ($user == null) {
            throw new BadRequestHttpException(Yii::t('app', 'Błędne ID użytkownika'));
        }

        if ($user->register_code) {
            throw new BadRequestHttpException(Yii::t('app', 'Konto nie zostało aktywowane'));
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->setToActive($user)) {
            Yii::$app->session->setFlash('success', Yii::t('app', 'Założyłeś konto! teraz możesz się zalogować.'));
            return $this->redirect('login');
        }

        return $this->render('finishRegistration', [
            'model' => $model
        ]);
    }

    public function actionSignupWithParent()
    {
        $this->layout = 'unLogged';
        $model = new SignupForm();

        if($email = Yii::$app->request->get('email'))
        {
            $user = User::findByEmail($email);
            $model->id = $user->id;
            $model->email = $user->email;
            $model->name = $user->name;
            $model->lastName = $user->last_name;
            $model->parentHelp = true;
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $this->redirect(['site/finish-registration-with-parent', 'id' => $model->id]);
        }

        return $this->render('signupWithParent', [
            'model' => $model,
            'childEmail' => $model->email
        ]);
    }

    public function actionFinishRegistrationWithParent($id)
    {
        $this->layout = 'unLogged';
        $model = new FinishRegistrationForm();
        $user = User::findById($id);
        $model->acceptations = false;
        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->setToActive($user)) {
            MailHelper::sendResponseToChild($user);
            Yii::$app->session->setFlash('success', Yii::t('app', 'Pomyślnie utworzono konto dziecka'));
            return $this->redirect('login');
        }

        return $this->render('finishRegistrationWithParent', [
            'model' => $model,
        ]);
    }

    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionRequestPasswordReset()
    {
        $this->layout = 'unLogged';
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Sprawdź swoją skrzynkę E-mail, na który wysłaliśmy link resetujący hasło.');

                return $this->goHome();
            } else {
                Yii::$app->session->setFlash('error', 'Przepraszamy, ale podany adres E-mail nie istnieje.');
            }
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
        $this->layout = 'unLogged';

        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', 'Nowe hasło zostało ustawione!');

            return $this->goHome();
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }

    public function actionRegulations()
    {
        $this->layout = 'unLogged';
        return $this->render('regulations');
    }

    public function actionPrivacypolicy()
    {
        $this->layout = 'unLogged';
        return $this->render('privacyPolicy');
    }

    public function actionCookies()
    {
        $this->layout = 'unLogged';
        return $this->render('cookies');
    }

    public function actionHowplay()
    {
        return $this->render('howPlay');
    }

    public function actionSettings()
    {
        $model = new AttributesUpdateForm();

        $userId = Yii::$app->user->getId();
        $user = User::findOne($userId);
        $model->id = $userId;

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->update()) {
            Yii::$app->session->setFlash('success', Yii::t('app', 'Dane zostały zapisane'));
            return $this->redirect('/site/settings');
        }
        return $this->render('settings', [
            'model' => $model,
            'user' => $user
        ]);
    }
}
