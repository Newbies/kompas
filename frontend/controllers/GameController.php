<?php

namespace frontend\controllers;

use backend\helpers\ArrayHelper;
use common\models\CompletedTask;
use common\models\fields\CompletedTaskFields;
use common\models\fields\GameFields;
use common\models\fields\TaskActivatorFields;
use common\models\fields\TaskFields;
use common\models\fields\UserGameFields;
use common\models\Game;
use common\models\Task;
use common\models\TaskActivator;
use common\models\Team;
use common\models\User;
use common\models\UserGame;
use Yii;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use yii\web\NotFoundHttpException;

class GameController extends AccessController
{

    public function actionIndex()
    {
        $userGameIds = ArrayHelper::map(UserGame::findInvitedOrActivatedByUserId(Yii::$app->user->id), UserGameFields::GAME_ID);

        $runningGames = new ActiveDataProvider([
            'query' => Game::find()->where([GameFields::STATUS => Game::STATUS_CURRENT, GameFields::ID => $userGameIds]),
        ]);

        $upcomingGames = new ActiveDataProvider([
            'query' => Game::find()->where([GameFields::STATUS => Game::STATUS_FUTURE, GameFields::ID => $userGameIds]),
        ]);

        $endedGames = new ActiveDataProvider([
            'query' => Game::find()->where([GameFields::STATUS => Game::STATUS_FINISHED, GameFields::ID => $userGameIds]),
        ]);

        return $this->render('index', [
            'runningGames' => $runningGames,
            'upcomingGames' => $upcomingGames,
            'endedGames' => $endedGames,
        ]);
    }

    public function actionLobby()
    {
        $runningGames = new ActiveDataProvider([
            'query' => Game::find()->where([GameFields::STATUS => 1, GameFields::TYPE => 0]),
        ]);

        $upcomingGames = new ActiveDataProvider([
            'query' => Game::find()->where([GameFields::STATUS => 2, GameFields::TYPE => 0]),
        ]);

        return $this->render('lobby', [
            'runningGames' => $runningGames,
            'upcomingGames' => $upcomingGames,
        ]);
    }

    public function actionJoin($id)
    {
        if (UserGame::findByUserIdAndGameId(Yii::$app->user->id, $id)) {
            Yii::$app->session->setFlash('error', Yii::t('app', 'Jesteś już w tej grze.'));
            return $this->redirect('/lobby');
        }

        UserGame::addUserToGameFromLobby(Yii::$app->user->id, $id);
        Yii::$app->session->setFlash('success', Yii::t('app', 'Zostałeś dodany do gry.'));
        return $this->redirect('/game/index');
    }

    public function actionView($id)
    {
        $userId = Yii::$app->user->id;

        $this->checkAccessToGame($id, $userId);
        $this->showPremiumTaskNotification($id);

        UserGame::setInviteStatusToActived($userId, $id);

        $userGame = UserGame::findByUserIdAndGameId($userId, $id);

        $game = $this->findModel($id);

        $finishTasksIds = ArrayHelper::map(
            CompletedTask::findByUserId($userId), CompletedTaskFields::TASK_ID);

        $finishedTasks = new ActiveDataProvider([
            'query' => Task::find()
                ->where([TaskFields::GAME_ID => $id])
                ->andWhere(['in', TaskFields::ID, $finishTasksIds]),
            'pagination' => [
                'pageSize' => 100,
            ],
        ]);

        $expiredTimedTaskIds = ArrayHelper::map(
            TaskActivator::getExpiredTimedTaskArray($userId, $id), TaskActivatorFields::TASK_ID
        );

        $unfinishedTaskModels = Task::find()
            ->where([TaskFields::GAME_ID => $id])
            ->andWhere(['not in', TaskFields::ID, $finishTasksIds])
            ->andWhere([TaskFields::STATUS => Task::STATUS_CURRENT])
            ->orWhere([TaskFields::STATUS => Task::STATUS_NOT_SET])
            ->andwhere([TaskFields::GAME_ID => $id])
            ->andWhere(['not in', TaskFields::ID, $finishTasksIds])
            ->andWhere(['not in', TaskFields::ID, $expiredTimedTaskIds]);

        $unfinishedTaskModelIds = ArrayHelper::map(
            $unfinishedTaskModels->all(),
            TaskFields::ID
        );

        $unfinishedTasks = new ActiveDataProvider([
            'query' => $unfinishedTaskModels,
            'sort' => ['defaultOrder' => [TaskFields::SHOW_NUMBER => SORT_ASC, TaskFields::ID => SORT_ASC]],
            'pagination' => [
                'pageSize' => 100,
            ],

        ]);

        $unDonePremiumTasks = new ActiveDataProvider([
            'query' => Task::find()
                ->where([TaskFields::GAME_ID => $id])
                ->andWhere(['not in', TaskFields::ID, $unfinishedTaskModelIds])
                ->andWhere(['not in', TaskFields::ID, $finishTasksIds])
                ->andWhere(['!=', TaskFields::STATUS, Task::STATUS_FUTURE]),
            'sort' => ['defaultOrder' => [TaskFields::SHOW_NUMBER => SORT_ASC, TaskFields::ID => SORT_ASC]],
            'pagination' => [
                'pageSize' => 100,
            ],
        ]);

        $allUnfinishedTasks = new ActiveDataProvider([
            'query' => Task::find()
                ->where([TaskFields::GAME_ID => $id])
                ->andWhere(['not in', TaskFields::ID, $finishTasksIds])
                ->andWhere(['!=', TaskFields::STATUS, Task::STATUS_FUTURE]),
            'sort' => ['defaultOrder' => [TaskFields::SHOW_NUMBER => SORT_ASC, TaskFields::ID => SORT_ASC]],
            'pagination' => [
                'pageSize' => 100,
            ],
        ]);

        if ($game->isTeamable() && User::hasTeam($userId, $id)) {
            $team = UserGame::getByUserIdAndGame($userId, $id)->getTeam();
            $showTeam = true;
            $userPosition = Team::getPlaceByTeamAndGameId($id, $team->id);
        } else {
            $userPosition = UserGame::getPlaceByUserAndGameId($id, $userId);
            $team = false;
            $showTeam = false;
        }

        $params = [
            'model' => $game,
            'finishedTasks' => $finishedTasks,
            'unfinishedTasks' => $unfinishedTasks,
            'unDonePremiumTasks' => $unDonePremiumTasks,
            'allUnfinishedTasks' => $allUnfinishedTasks,
            'userPosition' => $userPosition,
            'team' => $team,
            'showTeam' => $showTeam,
            'place' => $game->getPlace(),
            'organization' => $game->getOrganization(),
        ];

        if ($game->isTeamable() && !$userGame->hasTeam()) {
            Yii::$app->session->setFlash('premium', Yii::t('app', 'Jesteś w grze drużynowej, jednak administrator nie przypisał Cię do żadnej drużyny. Skontaktuj się z organizatorem, aby móc uczestniczyć w grze.'));
            return $this->render('viewClean', $params);
        }

        switch ($this->findModel($id)->status) {
            case 0:
                return $this->render('viewClose', $params);
                break;
            case 1:
                return $this->render('view', $params);
                break;
            case 2:
                return $this->render('viewUpcoming', $params);
                break;
        }
    }

    public function showPremiumTaskNotification($id)
    {

        $premiumMailedTask = Task::findPremium($id, true);

        foreach ($premiumMailedTask as $task) {
            if (!CompletedTask::findByUserIdAndTaskId(Yii::$app->user->id, $task->id) &&
                !Yii::$app->getRequest()->getCookies()->has('task_' . $task->id)) {
                Yii::$app->session->setFlash('premium',
                    Yii::t(
                        'app',
                        'Właśnie pojawiło się nowe zadanie Premium. ' .
                        Html::a(
                            'Przejdź do zadania i zgarnij punkty.',
                            ['/game/' . $id . '/task/' . $task->id],
                            ['style' => 'color:#ffffff;font-weight:600;text-decoration:underline;'])
                    ));
                break;
            }
        }
    }

    protected function findModel($id)
    {
        if (($model = Game::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionLeave($id)
    {
        if (UserGame::findByUserIdAndGameId(Yii::$app->user->id, $id)->delete()) {
            Yii::$app->session->setFlash('success', Yii::t('app', 'Opuszczenie gry zakończone sukcesem.'));
        } else {
            Yii::$app->session->setFlash('error', Yii::t('app', 'Opuszczenie gry nie powiodło się.'));
        }
        return $this->redirect('/game/index');
    }
}
