<?php
/**
 * Created by PhpStorm.
 * User: janpa
 * Date: 2017-05-14
 * Time: 19:49
 */

namespace frontend\controllers;

use common\models\fields\TeamFields;
use common\models\fields\UserGameFields;
use common\models\Game;
use common\models\Team;
use common\models\UserGame;
use Yii;
use yii\data\ActiveDataProvider;

class BoardController extends AccessController
{
    public function actionIndex($gameId)
    {
        $game = Game::findById($gameId);

        if ($game->isTeamable()) {
            $userGame = UserGame::findByUserIdAndGameId(Yii::$app->user->id, $game->id);

            $this->validateHasTeam($userGame, $game->id);

            $currentTeam = Team::findById($userGame->team_id)->id;
            $currentTeamPosition = Team::getPlaceByTeamAndGameId($gameId, $currentTeam);

            $query = Team::find()->where([TeamFields::GAME_ID => $gameId]);

            return $this->render('viewTeam', [
                'dataProvider' => $this->prepareAciveDataProvider($query),
                'game' => $game,
                'currentTeam' => $currentTeam,
                'currentTeamPosition' => $currentTeamPosition,
            ]);

        }
        $currentUser = Yii::$app->user->id;

        $currentUserPosition = UserGame::getPlaceByUserAndGameId($gameId, $currentUser);

        $query = UserGame::find()->where([UserGameFields::GAME_ID => $gameId]);

        return $this->render('viewSolo', [
            'dataProvider' => $this->prepareAciveDataProvider($query),
            'game' => $game,
            'currentUser' => $currentUser,
            'currentUserPosition' => $currentUserPosition
        ]);

    }

    private function validateHasTeam($userGame, $gameId)
    {
        if (!$userGame->hasTeam()) {
            Yii::$app->session->setFlash('error', Yii::t('app', 'Nie jesteś w żadnej drużynie'));
            return $this->redirect('/game/' . $gameId);
        }
    }

    private function prepareAciveDataProvider($query)
    {
        return new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 5,
            ],
            'sort' => [
                'defaultOrder' => [
                    'score' => SORT_DESC,
                    'last_update_score' => SORT_ASC,
                    'id' => SORT_DESC,
                ]
            ]
        ]);
    }

}
