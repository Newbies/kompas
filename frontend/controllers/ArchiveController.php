<?php

namespace frontend\controllers;

use backend\helpers\ArrayHelper;
use common\models\fields\GameFields;
use common\models\fields\UserGameFields;
use common\models\Game;
use common\models\UserGame;
use Yii;
use yii\data\ActiveDataProvider;

class ArchiveController extends AccessController
{

    public function actionIndex()
    {
        $userGameIds = ArrayHelper::map(UserGame::findByUserId(Yii::$app->user->id), UserGameFields::GAME_ID);

        $endedGames = new ActiveDataProvider([
            'query' => Game::find()->where([GameFields::STATUS => 0, GameFields::ID => $userGameIds]),
        ]);

        return $this->render('index', [
            'endedGames' => $endedGames,
        ]);
    }
}