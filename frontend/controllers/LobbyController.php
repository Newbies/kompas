<?php

namespace frontend\controllers;


use backend\helpers\ArrayHelper;
use common\models\fields\GameFields;
use common\models\fields\UserGameFields;
use common\models\Game;
use common\models\UserGame;
use Yii;
use yii\data\ActiveDataProvider;

/**
 * Site controller
 */
class LobbyController extends AccessController
{
    public function actionIndex()
    {
        $userGameId = ArrayHelper::map(UserGame::findByUserId(Yii::$app->user->id), UserGameFields::GAME_ID);

        $runningGames = new ActiveDataProvider([
            'query' => Game::find()
                ->where([GameFields::STATUS => Game::STATUS_CURRENT])
                ->andWhere([GameFields::TYPE => Game::TYPE_PUBLIC])
                ->andWhere(['not in', GameFields::ID, $userGameId]),
        ]);

        $upcomingGames = new ActiveDataProvider([
            'query' => Game::find()
                ->where([GameFields::STATUS => Game::STATUS_FUTURE])
                ->andWhere([GameFields::TYPE => Game::TYPE_PUBLIC])
                ->andWhere(['not in', GameFields::ID, $userGameId]),
        ]);

        return $this->render('index', [
            'runningGames' => $runningGames,
            'upcomingGames' => $upcomingGames,
        ]);
    }

    public function actionView($id)
    {
        $game = Game::findById($id);
        switch ($game->status) {
            case Game::STATUS_CURRENT:
                return $this->render('view', [
                    'model' => $game,
                    'place' => $game->getPlace(),
                    'organization' => $game->getOrganization(),
                ]);
            case Game::STATUS_FUTURE:
                return $this->render('viewFuture', [
                    'model' => $game,
                    'place' => $game->getPlace(),
                    'organization' => $game->getOrganization(),
                ]);
        }
    }

}
