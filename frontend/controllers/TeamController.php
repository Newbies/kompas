<?php
/**
 * Created by PhpStorm.
 * User: janpa
 * Date: 2017-05-14
 * Time: 19:49
 */

namespace frontend\controllers;

use backend\helpers\ArrayHelper;
use common\models\fields\UserFields;
use common\models\fields\UserGameFields;
use common\models\Game;
use common\models\User;
use common\models\UserGame;
use Yii;
use yii\data\ActiveDataProvider;

class TeamController extends AccessController
{
    public function actionView($gameId, $id)
    {
        $this->checkAccessToTeam($gameId, $id, Yii::$app->user->id);
        $game = Game::findById($gameId);

        $userIds = ArrayHelper::map(UserGame::findByTeamId($id), UserGameFields::USER_ID);

        $users = new ActiveDataProvider([
            'query' => User::find()->where([UserFields::ID => $userIds])
        ]);

        return $this->render('view', [
            'dataProvider' => $users,
            'game' => $game,
        ]);
    }

}