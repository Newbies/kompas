<?php
/**
 * Created by PhpStorm.
 * User: janpa
 * Date: 2017-02-01
 * Time: 23:54
 */

namespace frontend\controllers;

use common\models\Game;
use common\models\Task;
use common\models\UserGame;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\HttpException;


class AccessController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                    'logout' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['contact', 'settings', 'email-update', 'login-update', 'attributes-update',
                            'password-update', 'profile', 'index', 'view', 'create', 'update',
                            'delete', 'reader', 'lobby', 'join', 'leave', 'archive', 'activate'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function checkAccessToTask($gameId, $teamId, $userId)
    {
        $task = Task::findById($teamId);
        if (!$task) {
            throw new HttpException('404', Yii::t('app', 'Takie zadanie nie istnieje.'));
        }

        if ($gameId != $task->game_id) {
            throw new HttpException('409', Yii::t('app', 'Nie kombinuj mi tu (⌐ ͡■ ͜ʖ ͡■)'));
        }

        return $this->checkAccessToGame($gameId, $userId);
    }

    public function checkAccessToGame($gameId, $userId)
    {
        $game = Game::findById($gameId);

        if (!$game || $game->isPrivate()) {
            if (!UserGame::findByUserIdAndGameId($userId, $gameId)) {
                throw new HttpException('403', Yii::t('app', 'Nie masz uprawnień.'));
            }
        }
    }

    public function checkAccessToTeam($gameId, $teamId, $userId)
    {
        $userGame = UserGame::findByAllId($userId, $gameId, $teamId);

        if (!$userGame) {
            throw new HttpException('403', Yii::t('app', 'Nie masz uprawnień.'));
        }

        return $this->checkAccessToGame($gameId, $userId);
    }

    public function checkAccessToReader($gameId)
    {
        $game = Game::findById($gameId);
        switch ($game->status) {
            case 0:
                throw new HttpException('403', Yii::t('app', 'Gra już się skończyła'));
            case 2:
                throw new HttpException('403', Yii::t('app', 'Gra jeszcze się nie zaczeła'));
        }
    }
}

