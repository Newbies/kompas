<?php

namespace frontend\controllers;


use common\models\User;
use Yii;

/**
 * Site controller
 */
class UserController extends AccessController
{

    public function actionDelete()
    {
        $user = User::getLogged();
        Yii::$app->user->logout(true);
        $user->delete();
        return $this->goHome();
    }


}
