<?php

namespace frontend\controllers;

use common\models\CompletedTask;
use common\models\fields\CompletedTaskFields;
use common\models\Task;
use common\models\Game;
use common\models\TaskActivator;
use Yii;
use yii\web\Cookie;

class TaskController extends AccessController
{
    public function actionView($gameId, $id)
    {
        $this->setCookie($id);
        $this->checkAccessToTask($gameId, $id, Yii::$app->user->id);

        if (CompletedTask::findOne([CompletedTaskFields::USER_ID => Yii::$app->user->id,
            CompletedTaskFields::TASK_ID => $id])) {
            $this->showView($id, $gameId);
            return $this->showViewDone($id, $gameId);
        } else {
            $this->showView($id, $gameId);
            return $this->showView($id, $gameId);
        }
    }

    private function setCookie($id)
    {
        $newCookie = new Cookie();
        $newCookie->name = 'task_' . $id;
        $newCookie->value = $id;
        $newCookie->expire = time() + 60 * 60 * 24 * 180;
        Yii::$app->getResponse()->getCookies()->add($newCookie);
    }

    public function showView($teamId, $gameId)
    {
        $game = Game::findById($gameId);
        return $this->render('view', [
            'model' => Task::findById($teamId),
            'end' => false,
            'endGame' => $game->isEnd(),
        ]);
    }

    public function showViewDone($teamId, $gameId)
    {
        $game = Game::findById($gameId);
        return $this->render('view', [
            'model' => Task::findById($teamId),
            'end' => true,
            'endGame' => $game->isEnd(),
        ]);

    }

    public function actionActivate($id){
        $task = Task::findById($id);
        $activatorId = Yii::$app->user->getId();
        $task->activateTimedTask($activatorId);

        $this->redirect('/game/' . $task->game_id . '/task/'.$id);
    }
}
