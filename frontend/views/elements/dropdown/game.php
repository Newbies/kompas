<?php

use yii\bootstrap\Dropdown;
use yii\helpers\Url;

?>
<div class="dropdown">
    <a href="#" data-toggle="dropdown" class="dropdown-toggle">
        <svg xmlns="http://www.w3.org/2000/svg"
             width="5px" height="20px">
            <path fill-rule="evenodd" fill="rgb(255, 255, 255)"
                  d="M2.500,12.500 C1.119,12.500 -0.000,11.381 -0.000,10.000 C-0.000,8.619 1.119,7.500 2.500,7.500 C3.881,7.500 5.000,8.619 5.000,10.000 C5.000,11.381 3.881,12.500 2.500,12.500 ZM2.500,5.000 C1.119,5.000 -0.000,3.881 -0.000,2.500 C-0.000,1.119 1.119,0.000 2.500,0.000 C3.881,0.000 5.000,1.119 5.000,2.500 C5.000,3.881 3.881,5.000 2.500,5.000 ZM2.500,15.000 C3.881,15.000 5.000,16.119 5.000,17.500 C5.000,18.881 3.881,20.000 2.500,20.000 C1.119,20.000 -0.000,18.881 -0.000,17.500 C-0.000,16.119 1.119,15.000 2.500,15.000 Z"/>
        </svg>
    </a>
    <?=
    Dropdown::widget([
        'items' => [
            $team ? ['label' => Yii::t('app', 'Moja drużyna'),
                'url' => Url::to(['game/' . $model->id .  '/team/view/' . $team->id])] : "",
            $organization ? ['label' => Yii::t('app', 'Kontakt z organizatorem'),
                'url' => 'mailto:' . $organization->contact_email] : "",
            $model ? ['label' => Yii::t('app', 'Opuść grę'),
                'url' => Url::to(['/game/leave/' . $model->id]),
                'options' => ['class' => 'leave-item'],
                'linkOptions' => ['data-metod' => 'post', 'data-confirm' => Yii::t('app', 'Napewno?')]] : "",
        ]
    ]);
    ?>
</div>
