
<script>
    function initMap() {
        var map = new google.maps.Map(document.getElementById('map'), {
            center: {lat: parseFloat('<?php echo $place->lat;?>'), lng: parseFloat('<?php echo $place->lng;?>')},
            zoom: 13,
            streetViewControl: false,
            mapTypeControl: false,
            styles: [
                {
                    "featureType": "landscape",
                    "stylers": [
                        {
                            "color": "#ffffff"
                        }
                    ]
                },
                {
                    "featureType": "poi",
                    "elementType": "labels.text",
                    "stylers": [
                        {
                            "visibility": "off"
                        }
                    ]
                },
                {
                    "featureType": "road",
                    "stylers": [
                        {
                            "color": "#53ca33"
                        }
                    ]
                },
                {
                    "featureType": "road",
                    "elementType": "labels",
                    "stylers": [
                        {
                            "visibility": "off"
                        }
                    ]
                },
                {
                    "featureType": "water",
                    "elementType": "geometry.fill",
                    "stylers": [
                        {
                            "color": "#009c3f"
                        }
                    ]
                },
                {
                    "featureType": "water",
                    "elementType": "labels.text",
                    "stylers": [
                        {
                            "visibility": "off"
                        }
                    ]
                }
            ]
        });
        var marker = new google.maps.Marker({
            position: {lat: parseFloat('<?php echo $place->lat;?>'), lng: parseFloat('<?php echo $place->lng;?>')},
            map: map,
            icon: {
                url: "../img/mapMarker.png"
            }
        });
    }
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAkWTL_wf7ki01gcxjhXY-EUw2Ip_QVIME&callback=initMap"
        async defer></script>