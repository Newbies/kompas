<?php
    $place = \common\models\Team::getPlaceByTeamAndGameId($model->game_id, $model->id);
?>
<div class="board-row">
    <div class="col-xs-12 no-padding ranking-row row-<?php if ($currentTeam == $model->id): echo "current-user"; endif; ?>">
        <div class="col-xs-3">
            <div class="circle-position-regular pos-<?php echo $place; ?>"><?php echo $place ?></div>
        </div>
        <div class="col-xs-6 name">
            <?php echo $model->name ?>
        </div>
        <div class="col-xs-3 text-right score">
            <?php echo $model->score ?>
        </div>
    </div>
</div>
