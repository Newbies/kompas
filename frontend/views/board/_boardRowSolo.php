<?php
$place = \common\models\UserGame::getPlaceByUserAndGameId($model->game_id, $model->user_id);
?>
<div class="board-row">
    <div class="col-xs-12 no-padding ranking-row row-<?php if ($model->user_id == $currentUser): echo "current-user"; endif; ?>">
        <div class="col-xs-3">
            <div class="circle-position-regular pos-<?php echo $place; ?>"><?php echo $place ?></div>
        </div>
        <div class="col-xs-6 name">
            <?php echo \common\models\User::findOne(['id' => $model->user_id])->showMe() ?>
        </div>
        <div class="col-xs-3 text-right score">
            <?php echo $model->score ?>
        </div>
    </div>
</div>
