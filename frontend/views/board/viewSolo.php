<?php

use common\models\Game;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

?>
<div class="board">

    <!-- HEAD -->
    <div class="row head-panel">

        <div class="col-xs-12">
            <a class="pull-left back-btn dark-button" href="<?php echo yii\helpers\Url::to(['game/view', 'id' => $game->id]);?>">
                <?= Yii::t('app', 'Powrót') ?>
            </a>
            <div class="pull-right type-task-btn">
                <?=Yii::t('app','WYNIKI')?>
            </div>
        </div>
        <div class="col-xs-12 head-top-game">
            <div class="row">
                <div class="col-xs-10 game-name">
                    <?php echo $game->name?>
                </div>
            </div>
        </div>

    </div>

    <div class="col-xs-12 main">
        <div class="row">
            <div class="col-xs-12 title"><?=Yii::t('app','Twój wynik')?></div>
        </div>
        <div class="scores-summary">
            <div class="row">
                <div class="col-xs-5 scores-summary-right-panel">
                    <i class="icon-clipboard vertical-center"></i>
                    <div class="tasks-counts vertical-center">
                        <span class="done"><?php echo $game->getCompletedTasksCount();?></span>
                        <span class="all">/<?php echo $game->getAllTasksCount();?></span>
                    </div>
                </div>

                <div class="col-xs-7 scores-summary-left-panel">
                    <?php switch ($currentUserPosition) {
                        case 1:
                            echo '<i class="icon-award" style="color: #e6c218;"></i>';
                            break;
                        case 2:
                            echo '<i class="icon-award" style="color: #969b9f;"></i>';
                            break;
                        case 3:
                            echo '<i class="icon-award" style="color: #c77525;"></i>';
                            break;
                        default:
                            echo '<i class="icon-award" style="color: #d9d9d9;"></i>';
                            break;
                    } ?>

                    <span><?= $currentUserPosition?> Miejsce</span>
                </div>

                <div class="col-xs-12 scores-summary-middle-panel">
                    <div class="task-progress">
                        <div class="fill" style="width:<?= $game->getPointsCompletionPercent() * 100; ?>%"></div>
                        <span><?= $game->getCurrentPoints(); ?>/<?= $game->getAllPoints(); ?> pkt</span>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xs-12 no-padding ranking-table-header">
            <div class="row">
                <div class="col-xs-3"><?=Yii::t('app','Miejsce')?></div>
                <div class="col-xs-6"><?=Yii::t('app','Gracz')?></div>
                <div class="col-xs-3 text-right"><?=Yii::t('app','Wynik')?></div>
            </div>
        </div>

        <?php

        echo yii\widgets\ListView::widget([
            'dataProvider' => $dataProvider,
            'viewParams' => [
                    'currentUser' => $currentUser,
                    'currentPage' => $dataProvider->pagination
            ],
            'itemView' => '_boardRowSolo',
            'layout' => '{items}{pager}',
            'pager' => [
                'maxButtonCount' => 5,
            ],
            'options' => ['tag' => 'div', 'class'=>'ranking-table-content row']
        ]); ?>

    </div>
</div>
