<?php

/* @var $this \yii\web\View */

/* @var $content string */

use common\widgets\Alert;
use frontend\assets\AppAsset;
use kartik\social\GoogleAnalytics;
use yii\helpers\Html;

$user = \common\models\User::findOne(['id' => Yii::$app->user->identity->getId()]);

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,900&amp;subset=latin-ext" rel="stylesheet">
    <link rel="shortcut icon" href="<?php echo Yii::$app->request->baseUrl; ?>/favicon.ico" type="image/x-icon"/>
    <link rel="icon" href="<?php echo Yii::$app->request->baseUrl; ?>/favicon.ico" type="image/x-icon"/>
    <title>Kompas</title>
    <?php $this->head() ?>
    <?= GoogleAnalytics::widget([]); ?>

</head>
<body>
<?php $this->beginBody() ?>

<div class="head">
    <div class="container">
        <div class="row header-main">
            <div class="col-xs-12">
                <header>
                    <div class="col-xs-2 no-padding vertical-center header-menu">
                        <div id="menu-btn-show" class="menu-btn-header"></div>
                    </div>
                    <div class="col-xs-8 no-padding vertical-center header-logo">
                        <a href="<?php echo yii\helpers\Url::to(['/game/index']); ?> ">
                            <img src="/img/logo2.png" class="logo" id="menu-logo"/></a>
                    </div>
                    <div class="col-xs-2 no-padding vertical-center header-counter">
                        <a href="<?= yii\helpers\Url::to(['/game/index']); ?>">
                            <span class="pull-right">
                                <i class="icon-run"></i>
                                <span class="main-counter">
                                    <?= \common\models\UserGame::find()
                                        ->innerJoin('game', 'user_game.game_id = game.id')
                                        ->where(['user_game.user_id' => Yii::$app->user->identity->getId()])
                                        ->andWhere(['>', 'user_game.invite_status', 0])
                                        ->andWhere(['game.status' => 1])
                                        ->count();
                                    ?>
                                </span></a>
                        </span>
                    </div>
                </header>
            </div>
        </div>

        <?= Alert::widget() ?>
        <?= $content ?>
    </div>

</div>

<!-- menu with main elements -->

<div id="menu-panel" class="menu-panel">
    <div class="container">

        <div id="menu-btn-hide" class="menu-btn-panel"></div>

        <div class="menu-panel-content">
            <div class="menu-panel-links">
                <ul>
                    <li>
                        <a href="<?php echo yii\helpers\Url::to(['/game/index']); ?>"><?= Yii::t('app', 'Moje gry') ?></a>
                    </li>
                    <li>
                        <a href="<?php echo yii\helpers\Url::to(['/archive']); ?>"><?= Yii::t('app', 'Zakończone gry') ?></a>
                    </li>
                    <li>
                        <a href="<?php echo yii\helpers\Url::to(['/site/howplay']); ?>"><?= Yii::t('app', 'Jak grać?') ?></a>
                    </li>
                    <li>
                        <a href="<?php echo yii\helpers\Url::to(['/site/contact']); ?>"><?= Yii::t('app', 'Kontakt z twórcami') ?></a>
                    </li>
                </ul>
            </div>
            <div class="menu-user">
                <div class="col-xs-2 no-padding">
                    <i class="icon-user"></i>
                </div>
                <div class="col-xs-10 no-padding user-info">
                    <div class="user-name"><?php echo $user->name ?><?php echo $user->last_name ?></div>
                    <div class="user-email"><?php echo $user->email ?></div>
                </div>
                <div class="col-xs-12 no-padding user-links">
                    <?php if (\common\models\User::getLogged()->hasEnterToAdminPanel()): ?>
                        <a id="menu-admin-btn" class="dark-button admin-panel"
                           href="http:\\<?php echo Yii::$app->params['backDomain'] ?>"><?= Yii::t('app', 'Panel Administratora') ?></a>
                        <br/>
                    <?php endif; ?>
                    <a class="dark-button user-settings"
                       href="<?php echo yii\helpers\Url::to(['/site/settings']); ?>"><?= Yii::t('app', 'Ustawienia') ?></a>
                    <a class="dark-button user-logout" href="<?php echo yii\helpers\Url::to(['/site/logout']); ?>"
                       data-method="post"><?= Yii::t('app', 'Wyloguj się') ?></a>
                </div>

            </div>
        </div>

    </div>

</div>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
