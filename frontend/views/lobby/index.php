<?php

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Gry');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="lobby">

    <!-- HEAD -->
<!--    <div class="row head-panel">-->
<!--        <div class="col-xs-12">-->
<!--            <a class="pull-left back-btn dark-button" href="--><?php //echo yii\helpers\Url::to(['/game/index']); ?><!--">-->
<!--                --><?//= Yii::t('app', 'Powrót') ?>
<!--            </a>-->
<!--            <div class="pull-right title-head">--><?//= Yii::t('app', 'OTWARTE GRY') ?><!--</div>-->
<!--        </div>-->
<!--    </div>-->
<!---->
<!--    <div class="row lobby-filter">-->
<!--        <div class="col-xs-12 lobby-search">-->
<!--            <div class="search-clear-btn"></div>-->
<!--            <input id="lobbyInput" type="search" placeholder="--><?php //echo \Yii::t('app', 'Nazwa gry'); ?><!--">-->
<!--        </div>-->
<!--        <div class="col-xs-12 lobby-map">-->
<!--            <div id="map"></div>-->
<!--        </div>-->
<!--    </div>-->

    <div class="row section-header">
        <div class="col-xs-10 no-padding">
            <h1><?php echo \Yii::t('app', 'Wyszukane gry'); ?></h1>
        </div>
        <div class="col-xs-2 no-padding text-right counter">
            <?php
                $numberRunning = $runningGames->getTotalCount();
                $numberUpcoming = $upcomingGames->getTotalCount();
                $totalCount = $numberRunning + $numberUpcoming;
                echo $totalCount;
            ?>
        </div>
    </div>

    <?= yii\widgets\ListView::widget([
        'dataProvider' => $runningGames,
        'itemView' => '_lobbyRunning',
        'layout' => '{items}',
        'options' => ['tag' => 'div', 'class' => 'lobby-list-running'],
        'emptyText' => Yii::t('app', 'W obecniej chwili nie masz żadnych aktywnych gier do których możesz dołączyć.'),
        'emptyTextOptions' => ['class' => 'empty-text upcomming',]

    ]); ?>

    <?= yii\widgets\ListView::widget([
        'dataProvider' => $upcomingGames,
        'itemView' => '_lobbyUpcoming',
        'layout' => '{items}',
        'options' => ['tag' => 'div', 'class' => 'lobby-list-upcoming'],
        'emptyText' => Yii::t('app', 'W obecniej chwili nie masz żadnych nachodzących gier do których możesz dołączyć.'),
        'emptyTextOptions' => ['class' => 'empty-text',]
    ]); ?>
</div>