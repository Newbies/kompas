<div class="lobby-list-element">
    <a href="<?php echo yii\helpers\Url::to(['lobby/view', 'id' => $model->id]); ?> ">
        <div class="col-xs-12 no-padding">
            <div class="type-game"><?= Yii::t('app', 'nadchodząca gra') ?></div>
            <div class="no-padding"><h2 class="truncate"><?php echo $model->getGameShortName(); ?></h2></div>
            <p class="truncate"><?php echo $model->getGameLongDescription(); ?></p>
        </div>
        <div class="bottom-section col-xs-12">
            <div class="timer">
                <i class="icon-watch"></i>
                <span>
                    <?php echo \russ666\widgets\Countdown::widget([
                        'datetime' => $model->start_date,
                        'format' => '%-Dd %-Hh %-Mm %-Ss',
                        'events' => [
                            'finish' => 'function(){location.reload()}',
                        ],]) ?>
                </span>
            </div>
        </div>
    </a>
</div>
