<div class="lobby-list-element">
    <a href="<?php echo yii\helpers\Url::to(['lobby/view', 'id' => $model->id]); ?> ">
        <div class="col-xs-12 no-padding">
            <div class="type-game"><?= Yii::t('app', 'bieżąca gra') ?></div>
            <div class="no-padding"><h2 class="truncate"><?php echo $model->getGameShortName(); ?></h2></div>
            <p class="truncate"><?php echo $model->getGameLongDescription(); ?></p>
        </div>
        <div class="bottom-section col-xs-12">
            <div class="row">
                <div class="pull-left counters">
                    <div class="counters-tasks vertical-center">
                        <i class="icon-clipboard vertical-center"></i>
                        <div class="counters-tasks-label vertical-center"><?php echo $model->getAllTasksCount(); ?></div>
                    </div>
                    <div class="counters-points vertical-center">
                        <i class="icon-volt vertical-center"></i>
                        <div class="counters-points-label vertical-center"><?php echo $model->getAllPoints(); ?> <span>pkt</span>
                        </div>
                    </div>
                </div>
                <div class="pull-right timer">
                    <i class="icon-watch"></i>
                    <span>
                        <?php echo \russ666\widgets\Countdown::widget([
                            'datetime' => $model->end_date,
                            'format' => '%-Dd %-Hh %-Mm %-Ss',
                            'events' => [
                                'finish' => 'function(){location.reload()}',
                            ],]) ?>
                    </span>
                </div>
            </div>
        </div>
    </a>
</div>
