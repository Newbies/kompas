<div class="game game-upcoming game-lobby lobby-in-progress">

    <!-- HEAD -->
    <div class="row head-panel">

        <div class="col-xs-12">
            <a class="pull-left back-btn dark-button"
               href="<?php echo yii\helpers\Url::to(['/lobby']); ?> "><?= Yii::t('app', 'Powrót') ?></a>
            <div class="pull-right status-game-btn">
                <span class="vertical-center"><?= Yii::t('app', 'NACHODZĄCA GRA ') ?></span>
                <i class="icon-arrow vertical-center"></i>
            </div>
        </div>
        <div class="col-xs-12 head-top-game">
            <div class="row">
                <div class="col-xs-10 game-name">
                    <?php echo $model->name; ?>
                </div>
                <div class="col-xs-2 more-actions">
                    <?= $this->render('/elements/dropdown/lobby', [
                        'organization' => $organization,
                    ]) ?>
                </div>
            </div>
        </div>

        <div class="col-xs-12 more-info">
            <a role="button" data-toggle="collapse" href="#gameInfo" aria-expanded="true" aria-controls="gameInfo">
                <?= Yii::t('app', 'Więcej informacji') ?>
                <svg
                        xmlns="http://www.w3.org/2000/svg"
                        width="21px" height="13px">
                    <path fill-rule="evenodd" fill="rgb(255, 255, 255)"
                          d="M19.527,3.895 L11.337,12.490 C10.691,13.168 9.643,13.168 8.997,12.490 L8.704,12.183 C8.704,12.183 8.704,12.183 8.704,12.183 L0.515,3.588 C-0.132,2.910 -0.132,1.811 0.515,1.132 L1.100,0.519 C1.746,-0.160 2.793,-0.160 3.440,0.519 L10.167,7.579 L16.602,0.826 C17.248,0.147 18.296,0.147 18.942,0.826 L19.527,1.439 C20.173,2.118 20.173,3.217 19.527,3.895 Z"/>
                </svg>
            </a>
        </div>

        <div class="col-xs-12">
            <div class="collapse in" id="gameInfo">

                <!-- Information about game-->

                <div class="row">
                    <div class="col-xs-12 info-section">
                        <div class="small-text"><?= Yii::t('app', 'Opis gry') ?></div>
                        <div class="big-text"><?php echo $model->description ?></div>
                    </div>
                    <div class="col-xs-12 info-section info-section-date">
                        <div class="row">
                            <div class="col-xs-4">
                                <div class="small-text"><?= Yii::t('app', 'Termin') ?></div>
                            </div>
                            <div class="col-xs-8">
                                <div class="big-text"><?= Yii::t('app', 'od ') ?><?php echo $model->start_date ?></div>
                                <div class="big-text"><?= Yii::t('app', 'do ') ?><?php echo $model->end_date ?></div>
                            </div>
                        </div>
                    </div>
                    <?php if (isset($place->city)): ?>
                        <div class="col-xs-12 info-section info-section-date">
                            <div class="row">
                                <div class="col-xs-4">
                                    <div class="small-text"><?= Yii::t('app', 'Lokalizacja gry') ?></div>
                                </div>
                                <div class="col-xs-8">
                                    <div class="big-text"> <?= $place->postal_code . ", " . $place->city . ", " . $place->voivodeship ?></div>
                                </div>
                                <div class="col-xs-12">
                                    <div id="map"></div>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                </div>

            </div>
        </div>

        <div class="col-xs-12 timer">
            <i class="icon-watch vertical-center"></i>
            <span class="vertical-center">
                <?= Yii::t('app', 'Do rozpoczęcia pozostało') ?> </br>
                <?php echo \russ666\widgets\Countdown::widget([
                    'datetime' => $model->start_date,
                    'format' => '%-Dd %-Hh %-Mm %-Ss',
                    'events' => [
                        'finish' => 'function(){place.reload()}',
                    ],]) ?>
            </span>
        </div>

    </div>

    <div class="organizer">
        <div class="row">
            <?php if (isset($organization)): ?>
                <div class="<?= $organization->hasLogo() ? 'col-xs-8' : 'col-xs-12' ?> organizer-content">
                    <div class="organizer-title"><?= Yii::t('app', ' Organizator gry') ?></div>
                    <div class="organizer-name"><?= $organization->name; ?></div>
                    <div class="organizer-description"><?= $organization->description; ?></div>
                    <a href="mailto:<?= $organization->contact_email; ?>"
                       class="dark-button"><?= Yii::t('app', ' Kontakt') ?></a>
                </div>
                <?php if ($organization->hasLogo()) : ?>
                    <div class="col-xs-4 text-right organizer-img">
                        <div class="organizer-img-circle"
                             style="background-image: url('<?= $organization->getLogoSrc(); ?>');"></div>
                    </div>
                <?php endif; ?>
            <?php endif; ?>
        </div>
    </div>

    <!-- reader button -->
    <a href="<?= \yii\helpers\Url::to(['/game/join', 'id' => $model->id]) ?>">
        <div class="reader-btn">
            <i class="icon-specialplus vertical-center"></i>
            <span class="vertical-center"><?= Yii::t('app', ' Dołącz do gry') ?></span>
        </div>
    </a>

</div>

<?php if (isset($place)): ?>

    <?php
    $lat = $place->lat;
    $lng = $place->lng;
    ?>
    <?= $this->render('/elements/map/init_lobby', ['place' => $place]); ?>
<?php endif; ?>
