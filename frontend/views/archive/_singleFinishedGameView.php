<?php $usergame = \common\models\UserGame::findByUserIdAndGameId(Yii::$app->user->id, $model->id) ?>

<?php if ($model->isTeamable()) {
    $userPosition = \common\models\Team::getPlaceByTeamAndGameId($model->id, $usergame->team_id) ?: 0;
} else {
    $userPosition = \common\models\UserGame::getPlaceByUserAndGameId($model->id, Yii::$app->user->id);
}
?>


<div class="single-game" style="width: 260px;">
    <a href="<?php
    echo yii\helpers\Url::to(['game/view', 'id' => $model->id]); ?> ">
        <div class="col-xs-12 no-padding type-game <?= $model->isPrivate() ? 'type-private' : 'type-open' ?>">
            <span><?php echo $model->getTypeLikeText(); ?></span>
            <i class="pull-right icon-lock"></i>
        </div>
        <div class="col-xs-12 no-padding">
            <div class="no-padding"><h2 class="truncate"><?= $model->getGameShortName(); ?></h2></div>
            <p class="truncate"><?php echo $model->getGameShortDescription(); ?></p>
        </div>
        <div class="counters col-xs-12">
            <div class="row">
                <div class="col-xs-6 counters-right-panel">
                    <i class="icon-clipboard vertical-center"></i>
                    <div class="tasks-counts vertical-center">
                        <span class="done"><?= $model->getCompletedTasksCount(); ?></span>
                        <span class="all">/<?= $model->getAllTasksCount(); ?></span>
                    </div>
                </div>
                <div class="col-xs-6 counters-left-panel">
                    <div class="task-progress">
                        <div class="fill" style="width:<?= $model->getPointsCompletionPercent() * 100; ?>%"></div>
                        <span><?= $model->getCurrentPoints(); ?>/<?= $model->getAllPoints(); ?> pkt</span>
                    </div>
                </div>
            </div>

        </div>
        <?php if (!$model->isTeamable() || ($model->isTeamable() && $usergame->team_id != null)) { ?>
            <div class="col-xs-12 no-padding ranking-place">
                <?php switch ($userPosition) {
                    case 1:
                        echo '<i class="icon-award" style="color: #e6c218;"></i>';
                        break;
                    case 2:
                        echo '<i class="icon-award" style="color: #969b9f;"></i>';
                        break;
                    case 3:
                        echo '<i class="icon-award" style="color: #c77525;"></i>';
                        break;
                    default:
                        echo '<i class="icon-award" style="color: #d9d9d9;"></i>';
                        break;
                } ?>
                <span><?= $userPosition ?> Miejsce</span>
            </div>
        <?php } ?>
        <div class="date col-xs-12">
            <span><?= \Yii::t('app', 'zakończono'); ?></span> <?php echo $model->end_date; ?>
        </div>
    </a>
</div>
