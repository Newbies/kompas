<?php

?>
<div class="dashboard">

    <div class="row section-header">
        <div class="col-xs-10 vertical-center">
            <i class="icon-trash vertical-center"></i>
            <h1 class="vertical-center">
                <?php echo \Yii::t('app', 'Zakończone gry'); ?>
            </h1>
        </div>
        <div class="col-xs-2 vertical-center">
            <span class="counter">
                <?php echo $endedGames->getTotalCount(); ?>
            </span>
        </div>
    </div>

    <?= yii\widgets\ListView::widget([
        'dataProvider' => $endedGames,
        'itemView' => '_singleFinishedGameView',
        'layout' => '{items}',
        'options' => ['tag' => 'div', 'class' => 'game-list-view owl-carousel'],
        'emptyText' => Yii::t('app', 'W obecniej chwili') . " " . Yii::t('app', 'nie masz żadnych') . " " . Yii::t('app', 'zakończonych gier. '),
        'emptyTextOptions' => ['class' => 'empty-text', 'style' => 'width: calc(100% - 8px);',]
    ]); ?>

</div>
