<?php

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $game \common\models\Game*/

?>
<div class="team">

    <!-- HEAD -->
    <div class="row head-panel">
        <div class="col-xs-12">
            <a class="pull-left back-btn dark-button" href="<?php echo yii\helpers\Url::to(['game/' . $game->id]); ?>">
                <?= Yii::t('app', 'Powrót') ?>
            </a>
            <div class="pull-right type-task-btn">
                <?= Yii::t('app', 'DRUŻYNA') ?>
            </div>
        </div>
        <div class="col-xs-12 head-top-game">
            <div class="row">
                <div class="col-xs-10 game-name">
                    <?php echo $game['name'] ?>
                </div>
            </div>
        </div>
    </div>

    <div class="col-xs-12 main">
        <div class="team-head">
            <div class="row">
                <div class="col-xs-12 title"><?= Yii::t('app', 'Twoja drużyna') ?></div>
            </div>
        </div>

        <div class="team-content">
            <?=
            yii\widgets\ListView::widget([
                'dataProvider' => $dataProvider,
                'itemView' => '_teamRow',
                'layout' => '{items}',
                'options' => ['tag' => 'div', 'class' => 'row']
            ]); ?>
        </div>

    </div>
</div>
