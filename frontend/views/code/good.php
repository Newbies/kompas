<?php
use yii\helpers\Url;
use yii\web\UrlManager;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

?>

<div class="codeGood modal-bg">
    <svg
    xmlns="http://www.w3.org/2000/svg"
    xmlns:xlink="http://www.w3.org/1999/xlink"
    width="1145px" height="910px">
    <path fill-rule="evenodd"  stroke="rgb(255, 255, 255)" stroke-width="10px" stroke-dasharray="0, 60" stroke-linecap="round" stroke-linejoin="round" opacity="0.6" fill="none"
    d="M10.597,46.339 C10.597,46.339 225.119,-58.357 231.927,79.610 C238.734,217.577 329.262,370.179 590.456,234.526 C851.650,98.874 1129.968,164.206 1130.025,508.267 C1130.083,852.329 783.619,935.717 591.271,877.287 "/>
    </svg>
    <svg
    xmlns="http://www.w3.org/2000/svg"
    xmlns:xlink="http://www.w3.org/1999/xlink"
    width="1178px" height="826px">
    <path fill-rule="evenodd"  stroke="rgb(255, 255, 255)" stroke-width="10px" stroke-dasharray="0, 60" stroke-linecap="round" stroke-linejoin="round" opacity="0.6" fill="none"
    d="M10.369,810.824 C10.369,810.824 26.961,572.694 149.848,635.782 C272.735,698.870 450.156,696.772 463.274,402.745 C476.393,108.718 672.131,-99.646 970.125,72.335 C1268.120,244.315 1167.104,586.056 1020.329,723.419 "/>
    </svg>
</div>

<div class="codeReader codeGood">
    <div class="container">
        <div class="icon-modal">
            <i class="icon-check"></i>
        </div>
        <h1>Gratulacje<span>Kod jest poprawny. Wykonaj kolejne zadania i wyprzedź rywali</span></h1>
        <div style=" " class="score-text">
            <i class="icon-volt"></i>
            <span>+<?= $score ?> pkt</span>
        </div>
        <?= Html::a(Yii::t('app', 'Zamknij'), '', ['style'=>"padding-top:12px;", 'class' => 'btn btn-primary code-button', 'data-dismiss' => 'modal', 'onclick' => 'javascript:window.location.href = \'' .Yii::$app->urlManager->createAbsoluteUrl(['game/view','id' => $gameId]) . '\'']) ?>
    </div>
</div>
