<?php

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

?>

<div class="codeReader codeBad">
    <div class="container">
        <div class="icon-modal">!</div>
        <h1>Uwaga<span>Kod nie jest poprawny</span></h1>
        <?php $form = ActiveForm::begin(['options' => ['data-pjax' => true],'action' => '/game/'.$gameId.'/code/reader']); ?>
        <?= Html::submitbutton(Yii::t('app', 'Spróbuj ponownie'), ['id' => 'reader-btn', 'class' => 'btn btn-primary code-button']) ?>
        <?php ActiveForm::end(); ?>
    </div>
</div>
