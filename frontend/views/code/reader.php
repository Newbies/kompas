<?php

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

?>

<div class="codeReader">
    <div class="container">
        <h1>Znalazłeś kod?</h1>
        <?php $form = ActiveForm::begin(['options' => ['data-pjax' => true],'action' => '/game/'.$gameId.'/code/reader']); ?>
        <?= $form->field($model, 'code')->textInput(['autofocus' => true, 'placeholder' => Yii::t('app', 'Wpisz kod'), 'class' => 'code-input form-control'])->label(false) ?>
        <?= Html::submitbutton(Yii::t('app', 'Sprawdź'), ['id' => 'reader-btn', 'class' => 'btn btn-primary code-button']) ?>
        <?= Html::a(Yii::t('app', 'Anuluj'), '', ['class' => 'btn btn-close', 'data-dismiss' => 'modal']) ?>
        <?php ActiveForm::end(); ?>
    </div>
</div>
