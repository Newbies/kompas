<?php

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

?>

<div class="codeReader codeBad">
    <div class="container">
        <div class="icon-modal">!</div>
        <h1><span>Niestety upłynął czas na wykonanie tego zadania. Nie poddawaj się 🙂</span></h1>
        <?php $form = ActiveForm::begin(['options' => ['data-pjax' => true],'action' => '/game/'.$gameId.'/code/reader']); ?>
        <?= Html::submitbutton(Yii::t('app', 'Spróbuj ponownie'), ['id' => 'reader-btn', 'class' => 'btn btn-primary code-button']) ?>
        <?php ActiveForm::end(); ?>
    </div>
</div>
