<?php

use frontend\models\readerForms\ReaderForm;
use yii\bootstrap\Modal;
use yii\widgets\Pjax;



Modal::begin([
    'size' => 'modal-lg',
    'id' => 'exampleModal',
]);
Pjax::begin(['id' => 'code_reader', 'enableReplaceState' => false , 'timeout' => 5000, 'enablePushState' => false ]);
echo "<div id='modalContent'>";

echo $this->render('/code/reader',[
    'model' => new ReaderForm(),
    'gameId' => $gameId,
]);
echo "</div>";
Pjax::end();
Modal::end();
?>
