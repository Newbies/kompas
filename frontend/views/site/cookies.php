<?php

?>

<div class="regulations container">
    <div class="col-xs-12">
        <div class="col-xs-12 box">

            <div class="col-xs-12 text-center">
                <h1>POLITYKA PLIKÓW COOKIES
                    SERWISU INTERNETOWEGO KOMPAS
                </h1>
            </div>
            <br>
            <center><h4>§1. DEFINICJE</h4></center>
            <div class="paragraf">
                <p>
                <ol>
                    <li>Administrator – RST Sp. z o.o. sp.k. z siedzibą w Świdnicy, przy ul. Esperantystów 17, nr KRS
                        0000354129, NIP:884-271-11-31, REGON 021232406.
                    </li>
                    <li>Cookies - dane informatyczne, niewielkie pliki tekstowe, zapisywane i przechowywane na
                        urządzeniach za pośrednictwem, których Użytkownik korzysta ze strony internetowej
                        Administratora.
                    </li>
                    <li>Urządzenie - elektroniczne urządzenie, za pośrednictwem którego Użytkownik uzyskuje dostęp do
                        strony internetowej Administratora.
                    </li>
                <li>Użytkownik - oznacza podmiot, na rzecz którego zgodnie z Regulaminem i przepisami prawa mogą być
                świadczone usługi drogą elektroniczną lub z którym zawarta może być Umowa o świadczenie usług drogą
                elektroniczną.</li>
                </ol>
                </p>
            </div>
            <br>
            <center><h4>§2. STOSOWANIE PLIKÓW COOKIES</h4></center>
            <div class="paragraf">
                <p>
                <ol>
                    <li>Administrator za pośrednictwem strony internetowej wykorzystuje pliki Cookies.</li>
                    <li>Informacje zgromadzone na podstawie plików Cookies wykorzystywane są w celach właściwej</li>
                        optymalizacji działania strony internetowej, a także w celach statystycznych oraz reklamowych.
                    <li>Pliki Cookies rejestrują aktywność Użytkownika strony internetowej rozpoznając urządzenie,
                        dzięki czemu strona internetowa wyświetlana jest w sposób zoptymalizowany do indywidualnych
                        preferencji Użytkownika.</li>
                    <li>Stosowane na stronie internetowej rozwiązania są bezpieczne dla urządzeń Użytkowników
                        korzystających ze strony internetowej Administratora. Nie jest możliwe przedostanie się do
                        urządzeń Użytkowników niebezpiecznego lub złośliwego oprogramowania.</li>
                    <li>Administrator wykorzystuje dwa typy plików Cookies:</li>
                        <ol type="a">
                            <li>Cookies sesyjne: to pliki, które są przechowywane na urządzeniu Użytkownika i pozostają
                                tam do momentu zakończenia sesji danej przeglądarki. Zapisane informacje są wówczas
                                trwale usuwane z pamięci Urządzenia. Mechanizm Cookies sesyjnych nie pozwala na
                                pobieranie jakichkolwiek danych osobowych ani żadnych informacji poufnych z urządzenia
                                Użytkownika.</li>
                            <li>Cookies trwałe: są przechowywane na urządzeniu Użytkownika i pozostają tam do momentu
                                ich skasowania. Zakończenie sesji danej przeglądarki lub wyłączenie Urządzenia nie
                                powoduje ich usunięcia z urządzenia. Mechanizm Cookies trwałych nie pozwala na
                                pobieranie jakichkolwiek danych osobowych ani żadnych informacji poufnych z urządzenia
                                Użytkownika.</li>
                        </ol>
                </ol>
                </p>
            </div>
            <br>
            <center><h4>§3. POSOBY OKREŚLENIA WARUNKÓW PRZECHOWYWANIA LUB UZYSKIWANIA DOSTĘPÓW DO COOKIES</h4></center>
            <div class="paragraf">
                <p>
                <ol>
                    <li>Użytkownik ma możliwość ograniczenia lub wyłączenia dostępu plików Cookies do swojego urządzenia. W przypadku skorzystania z tej opcji korzystanie ze Serwisu będzie możliwe, poza funkcjami, które ze swojej natury wymagają plików Cookies.</li>
                    <li>Użytkownik może samodzielnie i w każdym czasie zmienić ustawienia dotyczące plików Cookies, określając warunki ich przechowywania i uzyskiwania dostępu przez pliki Cookies do urządzenia Użytkownika. Zmiany ustawień, o których mowa powyżej, Użytkownik może dokonać za pomocą ustawień przeglądarki internetowej lub  za pomocą konfiguracji usługi. Ustawienia te mogą zostać zmienione w szczególności w taki sposób, aby blokować automatyczną obsługę plików Cookies w ustawieniach przeglądarki internetowej bądź informować o ich każdorazowym zamieszczeniu Cookies na urządzeniu. Szczegółowe informacje o możliwości i sposobach obsługi plików Cookies dostępne są w ustawieniach oprogramowania (przeglądarki internetowej).</li>
                    <li>Użytkownik może w każdej chwili usunąć pliki Cookies korzystając z dostępnych funkcji w przeglądarce internetowej, której używa.</li>
                    <li>Ograniczenie stosowania plików Cookies, może wpłynąć na niektóre funkcjonalności dostępne na stronie internetowej Administratora.</li>
                </ol>
                </p>
            </div>
        </div>
    </div>
</div>
</div>
