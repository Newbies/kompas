<div class="how-play">
    <h2>Dołączanie do gry</h2>
    <div class="col-xs-12 info-card">
        <div class="row info-card-element">
            <div class="col-xs-12 col-sm-6">
                <h3>Jeśli zostałeś zaproszony do gry</h3>
                <p>Wszystkie gry do których zostałeś zaproszony są dostępne w widoku <strong>Moje gry</strong>. Wybierz grę aby do niej dołączyć i zobaczyć listę zadań do wykonania.</p>
            </div>
            <div class="col-xs-12 col-sm-6 img-gif">
                <img src="/img/howplay/how_to_play_1.jpg" alt="Kompas jak grać">
            </div>
            <div class="col-xs-12 col-sm-6">
                <h3>Jeśli nie zaproszono Cię do gry a chcesz zagrać</h3>
                <h4><span class="how-play-counter">1</span>Wyszukaj grę</h4>
                <p>Na ekranie <strong>Moje gry</strong> znajdź i dotknij przycisk <strong>Wyszukaj grę.</strong></p>
                <h4><span class="how-play-counter">2</span>Wybierz grę</h4>
                <p>Spośród listy dostępnych gier wybierz tą, która najbardziej Cię interesuje 😉</p>
                <h4><span class="how-play-counter">3</span>Dołącz do niej!</h4>
                <p>Zapoznaj się z grą i jeśli chcesz wziąć w niej udział dotknij przycisk <strong>Dołączy do gry</strong>. Jeśli chcesz dowiedzieć się czegoś więcej, skontaktuj się z organizatorem.Wyszukaj grę.</p>
            </div>
            <div class="col-xs-12 col-sm-6 img-gif">
                <div class="owl-carousel-regular">
                  <div>
                      <img src="/img/howplay/how_to_play_2.jpg" alt="Kompas jak grać">
                  </div>
                  <div>
                      <img src="/img/howplay/how_to_play_3.jpg" alt="Kompas jak grać">
                  </div>
                  <div>
                      <img src="/img/howplay/how_to_play_4.jpg" alt="Kompas jak grać">
                  </div>
                </div>
            </div>
        </div>
    </div>
    <h2>Elementy gry</h2>
    <div class="col-xs-12 no-padding game-info-card">
        <div class="row">
            <div class="col-xs-12 col-sm-6 game-element text-center">
                <div class="content-game-element">
                    <div class="row">
                        <div class="col-xs-12">
                            <i class="icon-run" style="background: transparent; color: #009c3f; border: 2px solid rgb(0, 156, 63)"></i>
                        </div>
                        <div class="col-xs-12">
                            <h3>Zadania standardowe</h3>
                            <p>Treść tych zadań widoczna jest cały czas. Możesz zaliczyć to zadanie wpisując kod przez cały okres trwania gry.</p>
                        </div>
                        <div class="col-xs-12">
                            <img src="/img/howplay/how_to_play_5.jpg" alt="Kompas jak grać">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 game-element text-center">
                <div class="content-game-element">
                    <div class="row">
                        <div class="col-xs-12">
                            <i class="icon-star" style="background: #e2bb2e;"></i>
                        </div>
                        <div class="col-xs-12">
                            <h3>Zadania premium</h3>
                            <p>Zadania premium to wyjątkowe zadania, które pojawiają się w trakcie trwania gry. Gdy pojawi się takie zadanie zostaniesz o nim poinformowany emailem. Ponieważ zadania te często mają ograniczony czas na ich rozwiązanie, mogą przeważyć o Twojej wygranej więc bądź czujny.</p>
                        </div>
                        <div class="col-xs-12">
                            <img src="/img/howplay/how_to_play_7.jpg" alt="Kompas jak grać">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 game-element text-center">
                <div class="content-game-element">
                    <div class="row">
                        <div class="col-xs-12">
                            <i class="icon-watch" style="background: #e2bb2e;"></i>
                        </div>
                        <div class="col-xs-12">
                            <h3>Zadania Czasowe</h3>
                            <p>Zadania czasowe podobnie jak zadania standardowe są widoczne na liście zadań do wykonania w trakcie trwania gry, jednak są one ograniczone czasowo. Treść zadania czasowego dostępna jest dopiero po dotknięciu przycisku <strong>Start</strong>. Możesz zaliczyć to zadanie wyłącznie po jego rozpoczęciu i musisz zdążyć przed upłynięciem widocznego przy nim czasu.</p>
                        </div>
                        <div class="col-xs-12">
                            <img src="/img/howplay/how_to_play_6.jpg" alt="Kompas jak grać">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <h2>Rodzaje zadań</h2>
    <div class="col-xs-12 no-padding game-info-card">
        <div class="row">
            <div class="col-xs-12 col-sm-6 game-element text-center">
                <div class="content-game-element">
                    <div class="row">
                        <div class="col-xs-12">
                            <i class="icon-watch"></i>
                        </div>
                        <div class="col-xs-12">
                            Zegar odlicza do końca gry, więc spiesz się, aby wykonać jak najwięcej zadań!
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 game-element text-center">
                <div class="content-game-element">
                    <div class="row">
                        <div class="col-xs-12">
                            <svg
                                xmlns="http://www.w3.org/2000/svg"
                                width="21px" height="13px">
                                <path fill-rule="evenodd" fill="rgb(255, 255, 255)"
                                d="M19.527,3.895 L11.337,12.490 C10.691,13.168 9.643,13.168 8.997,12.490 L8.704,12.183 C8.704,12.183 8.704,12.183 8.704,12.183 L0.515,3.588 C-0.132,2.910 -0.132,1.811 0.515,1.132 L1.100,0.519 C1.746,-0.160 2.793,-0.160 3.440,0.519 L10.167,7.579 L16.602,0.826 C17.248,0.147 18.296,0.147 18.942,0.826 L19.527,1.439 C20.173,2.118 20.173,3.217 19.527,3.895 Z"/>
                            </svg>
                        </div>
                        <div class="col-xs-12">
                            W widoku gry dotknij przycisku <strong>Więcej informacji</strong> aby zobaczyć szczegóły gry. Mogą tam się znajdować ważne dla danej gry informacje.
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 game-element text-center">
                <div class="content-game-element">
                    <div class="row">
                        <div class="col-xs-12">
                            <i class="icon-specialplus"></i>
                        </div>
                        <div class="col-xs-12">
                            Po kliknięciu w przycisk <strong>Sprawdź kod</strong> wpisujesz odnalezione kody, które zlokalizujesz w terenie bądź otrzymasz od animatora za poprawnie wykonane zadanie.
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 game-element text-center">
                <div class="content-game-element">
                    <div class="row">
                        <div class="col-xs-12">
                            <i class="icon-award"></i>
                        </div>
                        <div class="col-xs-12">
                            W <strong>Podsumowaniu gry</strong> widzisz aktualną ilość punktów na dany moment a w <strong>Rankingu</strong> zobaczysz jak idzie innym graczom. O miejscu w rankingu decyduje ilość zdobytych punktów za zadania oraz czas w jakim te punkty zdobyto. Jeśli zdobędziesz tę samą ilość punktów co Twój rywal, to o wyższej pozycji w rankingu gry zdecyduje kto z Was zdobył tą ilość punktów pierwszy, więc warto się spieszyć.
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
