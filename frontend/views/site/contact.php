<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ContactForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

?>
<div class="contact">

    <h1>Skontaktuj się z twórcami</h1>
    <div class="col-xs-12 no-padding">

        <?php $form = ActiveForm::begin(['id' => 'contact-form']); ?>

        <?= $form->field($model, 'subject')->textInput(['autofocus' => true, 'placeholder' => \Yii::t('app', 'Temat'), 'class' => 'input form-control'])->label(false) ?>

        <?= $form->field($model, 'body')->textarea(['rows' => 6, 'placeholder' => \Yii::t('app', 'Treść'), 'class' => 'input text-input form-control'])->label(false) ?>

        <?= $form->field($model, 'reCaptcha')->widget(\himiklab\yii2\recaptcha\ReCaptcha::className()) ?>

    </div>
        <div class="form-group">
            <?= Html::submitButton('Wyślij', ['class' => 'btn btn-primary button']) ?>
        </div>

    <?php ActiveForm::end(); ?>
</div>
