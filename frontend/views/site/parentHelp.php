<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */

/* @var $model \frontend\models\SignupForm */

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

?>

<div class="container">
    <div class="col-xs-12" style="color: whitesmoke">
        <div class="col-xs-12 text-center">
            <h1><?= \Yii::t('app', 'Poproś rodzica o zgodę') ?></h1>
        </div>
        <div class="col-xs-12 text-justify">
            <h4><?= \Yii::t('app', 'Wygląda na to, że ze wzlędu na Twój wiek potrzbujemy zgody rodzica na używanie konta. Podaj adres email rodzica lub opiekuna aby mógł dokończyć zakładanie konta.') ?>  </h4>
            <br>
            <h3><?= \Yii::t('app', 'Podaj email rodzica') ?></h3>
            <?php $form = ActiveForm::begin(['id' => 'parent-help-form']); ?>
            <div class="no-padding col-xs-12">
                <?= $form->field($model, 'parentEmail')->textInput(['autofocus' => true, 'placeholder' => \Yii::t('app', 'Mail rodzica'), 'class' => 'signup-input form-control'])->label(false) ?>
            </div>
            <?= Html::submitButton(Yii::t('app', 'Wyślij prośbę o wyrażenie zgody'), ['class' => 'btn btn-primary signup-button', 'name' => 'signup-button']) ?>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>

