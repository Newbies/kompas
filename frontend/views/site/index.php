
<!-- Mobile nav -->
<div class="cbp-spmenu cbp-spmenu-vertical cbp-spmenu-right" id="cbp-spmenu-s2">
    <div class="mobile_nav_close_button"><a href="" id="hideRight"><div data-icon="&#xe13f;" class="button_icon close_icon"></div></a></div>
    <nav id="mobile_menu_content">
        <br>
        <br>
        <a href="#more_info" >Czym jest Kompas?</a>
        <a href="#features" >Możliwości</a>
        <a href="#reviews" >Opinie</a>
        <a href="#screenshots" >Screenshoty</a>
        <a href="#contact" >Kontakt</a>
    </nav>
</div>
<!-- //Mobile nav -->

<div id="preloader_container">

    <!-- Preloader Screen -->
    <header class="preloader_header">
        <div class="preloader_loader">
            <svg class="preloader_inner" width="60px" height="60px" viewBox="0 0 80 80">
                <path class="preloader_loader_circlebg" d="M40,10C57.351,10,71,23.649,71,40.5S57.351,71,40.5,71 S10,57.351,10,40.5S23.649,10,40.5,10z"/>
                <path id="preloader_loader_circle" class="preloader_loader_circle" d="M40,10C57.351,10,71,23.649,71,40.5S57.351,71,40.5,71 S10,57.351,10,40.5S23.649,10,40.5,10z"/>
            </svg>
        </div>
    </header>
    <!-- //Preloader Screen -->


    <!-- Fullscreen homepage -->
    <section class="hero_fullscreen background_video menu_bar-waypoint" data-animate-down="menu_bar-hide" data-animate-up="menu_bar-hide" >
        <!-- This section class is where you can edit background choice (background_single, background_slider, background_video) you can also enable gradient overlay (gradient_overlay) and mockups (mockups)-->


        <!-- Logo and navigation -->
        <div class="container top_bar" >
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <img src="img/logo_kompas_light.png" alt="logo" class="logo"/>
                    <nav class="navigation_desktop">
                        <ul>
                            <li><a href="#more_info" class="visible-lg visible-md go_to_overview">Czym jest Kompas?</a></li>
                            <li><a href="#features" class="visible-lg visible-md">Możliwości</a></li>
                            <li><a href="#reviews" class="visible-lg visible-md">Opinie</a></li>
                            <li><a href="#screenshots" class="visible-lg visible-md" >Screenshoty</a></li>
                            <li><a href="#contact" class="visible-lg visible-md">Kontakt</a></li>
                            <li><a id="btn-login" class="login-btn">Zaloguj się</a></li>
                            <li><div class="mobile_nav_open_button hidden-lg hidden-md"  ><a href="" id="showRight_1" ><div data-icon="&#xe2f3;" class="button_icon close_icon"></div></a></div></li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
        <!-- //Logo and navigation -->


        <!-- Main content -->
        <div class="container align-center" id="main_content">
            <div class="content_container row" >
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-md-offset-3 col-lg-offset-3 home_content">
                    <h2 class="slogan">RZĄDZI W MIEJSKIEJ GRZE</h2>
                    <h6>Kompas to aplikacja do zarządzania grą miejską. Monitoruj na bieżąco poczynania uczestników – twórz alternatywne scenariusze i&nbsp;zadania specjalne.</h6>

                    <!-- CTA Buttons-->
                    <div class="cta_button_area">
                        <div><a href="#more_info" class="btn waves-effect waves-light">Sprawdź szczegóły!</a></div>
                    </div>
                    <!-- //CTA Buttons-->

                </div>

                <!-- Mockups-->
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 home_mockup">
                    <img src="img/mockups/1/back.png" alt="" class="mockup-animation1 home_mockup_image">
                    <img src="img/mockups/1/front.png" alt="" class="mockup-animation2 home_mockup_image">
                </div>
                <!-- //Mockups-->

            </div>
        </div>
        <!-- //Main content -->


        <!-- Video Background -->
        <div id="maximage_video">
            <video autoplay="autoplay" loop="loop" muted="muted" width="896" height="504">
                <source src="video/video.mp4" type="video/mp4" />
                <source src="video/video.webm" type="video/webm" />
                <source src="video/video.ogv" type="video/ogg" />
            </video>
        </div>
        <!-- //Video Background -->


        <!-- Slider Background -->
        <div id="maximage_slider">
            <img src="img/background_slider_01.jpg" alt=""/>
            <img src="img/background_slider_02.jpg" alt=""/>
            <img src="img/background_slider_03.jpg" alt=""/>
            <img src="img/background_slider_04.jpg" alt=""/>
        </div>
        <!-- //Slider Background -->


        <!-- Single Image Background -->
        <div id="maximage_single">
            <img src="img/home_background_01.jpg" alt=""/>
        </div>
        <!-- //Single Image Background -->


    </section><!-- //Homepage -->


    <!-- Menu bar -->
    <header id="menu_bar" class="menu_bar">
        <div class="container">
            <a href="" class="go_to_home logo_dark"><img src="img/logo_kompas_dark.png" alt="logo" class="logo"/></a>
            <a href="" class="go_to_home logo_light"><img src="img/logo_kompas_light.png" alt="logo" class="logo"/></a>
            <nav class="menu_bar_navigation">
                <ul>
                    <li class="visible-lg visible-md"><a href="#more_info" class="go_to_overview" >Czym jest KOMPAS?</a></li>
                    <li class="visible-lg visible-md"><a href="#features" >Możliwości</a></li>
                    <li class="visible-lg visible-md"><a href="#reviews" >Opinie</a></li>
                    <li class="visible-lg visible-md"><a href="#screenshots" >Screenshoty</a></li>
                    <li class="visible-lg visible-md"><a href="#contact" >Kontakt</a></li>
                    <li><a id="btn-login-nav" class="login-btn">Zaloguj się</a></li>
                    <li class="hidden-lg hidden-md"><div class="mobile_nav_open_button "><a href="" id="showRight_2"><div data-icon="&#xe2f3;" class="button_icon close_icon"></div></a></div></li>
                </ul>
            </nav>
        </div>
    </header><!-- // Menu bar -->


    <!-- More Info #################### -->

    <section id="more_info" data-animate-down="menu_bar-show" data-animate-up="menu_bar-hide" class="menu_bar-waypoint subsection">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-10 col-md-10 col-lg-10 col-sm-offset-1 col-md-offset-1 col-lg-offset-1 align-center content_anim1 intro">
                    <h3>Czym jest KOMPAS?</h3>
                    <p>Kompas ta aplikacja do zarządzania grą miejską dla organizatorów takich inicjatyw. To unikatowe rozwiązanie pozwoli stworzyć grę za pomocą kilku kliknięć i od razu udostępnić ją zainteresowanym graczom. Wszystkie materiały znajdują się w jednym miejscu, a uczestnik gry widzi je na swoim telefonie.</p>

                </div>
            </div>
        </div>

        <div class="more_info_mockup content_anim2 ">
            <img src="img/mockups/subsection_mockups/1.png" alt="" class="img_responsive"/>
        </div>

        <div class="container">
            <div class="row">

                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                    <div class="iconbox content_anim3">
                        <div data-icon="&#xe024;" class="iconbox_icon"></div>
                        <div class="iconbox_content">
                            <h5>Responsywny wygląd</h5>
                            <p>Dzięki temu możesz używać naszej aplikacji na dowolnym urządzeniu.</p>
                        </div>
                    </div>
                </div>

                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                    <div class="iconbox content_anim4">
                        <div data-icon="&#xe08e;" class="iconbox_icon"></div>
                        <div class="iconbox_content">
                            <h5>Budowa modułowa</h5>
                            <p>Możesz włączać i wyłączać funkcjonalności, a także rozubudowywać program o własne potrzeby.</p>
                        </div>
                    </div>
                </div>

                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                    <div class="iconbox content_anim5">
                        <div data-icon="&#xe0be;" class="iconbox_icon"></div>
                        <div class="iconbox_content">
                            <h5>Zadania, wyniki, statusy</h5>
                            <p>Wszystko w jednym miejscu, dostępne od jednego kliknięcia.</p>
                        </div>
                    </div>
                </div>

            </div>
        </div>

    </section><!-- //More info -->



    <!-- Features #################### -->

    <section id="features" class="subsection">
        <div class="container">
            <div class="row">

                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                    <div class="mockup_container">
                        <img src="img/mockups/subsection_mockups/2a.png" alt="" class="img_responsive mockup_back content_anim10" />
                        <img src="img/mockups/subsection_mockups/2b.png" alt="" class="img_responsive mockup_front content_anim9" />
                    </div>
                </div>

                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">

                    <div class="intro content_anim6">
                        <h3>Niewielka aplikacja – moc możliwości</h3>
                        <p>KOMPAS został stworzony z myślą o wszystkich organizatorach Gier Miejskich lub rozgrywek opartych na rozwiązywaniu zadań w dowolnej przestrzeni i czasie, którzy chcą jeszcze lepiej, szybciej i efektywniej zarządzać przebiegiem rozgrywki. Aplikacja daje możliwość stworzenia dowolnej ilości gier, które toczą się niezależnie.</p>
                    </div>

                    <div class="iconbox content_anim7">
                        <div data-icon="&#xe037;" class="iconbox_icon"></div>
                        <div class="iconbox_content">
                            <h5>Przemyślana użyteczność</h5>
                            <p>Gracz na bieżąco widzi zadania do wykonania lub miejsca, które jeszcze może odwiedzić. </p>
                        </div>
                    </div>

                    <div class="iconbox content_anim8">
                        <div data-icon="&#xe095;" class="iconbox_icon"></div>
                        <div class="iconbox_content">
                            <h5>Bieżący podgląd stanu gry</h5>
                            <p>Gracz ma dostęp do najważniejszych informacji dotyczących gry i zasad, a do uczestnictwa potrzebuje jedynie smartfona z dostępem do Internetu.</p>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section><!-- //Features -->


    <!-- Features - Video #################### -->

    <section id="features_video" class="subsection">
        <div class="container">

            <div class="row">
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                    <div class="intro content_anim12">
                        <h3>Co zyskujesz dzięki KOMPASOWI?</h3>
                        <p>Projektowanie miejskich rozrywek nigdy jeszcze nie było tak proste!</p>
                        <p>Dzięki narzędziom administracyjnym, możesz w prosty sposób zaplanować i uruchomić całą rozgrywkę.</p>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5 col-sm-offset-1 col-md-offset-1 col-lg-offset-1">
                    <ul class="icon_list">
                        <li class="content_anim13"><div data-icon="&#xe24f;" class="icon_small float-left"></div><h6>Możesz stworzyć grę indywidualną lub zespołową (drużynową)</h6></li>
                        <li class="content_anim14"><div data-icon="&#xe1d7;" class="icon_small float-left"></div><h6>Będziesz mógł zapomnieć o drukowaniu dużej ilości materiałów dla uczestników - wszystko w jednym miejscu</h6></li>
                        <li class="content_anim15"><div data-icon="&#xe027;" class="icon_small float-left"></div><h6>Dostajesz to także narzędzie do łatwiejszego kontaktu z uczestnikiem - dodatkowe zadania</h6></li>
                        <li class="content_anim16"><div data-icon="&#xe102;" class="icon_small float-left"></div><h6>Kompas umożliwia podgląd wyników na bieżąco</h6></li>
                        <li class="content_anim16"><div data-icon="&#xe102;" class="icon_small float-left"></div><h6>Uczestnik potrzebuje jedynie telefonu do przystąpienia do gry</h6></li>
                    </ul>
                </div>
            </div>
        </div>
    </section><!-- //Features - Video -->


    <!-- Reviews #################### -->

    <section id="reviews" class="subsection_parallax"  data-stellar-background-ratio="0.6">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-8 col-md-8 col-lg-8 col-sm-offset-2 col-md-offset-2 col-lg-offset-2 content_anim17">
                    <div class="owl-carousel">

                        <div class="single_review">
                            <img src="img/review_01.jpg" alt="" class="img-circle"/>
                            <div class="review_content">
                                <h6>Możliwość zabawy z KOMPASEM pozwalała naszej drużynie śledzić ruchy rywali. Dzięki temu mogliśmy planować kolejne ruchy tak, aby być ciągle przed nimi.”</h6>
                                <p>- Łukasz, Scrum Master w firmie IT</p>
                            </div>
                        </div>

                        <div class="single_review">
                            <img src="img/review_02.jpg" alt="" class="img-circle"/>
                            <div class="review_content">
                                <h6>KOMPAS nie pozwola się zgubić. Odpalasz aplikację i idziesz jak po sznurku.</h6>
                                <p>- Kasia, blogerka kulinarna wyjadalnia.pl</p>
                            </div>
                        </div>

                        <div class="single_review">
                            <img src="img/review_03.jpg" alt="" class="img-circle"/>
                            <div class="review_content">
                                <h6>Zaplanowanie zabawy dla kiludziesięcioosobowej grupy to prawdziwe wyzwanie. Dzięki KOMPASOWI zbudowaliśmy scenariusz, podzieliliśmy uczestników na grupy i mogliśmy monitorować postępy gry. Dzięki opcji zadań specjalnych mieliśmy możliwość wprowadzenia niespodzianek do zabawy. Odbiór uczestników był entuzjastyczny!</h6>
                                <p>- Marta, współautorka scenariuszy gier miejskich</p>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section><!-- //Reviews -->

    <!-- Screenshots #################### -->

    <section id="screenshots" class="subsection">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 align-center">
                    <div class="intro content_anim25">
                        <h3>Screenshoty z aplikacji</h3>
                        <p>Możesz sprawdzić, jak prosta w obsłudze i funkcjonalna jest nasza aplikacja.</p>
                    </div>
                </div>
            </div>
        </div>

        <div class="content_anim26" id="screenshots_gallery">
            <div class="owl-carousel-screenshots">
                <div><a href="img/screenshots/1.jpg" data-lightbox-gallery="gallery1" ><img src="img/screenshots/1.jpg" class="shadow" alt=""></a></div>
                <div><a href="img/screenshots/3.jpg" data-lightbox-gallery="gallery1" ><img src="img/screenshots/3.jpg" class="shadow" alt=""></a></div>
                <div><a href="img/screenshots/5.jpg" data-lightbox-gallery="gallery1" ><img src="img/screenshots/5.jpg" class="shadow" alt=""></a></div>
                <div><a href="img/screenshots/6.jpg" data-lightbox-gallery="gallery1" ><img src="img/screenshots/6.jpg" class="shadow" alt=""></a></div>
                <div><a href="img/screenshots/7.jpg" data-lightbox-gallery="gallery1" ><img src="img/screenshots/7.jpg" class="shadow" alt=""></a></div>
                <div><a href="img/screenshots/8.jpg" data-lightbox-gallery="gallery1" ><img src="img/screenshots/8.jpg" class="shadow" alt=""></a></div>
                <div><a href="img/screenshots/9.jpg" data-lightbox-gallery="gallery1" ><img src="img/screenshots/9.jpg" class="shadow" alt=""></a></div>
            </div>
        </div>

    </section><!-- //Screenshots-->


    <!-- Newsletter #################### -->

    <section id="newsletter" class="subsection_parallax"  data-stellar-background-ratio="0.6">
        <div class="container">
            <div class="row">

            </div>
            <!-- //Newsletter form -->



        </div>
    </section><!-- //Newsletter-->


    <section id="contact" class="subsection">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5 content_anim29">
                    <h3>Kontakt</h3>
                    <p>Za KOMPASEM stoi młody zespół - Newbies powered by RST Software Masters.</p>
                    <a href="http://newbies.pl/"><img src="img/logo-newbies.png" alt="Newbies by RST Software Masters" /></a>
                    <p>Programowanie to ich pasja. Więcej informacji znajdziesz na ich <a href="http://newbies.pl/">stronie</a> :)</p>


                </div>
                <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5 col-sm-offset-2 col-md-offset-2 col-lg-offset-2 content_anim30">
                    <p>Możesz wysłać nam wiadomość, wypełniając poniższy formularz:</p>


                    <!-- Contact Form -->
                    <form action="php/contactform.php" id="contact-form" method="post">
                        <div class="contact_form">
                            <div class="input-field">
                                <input id="first_name" type="text" name="contact-name">
                                <label for="first_name">Imię i nazwisko</label>
                            </div>
                            <div class="input-field">
                                <input id="contact_email" type="email" name="contact-email">
                                <label for="contact_email">Adres email</label>
                            </div>
                            <div class="input-field">
                                <textarea class="materialize-textarea" name="contact-message"></textarea>
                                <label>Treść wiadomości</label>
                            </div>
                        </div>
                        <button class="btn waves-effect waves-light" type="submit" name="action">WYŚLIJ</button>
                    </form>
                    <!-- //Contact Form -->


                    <div id="message"><div id="alert"></div></div><!-- Message container -->
                </div>



            </div>
        </div>
    </section><!-- //More info -->


    <section id="footer" class="subsection">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 align-center">

                    <!-- Social Icons -->
                    <div class="social_icons_container align-center">
                        <div class="social_icons">
                            <a href="/site/regulations">Regulamin</a>
                        </div>
                    </div>
                    <!-- //Social Icons -->

                    <p><small>Copyright © 2017 RST Software Masters</small></p>
                </div>
            </div>
        </div>
    </section><!-- //More info -->


</div><!-- //preloader -->



</body>
</html>