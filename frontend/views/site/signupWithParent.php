<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */

/* @var $model \frontend\models\SignupForm */

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

?>
<div class="signup">
    <div class="container">
        <div class="text-center" style="color: whitesmoke">
            <h1><?= \Yii::t('app', 'Zakłdanie konta dziecka') ?></h1>
        </div>
        <div class="no-padding col-xs-12" style="color: whitesmoke">
            <h4><?= \Yii::t('app', 'Wygląda na to, że ') . $childEmail . \Yii::t('app', ' potrzbuje twojej pomocy aby założyć konto w aplikacji Kompas.') ?>  </h4>
            <h4><?= \Yii::t('app', 'Dowiedz się więcej o aplikacji ') . Html::a('Kompas', 'http://kompas.newbies.pl') ?></h4>
            <br>
            <h4><?= \Yii::t('app', 'Czy te dane wyglądają na prawdziwe?') ?></h4>
        </div>
        <?php $form = ActiveForm::begin(['id' => 'signup-form']); ?>

        <h3><?= Yii::t('app', 'Email dziecka') ?></h3>
        <div class="no-padding col-xs-12">
            <?= $form->field($model, 'email')->textInput(['autofocus' => true, 'type' => 'email', 'disabled' => $model->id, 'placeholder' => \Yii::t('app', 'Email'), 'class' => 'signup-input form-control'])->label(false) ?>
        </div>
        <h3><?= Yii::t('app', 'Imię dziecka') ?></h3>
        <div class="no-padding col-xs-12">
            <?= $form->field($model, 'name')->textInput(['autofocus' => true, 'placeholder' => \Yii::t('app', 'Imię'), 'class' => 'signup-input form-control'])->label(false) ?>
        </div>
        <h3><?= Yii::t('app', 'Nazwisko dziecka') ?></h3>
        <div class="no-padding col-xs-12">
            <?= $form->field($model, 'lastName')->textInput(['autofocus' => true, 'placeholder' => \Yii::t('app', 'Nazwisko'), 'class' => 'signup-input form-control'])->label(false) ?>
        </div>
        <div class="col-xs-12 text-center, no-padding">
            <?= $form->field($model, 'reCaptcha')->widget(\himiklab\yii2\recaptcha\ReCaptcha::className()) ?>
        </div>

        <?= Html::submitButton('Dalej', ['class' => 'btn btn-primary signup-button', 'name' => 'signup-button']) ?>
        <?php ActiveForm::end(); ?>

    </div>
</div>

