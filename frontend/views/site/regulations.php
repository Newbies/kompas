<?php

?>

<div class="regulations container">
    <div class="col-xs-12">
        <div class="col-xs-12 box">

            <div class="col-xs-12 text-center">
                <h1>REGULAMIN Serwisu internetowego KOMPAS</h1>
            </div>
            <center><h4>§1 Postanowienia Ogólne</h4></center>
            <div class="paragraf">
                <p>
                <ol>
                    <li>Regulamin określa zasady świadczenia usług drogą elektroniczną przez RST Sp. z o.o. sp.k. z
                        siedzibą w Świdnicy, przy ul. Esperantystów 17, nr KRS 0000354129, NIP:884-271-11-31, REGON
                        021232406, za pomocą serwisu internetowego Kompas. Warunkiem korzystania z serwisu internetowego
                        Kompas jest zapoznanie się i akceptacja Regulaminu.
                    </li>
                    <li>Użyte w Regulaminie określenia oznaczają:</li>

                    <ol type="a">
                        <li>Operator – RST sp. z o.o. sp.k.</li>
                        <li>Serwis – serwis internetowy umieszczony pod adresem <a
                                    href="http://kompas.newbies.pl">http://kompas.newbies.pl</a> umożliwiający dostęp do
                            Usług.
                        </li>
                        <li>Konto – oznaczony indywidualną nazwą i hasłem podanym przez Użytkownika, zbiór
                            zasobów w Serwisie, w którym gromadzone są dane Użytkowników;
                        </li>
                        <li>Usługa – usługa świadczona w ramach Serwisu</li>
                        <li>Użytkownik – podmiot korzystający z Serwisu</li>
                        <li>Konto – konto Użytkownika w Serwisie</li>
                        <li>Umowa – umowa o świadczenie usług za pośrednictwem Serwisu – zawierana pomiędzy
                            Operatorem i Użytkownikiem, na czas nieokreślony, na warunkach określonych w Regulaminie.
                        </li>
                        <li>Regulamin – niniejszy regulamin, określający zasady korzystania z Serwisu.</li>
                    </ol>
                </ol>
                </p>
            </div>
            <br>
            <center><h4>§ 2. ZASADY KORZYSTANIA Z SERWISU</h4></center>
            <div class="paragraf">
                <p>
                <ol>
                    <li>Do zawierania Umów dotyczących Usług konieczne jest założenie konta Użytkownika.</li>
                    <li>Umowa Użytkownika z Operatorem jest zawierana na czas nieokreślony.</li>
                    <li>Za chwilę zawarcia Umowy o świadczenie usług drogą elektroniczną uznaje się dzień, w którym
                        Użytkownik założy Konto w Serwisie. Od momentu potwierdzenia rejestracji Użytkownik uzyskuje
                        dostęp
                        do swojego Konta.
                    </li>
                    <li>Użytkownik zobowiązany jest do korzystania z Serwisu w sposób zgodny z niniejszym
                        Regulaminem, obowiązującym prawem oraz dobrymi obyczajami.
                    </li>
                    <li>Użytkownik zakładający Konto proszony jest o akceptację warunków Regulaminu.</li>
                    <li>Użytkownik jest zobowiązany podać elektroniczny adres email umożliwiający wysłanie linku
                        aktywującego Konto.
                    </li>
                    <li>Użytkownicy mogą w dowolnym czasie zaprzestać korzystania z Serwisu i usunąć Konto poprzez
                        użycie funkcji Serwisu: „Usuń konto”. Aby usunąć konto, należy:
                    </li>
                    <ol type="a">
                        <li>Zalogować się do serwisu kompas.newbies.pl.</li>
                        <li>Rozwinąć menu aplikacji.</li>
                        <li>Wybrać opcję “Ustawienia”.</li>
                        <li>Kliknąć przycisk “Usuń konto”.</li>
                    </ol>
                    <li>Założenie i prowadzenie Konta Użytkownika jest bezpłatne.</li>
                    <li>Aby w pełni korzystać z Serwisu obligatoryjne jest spełnienie następujących wymagań
                        technicznych:
                    </li>
                    <ol type="a">
                        <li>Obsługiwane przeglądarki na urządzeń mobilnych:</li>
                        <ol type="I">
                            <li>iOS Safari 7.1 lub nowsza</li>
                            <li>Chrome</li>
                            <li>Firefox</li>
                            <li>Android 4 lub nowszy</li>
                        </ol>
                        <li>Obsługiwane przeglądarki na komputerach osobistych:</li>
                        <ul type="disc">
                            <li>Internet Explorer 10.0 lub nowszy</li>
                            <li>Edge 12.0 lub nowszy</li>
                            <li>Firefox 28.0 lub nowszy</li>
                            <li>Chrome 21.0 lub nowszy</li>
                            <li>Safari 6.1 lub nowszy</li>
                            <li>Opera 12.1 lub nowsza</li>
                        </ul>
                        <li>Włączone mechanizmy obsługi JavaScript oraz Cookies.</li>
                    </ol>
                </ol>
                </p>
            </div>
            <br>
            <center><h4>§ 3. RODZAJ I ZAKRES USŁUG</h4></center>
            <div class="paragraf">
                <p>
                <ol>
                    <li>Operator umożliwia Użytkownikowi w ramach Serwisu branie udziału w grach miejskich w
                        następujących
                        dwóch trybach:
                    </li>
                    <ol type="a">
                        <li>tryb organizatora umożliwiający: przeszukiwanie bazy gier, zorganizowanie wybranej gry
                            dla uczestników,
                        </li>
                        <li>tryb gracza umożliwiający: przeszukiwanie bazy gier, zapoznanie się z informacjami na
                            temat toczących się gier, w których uczestnik może wziąć udział, branie udziału w wybranych
                            grach w
                            pojedynkę lub poprzez dołączenie do gry o ograniczonym dostępie na zaproszenie przez
                            organizatora,
                            zapoznanie się z założeniami rozpoczętej gry oraz zadaniami do wykonania w jej ramach.
                            Dodatkowo każdy Użytkownik ma możliwość przejścia do podglądu własnego profilu tj.
                            rezultatów gier,
                            które prowadził lub, w których brał udział.
                        </li>
                    </ol>
                    <li>Operator potwierdza, że właścicielem całej treści wprowadzonej za pomocą Usługi jest
                        Użytkownik. Zakazane jest, aby Użytkownik dostarczał treści bezprawne.
                    </li>
                    <li>Operator zastrzega sobie prawo do wprowadzenia ograniczeń w korzystaniu ze strony Serwisu
                        internetowego spowodowanych jego serwisem technicznym, pracami konserwacyjnymi, pracami nad
                        polepszeniem jego funkcjonalności lub z przyczyn zdarzeń losowych. Jednocześnie Operator
                        zobowiązuje
                        się do dołożenia wszelkich starań, by wspomniane przerwy, spowodowane jego działaniem, odbywały
                        się
                        w godzinach nocnych i trwały jak najkrócej.
                    </li>
                    <li>Operator nie ponosi odpowiedzialności za szkody wynikające z:
                        <ul type="disc">
                            <li>przerw w dostawie usługi lub niedostępności Usług niezależnych od Operatora Serwisu,
                            </li>
                            <li>przerw w dostawie będących następstwem zdarzeń, których Operator nie mógł przewidzieć,
                            </li>
                            <li>niepoprawnego funkcjonowania systemu niebędącego winą Operatora,</li>
                            <li>dostępu do konta Użytkownika przez osoby nieautoryzowane,</li>
                            <li>niebezpieczeństw związanych z użytkowaniem sieci,</li>
                            <li>nieznajomości lub nieprzestrzegania Regulaminu.</li>
                        </ul>
                </ol>
                </p>
            </div>
            <br>
            <center><h4>§ 4. DANE OSOBOWE</h4></center>
            <div class="paragraf">
                <p>
                <ol>
                    <li>Dane osobowe przetwarzane są wyłącznie w celu świadczenia usług, o których mowa w niniejszym
                        Regulaminie.
                    </li>
                    <li>Dane osobowe Użytkownika nie będą ujawniane innym osobom dla celów marketingowych bez jego
                        wyraźnej zgody. Do swoich danych Użytkownik ma zawsze dostęp w celu ich weryfikacji, modyfikacji
                        lub
                        usunięcia z bazy Operatora.
                    </li>
                    <li>Szczegółowe informacje dotyczące ochrony danych osobowych znajdują się w dokumencie Polityka
                        prywatności.
                    </li>
                </ol>
                </p>
            </div>
            <br>
            <center><h4>§ 5. REKLAMACJE</h4></center>
            <div class="paragraf">
                <p>
                <ol>
                    <li>W przypadku, gdy Użytkownik uzna, że Operator nie realizuje usług lub realizuje je niewłaściwie
                        może złożyć reklamację.
                    </li>
                    <li>Zgłoszenie reklamacyjne powinny być zgłaszane na adres: <a href="mailto:kompas@rst.com.pl">kompas@rst.com.pl</a>
                        lub listownie na adres siedziby Operatora.
                    </li>
                    <li>W treści zgłoszenia należy podać: imię i nazwisko, adres poczty elektronicznej, swoje uwagi i
                        żądania.
                    </li>
                    <li>Do właściwie złożonej reklamacji Operator ustosunkuje się w przeciągu 14 dni w przypadku
                        Użytkowników będących konsumentami w myśl prawa polskiego, w przypadku pozostałych Użytkowników
                        nie
                        będących konsumentami reklamacja zostanie rozpatrzona w terminie 30 dni.
                    </li>
                    <li>Odpowiedź na reklamację wysyłana jest na adres e-mail Użytkownika.</li>
                </ol>
                </p>
            </div>
            <br>
            <center><h4>§ 6. PRAWA AUTORSKIE</h4></center>
            <div class="paragraf">
                <p>
                <ol>
                    <li>Zdjęcia i inne pozostałe materiały zamieszczone na stronie Serwisu, takie jak teksty, grafiki
                        należą do Operatora lub zostały użyte za zgodą osób trzecich, posiadających do nich prawa
                        autorskie.
                    </li>
                    <li>Kopiowanie zdjęć i innych materiałów graficznych w tym ich udostępnianie w Internecie bez
                        pisemnej zgody Operatora lub innej osoby trzeciej, posiadającej do nich prawa autorskie jest
                        zabronione.
                    </li>
                    <li>Zabrania się również pobierania zdjęć ze strony Serwisu oraz wykorzystywania ich do celów
                        marketingowych oraz handlowych.
                    </li>
                </ol>
                </p>
            </div>
            <br>
            <center><h4>§ 7. POSTANOWIENIA KOŃCOWE</h4></center>
            <div class="paragraf">
                <p>
                    <ol>
                    <li>Operator zastrzega sobie prawo do wprowadzania zmian w Regulaminie. O każdych zmianach Operator
                    będzie informować Użytkowników za pośrednictwem strony internetowej Serwisu, oraz w korespondencji
                    mailowej przesyłanej na adres e-mail Użytkownika, podany przez niego podczas rejestracji Konta.</li>
                    <li>Zmiany wprowadzane w Regulaminie nie mają na celu naruszać praw nabytych przez Użytkowników
                    przed wprowadzeniem zmian.</li>
                    <li>Postanowienia niniejszego Regulaminu nie mają na celu wyłączać ani ograniczać jakichkolwiek
                    praw Użytkownika będącego konsumentem, przysługujących mu na mocy bezwzględnie obowiązujących
                    przepisów prawa. W przypadku niezgodności postanowień niniejszego Regulaminu z powyższymi
                    przepisami, pierwszeństwo mają te przepisy.</li>
                    <li>Wszelkie spory między Operatorem, a Użytkownikiem będą rozstrzygane w sposób polubowny.</li>
                    <li>Szczegółowe informacje, dane kontaktowe oraz wykaz polubownych sądów konsumenckich znajdują
                    się na stronach internetowych urzędów właściwych do ochrony praw konsumentów. Użytkownik może złożyć
                    swoją skargę np. za pośrednictwem unijnej platformy internetowej ODR (online dispute resolution),
                    dostępnej pod adresem: <a href="http://ec.europa.eu/consumers/odr/">http://ec.europa.eu/consumers/odr/</a>.</li>
                    <li>W przypadku braku możliwości polubownego rozwiązania sporu, Sądem właściwym do rozpatrywania
                    sporu z Użytkownikiem będącym konsumentem jest sąd właściwy ustalany według przepisów powszechnie
                    obowiązujących.</li>
                    <li>W przypadku sporu z Użytkownikiem niebędącym konsumentem – sądem rozstrzygającym będzie sąd
                    właściwy miejscowo dla siedziby Operatora.</li>
                    <li>Regulamin obowiązuje od dnia 1 maja 2018 roku.</li>
                </ol>
                </p>
            </div>
        </div>
    </div>
</div>
</div>
