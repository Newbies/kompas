<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\FinishRegistrationForm */

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

?>


<div class="container">

    <?php $form = ActiveForm::begin(['id' => 'finish-registration-form']); ?>

    <h3><?= Yii::t('app', 'Podaj hasło') ?></h3>
    <div class="no-padding col-xs-12">
        <?= $form->field($model, 'password')->passwordInput(['autofocus' => true, 'placeholder' => \Yii::t('app', 'Hasło'), 'class' => 'signup-input form-control'])->label(false) ?>
    </div>
    <h3><?= Yii::t('app', 'Powtórz hasło') ?></h3>
    <div class="no-padding col-xs-12">
        <?= $form->field($model, 'passwordRepeat')->passwordInput(['autofocus' => true, 'placeholder' => \Yii::t('app', 'Hasło'), 'class' => 'signup-input form-control'])->label(false) ?>
    </div>
    <div class="col-xs-12 signup-checkbox text-center">
        <?= $form->field($model, 'regulations')
            ->checkbox(['template' =>
                "<div class='col-xs-12 col-xs-12 signup-checkbox'>{input}" .
                Yii::t('app', ' Oświadczam, iż zapoznałem się oraz akceptuje postanowienia ') .
                "<a href='" . \yii\helpers\Url::to(['/site/regulations']) . "'><b>Regulaminu Serwisu Kompas</b></a>" .
                Yii::t('app', ' oraz ') .
                "<a href='" . \yii\helpers\Url::to(['/site/privacypolicy']) . "'><b>Polityki Prywatności Serwisu Kompas</b></a>" .
                "</div>\n<div class='col-xs-12'>{error}</div>",])
        ?>
    </div>

    <?= Html::submitButton('Utwórz konto', ['class' => 'btn btn-primary signup-button', 'name' => 'signup-button']) ?>
    <?php ActiveForm::end(); ?>

</div>


