<?php

?>

<div class="regulations container">
    <div class="col-xs-12">
        <div class="col-xs-12 box">

            <div class="col-xs-12 text-center">
                <h1>POLITYKA PRYWATNOŚCI SERWISU INTERNETOWEGO KOMPAS</h1>
            </div>
            <div class="paragraf">
                <p>
                    <ol>
                    <li>Niniejsza Polityka Prywatności określa sposób zbierania, przetwarzania i przechowywania danych osobowych koniecznych do realizacji usług świadczonych za pośrednictwem Serwisu Internetowego Kompas (Serwis) przez spółkę RST sp. z o.o. sp.k.
                    <li>Serwis jest aplikacją internetową umożliwiającą branie udziału w grach miejskich.
                    <li>RST sp. z o.o. sp.k. zobowiązuje się do zachowania bezpieczeństwa i poufności pozyskanych danych osobowych.
                    <li>Administratorem Danych Osobowych jest RST Sp. z o.o. sp.k. z siedzibą w Świdnicy, przy ul. Esperantystów 17, nr KRS 0000354129, NIP:884-271-11-31, REGON 021232406.
                    <li>Administrator zobowiązuje się do zachowania bezpieczeństwa i poufności danych osobowych.
                    <li>W Serwisie zbierane są następujące dane osobowe:
                    <ol type="a">
                        <li>Adres e-mail – na adres email podany w Serwisie zostaną wysłane wiadomości informujące o nowych zadaniach w grze.
                        <li>Imię i nazwisko – jeżeli  adres e-mail składa się z imienia i nazwiska, te dane osobowe będą przez Administratora przetwarzane,
                        <li>Adres IP – informacje wynikające z ogólnych zasad połączeń realizowanych w Internecie wykorzystywane są przez administratora Serwisu w celach technicznych. Adresy IP mogą też być wykorzystywane w celach statystycznych.
                        <li>Cookies – Serwis internetowy wykorzystuje technologię Cookies w celu dostosowania jego funkcjonowania do indywidualnych potrzeb Użytkownika. Użytkownik może zgodzić się na to, aby wpisane dane zostały zapamiętane, dzięki czemu będzie mógł bez powtórnego wpisywania korzystać z nich przy następnych odwiedzinach na stronach internetowych Serwisu. Właściciele innych witryn nie będą mieli dostępu do tych danych. W razie braku zgody na personalizowanie Serwisu, proponujemy wyłączenie obsługi Cookies w opcjach przeglądarki internetowej. Szczegółowe regulacje dotyczące Cookies zawiera Polityka Plików Cookies.
                    </ol>
                    <li>Podanie  powyższych danych jest konieczne w przypadku dobrowolnej rejestracji w Serwisie. Każdy Użytkownik Serwisu ma możliwość wyboru, czy i w jakim zakresie chce korzystać z  usług i udostępniać informacje o sobie. Jeżeli z jakiś względów Użytkownik nie życzy sobie pozostawienia swoich danych osobowych to ma prawo do ich usunięcia lub nie korzystania z Serwisu.
                    <li>Jeżeli Użytkownik wprowadzi do Serwisu inne niż określone w Polityce Prywatności dane, w tym dane osobowe, robi to na własne ryzyko oraz ponosi za nie odpowiedzialność w tym odpowiedzialność za legalność ich przetwarzania.
                    <li>W celu zapewnienia właściwego działania Serwisu, możemy pobierać (w tym automatycznie) i przetwarzać informacje, które nie stanowią danych osobowych. Dane dotyczące aktywności Użytkowników w aplikacjach mogą być przetwarzane przez nas do celów statystycznych. Dotyczy to danych takich jak np.
                    <ol type="a">
                        <li>czas korzystania,
                        <li>wyświetlone okna,
                        <li>uruchomione funkcjonalności.
                    </ol>
                    Anonimowe dane statystyczne są zbierane przy pomocy narzędzia Google Analytics lub innych podobnych narzędzi.
                    <li>Podanie danych osobowych jest dobrowolne. Użytkownik ma prawo do  usunięcia danych. Brak podania danych i usunięcie danych powoduje brak możliwości korzystania z funkcjonalności Serwisu. Dane są przechowywane w bazie Administratora i są zabezpieczone przed dostępem osób nieuprawnionych.
                    <li>Użytkownik ma prawo żądania od Administratora danych osobowych dostępu do danych osobowych dotyczących oraz w każdej chwili cofnąć udzieloną zgodę na przetwarzanie danych osobowych, możesz żądać od Administratora poprawiania, uzupełniania, uaktualnienia, sprostowania oraz żądania usunięcia swoich danych osobowych.
                    <li>Dane osobowe o których mowa powyżej wykorzystywane są w celu właściwego świadczenia usług przez Serwis, dane przetwarzane są w oparciu o zgodę, wyrażoną podczas rejestracji w Serwisie lub w związku ze świadczeniem na rzecz Użytkownika usług w związku z zawartą umową drogą elektroniczną o której mowa w Regulaminie serwisu internetowego Kompas.
                    <li>Dane osobowe Użytkownika przetwarzane są przez czas trwania umowy o świadczenie usług drogą elektroniczną łączącą Użytkownika z  RST sp. z o.o. sp.k., jest to niezbędne do świadczenia ww. usług.
                    <li>Danych osobowe nie są udostępniane osobom trzecim do celów marketingowych, bez wyraźniej zgody Użytkownika, którego dane dotyczą.
                    <li>Administrator danych osobowych ma prawo powierzenia przetwarzania danych osobowych Użytkowników innym podmiotom na podstawie pisemnych umów w celu wykonania przez te firmy usług na rzecz Administratora Serwisu prowadzących do jego poprawnego działania, np. usług związanych z hostingiem, utrzymaniem oraz zarządzaniem serwerami na których funkcjonuje Serwis.
                    <li>Przez właściwe świadczenie usług, rozumiemy przetwarzanie Twoich danych osobowych przede wszystkim w celu:
                    <ol type="a">
                        <li>rejestracji Konta,
                        <li>w razie potrzeby kontaktu mailowego,
                        <li>przekazania informacji o statusie Konta użytkownika,
                        <li>informacji o planowanych przerwach technicznych,
                        <li>informacji o ewentualnej awarii systemu.
                    </ol>
                    <li>Użytkownik może wnieść skargę do organu nadzorczego, jeżeli dane osobowe są przetwarzane w sposób nieprawidłowy lub niezgodny z niniejszą Polityką Prywatności.
                    <li>W razie pytań wątpliwości czy zastrzeżeń do przetwarzania danych prosimy o Kontakt z Serwisem:  kompas@rst.com.pl
                    <li>W sprawach nieuregulowanych Polityką Prywatności Serwisu internetowego Kompas zastosowanie mają przepisy powszechnie obowiązujące.
                    <li>Polityka Prywatności obowiązuje od 1 maja 2018 r.
                </ol>
                </p>
            </div>
        </div>
    </div>
</div>
</div>
