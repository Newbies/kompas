<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\authclient\widgets\AuthChoice;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

?>
<div class="container">

    <div class="login">
        <div class="row">
            <div class="col-xs-12">

                <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>

                <?= $form->field($model, 'email')->textInput(['autofocus' => true, 'type'=> 'email', 'placeholder' => \Yii::t('app', 'Email'), 'class' => 'login-input form-control'])->label(false) ?>

                <?= $form->field($model, 'password')->passwordInput(['placeholder' => \Yii::t('app', 'Hasło'), 'class' => 'login-input form-control'])->label(false) ?>

                <div class="form-group">
                    <?= Html::submitButton('Zaloguj się', ['class' => 'btn btn-primary login-button', 'name' => 'login-button']) ?>
                </div>
                <?php ActiveForm::end(); ?>


                <?php /*$authAuthChoice = AuthChoice::begin(['baseAuthUrl' => ['site/auth'], 'autoRender' => false]);
                foreach ($authAuthChoice->getClients() as $client) {
                    echo "<a class='btn btn-block btn-social btn-lg btn-facebook $client->name' href='".\yii\helpers\Url::to(['site/auth', 'authclient'=> $client->name, ])."'>
                          <span class='fa fa-facebook'></span>".Yii::t('app','Zaloguj się przez ').$client->getTitle()."</a>";
                }
                AuthChoice::end(); */?>

                <div class="col-xs-12 text-center">
                    <p class="signup-text"><?= Yii::t('app', 'Nie masz jeszcze konta ?') ?></p>
                </div>
                <a href="<?php echo yii\helpers\Url::to(['/site/signup']); ?>">
                    <div class="col-xs-12 btn btn-primary signup-btn">
                        <?= Yii::t('app', 'Załóż konto') ?>
                    </div>
                </a>


            </div>
        </div>
    </div>
</div>
<div class="footer forget">
    <div class="container">
        <div class="row">
            <div class="col-xs-3 icon-forget">
                <span><i class="icon-lockarrow"></i></span>
            </div>
            <div class="col-xs-9">
                <div class="pull-right footer-row">
                    <span>Zapomniałeś hasła?</span> <?= Html::a('Zresetuj', ['site/request-password-reset'], ['class' => 'dark-button reset-password']) ?>
                </div>
            </div>
        </div>
    </div>
</div>
