<?php

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

?>

<div class=" container site-request-password-reset">
    <div class="col-xs-12 no-padding">

        <?php $form = ActiveForm::begin(['id' => 'request-password-reset-form']); ?>

        <?= $form->field($model, 'email')->textInput(['autofocus' => true, 'type'=> 'email','placeholder' => \Yii::t('app', 'Email'), 'class' => 'email-input form-control'])->label(false) ?>

        <?= $form->field($model, 'reCaptcha')->widget(\himiklab\yii2\recaptcha\ReCaptcha::className()) ?>

        <div class="form-group">
            <?= Html::submitButton('Resetuj', ['class' => 'btn btn-primary reset-button']) ?>
        </div>

        <div class="form-group">
            <a class="back-btn dark-button" href="<?php echo yii\helpers\Url::to(['/site/login']); ?>">
                <?= Yii::t('app', 'Powrót') ?>
            </a>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
</div>
