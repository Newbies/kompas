<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\SignupForm */

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

?>
<div class="signup">
    <div class="container">

        <?php $form = ActiveForm::begin(['id' => 'signup-form']); ?>

        <h3><?= Yii::t('app', 'Podaj email') ?></h3>
        <div class="no-padding col-xs-12">
            <?= $form->field($model, 'email')->textInput(['autofocus' => true, 'type' => 'email', 'disabled' => $model->id, 'placeholder' => \Yii::t('app', 'Email'), 'class' => 'signup-input form-control'])->label(false) ?>
        </div>
        <h3><?= Yii::t('app', 'Podaj imię') ?></h3>
        <div class="no-padding col-xs-12">
            <?= $form->field($model, 'name')->textInput(['autofocus' => true, 'placeholder' => \Yii::t('app', 'Imie'), 'class' => 'signup-input form-control'])->label(false) ?>
        </div>
        <h3><?= Yii::t('app', 'Podaj nazwisko') ?></h3>
        <div class="no-padding col-xs-12">
            <?= $form->field($model, 'lastName')->textInput(['autofocus' => true, 'placeholder' => \Yii::t('app', 'Nazwisko'), 'class' => 'signup-input form-control'])->label(false) ?>
        </div>
        <h3><?= Yii::t('app', 'Data urodzenia') ?></h3>

        <div class="col-xs-4 no-padding">
            <?= $form->field($model, 'birthDay')->textInput(['type' => 'number','autofocus' => true, 'placeholder' => \Yii::t('app', 'DD'), 'class' => 'signup-input form-control'])->label(false) ?>
        </div>
        <div class="col-xs-4 small-padding">
            <?= $form->field($model, 'birthMonth')->textInput(['type' =>'number','autofocus' => true, 'placeholder' => \Yii::t('app', 'MM'), 'class' => 'signup-input form-control'])->label(false) ?>
        </div>
        <div class="col-xs-4 no-padding">
            <?= $form->field($model, 'birthYear')->textInput(['type' =>'number', 'autofocus' => true, 'placeholder' => \Yii::t('app', 'RRRR'), 'class' => 'signup-input form-control'])->label(false) ?>
        </div>

        <br>

        <div class="col-xs-12 text-center, no-padding">
            <?= $form->field($model, 'reCaptcha')->widget(\himiklab\yii2\recaptcha\ReCaptcha::className()) ?>
        </div>

        <?= Html::submitButton('Dalej', ['class' => 'btn btn-primary signup-button', 'name' => 'signup-button']) ?>
        <?php ActiveForm::end(); ?>

        <a class="back-btn dark-button" href="<?php echo yii\helpers\Url::to(['/site/login']); ?>">
            <?= Yii::t('app', 'Powrót') ?>
        </a>
    </div>
</div>

