<?php

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

?>
<div class="profile-attributes-update">
    <div class="col-xs-12 no-padding ">
        <div class="h4">
            Dane logowania
        </div>
        <div >
            <?php $form = ActiveForm::begin(['id' => 'attributes-update']); ?>

            <div class="h3">
                <?php if ($user->email) {
                    echo 'obecny email: ' . $user->email;
                } ?>
            </div>
            <?= $form->field($model, 'email')->textInput(['autofocus' => true, 'placeholder' => \Yii::t('app', 'Nowy email'), 'class' => 'input form-control'])->label(false) ?>
        </div>
        <div class="h4">
            Imię & nazwisko
        </div>
        <div >
            <?= $form->field($model, 'name')->textInput(['value' => $user->name, 'autofocus' => true, 'placeholder' => \Yii::t('app', 'Imię'), 'class' => 'input form-control'])->label(false) ?>
            <?= $form->field($model, 'last_name')->textInput(['value' => $user->last_name, 'autofocus' => true, 'placeholder' => \Yii::t('app', 'Nazwisko'), 'class' => 'input form-control'])->label(false) ?>
        </div>

        <div class="h4">
            Hasło logowania
        </div>
        <div >
            <?= $form->field($model, 'password_old')->passwordInput(['autofocus' => true, 'placeholder' => \Yii::t('app', 'Stare hasło'), 'class' => 'input form-control'])->label(false) ?>
            <?= $form->field($model, 'password')->passwordInput(['autofocus' => true, 'placeholder' => \Yii::t('app', 'Nowe hasło'), 'class' => 'input form-control'])->label(false) ?>
            <?= $form->field($model, 'password_repeat')->passwordInput(['autofocus' => true, 'placeholder' => \Yii::t('app', 'Powtórz nowe hasło'), 'class' => 'input form-control'])->label(false) ?>
        </div>
        <?= $form->field($model, 'id')->textInput(['type' => 'hidden', 'value' => $user->id])->label(false)?>
        <?= $form->field($model, 'password_old_hash')->textInput(['type' => 'hidden', 'value' => $user->password_hash])->label(false)?>

        <div class="div-delete">
            <a href="<?= \yii\helpers\Url::to(['/user/delete']) ?>" data-method="post"
               data-confirm="<?= Yii::t('app', 'Czy napewno chcesz usunąć swoje konto?') ?>"
               class="btn btn-block btn-danger" id="settings-delete-btn">
                <?= Yii::t('app', 'Usuń konto') ?>
            </a>
        </div>

        <div class="form-group">
            <?= Html::submitButton('Zapisz', ['class' => 'btn btn-primary button']) ?>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>
