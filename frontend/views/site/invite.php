<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Zaproszenie do Gry';
$this->params['breadcrumbs'][] = $this->title;

$gameName = \common\models\Game::findOne(['id' => $game])->name;
?>
<div class="invite">
    <div class="container">
        <div class="col-xs-12">

            <?php $form = ActiveForm::begin(['id' => 'invite-form']); ?>
            <h3><?=Yii::t('app','Pola wymagane')?></h3>
            <div class="div-border">
                <?= $form->field($model, 'login')->textInput(['autofocus' => true, 'placeholder' => \Yii::t('app', 'Login'), 'class' => 'signup-input form-control'])->label(false) ?>

                <?= $form->field($model, 'email')->textInput(['value' => $email,'autofocus' => true, 'placeholder' => \Yii::t('app', 'Email'), 'class' => 'signup-input form-control'])->label(false) ?>

                <?= $form->field($model, 'password')->passwordInput(['placeholder' => \Yii::t('app', 'Hasło'), 'class' => 'signup-input form-control'])->label(false) ?>

                <?= $form->field($model, 'repassword')->passwordInput(['placeholder' => \Yii::t('app', 'Powtórz hasło'), 'class' => 'signup-input form-control'])->label(false) ?>
            </div>

            <h3><?=Yii::t('app','Pola dodatkowe')?></h3>
            <div class="div-border">
                <?= $form->field($model, 'name')->textInput(['autofocus' => true, 'placeholder' => \Yii::t('app', 'Imie'), 'class' => 'signup-input form-control'])->label(false) ?>

                <?= $form->field($model, 'last_name')->textInput(['autofocus' => true, 'placeholder' => \Yii::t('app', 'Nazwisko'), 'class' => 'signup-input form-control'])->label(false) ?>
            </div>

            <div class="form-group">
                <?= Html::submitButton('Aktywuj', ['class' => 'btn btn-primary signup-button', 'name' => 'signup-button']) ?>
            </div>
            <?php ActiveForm::end(); ?>

        </div>
    </div>
</div>