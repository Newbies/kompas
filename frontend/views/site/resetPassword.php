<?php

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

?>

<div class="resetPassword container">
    <div class="col-xs-12 no-padding">

        <?php $form = ActiveForm::begin(['id' => 'reset-password-form']); ?>

        <?= $form->field($model, 'password')->passwordInput(['autofocus' => true, 'placeholder' => \Yii::t('app', 'Nowe hasło'), 'class' => 'password-input form-control'])->label(false) ?>

        <div class="form-group">
            <?= Html::submitButton('Ustaw', ['class' => 'btn btn-primary set-button']) ?>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
</div>
