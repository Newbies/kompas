<?php
    $userId = \Yii::$app->user->getId();
?>
<div class="task">

    <!-- HEAD -->
    <div class="row head-panel">

        <div class="col-xs-12">
            <a class="pull-left back-btn dark-button"
               href="<?php use common\models\Task;
               use yii\helpers\Html;

               echo yii\helpers\Url::to(['game/view', 'id' => $model->game_id]); ?>">
                <?= Yii::t('app', 'Powrót') ?>
            </a>
            <?php if ($model->isTimed()) { ?>
            <div class="pull-right type-task-btn type-premium">
                <i class="icon-watch"></i>
                <?= Yii::t('app', 'ZADANIE CZASOWE') ?>
            </div>
            <?php } else if ($model->isPremium()) { ?>
                <div class="pull-right type-task-btn type-premium">
                    <i class="icon-star"></i>
                    <?= Yii::t('app', 'ZADANIE PREMIUM') ?>
                </div>
            <?php } else if ($model->isStandard()) { ?>
                <div class="pull-right type-task-btn">
                    <?= Yii::t('app', 'ZADANIE STANDARDOWE') ?>
                </div>
            <?php } ?>
        </div>
        <div class="col-xs-12 head-top-game">
            <div class="row">
                <div class="col-xs-10 task-name">
                    <?php echo $model->name; ?>
                </div>
            </div>
        </div>

        <?php if (($model->isPremium()) && (!$endGame)) { ?>

            <?php if (($model->status == Task::STATUS_CURRENT) != ($end)) { ?>

                <div class="col-xs-12 timer timer-current">
                    <i class="icon-watch vertical-center"></i>
                    <span class="vertical-center">
                        <?= Yii::t('app', 'Do zakończenia pozostało ') ?><br/>
                        <?php echo \russ666\widgets\Countdown::widget([
                            'datetime' => $model->end_time,
                            'format' => '%-Dd %-Hh %-Mm %-Ss',
                            'events' => [
                                'finish' => 'function(){location.reload()}',
                            ],]) ?>
                    </span>
                </div>

            <?php } else if ($model->status == Task::STATUS_FINISHED) { ?>

                <div class="col-xs-12 timer timer-ended">
                    <i class="icon-watch vertical-center"></i>
                    <span class="vertical-center">
                        <?= Yii::t('app', 'Czas na wykonanie minął') ?>
                        <?= $model->end_time; ?>
                    </span>
                </div>

            <?php } ?>

        <?php } ?>


        <?php if ($model->isTimed() && !$model->isTimedTaskExpired($userId) && $model->isActivated($userId) && !$endGame) { ?>

                <div class="col-xs-12 timer timer-current">
                    <i class="icon-watch vertical-center"></i>
                    <span class="vertical-center">
                        <?= Yii::t('app', 'Do zakończenia pozostało ') ?><br/>
                        <?php echo \russ666\widgets\Countdown::widget([
                            'datetime' => $model->getTimedTaskEndTime($userId),
                            'format' => '%-Dd %-Hh %-Mm %-Ss',
                            'events' => [
                                'finish' => 'function(){location.reload()}',
                            ],]) ?>
                    </span>
                </div>
        <?php } else if ($model->isTimedTaskExpired($userId) ){ ?>

            <div class="col-xs-12 timer timer-ended">
                <i class="icon-watch vertical-center"></i>
                <span class="vertical-center">
                        <?= Yii::t('app', 'Czas na wykonanie minął') ?>
                        <?= $model->getTimedTaskEndTime($userId); ?>
                    </span>
            </div>

        <?php } ?>

    </div>

    <!-- MAIN -->
    <div class="main col-xs-12">

        <div class="task-head">

            <div class="row">
                <div class="col-xs-12">
                    <div class="pull-left">
                        <div class="status-task-btn <?php if ($end) { ?>status-done<?php }
                        if (($endGame) || ($model->status == Task::STATUS_FINISHED)) { ?>status-not-done<?php } ?>">

                            <?php if ($end) { ?>
                                <i class="icon-check vertical-center"></i>
                                <span class="vertical-center"><?= Yii::t('app', 'WYKONANE') ?></span>
                            <?php } else { ?>

                                <?php if ($endGame) { ?>
                                    <i class="icon-trash vertical-center"></i>
                                    <span class="vertical-center"><?= Yii::t('app', 'NIE WYKONANE') ?></span>
                                <?php } else if (($model->isPremium()) && ($model->status == Task::STATUS_FINISHED)) { ?>
                                    <i class="icon-trash vertical-center"></i>
                                    <span class="vertical-center"><?= Yii::t('app', 'PRZETERMINOWANE') ?></span>
                                <?php } else { ?>
                                    <i class="icon-run vertical-center"></i>
                                    <span class="vertical-center"><?= Yii::t('app', 'DO WYKONANIA') ?></span>
                                <?php } ?>

                            <?php } ?>
                        </div>
                    </div>
                    <div class="pull-right score">
                        <i class="icon-volt"></i>
                        <span class="text-score"><?php echo $model->score; ?></span>
                        <span class="text-pkt"><?= Yii::t('app', 'pkt') ?></span>
                    </div>
                </div>
            </div>

        </div>

        <div class="task-body">

            <div class="title"><?= Yii::t('app', 'Opis zadania') ?></div>
            <?php if ($model->isActivated($userId)) { ?>
                <div class="text"><?php echo $model->description ?></div>

                <?php if ($model->haveImage()) { ?>
                    <div class="title"><?= Yii::t('app', 'Zdjęcie') ?></div>
                    <div class="img">
                        <img src="<?php echo "http://" . Yii::$app->params['backDomain'] . "/" . $model->image ?>"
                             class="img-responsive center-block"/>
                    </div>
                <?php } ?>
            <?php } else {?>
                <div class="text"><?php echo Yii::t('app', 'To jest zadanie z ograniczonym czasem. Na jego rozwiązanie masz: ') . $model->time . Yii::t('app', ' (minut) od wciśnięcia przycisku Start.')?></div>
                <?php echo Html::a('START!', ['/game/' . $model->game_id . '/task/activate/' . $model->id], ['class' => 'btn btn-block btn-success']);?>
            <?php } ?>
        </div>


        <!-- reader button-->

        <?php if (($endGame) || ($model->status == 'incoming') || ($model->status == 'ended')) {
        } else { ?>
            <a id="reader-btn" data-toggle="modal" data-target="#exampleModal">
                <div class="reader-btn">
                    <i class="icon-specialplus vertical-center"></i>
                    <span class="vertical-center"><?= Yii::t('app', ' Sprawdź kod!') ?></span>
                </div>
            </a>
        <?php } ?>

    </div>

</div>
</div>

<?= $this->render('/code/modal/codeReaderModalView', ['gameId' => $model->game_id]); ?>
