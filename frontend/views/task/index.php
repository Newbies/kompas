<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Tasks';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="task">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            'name',
            'description:ntext',
            [
                'attribute' => 'Premium',
                'value' => function($dataProvider){
                    $dataProvider->premium ? "tak" : "nie";
                },
            ],
            [   
                'label' => 'Obrazek',
                'format' => 'html',
                'value' => function($dataProvider){

                return Html::img('http://local.backend-kompas.com/'.$dataProvider->image, ['width' => '30', 'height' => '30']);

                },

            ],

        ],
    ]); ?>
</div>
