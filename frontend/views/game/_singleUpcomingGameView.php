<div class="single-upcoming-game" style="width: 260px;">
     <a href="<?php echo yii\helpers\Url::to(['game/view', 'id' => $model->id]);?> ">
     <?php $gametype = $model->type; ?>
         <div class="col-xs-12 no-padding type-game <?php if ($gametype == 1) { echo 'type-private'; } else { echo 'type-open'; } ?>">
             <span><?php echo $model->getTypeLikeText(); ?></span>
             <i class="pull-right icon-lock"></i>
         </div>
        <div class="description">
            <h2 class="truncate"><?php echo $model->getGameShortName();?></h2>
             <p class="truncate"><?php echo $model->getGameShortDescription(); ?></p>
        </div>

        <div class="timer col-xs-12">
            <i class="icon-watch"></i>
            <span>
                <?php echo \russ666\widgets\Countdown::widget([
                    'datetime' => $model->start_date,
                    'format' => '%-Dd %-Hh %-Mm %-Ss',
                    'events' => [
                        'finish' => 'function(){location.reload()}',
                    ],]) ?>
            </span>
        </div>
     </a>
</div>
