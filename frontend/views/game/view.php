<?php

$enjoyedGame = $model->isUserInGame(Yii::$app->user->id);
?>
<div class="game game-inprogress">

    <?php if (Yii::$app->session->hasFlash('premium')): ?>
        <div class="alert alert-warning alert-premium">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button" style="margin-top:4px">×</button>
            <?= Yii::$app->session->getFlash('premium') ?>
        </div>
    <?php endif; ?>

    <!-- HEAD -->
    <div class="row head-panel">

        <div class="col-xs-12">
            <a class="pull-left back-btn dark-button" href="<?php if ($enjoyedGame == 1) {
                echo yii\helpers\Url::to(['/game/index']);
            } else {
                echo yii\helpers\Url::to(['/game/archive']);
            } ?>">
                <?= Yii::t('app', 'Powrót') ?>
            </a>
            <div class="pull-right status-game-btn">
                <span class="vertical-center"><?= Yii::t('app', 'GRA AKTYWNA ') ?></span>
                <i class="icon-run vertical-center"></i>
            </div>
        </div>
        <div class="col-xs-12 head-top-game">
            <div class="row">
                <div class="col-xs-10 game-name">
                    <?php echo $model->name; ?>
                </div>
                <div class="col-xs-2 more-actions">
                    <?= $this->render('/elements/dropdown/game', [
                        'model' => $model,
                        'organization' => $organization,
                        'team' => $team
                    ]) ?>
                </div>
            </div>
        </div>

        <div class="col-xs-12 more-info">
            <a role="button" data-toggle="collapse" href="#gameInfo" aria-expanded="false" aria-controls="gameInfo"
               class="button-more-info" onclick="displayMap()">
                <?= Yii::t('app', 'Więcej informacji') ?>
                <svg
                        xmlns="http://www.w3.org/2000/svg"
                        width="21px" height="13px">
                    <path fill-rule="evenodd" fill="rgb(255, 255, 255)"
                          d="M19.527,3.895 L11.337,12.490 C10.691,13.168 9.643,13.168 8.997,12.490 L8.704,12.183 C8.704,12.183 8.704,12.183 8.704,12.183 L0.515,3.588 C-0.132,2.910 -0.132,1.811 0.515,1.132 L1.100,0.519 C1.746,-0.160 2.793,-0.160 3.440,0.519 L10.167,7.579 L16.602,0.826 C17.248,0.147 18.296,0.147 18.942,0.826 L19.527,1.439 C20.173,2.118 20.173,3.217 19.527,3.895 Z"/>
                </svg>
            </a>
        </div>

        <div class="col-xs-12">
            <div class="collapse" id="gameInfo">

                <!-- Information about game-->

                <div class="row">
                    <div class="col-xs-12 info-section">
                        <div class="small-text"><?= Yii::t('app', 'Opis gry') ?></div>
                        <div class="big-text"><?php echo $model->description ?></div>
                    </div>
                    <div class="col-xs-12 info-section info-section-date">
                        <div class="row">
                            <div class="col-xs-4">
                                <div class="small-text"><?= Yii::t('app', 'Termin') ?></div>
                            </div>
                            <div class="col-xs-8">
                                <div class="big-text"><?= Yii::t('app', 'od ') ?><?php echo $model->start_date ?></div>
                                <div class="big-text"><?= Yii::t('app', 'do ') ?><?php echo $model->end_date ?></div>
                            </div>
                        </div>
                    </div>
                    <?php if (isset($place->city)): ?>
                        <div class="col-xs-12 info-section info-section-date">
                            <div class="row">
                                <div class="col-xs-4">
                                    <div class="small-text"><?= Yii::t('app', 'Lokalizacja gry') ?></div>
                                </div>
                                <div class="col-xs-8">
                                    <div class="big-text"> <?= $place->postal_code . ", " . $place->city . ", " . $place->voivodeship ?></div>
                                </div>
                                <div class="col-xs-12">
                                    <div id="map"></div>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                </div>

            </div>
        </div>

        <div class="col-xs-12 timer">
            <i class="icon-watch vertical-center"></i>
            <span class="vertical-center">
                <?= Yii::t('app', 'Pozostało ') ?>
                <?php echo \russ666\widgets\Countdown::widget([
                    'datetime' => $model->end_date,
                    'format' => '%-Dd %-Hh %-Mm %-Ss',
                    'events' => [
                        'finish' => 'function(){location.reload()}',
                    ],]) ?>
            </span>
        </div>

    </div>

    <!-- sliders with tasks -->
    <!--Active tasks -->

    <div class="row section-header">
        <div class="col-xs-10 vertical-center">
            <i class="icon-list vertical-center"></i>
            <h1 class="vertical-center">
                <?php echo \Yii::t('app', 'Zadania do wykonania'); ?>
            </h1>
        </div>
        <div class="col-xs-2 vertical-center">
            <span class="counter">
                <?php echo $unfinishedTasks->getTotalCount(); ?>
            </span>
        </div>
    </div>

    <?= yii\widgets\ListView::widget([
        'dataProvider' => $unfinishedTasks,
        'itemView' => '_unFinishedTask',
        'layout' => '{items}',
        'options' => ['tag' => 'div', 'class' => 'task-list-view owl-carousel'],
        'emptyText' => Yii::t('app', 'W obecniej chwili') . " " . Yii::t('app', 'nie masz żadnych') . " " . Yii::t('app', 'aktywnych zadań. '),
        'emptyTextOptions' => ['class' => 'empty-text', 'style' => 'width: calc(100% - 8px);',]
    ]); ?>

    <!-- Done all tasks -->

    <div class="row section-header">
        <div class="col-xs-10 vertical-center">
            <i class="icon-check vertical-center"></i>
            <h1 class="vertical-center">
                <?php echo \Yii::t('app', 'Wykonane zadania '); ?>
            </h1>
        </div>
        <div class="col-xs-2 vertical-center">
            <span class="counter">
                <?php echo $finishedTasks->getTotalCount(); ?>
            </span>
        </div>
    </div>

    <?= yii\widgets\ListView::widget([
        'dataProvider' => $finishedTasks,
        'itemView' => '_finishedTask',
        'layout' => '{items}',
        'options' => ['tag' => 'div', 'class' => 'task-list-view owl-carousel'],
        'emptyText' => Yii::t('app', 'W obecniej chwili') . " " . Yii::t('app', 'nie masz żadnych') . " " . Yii::t('app', 'wykonanych zadań. '),
        'emptyTextOptions' => ['class' => 'empty-text', 'style' => 'width: calc(100% - 8px);',]
    ]); ?>

    <!-- Un done premium tasks -->

    <div class="row section-header">
        <div class="col-xs-10 vertical-center">
            <i class="icon-trash vertical-center"></i>
            <h1 class="vertical-center">
                <?php echo \Yii::t('app', 'Niewykonane zadania'); ?>
            </h1>
        </div>
        <div class="col-xs-2 vertical-center">
            <span class="counter">
                <?php echo $unDonePremiumTasks->getTotalCount(); ?>
            </span>
        </div>
    </div>

    <?= yii\widgets\ListView::widget([
        'dataProvider' => $unDonePremiumTasks,
        'itemView' => '_unDonePremiumTask',
        'layout' => '{items}',
        'options' => ['tag' => 'div', 'class' => 'task-list-view owl-carousel'],
        'emptyText' => Yii::t('app', 'W obecniej chwili') . " " . Yii::t('app', 'nie masz żadnych') . " " . Yii::t('app', 'przeterminowanych zadań. '),
        'emptyTextOptions' => ['class' => 'empty-text', 'style' => 'width: calc(100% - 8px);',]
    ]); ?>


    <!-- summary game-->

    <div class="row section-header summary-header">
        <div class="col-xs-12">
            <h1>
                <?php echo \Yii::t('app', 'Podsumowanie gry'); ?>
            </h1>
        </div>
    </div>

    <div class="col-xs-12 summary">

        <div class="counters">
            <div class="row">
                <div class="col-xs-4 counters-right-panel">
                    <i class="icon-clipboard vertical-center"></i>
                    <div class="tasks-counts vertical-center">
                        <span class="done"><?php echo $model->getCompletedTasksCount(); ?></span>
                        <span class="all">/<?php echo $model->getAllTasksCount(); ?></span>
                    </div>
                </div>
                <div class="col-xs-8 counters-left-panel">
                    <div class="task-progress">
                        <div class="fill" style="width:<?= $model->getPointsCompletionPercent() * 100; ?>%"></div>
                        <span><?= $model->getCurrentPoints(); ?>/<?= $model->getAllPoints(); ?> pkt</span>
                    </div>
                </div>
            </div>
        </div>

        <?php if ($showTeam): ?>
            <div class="team-section">
                <div class="row">
                    <div class="col-xs-5 team-text">
                        <?php echo \Yii::t('app', 'Twoja drużyna'); ?>
                    </div>
                    <div class="col-xs-7 team-name">
                        <a href="<?php echo yii\helpers\Url::to(['game/' . $model->id . '/team/view/' . $team->id]); ?>"><?= $team->name ?></a>
                    </div>
                </div>
            </div>
        <?php endif; ?>

        <div class="ranking-section">
            <div class="row">
                <div class="col-xs-12">
                    <div class="pull-left ranking-place">

                        <?php switch ($userPosition) {
                            case 1:
                                echo '<i class="icon-award" style="color: #e6c218;"></i>';
                                break;
                            case 2:
                                echo '<i class="icon-award" style="color: #969b9f;"></i>';
                                break;
                            case 3:
                                echo '<i class="icon-award" style="color: #c77525;"></i>';
                                break;
                            default:
                                echo '<i class="icon-award" style="color: #d9d9d9;"></i>';
                                break;
                        } ?>

                        <span><?= $userPosition ?> Miejsce</span>
                    </div>
                    <div class="pull-right ranking-button">
                        <a class="dark-button"
                           href="<?php echo yii\helpers\Url::to(['game/' . $model->id . '/board']); ?> "><?= Yii::t('app', 'Pokaż Ranking') ?></a>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <div class="organizer">
        <div class="row">
            <?php if (isset($organization)): ?>
                <div class="<?= $organization->hasLogo() ? 'col-xs-8' : 'col-xs-12' ?> organizer-content">
                    <div class="organizer-title"><?= Yii::t('app', ' Organizator gry') ?></div>
                    <div class="organizer-name"><?= $organization->name; ?></div>
                    <div class="organizer-description"><?= $organization->description; ?></div>
                    <a href="mailto:<?= $organization->contact_email; ?>"
                       class="dark-button"><?= Yii::t('app', ' Kontakt') ?></a>
                </div>
                <?php if ($organization->hasLogo()) : ?>
                    <div class="col-xs-4 text-right organizer-img">
                        <div class="organizer-img-circle"
                             style="background-image: url('<?= $organization->getLogoSrc(); ?>');"></div>
                    </div>
                <?php endif; ?>
            <?php endif; ?>
        </div>
    </div>

    <!-- reader button -->
    <a id="reader-btn" data-toggle="modal" data-target="#exampleModal">
        <div class="reader-btn">
            <i class="icon-specialplus vertical-center"></i>
            <span class="vertical-center"><?= Yii::t('app', ' Sprawdź kod!') ?></span>
        </div>
    </a>

</div>

<?php if (isset($place->city)): ?>

    <?php
    $lat = $place->lat;
    $lng = $place->lng;
    ?>
    <?= $this->render('/elements/map/init_game', ['place' => $place]); ?>
<?php endif; ?>

<?= $this->render('/code/modal/codeReaderModalView', ['gameId' => $model->id]); ?>
