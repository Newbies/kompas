<div class="finish-game <?php use common\models\Task;

if ($model->isPremium()) { ?>premium-game<?php } ?>" style="width: 260px;">
    <a href="<?php echo yii\helpers\Url::to(['game/' . $model->getGame()->id . '/task/' . $model->id]); ?> ">

        <!-- NAME-->
        <div class="col-xs-12 no-padding">
            <h2 class="truncate"><?php echo $model->getTaskShortName(20); ?></h2>
        </div>
        <!-- /NAME-->

        <!-- IMAGE & DISCTIPTION-->
        <div class="col-xs-4 no-padding photo">
            <div>
                <i class="icon-check"></i>
            </div>
        </div>
        <div class="col-xs-8 no-padding discription-image">
            <?php echo $model->getTaskShortDescription(60); ?>
        </div>
        <!-- /IMAGE-->

        <!-- SCORE-->
        <div class="col-xs-12 no-padding score">

            <?php if ($model->isPremium()) { ?>
                <div class="row">
                    <div class="col-xs-8">
                        <i class="icon-volt"></i>
                        <span class="text-score"><?= $model->score; ?></span>
                        <span class="text-pkt"> <?= Yii::t('app', ' pkt') ?> </span>
                    </div>
                    <div class="col-xs-4 premium-star">
                        <span>
                            <i class="icon-star"></i>
                        </span>
                    </div>
                </div>
            <?php } else { ?>
                <i class="icon-volt"></i>
                <span class="text-score"><?= $model->score; ?></span>
                <span class="text-pkt"> <?= Yii::t('app', ' pkt') ?> </span>
            <?php } ?>

        </div>
        <!-- /SCORE-->

    </a>
</div>
