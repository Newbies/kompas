<?php
    $userId = \Yii::$app->user->getId();
?>
<div class="un-done-game <?php if ($model->isPremium()) { ?>premium-game<?php } ?>" style="width: 260px;">

    <a href="<?php echo yii\helpers\Url::to(['game/'.$model->getGame()->id.'/task/'.$model->id]); ?> ">

        <!-- NAME-->
        <div class="col-xs-12 no-padding">
           <h2 class="truncate"><?php echo $model->getTaskShortName(20); ?></h2>
        </div>
        <!-- /NAME-->

        <!-- IMAGE & DISCTIPTION-->
        <?php if ($model->haveImage()) { ?>
            <div class="col-xs-4 no-padding photo">
                <div style="background-image: url('<?php echo "http://" . Yii::$app->params['backDomain'] . "/" . $model->image; ?>');"></div>
            </div>
            <div class="col-xs-8 no-padding discription-image">
                <?php echo $model->getTaskShortDescription(60); ?>
            </div>
        <?php } else { ?>
            <div class="col-xs-12 no-padding discription-noimage">
                <?php echo $model->getTaskShortDescription(130); ?>
            </div>
        <?php } ?>
        <!-- /IMAGE-->

        <!-- SCORE-->
        <div class="col-xs-12 no-padding score">

            <?php if ($model->isPremium()) { ?>
                <div class="row">
                    <div class="col-xs-8">
                        <i class="icon-volt"></i>
                        <span class="text-score"><?php echo $model->score; ?></span>
                        <span class="text-pkt"> <?= Yii::t('app', ' pkt') ?> </span>
                    </div><!--
                    --><div class="col-xs-4 premium-star">
                        <span>
                            <i class="icon-star"></i>
                        </span>
                    </div>
                </div>
            <?php } else { ?>
                <i class="icon-volt"></i>
                <span class="text-score"><?php echo $model->score; ?></span>
                <span class="text-pkt"> <?= Yii::t('app', ' pkt') ?> </span>
            <?php } ?>

        </div>
        <!-- /SCORE-->

        <!-- COUNTDOWN -->
        <div class="col-xs-12 no-padding countdown-task countdown-ended">
            <i class="icon-watch"></i>
            <div class="countdown-text">
                <span><?= Yii::t('app', 'Czas na wykonanie minął') ?></span>
                <div>
                    <?php if ($model->isTimed()) { ?>
                    <?= $model->getTimedTaskEndTime($userId) ?>
                    <?php } else { ?>
                    <?= $model->end_time; ?>
                    <?php } ?>
                </div>
            </div>
        </div>
        <!-- /COUNTDOWN -->
    </a>
</div>
