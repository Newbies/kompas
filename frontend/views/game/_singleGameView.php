<div class="single-game" style="width: 260px;">
    <a href="<?= yii\helpers\Url::to(['game/view', 'id' => $model->id]); ?> ">
        <div class="col-xs-12 no-padding type-game <?= $model->isPrivate() ? 'type-private' : 'type-open' ?>">
            <span><?= $model->getTypeLikeText(); ?></span>
            <i class="pull-right icon-lock"></i>
        </div>
        <div class="col-xs-12 no-padding">
            <div class="no-padding"><h2 class="truncate"><?= $model->getGameShortName(); ?></h2></div>
            <p class="truncate"><?= $model->getGameShortDescription(); ?></p>
        </div>
        <div class="counters col-xs-12">
            <div class="row">
                <div class="col-xs-6 counters-right-panel">
                    <i class="icon-clipboard vertical-center"></i>
                    <div class="tasks-counts vertical-center">
                        <span class="done"><?= $model->getCompletedTasksCount(); ?></span>
                        <span class="all">/<?= $model->getAllTasksCount(); ?></span>
                    </div>
                </div>
                <div class="col-xs-6 counters-left-panel">
                    <div class="task-progress">
                        <div class="fill" style="width:<?= $model->getPointsCompletionPercent() * 100; ?>%"></div>
                        <span><?= $model->getCurrentPoints(); ?>/<?= $model->getAllPoints(); ?> pkt</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="timer col-xs-12">
            <i class="icon-watch"></i>
            <span>
                <?= \russ666\widgets\Countdown::widget([
                    'datetime' => $model->end_date,
                    'format' => '%-Dd %-Hh %-Mm %-Ss',
                    'events' => [
                        'finish' => 'function(){location.reload()}',
                    ],]) ?>
            </span>
        </div>
    </a>
</div>
