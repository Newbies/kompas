<div class="un-finish-game


<?php use common\models\fields\GameFields;
use common\models\Game;
use common\models\Task;

$userId = \Yii::$app->user->getId();

if ($model->isPremium()) { ?>premium-game<?php } ?>" style="width: 260px;">

    <a href="<?php echo yii\helpers\Url::to(['game/'.$model->getGame()->id.'/task/'.$model->id]); ?> ">

        <!-- NAME-->
        <div class="col-xs-12 no-padding">
           <h2 class="truncate"><?php echo $model->getTaskShortName(20); ?></h2>
        </div>
        <!-- /NAME-->

        <!-- IMAGE & DESCRIPTION-->
        <?php if ($model->haveImage()) { ?>
            <?php if ($model->isActivated($userId)) { ?>
            <div class="col-xs-4 no-padding photo">
                <div style="background-image: url('<?php echo "http://" . Yii::$app->params['backDomain'] . "/" . $model->image; ?>');"></div>
            </div>
            <div class="col-xs-8 no-padding discription-image">
                <?php echo $model->getTaskShortDescription(60); ?>
            </div>
                <?php } else { ?>
                <div class="col-xs-12 no-padding discription-noimage">
                    <?php echo Yii::t('app', 'To jest zadanie z ograniczonym czasem. Sprawdź szczegóły aby dowiedzieć się więcej'); ?>
                </div>
            <?php } ?>
        <?php } else { ?>
            <div class="col-xs-12 no-padding discription-noimage">
                <?php echo $model->isActivated($userId) ? $model->getTaskShortDescription(130) : Yii::t('app', 'To jest zadanie z ograniczonym czasem. Sprawdź szczegóły aby dowiedzieć się więcej'); ?>
            </div>
        <?php } ?>
        <!-- /IMAGE-->

        <!-- SCORE-->
        <div class="col-xs-12 no-padding score">

            <?php if ($model->isPremium()) { ?>
                <div class="row">
                    <div class="col-xs-8">
                        <i class="icon-volt"></i>
                        <span class="text-score"><?php echo $model->score; ?></span>
                        <span class="text-pkt"> <?= Yii::t('app', ' pkt') ?> </span>
                    </div><!--
                    --><div class="col-xs-4 premium-star">
                        <span>
                            <i class="icon-star"></i>
                        </span>
                    </div>
                </div>
            <?php } else { ?>
                <i class="icon-volt"></i>
                <span class="text-score"><?php echo $model->score; ?></span>
                <span class="text-pkt"> <?= Yii::t('app', ' pkt') ?> </span>
            <?php } ?>

        </div>
        <!-- /SCORE-->
        
        <!-- COUNTDOWN -->

        <?php if ((Game::findOne([GameFields::ID => $model->game_id])->status != Game::STATUS_FINISHED) and ($model->isPremium()) and ($model->status == Task::STATUS_CURRENT)) { ?>

            <div class="col-xs-12 no-padding countdown-task countdown-current">
                <i class="icon-watch"></i>
                <div class="countdown-text">
                    <span><?= Yii::t('app', 'Do zakończenia pozostało ') ?></span>
                    <div>
                        <?php echo \russ666\widgets\Countdown::widget([
                            'datetime' => $model->end_time,
                            'format' => '%-Dd %-Hh %-Mm %-Ss',
                            'events' => [
                                'finish' => 'function(){location.reload()}',
                            ],]) ?>
                    </div>
                </div>
            </div>

        <?php } ?>

        <!-- /COUNTDOWN -->

        <!-- BEFORE ACTIVATION TIME INFO -->
        <?php if ($model->isTimed() && !$model->isActivated($userId)) { ?>
        <div class="col-xs-12 no-padding countdown-task countdown-current">
            <i class="icon-watch"></i>
            <div class="countdown-text">
                <span>
                    <?= Yii::t('app', 'Czas na wykonanie zadania ') . $model->time . Yii::t('app', ' Minut ')?>
                </span>
            </div>
        </div>
        <?php } ?>
        <!-- / BEFORE ACTIVATION TIME INFO -->

        <!-- BEFORE ACTIVATION TIME INFO -->
        <?php if ($model->isTimed() && $model->isActivated($userId)) { ?>
            <div class="col-xs-12 no-padding countdown-task countdown-current">
                <i class="icon-watch"></i>
                <div class="countdown-text">
                    <span><?= Yii::t('app', 'Do zakończenia pozostało ') ?></span>
                    <div>
                        <?php echo \russ666\widgets\Countdown::widget([
                            'datetime' => $model->getTimedTaskEndTime($userId),
                            'format' => '%-Dd %-Hh %-Mm %-Ss',
                            'events' => [
                                'finish' => 'function(){location.reload()}',
                            ],]) ?>
                    </div>
                </div>
            </div>
        <?php } ?>
        <!-- / BEFORE ACTIVATION TIME INFO -->

    </a>
</div>
