<?php

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Gry');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dashboard">

    <div class="search-open">
        <a class="search-btn" href="<?php echo yii\helpers\Url::to(['/lobby']); ?> ">
            <i class="icon-magnify vertical-center"></i>
            <span class="vertical-center"><?= Yii::t('app', 'Wyszukaj grę') ?></span>
        </a>
    </div>

    <div class="row section-header">
        <div class="col-xs-10 vertical-center">
            <i class="icon-run vertical-center"></i>
            <h1 class="vertical-center">
                <?php echo \Yii::t('app', 'Bieżące gry'); ?>
            </h1>
        </div>
        <div class="col-xs-2 vertical-center">
            <span class="counter">
                <?php echo $runningGames->getTotalCount(); ?>
            </span>
        </div>
    </div>

    <?= yii\widgets\ListView::widget([
        'dataProvider' => $runningGames,
        'itemView' => '_singleGameView',
        'layout' => '{items}',
        'options' => ['tag' => 'div', 'class' => 'game-list-view owl-carousel'],
        'emptyText' => Yii::t('app', 'W obecniej chwili') . " " . Yii::t('app', 'nie masz żadnych') . " " . Yii::t('app', 'aktywnych gier. '),
        'emptyTextOptions' => ['class' => 'empty-text', 'style' => 'width: calc(100% - 8px);',]
    ]); ?>

    <div class="row section-header">
        <div class="col-xs-10 vertical-center">
            <i class="icon-arrow vertical-center"></i>
            <h1 class="vertical-center">
                <?php echo \Yii::t('app', 'Nadchodzące gry'); ?>
            </h1>
        </div>
        <div class="col-xs-2 vertical-center">
            <span class="counter">
                <?php echo $upcomingGames->getTotalCount(); ?>
            </span>
        </div>
    </div>

    <?= yii\widgets\ListView::widget([
        'dataProvider' => $upcomingGames,
        'itemView' => '_singleUpcomingGameView',
        'layout' => '{items}',
        'options' => ['tag' => 'div', 'class' => 'upcoming-game-list-view owl-carousel'],
        'emptyText' => Yii::t('app', 'W obecniej chwili') . " " . Yii::t('app', 'nie masz żadnych') . " " . Yii::t('app', 'nadchodzących gier. '),
        'emptyTextOptions' => ['class' => 'empty-text', 'style' => 'width: calc(100% - 8px);',]
    ]); ?>

</div>
