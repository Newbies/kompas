jQuery(document).on('click change', function() {

    var header = $('.header-main');
    var game = $('.game');
    function checkModal(){
        if($('#exampleModal').css('display') == 'none'){
            $('.header-main').removeClass('modal-blur');
            $('.game').removeClass('modal-blur');
            $('.task').removeClass('modal-blur');
        } else if ($('#exampleModal').css('display') == 'block') {
            $('.header-main').addClass('modal-blur');
            $('.game').addClass('modal-blur');
            $('.task').addClass('modal-blur');
        }
    }
    setTimeout(checkModal, 250);

});
