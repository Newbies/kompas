/**
 * Created by mBoiler on 26.05.2017.
 * Updated by Kamil Zubrzycki on 21.10.2017.
 */

jQuery(document).ready(function($) {

    /* show/hide menu */

    var menu = $('#menu-panel');
    $('#menu-btn-show').click(function() {
        $(menu).addClass('active');
        $('.head').addClass('active-menu');
        $('body').css('overflow', 'hidden');
    });
    $('#menu-btn-hide').click(function() {
        $(menu).removeClass('active');
        $('.head').removeClass('active-menu');
        $('body').css('overflow', 'auto');
    });

    /* active menu element */

    var current = location.pathname;
    $('.menu-panel-links ul li a').each(function(){
        var $this = $(this);
        // if the current path is like this link, make it active
        if($this.attr('href').indexOf(current) !== -1){
            $this.addClass('active');
        }
    })

});
