$(document).ready(function() {

    /* owl carousel (for game and tasks) custom settings */
    $(".owl-carousel").owlCarousel({
        items: 1.25,
        autoWidth: true,
        rewind: false,
        loop: false,
        margin: 8,
        autoHeight: false
    });

    /* owl carousel (for regular text/images) */
    $(".owl-carousel-regular").owlCarousel({
        items: 1,
        nav: false,
        autoplay: true,
        autoplayTimeout: 2500,
        smartSpeed: 750,
        loop: true
    });

    /* operations on search input in lobby.php */
    $('#lobbyInput').on('change keyup', function() {
        inputval = $(this).val();
        if(inputval == '') {
            $(this).parent().addClass('empty');
            $(this).parent().removeClass('not-empty');
        } else {
            $(this).parent().addClass('not-empty');
            $(this).parent().removeClass('empty');
        }
    });

    $('.search-clear-btn').on('click', function() {
        $('#lobbyInput').val("");
        $('#lobbyInput').parent().removeClass('not-empty');
    });

});
