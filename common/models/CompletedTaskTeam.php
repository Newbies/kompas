<?php

namespace common\models;

use common\models\fields\CompletedTaskTeamFields;

/**
 * This is the model class for table "completed_task_team".
 *
 * @property integer $id
 * @property integer $team_id
 * @property integer $task_id
 * @property integer $time
 * @property integer $activator_id
 */
class CompletedTaskTeam extends \yii\db\ActiveRecord
{

    public static function add($team_id, $task_id, $activator_id)
    {
        $model = new self();
        $model->task_id = $task_id;
        $model->team_id = $team_id;
        $model->time = time();
        $model->activator_id = $activator_id;
        $model->save();
    }

    public static function findByTeamIdAndTaskId($team_id, $task_id)
    {
        return self::findAll([CompletedTaskTeamFields::TASK_ID => $task_id, CompletedTaskTeamFields::TEAM_ID => $team_id]);
    }

    public static function findByTaskIdSortedByTime($task_id)
    {
        return self::find()->where([CompletedTaskTeamFields::TASK_ID => $task_id])
            ->orderBy([CompletedTaskTeamFields::TIME => SORT_DESC])
            ->all();
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['team_id', 'task_id', 'time', 'activator_id'], 'integer'],
        ];
    }
}
