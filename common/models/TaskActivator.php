<?php

namespace common\models;

use common\models\fields\TaskActivatorFields;
use Yii;

/**
 * This is the model class for table "task_activator".
 *
 * @property int $id
 * @property int $activator_id
 * @property int $activator_team_id
 * @property int $task_id
 * @property int $game_id
 * @property int $start_time
 * @property int $end_time
 */
class TaskActivator extends \yii\db\ActiveRecord
{
    const ACTIVATOR_TEAM_ID_NOT_SET = -1;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'task_activator';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['activator_id', 'activator_team_id', 'task_id', 'start_time', 'end_time', 'game_id'], 'integer'],
            [['activator_id', 'activator_team_id', 'task_id', 'start_time', 'end_time', 'game_id'], 'required']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'activator_id' => Yii::t('app', 'Activator ID'),
            'activator_team_id' => Yii::t('app', 'Activator Team ID'),
            'task_id' => Yii::t('app', 'Task ID'),
            'start_time' => Yii::t('app', 'Start Time'),
            'end_time' => Yii::t('app', 'End Time'),
            'game_id' => Yii::t('app', 'Game ID'),
        ];
    }

    public static function findByActivatorDetails($activatorId, $taskId)
    {
        $task = Task::findById($taskId);
        $userGame = UserGame::findByUserIdAndGameId($activatorId, $task->game_id);

        if($userGame->hasTeam())
        {
            if($activatorUser = self::findByActivatorTeamIdAndTaskId($userGame->team_id, $task->id)) {
                return $activatorUser;
            }
        }

        if($activatorTeam = self::findByActivatorIdAndTaskId($activatorId, $taskId)) {
            return $activatorTeam;
        }
        return null;
    }

    public static function findByActivatorIdAndTaskId($activatorId, $taskId)
    {
        return self::findOne([TaskActivatorFields::ACTIVATOR_ID => $activatorId, TaskActivatorFields::TASK_ID=>$taskId]);
    }

    public static function findByActivatorTeamIdAndTaskId($activatorTeamId, $taskId)
    {
        return self::findOne([TaskActivatorFields::ACTIVATOR_TEAM_ID => $activatorTeamId, TaskActivatorFields::TASK_ID=>$taskId]);
    }

    public static function checkActivation($activatorId, $taskId)
    {
        $task = Task::findById($taskId);
        $userGame = UserGame::findByUserIdAndGameId($activatorId, $task->game_id);

        if($userGame->hasTeam())
        {
            if(self::findByActivatorTeamIdAndTaskId($userGame->team_id, $task->id)) {
                return true;
            }
        }

        if(self::findByActivatorIdAndTaskId($activatorId, $taskId)) {
            return true;
        }
        return false;
    }

    public static function findByActivatorAndGameId($activatorId, $gameId)
    {
        $userGame = UserGame::findByUserIdAndGameId($activatorId, $gameId);

        if($userGame->hasTeam()){
            return self::findAll([TaskActivatorFields::ACTIVATOR_TEAM_ID => $userGame->team_id, TaskActivatorFields::GAME_ID => $gameId]);
        }

        return self::findAll([TaskActivatorFields::ACTIVATOR_ID => $activatorId, TaskActivatorFields::GAME_ID => $gameId]);
    }

    public static function getExpiredTimedTaskArray($activatorId, $gameId)
    {
        $expiredTaskActivators = array();
        if($taskActivators =  self::findByActivatorAndGameId($activatorId, $gameId)){
            foreach ($taskActivators as $taskActivator)
            {
                if($taskActivator->isExpired()){
                    array_push($expiredTaskActivators, $taskActivator);
                }
            }
        }
        return $expiredTaskActivators;
    }

    public function afterFind()
    {
        $this->start_time = date("Y-m-d H:i:s", $this->start_time);
        $this->end_time = date("Y-m-d H:i:s", $this->end_time);
        parent::afterFind();
    }

    public function isExpired()
    {
        $currentTime = date("Y-m-d H:i:s");

        if($currentTime >= $this->end_time)
        {
            return true;
        }
        return false;
    }
}