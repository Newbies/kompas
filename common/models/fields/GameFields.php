<?php

namespace common\models\fields;

class GameFields
{
    const  ID = 'id';
    const  OWNER_ORGANIZATION_ID = 'owner_organization_id';
    const  NAME = 'name';
    const  DESCRIPTION = 'description';
    const  TYPE = 'type';
    const  START_DATE = 'start_date';
    const  END_DATE = 'end_date';
    const  STATUS = 'status';
    const  RANKING = 'ranking';
}