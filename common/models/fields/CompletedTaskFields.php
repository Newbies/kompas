<?php

namespace common\models\fields;

class CompletedTaskFields
{
    const  ID = 'id';
    const  USER_ID = 'user_id';
    const  TASK_ID = 'task_id';
    const  TIME = 'time';
}