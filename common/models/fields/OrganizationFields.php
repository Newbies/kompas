<?php

namespace common\models\fields;

class OrganizationFields
{
    const  ID = 'id';
    const  NAME = 'name';
    const  CONTACT_EMAIL = 'contact_email';
    const  ENTRY_KEY = 'entry_key';

}