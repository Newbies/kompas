<?php

namespace common\models\fields;

class TeamFields
{
    const  ID = 'id';
    const  NAME = 'name';
    const  GAME_ID = 'game_id';
    const  SCORE = 'score';
    const  MEMBERS = 'members';
    const  POINTS = 'points';
    const  LAST_UPDATE_SCORE = 'last_update_score';
}