<?php

namespace common\models\fields;

class TaskFields
{
    const  ID = 'id';
    const  GAME_ID = 'game_id';
    const  NAME = 'name';
    const  DESCRIPTION = 'description';
    const  CODE = 'code';
    const  SCORE = 'score';
    const  IMAGE = 'image';
    const  IMAGE_OWNER = 'image';
    const  TASK_OWNER = 'task_owner';
    const  TYPE = 'type';
    const  MAILED = 'mailed';
    const  START_TIME = 'start_time';
    const  END_TIME = 'end_time';
    const  STATUS = 'status';
    const  TIME = 'time';
    const  SHOW_NUMBER = 'show_number';
}