<?php

namespace common\models\fields;

class UserGameFields
{
    const  ID = 'id';
    const  USER_ID = 'user_id';
    const  GAME_ID = 'game_id';
    const  TEAM_ID = 'team_id';
    const  SCORE = 'score';
    const  LAST_UPDATE_SCORE = 'last_update_score';
    const  TAG = 'tag';
    const  INVITE_STATUS = 'invite_status';
    const  INVITE_STATUS_ADD = 0;
    const  INVITE_STATUS_INVITED = 1;
    const  INVITE_STATUS_ACTIVATED = 2;
}