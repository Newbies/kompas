<?php
/**
 * Created by PhpStorm.
 * User: tycjan
 * Date: 2017-11-07
 * Time: 21:59
 */

namespace common\models\fields;


class PlaceFields
{
    const CITY = 'city';
    const GAME_ID = 'game_id';
    const ID = 'id';
    const LAT = 'lat';
    const LNG = 'lng';
    const VOIVODESHIP = 'voivodeship';
    const POSTAL_CODE = 'postal_code';
}