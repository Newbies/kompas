<?php

namespace common\models\fields;

class ReaderUseTimeFields
{
    const  ID = 'id';
    const  USER_ID = 'user_id';
    const  TIMESTAMP= 'timestamp';
}