<?php
/**
 * Created by PhpStorm.
 * User: mBoiler
 * Date: 15.10.2017
 * Time: 14:39
 */

namespace common\models\fields;


class UserOrgFields
{
    const ID = "id";
    const USER_ID = "user_id";
    const ORG_ID = "org_id";
}