<?php

namespace common\models\fields;

class UserFields
{
    const  ID = 'id';
    const  ROLE = 'role';
    const  EMAIL = 'email';
    const  PASSWORD = 'password';
    const  NAME = 'name';
    const  LAST_NAME = 'last_name';
    const  AUTH_KEY = 'auth_key';
    const  PASSWORD_HASH = 'password_hash';
    const  PASSWORD_RESET_TOKEN = 'password_reset_token';
    const  CREATED_AT = 'created_at';
    const  UPDATED_AT = 'updated_at';
    const  STATUS = 'status';
    const  TEAM_ID = 'team_id';
    const  REGISTRATION_CODE = 'registration_code';
    const  ORGANIZATION_ID = 'organization_id';
}