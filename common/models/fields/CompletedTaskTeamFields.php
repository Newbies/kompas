<?php
/**
 * Created by PhpStorm.
 * User: mBoiler
 * Date: 20.11.2017
 * Time: 19:41
 */

namespace common\models\fields;


class CompletedTaskTeamFields
{
    const ID = "id";
    const TEAM_ID = "team_id";
    const TASK_ID = "task_id";
    const TIME = "time";
    const ACTIVATOR_ID = "activator_id";
}