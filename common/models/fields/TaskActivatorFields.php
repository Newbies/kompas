<?php

namespace common\models\fields;


class TaskActivatorFields
{
    const  ID = 'id';
    const  ACTIVATOR_ID = 'activator_id';
    const  ACTIVATOR_TEAM_ID = 'activator_team_id';
    const  TASK_ID = 'task_id';
    const  START_TIME  = 'start_time';
    const  END_TIME  = 'end_time';
    const  GAME_ID  = 'game_id';
}