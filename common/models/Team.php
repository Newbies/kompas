<?php

namespace common\models;

use common\models\fields\TeamFields;
use Yii;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "team".
 *
 * @property integer $id
 * @property string $name
 * @property integer $game_id
 * @property User[] $users
 * @property integer score
 * @property integer $last_update_score
 */
class Team extends \yii\db\ActiveRecord
{

    public $addNext;

    public static function findByIdsAndGameIds($ids, $game_id)
    {
        return self::findOne(['in', TeamFields::ID, $ids, TeamFields::GAME_ID => $game_id]);
    }

    public static function findById($id)
    {
        return self::findOne([TeamFields::ID => $id]);
    }

    public static function findByGameIdAndTeamId($gameId, $teamId)
    {
        return self::findOne([TeamFields::GAME_ID => $gameId, TeamFields::ID => $teamId]);
    }

    public static function getTeamMembersCount($gameId, $teamId)
    {
        return UserGame::getMembersCountByGameIdAndTeamId($gameId, $teamId);
    }

    public static function getTeamScore($gameId, $teamId)
    {
        return UserGame::getTeamScore($gameId, $teamId);
    }

    public static function getPlaceByTeamAndGameId($gameId, $teamId)
    {
        $sortedTeam = Team::find()
            ->where([TeamFields::GAME_ID => $gameId])
            ->orderBy([TeamFields::SCORE => SORT_DESC,
                TeamFields::LAST_UPDATE_SCORE => SORT_ASC,
                TeamFields::ID => SORT_DESC])
            ->all();

        $position = 1;

        foreach ($sortedTeam as $team) {
            if ($team->id == $teamId) {
                return $position;
            }
            $position++;
        }

        return 0;
    }

    public static function findByGameId($gameId)
    {
        return self::findAll([TeamFields::GAME_ID => $gameId]);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [

            [['score'], 'integer'],

            [['name'], 'required'],
            [['name'], 'string', 'max' => 255,],

            [['addNext'], 'integer'],

            [['game_id'], 'integer'],

            [['last_update_score'], 'integer'],

            [['name'], 'safe', 'on' => 'search'],

            [['name', 'game_id', 'score'],
                'filter', 'filter' => '\yii\helpers\HtmlPurifier::process']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Nazwa'),
            'score' => Yii::t('app', 'Punkty'),
            'game_id' => Yii::t('app', 'Game ID'),
            'addNext' => Yii::t('app', ''),
            'last_update_score' => Yii::t('app', 'Data ostatniej aktualizacji punktów'),
        ];
    }

    public function search($gameId, $params)
    {
        $this->load($params);
        $query = self::find()->where([TeamFields::GAME_ID => $gameId]);

        $query->andFilterWhere(['like', TeamFields::NAME, $this->name]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => [TeamFields::ID => SORT_ASC]]
        ]);

        return $dataProvider;
    }

    public function getTeamShortName($length = 16, $prefix = '...')
    {
        return $this->getShorterText($this->name, $length, $prefix);
    }

    public function getShorterText($text, $length, $prefix)
    {
        if (strlen($text) > $length) {
            $text = mb_substr($text, 0, $length) . $prefix;
        }
        return \yii\helpers\Html::encode($text);
    }

    public function delete()
    {
        UserGame::clearTeam($this->id);
        return parent::delete();
    }

}
