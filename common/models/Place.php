<?php

namespace common\models;

use common\models\fields\PlaceFields;
use Yii;
use yii\base\ErrorException;

/**
 * This is the model class for table "location".
 *
 * @property integer $id
 * @property string $city
 * @property string $voivodeship
 * @property string $postal_code
 * @property double $lat
 * @property double $lng
 */
class Place extends \yii\db\ActiveRecord
{
    const DEFAULT_LNG = 18.9;
    const DEFAULT_LAT = 52.0;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['city'], 'required', 'message' => 'Wybierz miasto z listy'],
            [['city'], 'string', 'max' => 256],
            [['city'], 'validatePlace'],

            [['voivodeship'], 'required'],
            [['voivodeship'], 'string', 'max' => 256],

            [['postal_code'], 'required'],
            [['postal_code'], 'string', 'max' => 256],

            [['lat'], 'required'],
            [['lat'], 'number'],

            [['lng'], 'required'],
            [['lng'], 'number'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'city' => Yii::t('app', 'Miasto'),
            'voivodeship' => Yii::t('app', 'Województwo'),
            'postal_code' => Yii::t('app', 'Kod Pocztowy'),
            'lat' => Yii::t('app', 'Szerokość geograficzna'),
            'lng' => Yii::t('app', 'Długość geograficzna'),
        ];
    }

    public function validatePlace(){
        if($this->city != null && ($this->lat == null || $this->lng == null || $this->voivodeship == null || $this->postal_code == null)){
            $this->addError('city', Yii::t('app', 'Wybierz miasto z listy'));
        }
    }

    public static function findById($id)
    {
        return self::findOne([PlaceFields::ID => $id]);
    }

    public static function findByIdToUpdate($id){
        $place = self::findById($id);
        if(isset($place->id)){
            return $place;
        } else {
            return new self;
        }
    }

    public static function findByCity($city)
    {
        return self::findOne([PlaceFields::CITY => $city]);
    }

    public static function getLng($id){
        try{
            $lng = self::findOne($id)->lng;
        } catch (ErrorException $e){
            $lng = self::DEFAULT_LNG;
        }
        return $lng;
    }

    public static function getLat($id){
        try{
            $lat = self::findOne($id)->lat;
        } catch (ErrorException $e){
            $lat = self::DEFAULT_LAT;
        }
        return $lat;
    }
}
