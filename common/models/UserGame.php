<?php

namespace common\models;

use backend\helpers\ArrayHelper;
use common\models\fields\GameFields;
use common\models\fields\TeamFields;
use common\models\fields\UserFields;
use common\models\fields\UserGameFields;
use Yii;

/**
 * This is the model class for table "user_game".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $game_id
 * @property integer $team_id
 * @property string $tag
 * @property integer $score
 * @property integer $invite_status
 * @property integer $last_update_score
 *
 */
class UserGame extends \yii\db\ActiveRecord
{
    const INVITE_STATUS_ADD = 0;
    const INVITE_STATUS_INVITED = 1;
    const INVITE_STATUS_ACTIVATED = 2;

    public static function findByGameId($game_id)
    {
        $userGame = self::findAll([UserGameFields::GAME_ID => $game_id]);
        if ($userGame) {
            return $userGame;
        }
        return false;
    }

    public static function deleteByUserIdAndGameId($userId, $gameId)
    {
        self::deleteAll([UserGameFields::USER_ID => $userId, UserGameFields::GAME_ID => $gameId]);
    }

    public static function findByGameIdWithTeam($game_id)
    {
        $userGame = self::find()->where([UserGameFields::GAME_ID => $game_id])->andWhere(['>', UserGameFields::TEAM_ID, 0])->all();
        if ($userGame) {
            return $userGame;
        }
        return false;
    }

    public static function getByUserIdAndGame($user_id, $game_id)
    {
        $userGame = self::findOne([UserGameFields::GAME_ID => $game_id, UserGameFields::USER_ID => $user_id]);
        if ($userGame) {
            return $userGame;
        }
        return false;
    }

    public static function getMembersCountByGameIdAndTeamId($gameId, $teamId): int
    {
        if ($membersArray = ArrayHelper::map(self::findAllByGameIdAndTeamId($gameId, $teamId), UserGameFields::ID)) {
            return count($membersArray);
        }
        return 0;
    }

    public static function findAllByGameIdAndTeamId($gameId, $teamId)
    {
        return self::findAll([UserGameFields::GAME_ID => $gameId, UserGameFields::TEAM_ID => $teamId]);
    }

    public static function getTeamScore($gameId, $teamId): int
    {
        $teamScore = self::findOneByGameIdAndTeamId($gameId, $teamId);
        if (isset($teamScore->score)) {
            return $teamScore->score;
        }
        return 0;
    }

    public static function findOneByGameIdAndTeamId($gameId, $teamId)
    {
        return self::findOne([UserGameFields::GAME_ID => $gameId, UserGameFields::TEAM_ID => $teamId]);
    }

    public static function clearTeam($teamId)
    {
        foreach (self::findByTeamId($teamId) as $userGame) {
            $userGame->removeTeam();
        }
    }

    public static function findByTeamId($teamId): array
    {
        return self::findAll([UserGameFields::TEAM_ID => $teamId]);
    }

    public static function findByUserId($userId)
    {
        return self::findAll([UserGameFields::USER_ID => $userId]);
    }

    public static function findInvitedOrActivatedByUserId($userId)
    {
        return self::find()->where([UserGameFields::USER_ID => $userId])->andWhere(['>', UserGameFields::INVITE_STATUS, 0])->all();
    }

    public static function addUserToGame($userId, $gameId, $tag)
    {
        if (!self::findByUserIdAndGameId($userId, $gameId)) {
            $userGame = new UserGame();
            $userGame->user_id = $userId;
            $userGame->game_id = $gameId;
            $userGame->score = 0;
            $userGame->invite_status = self::INVITE_STATUS_ADD;
            $userGame->tag = $tag;
            $userGame->last_update_score = time();
            $userGame->save();
        }
    }

    public static function findByUserIdAndGameId($userId, $gameId)
    {
        return self::findOne([UserGameFields::GAME_ID => $gameId, UserGameFields::USER_ID => $userId]);
    }

    public static function addUserToGameFromLobby($userId, $gameId)
    {
        if (!self::findByUserIdAndGameId($userId, $gameId)) {
            $userGame = new UserGame();
            $userGame->user_id = $userId;
            $userGame->game_id = $gameId;
            $userGame->score = 0;
            $userGame->invite_status = self::INVITE_STATUS_ACTIVATED;
            $userGame->last_update_score = time();
            $userGame->save();
        }
    }

    public static function findByAllId($userId, $gameId, $teamId)
    {
        return self::find()
            ->where([UserGameFields::USER_ID => $userId])
            ->andWhere([UserGameFields::GAME_ID => $gameId])
            ->andWhere([UserGameFields::TEAM_ID => $teamId])
            ->one();
    }

    public static function setInviteStatusToActived($userId, $gameId)
    {
        $userGame = self::findByUserIdAndGameId($userId, $gameId);
        if ($userGame && $userGame->invite_status != self::INVITE_STATUS_ACTIVATED) {
            $userGame->invite_status = self::INVITE_STATUS_ACTIVATED;
            $userGame->save();
        }
    }

    public static function setStatusToActive($userId, $gameId)
    {
        $userGame = self::findByUserIdAndGameId($userId, $gameId);
        if ($userGame && $userGame->invite_status == self::INVITE_STATUS_INVITED) {
            $userGame->invite_status = self::INVITE_STATUS_ACTIVATED;
            $userGame->save();
        }
    }

    public static function setInviteStatusToInvite($userId, $gameId)
    {
        $userGame = self::findByUserIdAndGameId($userId, $gameId);
        if ($userGame && $userGame->invite_status != self::INVITE_STATUS_INVITED) {
            $userGame->invite_status = self::INVITE_STATUS_INVITED;
            $userGame->save();
        }
    }

    public static function getPlaceByUserAndGameId($gameId, $userId)
    {
        $usersGame = UserGame::find()->where([UserGameFields::GAME_ID => $gameId])
            ->orderBy([
                UserGameFields::SCORE => SORT_DESC,
                UserGameFields::LAST_UPDATE_SCORE => SORT_ASC,
                UserGameFields::ID => SORT_DESC
            ])->all();

        $position = 1;
        foreach ($usersGame as $usergame) {
            if ($usergame->user_id == $userId) {
                return $position;
            }
            $position++;
        }
        return 0;
    }

    public function setToInvited()
    {
        if ($this->invite_status != $this::INVITE_STATUS_ACTIVATED) {
            $this->invite_status = $this::INVITE_STATUS_INVITED;
            $this->save(false);
        }
    }

    public function hasTeam()
    {
        if ($this->team_id == null) {
            return false;
        }
        return true;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id'], 'required'],
            [['user_id'], 'integer'],

            [['game_id'], 'required'],
            [['game_id'], 'integer'],

            [['score'], 'required'],
            [['score'], 'integer'],
            [['score'], 'default', 'value' => 0],

            [['tag'], 'string', 'max' => 45],
            [['tag'], 'safe', 'on' => 'search'],

            [['team_id'], 'integer'],
            [['team_id'], 'default', 'value' => null],

            [['invite_status'], 'integer'],

            [['last_update_score'], 'integer'],

            //PURIFIER FILTER
            [['user_id', 'game_id', 'score', 'team_id', 'invite_status', 'last_update_score'],
                'filter', 'filter' => '\yii\helpers\HtmlPurifier::process']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'tag' => Yii::t('app', 'Etykieta'),
            'id' => Yii::t('app', 'app'),
            'user_id' => Yii::t('app', 'User ID'),
            'game_id' => Yii::t('app', 'Game ID'),
            'score' => Yii::t('app', 'Punkty'),
            'team_id' => Yii::t('app', 'TEAM ID'),
            'last_update_score' => Yii::t('app', 'Data ostatniej aktualizacji punktów'),
        ];
    }

    public function getTeam(): Team
    {
        return Team::findOne([TeamFields::ID => $this->team_id]);
    }

    public function getUser(): User
    {
        return User::findOne([UserFields::ID => $this->user_id]);
    }

    public function getGame(): Game
    {
        return Game::findOne([GameFields::ID => $this->game_id]);
    }

    public function changeTeam($teamId)
    {
        $team = Team::findById($teamId);
        $this->team_id = $teamId;
        $this->score = $team->score;
        $this->last_update_score = time();
        $this->save();
    }

    public function removeTeam()
    {
        $this->team_id = null;
        $this->score = 0;
        $this->last_update_score = time();
        $this->save();
    }

    public function upScore($score)
    {
        $this->score += $score;
        $this->last_update_score = time();
        $this->setTeamScore();
        $this->save();
    }

    public function setTeamScore()
    {
        if (!$this->team_id == null) {
            $team = Team::findById($this->team_id);
            $team->last_update_score = time();
            $team->score = $this->score;
            $team->save();
        }
    }

    public function getInvitedStatusText()
    {
        switch ($this->invite_status) {
            case 0:
                return Yii::t('app', 'Dodany');
                break;
            case 1:
                return Yii::t('app', 'Zaproszony');
                break;
            case 2:
                return Yii::t('app', 'Aktywowany');
                break;
            default:
                return Yii::t('app', 'Wystąpił błąd');
                break;
        }
    }

    public function loadDefaultValues($skipIfSet = true)
    {
        return parent::loadDefaultValues($skipIfSet);
    }
}
