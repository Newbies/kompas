<?php

namespace common\models;

use common\models\fields\CompletedTaskFields;
use common\models\fields\ReaderUseTimeFields;
use common\models\fields\UserFields;
use common\models\fields\UserGameFields;
use common\models\fields\UserOrgFields;
use DateTime;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\data\ActiveDataProvider;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "user".
 *
 * @property string $id
 * @property integer $role
 * @property string $email
 * @property string $name
 * @property string $last_name
 * @property string $auth_key
 * @property string $password_hash
 * @property string $password_reset_token
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $status
 * @property integer $register_code
 * @property string $parent_email
 * @property integer $organization_id
 *
 * @property CompleteTask[] $completeTasks
 * @property Games[] $games
 * @property UserOption[] $userOptions
 * @property UserGames[] $userGames
 */
class User extends \yii\db\ActiveRecord implements IdentityInterface
{
    //Roles
    const ROLE_DEFAULT = 0;
    const ROLE_ADMIN = 1;
    const ROLE_SUPER_ADMIN = 2;

    //Rodo independent user age
    const INDEPENDENT_AGE = 16;

    //Default params
    const  DEFAULT_PASSWORD = "Z0s9E8!@#";
    const  DEFAULT_PASSWORD_HASH = '$2y$13$aZ6VEze077Kixka7eIL05ud2r/OG3xVq0nZ.5O981fwsWsLrUqSbe';
    const  DEFAULT_AUTH_KEY = '_xM86bsrTpsKtGvGUpPoBll76FUdeOoB';

    //Account registration status
    const  STATUS_ACTIVE = 1;
    const  STATUS_INACTIVE = 0;

    //Account delay registration days
    const DELETE_TIME = 604800;

    public $inviteStatus;
    public $tag;

    /**
     * Finds user by email
     *
     * @param string $email
     * @return static|null
     */
    public static function findByEmail($email)
    {
        return self::findOne([UserFields::EMAIL => $email]);
    }

    /**
     * Finds admin by email
     *
     * @param string $email
     * @return array|User|null|\yii\db\ActiveRecord[]
     */
    public static function findAdminByEmail($email)
    {
        return self::find()
            ->where([UserFields::EMAIL => $email])
            ->andWhere(['>', UserFields::ROLE, 0])
            ->one();
    }


    /**
     * Return unactivated user array
     * @return array|Task[]|User[]|\yii\db\ActiveRecord[]
     */
    public static function findUnactivated()
    {
        return self::find()->where([UserFields::STATUS => self::STATUS_INACTIVE])->all();
    }

    public static function findIdentity($id)
    {
        return self::findOne([UserFields::ID => $id]);
    }

    /**
     * Authenticates an API user
     *
     * @param string $email email or access token to validate
     * @param string $password password to validate
     * @return User|null authenticated user
     */
//    public static function apiAuth($email, $password)
//    {
//        $user = User::findOne(['email' => $email]);
//
//        // email is mandatory field
//        if (empty($email)) {
//            return null;
//        }
//
//
//        // if password is supplied, authenticate using email and password
//        if (!empty($password)) {
//
//            // if no record matching the requested user
//            if (empty($user)) {
//                return null;
//            }
//
//            // validate password
//            $isPass = $user->validatePassword($password);
//
//            // if password validation fails
//            if (!$isPass) {
//                return null;
//            }
//
//        } else {
//            // if password is empty, authenticate using email value as access token
//            $user = self::findOne([
//                'auth_key' => $email,
//            ]);
//
//            // if no record matching the requested user
//            if (empty($user)) {
//                return null;
//            }
//        }
//
//        // if user validates
//        return $user;
//        return User::findOne(['auth_key' => $auth,]);
//
//    }

    public static function findByPasswordResetToken($token)
    {
        if (!self::isPasswordResetTokenValid($token)) {
            return null;
        }

        return self::findOne([UserFields::PASSWORD_RESET_TOKEN => $token]);
    }

    /**
     * Check is user independent and can create account on it own
     * @param $birthDate
     * @return boolean
     */
    public static function isUserIndependent($birthDate): bool
    {
        $currentDate = new DateTime();
        $userAge = $birthDate->diff($currentDate);
        if($userAge->y >= User::INDEPENDENT_AGE){
            return true;
        }
        return false;
    }

    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }

        $timestamp = (int)substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        return $timestamp + $expire >= time();
    }

    public static function getLogged(): User
    {
        return self::findOne([UserFields::ID => Yii::$app->user->id]);
    }

    static public function findById($id)
    {
        return self::findOne([UserFields::ID => $id]);
    }

    static public function findByIds($id)
    {
        return self::findAll([UserFields::ID => $id]);
    }

    /**
     * Finds an identity by the given token.
     * @param mixed $token the token to be looked for
     * @param mixed $type the type of the token. The value of this parameter depends on the implementation.
     * For example, [[\yii\filters\auth\HttpBearerAuth]] will set this parameter to be `yii\filters\auth\HttpBearerAuth`.
     * @return IdentityInterface the identity object that matches the given token.
     * Null should be returned if such an identity cannot be found
     * or the identity is not in an active state (disabled, deleted, etc.)
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
    }

    public static function hasTeam($id, $gameId)
    {
        $userGame = UserGame::findByUserIdAndGameId($id, $gameId);

        if ($userGame->team_id === null) {
            return false;
        }
        return true;
    }

    public static function create($email, $name, $lastName,$role = self::ROLE_DEFAULT)
    {
        $user = new User();
        $user->email = $email;
        $user->name = $name;
        $user->last_name = $lastName;
        $user->status = self::STATUS_INACTIVE;
        $user->role = $role;
        $user->register_code = strval(rand(10000000, 99999999));
        $user->password_hash = self::DEFAULT_PASSWORD_HASH;
        $user->auth_key = self::DEFAULT_AUTH_KEY;
        $user->save();
        return $user;
    }

    public function getCreatedDate()
    {
        return $this->created_at;
    }

    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    public function updateFields($params)
    {
        $this->setAttributes($params);
        return $this->save();
    }

    public function setAttributes($values, $safeOnly = true)
    {
        parent::setAttributes($values, $safeOnly);

        if (!empty($values['password'])) {
            $this->setPassword($values['password']);
            $this->generatePasswordResetToken();
        }
    }

    /**
     * Generates new password reset token
     */
    public
    function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['name', 'string', 'max' => 45],
            [['name', 'inviteStatus'], 'safe', 'on' => 'search'],

            ['last_name', 'string', 'max' => 45],
            ['last_name', 'safe', 'on' => 'search'],

            [['role'], 'required', 'on' => 'create'],
            [['role'], 'integer', 'on' => 'apiPut'],
            [['role'], 'integer', 'on' => 'create'],

            [['email'], 'required', 'on' => 'create'],
            [['email'], 'string', 'max' => 45],
            [['email'], 'safe', 'on' => 'search'],
            [['email'], 'unique'],

            [['parent_email'], 'string', 'max' => 45],
            [['parent_email'], 'safe', 'on' => 'search'],
            [['parent_email'], 'unique'],

            [['auth_key'], 'required', 'on' => 'create'],
            [['auth_key'], 'string', 'max' => 32],

            [['password_hash'], 'required', 'on' => 'create'],
            [['password_hash'], 'string', 'max' => 255],

            [['status'], 'integer'],
            [['status'], 'safe', 'on' => 'search'],

            [['register_code'], 'integer'],
            [['register_code'], 'safe', 'on' => 'search'],
            [['register_code'], 'unique',],

            [['password_reset_token'], 'string', 'max' => 255],

            [['inviteStatus'], 'integer'],
            [['inviteStatus'], 'safe', 'on' => 'search'],

            [['tag'], 'string'],
            [['tag'], 'safe', 'on' => 'search'],

            [['role', 'email', 'auth_key', 'password_hash', 'name', 'last_name',],
                'filter', 'filter' => '\yii\helpers\HtmlPurifier::process']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'role' => Yii::t('app', 'Rola'),
            'email' => Yii::t('app', 'Email'),
            'name' => Yii::t('app', 'Imie'),
            'last_name' => Yii::t('app', 'Nazwisko'),
            'auth_key' => Yii::t('app', 'Auth Key'),
            'password_hash' => Yii::t('app', 'Password Hash'),
            'password_reset_token' => Yii::t('app', 'Password Reset Token'),
            'created_at' => Yii::t('app', 'Data stworzenia'),
            'updated_at' => Yii::t('app', 'Data edycji'),
        ];
    }

    public function setActive()
    {
        $this->status = self::STATUS_ACTIVE;
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }


    /**
     * Check register code is associated to user
     * @param $registerCode
     * @return bool
     */
    public function checkRegisterCode($registerCode): bool
    {
        if($registerCode == $this->register_code){
            return true;
        }
        return false;
    }

    public function getAuthKey()
    {
        return $this->auth_key;
    }

    public function getId()
    {
        return $this->id;
    }

    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }

    public function showMe()
    {
        if ($this->name && $this->last_name) {
            return $this->name . " " . $this->last_name;
        }
        return $this->email;
    }

    public function search($gameId, $params)
    {
        $this->load($params);
        $query = self::find()
            ->join('LEFT JOIN', 'user_game', 'user.id =user_game.user_id')
            ->where(['user_game.game_id' => $gameId]);

        $query->andFilterWhere(['like', UserFields::EMAIL, $this->email]);
        $query->andFilterWhere(['like', UserFields::NAME, $this->name]);
        $query->andFilterWhere(['like', UserFields::LAST_NAME, $this->last_name]);
        $query->andFilterWhere(['user_game.invite_status' => $this->inviteStatus]);
        $query->andFilterWhere(['like', 'user_game.tag', $this->tag]);


        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => [UserFields::ID => SORT_ASC]],
            'pagination' => [
                'pageSize' => 25,
            ],
        ]);
        return $dataProvider;
    }

    public function searchToTeam($gameId, $params)
    {
        $this->load($params);
        $query = self::find()
            ->join('LEFT JOIN', 'user_game', 'user.id =user_game.user_id')
            ->where(['user_game.game_id' => $gameId])
            ->andWhere(['user_game.team_id' => null]);

        $query->andFilterWhere(['like', UserFields::EMAIL, $this->email]);
        $query->andFilterWhere(['like', UserFields::NAME, $this->name]);
        $query->andFilterWhere(['like', UserFields::LAST_NAME, $this->last_name]);
        $query->andFilterWhere(['user_game.invite_status' => $this->inviteStatus]);
        $query->andFilterWhere(['like', 'user_game.tag', $this->tag]);


        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => [UserFields::ID => SORT_ASC]],
            'pagination' => [
                'pageSize' => 25,
            ],
        ]);
        return $dataProvider;
    }

    public function hasOrganization()
    {
        return boolval(UserOrg::findAll([UserOrgFields::USER_ID => $this->id]));
    }

    public function addToOrganization($orgId)
    {
        $userOrg = new UserOrg();
        $userOrg->user_id = $this->id;
        $userOrg->org_id = $orgId;
        return $userOrg->save();
    }

    public function leaveOrganization($orgId)
    {
        return UserOrg::deleteAll([UserOrgFields::USER_ID => $this->id, UserOrgFields::ORG_ID => $orgId]);
    }

    public function upgradeToAdmin()
    {
        if ($this->role < 1) {
            $this->role = 1;
            $this->update();
        }
    }

    public function downgradeFromAdmin()
    {
        if ($this->role == 1) {
            if (UserOrg::find()->where([UserOrgFields::USER_ID => $this->id])->count() == 0) {
                $this->role = 0;
                $this->update();
            }
        }
    }

    public function delete()
    {
        UserGame::deleteAll([UserGameFields::USER_ID => $this->id]);
        ReaderUseTime::deleteAll([ReaderUseTimeFields::USER_ID => $this->id]);
        CompletedTask::deleteAll([CompletedTaskFields::USER_ID => $this->id]);
        return parent::delete();
    }

    public function hasEnterToAdminPanel()
    {
        if ($this->role == 0) {
            return false;
        }
        return true;
    }

    /**
     * Validates the given auth key.
     *
     * This is required if [[User::enableAutoLogin]] is enabled.
     * @param string $authKey the given auth key
     * @return bool whether the given auth key is valid.
     * @see getAuthKey()
     */
    public function validateAuthKey($authKey)
    {
    }
}
