<?php

namespace common\models;


use common\models\fields\GameFields;
use common\models\fields\OrganizationFields;
use common\models\fields\TaskFields;
use common\models\fields\TeamFields;
use common\models\fields\UserGameFields;
use DateTime;
use Exception;
use Yii;
use yii\data\ActiveDataProvider;

/**
 * This is the model class for table "game".
 *
 * @property string $id
 * @property string $owner_organization_id
 * @property string $name
 * @property string $description
 * @property integer $type
 * @property integer $start_date
 * @property string $end_date
 * @property integer $status
 * @property integer $ranking
 * @property integer $place_id
 * @property User $ownerUser
 * @property Task[] $tasks
 * @property UserGame[] $userGames
 */
class Game extends \yii\db\ActiveRecord
{
    CONST STATUS_FUTURE = 2;
    CONST STATUS_CURRENT = 1;
    CONST STATUS_FINISHED = 0;

    CONST RANKING_SOLO = 0;
    CONST RANKING_TEAM = 1;

    CONST TYPE_PUBLIC = 0;
    CONST TYPE_PRIVATE = 1;

    public static function getStatusText($statusType)
    {
        if ($statusType == self::STATUS_FUTURE) {
            return "przyszła";
        }
        if ($statusType == self::STATUS_CURRENT) {
            return "bieżąca";
        }
        if ($statusType == self::STATUS_FINISHED) {
            return "zakończona";
        }
    }

    public static function findById($id)
    {
        return self::findOne([GameFields::ID => $id]);
    }

    public static function getEndTime($id)
    {
        return static::findOne($id)->end_date;
    }

    public static function getStartTime($id)
    {
        return static::findOne($id)->start_date;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['owner_organization_id'], 'required'],
            [['owner_organization_id'], 'integer'],

            [['name'], 'required'],
            [['name'], 'string', 'max' => 255],

            [['description'], 'required'],
            [['description'], 'string'],

            [['start_date'], 'required'],
            [['start_date'], 'safe'],
            [['end_date'], 'required'],
            [['end_date'], 'safe'],
            [['end_date'], 'validateDates'],

            [['type'], 'integer'],

            [['status'], 'integer'],

            [['ranking'], 'integer'],

            [['place_id'], 'integer'],

            //SEARCH FILTER
            [['name', 'status'], 'safe', 'on' => 'search'],

            //PURIFIER FILTER
            [['name', 'description', 'owner_organization_id', 'start_date', 'end_date', 'type', 'ranking'],
                'filter', 'filter' => '\yii\helpers\HtmlPurifier::process']
        ];
    }

    public function validateDates()
    {
        if ($this->end_date <= $this->start_date) {
            $this->addError('start_date', Yii::t('app', 'Data zakończenia musi być większa niż data rozpoczęcia.'));
            $this->addError('end_date', Yii::t('app', 'Data zakończenia musi być większa niż data rozpoczęcia.'));
        }
    }

    public function afterFind()
    {
        $this->start_date = $this->isTimestamp($this->start_date) ? date("Y-m-d H:i:s", $this->start_date) : $this->start_date;
        $this->end_date = $this->isTimestamp($this->end_date) ? date("Y-m-d H:i:s", $this->end_date) : $this->end_date;
        parent::afterFind();
    }

    public function isTimestamp($string)
    {
        try {
            new DateTime('@' . $string);
        } catch (Exception $e) {
            return false;
        }
        return true;
    }

    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'owner_organization_id' => Yii::t('app', 'Id Organizacji'),
            'name' => Yii::t('app', 'Nazwa'),
            'description' => Yii::t('app', 'Opis'),
            'type' => Yii::t('app', 'Dostępność'),
            'start_date' => Yii::t('app', 'Data rozpoczęcia'),
            'end_date' => Yii::t('app', 'Data zakończenia'),
            'status' => Yii::t('app', 'Status'),
            'ranking' => Yii::t('app', 'Tryb gry:'),
            'city' => Yii::t('app', 'Miasto'),
        ];
    }

    public function setStatus()
    {
        $currentDateTime = date("Y-m-d H:i:s");

        if ($this->start_date < $currentDateTime && $this->end_date > $currentDateTime) {
            $this->status = self::STATUS_CURRENT;
        } else if ($this->start_date > $currentDateTime) {
            $this->status = self::STATUS_FUTURE;
        } else if ($this->end_date < $currentDateTime) {
            $this->status = self::STATUS_FINISHED;
        }
        $this->toTimeStamp();
    }

    public function toTimeStamp()
    {
        $this->start_date = strtotime($this->start_date);
        $this->end_date = strtotime($this->end_date);
    }

    public function getGameShortDescription($length = 88, $prefix = '...')
    {
        $description = strip_tags($this->description);
        if (strlen($description) > $length) {
            $description = mb_substr($description, 0, $length) . $prefix;
        }
        return \yii\helpers\Html::encode($description);
    }

    public function getGameLongDescription($length = 148, $prefix = '...')
    {
        $description = strip_tags($this->description);
        if (strlen($description) > $length) {
            $description = mb_substr($description, 0, $length) . $prefix;
        }
        return \yii\helpers\Html::encode($description);
    }

    public function getGameShortName($length = 19, $prefix = '...')
    {
        $name = $this->name;
        if (strlen($name) > $length) {
            $name = mb_substr($name, 0, $length) . $prefix;
        }
        return \yii\helpers\Html::encode($name);
    }

    /**
     * Get percent of task completed
     * @return float 0 to 1
     */
    public function getTaskCompletionPercent()
    {
        if ($this->getAllTasksCount() > 0) {
            return ($this->getCompletedTasksCount() / $this->getAllTasksCount());
        } else {
            return 0;
        }
    }

    public function getAllTasksCount()
    {
        $tasks = Task::find()->where([TaskFields::GAME_ID => $this->id])->andWhere(['!=', TaskFields::STATUS, Task::STATUS_FUTURE])->all();
        return sizeof($tasks);
    }

    public function getCompletedTasksCount()
    {
        $tasks = Task::findAll([TaskFields::GAME_ID => $this->id]);
        $taskIds = array();

        foreach ($tasks as $task) {
            array_push($taskIds, $task->id);
        }
        $completedTask = CompletedTask::findByUserIdAndTaskId(Yii::$app->user->id, $taskIds);
        return sizeof($completedTask);
    }

    public function getPointsCompletionPercent()
    {
        if ($this->getAllPoints() > 0) {
            $pointsPercent = ($this->getCurrentPoints() / $this->getAllPoints());
            $pointsPercentFloat = number_format((float)$pointsPercent, 2, '.', '');
            return (string)$pointsPercentFloat;
        } else {
            return 0;
        }
    }

    public function getAllPoints()
    {
        $tasks = Task::findAll([TaskFields::GAME_ID => $this->id]);
        $points = 0;

        foreach ($tasks as $task) {
            $points += (int)$task->score;
        }

        return $points;
    }

    public function getCurrentPoints()
    {
        $tasks = Task::findAll([TaskFields::GAME_ID => $this->id]);
        $taskIds = array();

        foreach ($tasks as $task) {
            array_push($taskIds, $task->id);
        }
        $completedTasks = CompletedTask::findByUserIdAndTaskId(Yii::$app->user->id, $taskIds);

        $score = 0;
        foreach ($completedTasks as $completedTask) {
            $score += (int)Task::findOne([TaskFields::ID => $completedTask->task_id])->score;
        }
        return $score;
    }

    public function search($params)
    {
        $this->load($params);
        $query = self::find();

        $query->andFilterWhere(['status' => $this->status]);
        $query->andFilterWhere(['like', 'name', $this->name]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['id' => SORT_ASC]]
        ]);

        return $dataProvider;
    }


    public function getCircleColor()
    {
        switch ($this->status) {
            case 0:
                return "text-red";
                break;
            case 1:
                return "text-green";
                break;
            case 2:
                return "text-yellow";
                break;
            default:
                return "status-not-set";
                break;
        }
    }

    public function isIndividual()
    {
        return boolval($this->ranking);
    }

    public function getRankingLikeText()
    {
        switch ($this->ranking) {
            case 0:
                return Yii::t('app', 'Indywidualna');
                break;
            case 1:
                return Yii::t('app', 'Drużynowa');
                break;
        }
    }

    public function getOrganizatorName()
    {
        return Organization::findOne([OrganizationFields::ID => $this->owner_organization_id])->name;
    }

    public function getTypeLikeText()
    {
        switch ($this->type) {
            case 0:
                return Yii::t('app', 'Gra otwarta');
                break;
            case 1:
                return Yii::t('app', 'Gra prywatna');
                break;
        }
    }

    public function getScoringModeLikeText()
    {
        switch ($this->scoring_mode) {
            case 0:
                return Yii::t('app', 'Średnia');
                break;
            case 1:
                return Yii::t('app', 'Jeden za wszystkich');
                break;
        }
    }

    public function isEnd()
    {
        if ($this->status == 0) {
            return true;
        }
        return false;
    }

    public function delete()
    {
        UserGame::deleteAll([UserGameFields::GAME_ID => $this->id]);
        foreach (Team::findAll([UserGameFields::GAME_ID => $this->id]) as $team) {
            $team->delete();
        }

        foreach (Task::findAll([UserGameFields::GAME_ID => $this->id]) as $task) {
            $task->delete();
        }
        return parent::delete();
    }

    public function isAverageScoringMode()
    {
        return !boolval($this->scoring_mode);
    }

    public function isAllScoringMode()
    {
        return boolval($this->scoring_mode);
    }

    public function isPrivate()
    {
        if ($this->type == 0) {
            return false;
        }
        return true;
    }

    public function isPublic()
    {
        if ($this->type == 0) {
            return true;
        }
        return false;
    }

    public function getUserPosition($userId)
    {
        if ($this->isTeamable()) {
            $userGame = UserGame::findOne([UserGameFields::GAME_ID => $this->id, UserGameFields::USER_ID => $userId]);
            return Team::getPlaceByTeamAndGameId($this->id, $userGame->team_id);
        } else {
            return UserGame::getPlaceByUserAndGameId($this->id, $userId);
        }
    }

    public function isTeamable()
    {
        return boolval($this->ranking);
    }

    public function isUserInGame($userId)
    {
        return boolval(UserGame::find()
            ->where([UserGameFields::USER_ID => $userId])
            ->andWhere([UserGameFields::GAME_ID => $this->id])
            ->count()
        );
    }

    public function getTaskCount()
    {
        $tasks = Task::findAll([TaskFields::GAME_ID => $this->id]);
        return sizeof($tasks);
    }

    public function getUserCount()
    {
        return UserGame::find()->where([UserGameFields::GAME_ID => $this->id])->count();
    }

    public function getTeamCount()
    {
        return Team::find()->where([TeamFields::GAME_ID => $this->id])->count();
    }

    public function getPlace()
    {
        return Place::findById($this->place_id);
    }

    public function getOrganization()
    {
        return Organization::findById($this->owner_organization_id);
    }

    public function hasPlayers()
    {
        if ($this->isTeamable()) {
            return Team::findByGameId($this->id);
        } else {
            return UserGame::findByGameId($this->id);
        }
    }

    public function copyAndSave($newName, $newStartDate, $newEndDate)
    {

        $newStartDate = strtotime($newStartDate);
        $newEndDate = strtotime($newEndDate);

        $newGame = new Game();
        $newGame->owner_organization_id = $this->owner_organization_id;
        $newGame->name = $newName;
        $newGame->description = $this->description;
        $newGame->start_date = $newStartDate;
        $newGame->end_date = $newEndDate;
        $newGame->type = $this->type;
        $newGame->status = $this::STATUS_FUTURE;
        $newGame->place_id = $this->place_id;
        $newGame->ranking = $this->ranking;

        if ($newGame->save()) {
            Task::copy($this->id, $newGame->id, strtotime($this->start_date),$newStartDate);
            return $newGame;
        }
        return false;
    }
}
