<?php

namespace common\models;

/**
 * This is the model class for table "user_org".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $org_id
 */
class UserOrg extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id'], 'required'],
            [['user_id'], 'integer'],

            [['org_id'], 'required'],
            [['org_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'org_id' => 'Org ID',
        ];
    }
}
