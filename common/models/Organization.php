<?php

namespace common\models;

use common\models\fields\OrganizationFields;
use common\models\fields\UserOrgFields;
use Yii;
use yii\web\UploadedFile;

/**
 * This is the model class for table "organization".
 *
 * @property integer $id
 * @property string $name
 * @property string $contact_email
 * @property string $owner_user_id
 * @property string $logo
 * @property string $description
 * @property string $file
 */
class Organization extends \yii\db\ActiveRecord
{
    const ORGANIZATION_SIZE = 3;

    public $file;

    static public function findById($id)
    {
        return self::findOne([OrganizationFields::ID => $id]);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255],

            [['contact_email'], 'string', 'max' => 255],
            [['contact_email'], 'email'],

            [['owner_user_id'], 'required'],
            [['owner_user_id'], 'integer'],

            [['logo'], 'string', 'max' => 255],

            [['description'], 'string', 'max' => 255],

            [['file'], 'file'],

            [['name', 'contact_email', 'owner_user_id', 'logo', 'description'],
                'filter', 'filter' => '\yii\helpers\HtmlPurifier::process']

        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'name' => Yii::t('app', 'Nazwa organizacji'),
            'contact_email' => Yii::t('app', 'Email kontaktowy'),
            'entry_key' => Yii::t('app', 'Klucz dołączenia'),
            'description' => Yii::t('app', 'Opis'),
        ];
    }

    public function isFull()
    {
        if (!UserOrg::find()->where([UserOrgFields::ORG_ID => $this->id])->count() >= $this::ORGANIZATION_SIZE) {
            return true;
        }
        return false;
    }

    public function discardUser($userId)
    {
        $user = User::findById($userId);
        $user->leaveOrganization($this->id);
        $user->downgradeFromAdmin();
    }

    public function getOrganizationShortName($length = 16, $prefix = '...')
    {
        return $this->getShorterText($this->name, $length, $prefix);
    }

    public function getShorterText($text, $length, $prefix)
    {
        if (strlen($text) > $length) {
            $text = mb_substr($text, 0, $length) . $prefix;
        }
        return \yii\helpers\Html::encode($text);
    }

    public function getOrganizationShortDescription($length = 16, $prefix = '...')
    {
        return $this->getShorterText($this->description, $length, $prefix);
    }

    public function getLogoSrc(): String
    {
        return 'http://' . Yii::$app->params['backDomain'] . '/' . $this->logo;
    }

    public function removeLogo()
    {
        if ($this->hasLogo()) {
            unlink($this->logo);
        }
        $this->logo = "";
        $this->save();
    }

    public function hasLogo()
    {
        return boolval($this->logo);
    }

    public function updateLogo()
    {
        $oldLogo = $this->logo;
        $this->logo = $this->getUploadImage();
        if ($this->save()) {
            if (!empty($oldLogo)) {
                unlink($oldLogo);
            }
        }
    }

    public function getUploadImage()
    {
        $imageName = Yii::$app->security->generateRandomString(32);
        $this->file = UploadedFile::getInstance($this, 'file');
        if ($this->file) {
            if ($this->file->extension == 'jpg' || $this->file->extension == 'png') {
                $this->file->saveAs('logos/' . $imageName . '.' . $this->file->extension);
                return 'logos/' . $imageName . '.' . $this->file->extension;
            }
        }
        return '';
    }
}
