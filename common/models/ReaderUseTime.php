<?php

namespace common\models;

use common\models\fields\ReaderUseTimeFields;

/**
 * This is the model class for table "reader_use_time".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $timestamp
 */
class ReaderUseTime extends \yii\db\ActiveRecord
{
    const COOLDOWN = 15;

    public static function addTimeTouser($userId)
    {
        $readerUseTime = new ReaderUseTime();
        $readerUseTime->user_id = $userId;
        $readerUseTime->timestamp = time() + self::COOLDOWN;
        $readerUseTime->save();
    }

    public static function findByUserId($userId)
    {
        return self::findOne([ReaderUseTimeFields::USER_ID => $userId]);
    }

    public function updateTime()
    {
        $this->timestamp = time() + self::COOLDOWN;
        $this->update();
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'timestamp'], 'required'],
            [['user_id', 'timestamp'], 'integer'],
            //PURIFIER FILTER
            [['user_id', 'timestamp',],
                'filter', 'filter' => '\yii\helpers\HtmlPurifier::process']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'timestamp' => 'Timestamp',
        ];
    }
}
