<?php

namespace common\models;

use common\models\fields\CompletedTaskFields;
use Yii;

/**
 * This is the model class for table "completed_task".
 *
 * @property int $id
 * @property int $user_id
 * @property int $task_id
 * @property int $time
 *
 * @    property User $user
 * @property Task $task
 */
class CompletedTask extends \yii\db\ActiveRecord
{
    public static function findByTaskIdSortByTime($taskId)
    {
        return self::find()->where([CompletedTaskFields::TASK_ID => $taskId])
            ->orderBy([CompletedTaskFields::TIME => SORT_DESC])
            ->all();
    }

    public static function findByUserId($userId)
    {
        return self::findAll([CompletedTaskFields::USER_ID => $userId]);
    }

    public static function findByUserIdAndTaskId($userId, $taskId)
    {
        return self::findAll([CompletedTaskFields::USER_ID => $userId,
            CompletedTaskFields::TASK_ID => $taskId]);
    }

    public static function add($user_id, $task_id)
    {
        $completeTask = new CompletedTask();
        $completeTask->user_id = $user_id;
        $completeTask->task_id = $task_id;
        $completeTask->time = time();
        $completeTask->save();
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id'], 'required'],
            [['user_id'], 'integer'],
            [['user_id'], 'exist', 'skipOnError' => true,
                'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],

            [['task_id'], 'required'],
            [['task_id'], 'integer'],
            [['task_id'], 'exist', 'skipOnError' => true,
                'targetClass' => Task::className(), 'targetAttribute' => ['task_id' => 'id']],

            [['time'], 'required'],
            [['time'], 'integer'],

            //PURIFIER FILTER
            [['user_id', 'task_id', 'time'],
                'filter', 'filter' => '\yii\helpers\HtmlPurifier::process']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'task_id' => Yii::t('app', 'Task ID'),
            'team_id' => Yii::t('app', 'Team ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTask()
    {
        return $this->hasOne(Task::className(), ['id' => 'task_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTeam()
    {
        return $this->hasOne(Team::className(), ['id' => 'team_id']);
    }
}
