<?php

namespace common\models;

use common\models\fields\ForceLogoutFields;

/**
 * This is the model class for table "force_logout".
 *
 * @property integer $id
 * @property integer $user_id
 */
class ForceLogout extends \yii\db\ActiveRecord
{

    static public function addUser($userId)
    {
        $force = new ForceLogout();
        $force->user_id = $userId;
        $force->save();
    }

    static public function removeUser($userId)
    {
        return self::deleteAll([ForceLogoutFields::USER_ID => $userId]);
    }

    public static function findByUserId($userId)
    {
        return self::findOne([ForceLogoutFields::USER_ID => $userId]);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
        ];
    }
}
