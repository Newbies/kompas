<?php

namespace common\models;

use common\models\fields\CompletedTaskFields;
use common\models\fields\CompletedTaskTeamFields;
use common\models\fields\TaskFields;
use Yii;
use yii\data\ActiveDataProvider;
use yii\web\UploadedFile;

/**
 * This is the model class for table "task".
 *
 * @property string $id
 * @property string $game_id
 * @property string $name
 * @property string $description
 * @property string $code
 * @property string $score
 * @property string $image
 * @property integer $task_owner
 * @property integer $type
 * @property integer $time
 * @property integer $mailed
 * @property integer $start_time
 * @property integer $end_time
 * @property integer $status
 * @property integer $show_number
 *
 *
 * @property CompletedTask[] $completedTasks
 * @property Image[] $images
 * @property Game $game
 */
class Task extends \yii\db\ActiveRecord
{
    CONST TYPE_STANDARD = 0;
    CONST TYPE_PREMIUM = 1;
    CONST TYPE_TIMED = 2;

    CONST STATUS_FUTURE = 2;
    CONST STATUS_CURRENT = 1;
    CONST STATUS_FINISHED = 0;
    CONST STATUS_NOT_SET = -1;

    const MAILED = 1;
    const NOT_MAILED = 0;
    const TIME_NOT_SET = 0;
    const EMPTY_TIME = 0;

    /**
     * @inheritdoc
     */
    public $file;
    public $addNext;

    public static function findById($id)
    {
        return self::find()->where([TaskFields::ID => $id])->one();
    }

    public static function findPremium($gameId, $isMailed)
    {
        return self::find()
            ->where([TaskFields::GAME_ID => $gameId, TaskFields::TYPE => self::TYPE_PREMIUM, TaskFields::MAILED => $isMailed])
            ->all();
    }

    public static function findAllPremium()
    {
        return self::find()->where([TaskFields::TYPE => self::TYPE_PREMIUM])->all();
    }

    public static function findAllTimed()
    {
        return self::find()->where([TaskFields::TYPE => self::TYPE_TIMED])->all();
    }

    public static function upShowNumberBelow($gameId, $showNumber)
    {
        $tasks = self::find()->where([TaskFields::GAME_ID => $gameId,])
            ->andWhere(['>=', TaskFields::SHOW_NUMBER, $showNumber])
            ->all();

        foreach ($tasks as $task) {

            if ($task->show_number == $showNumber) {
                $task->upShowNumber();
                $showNumber++;
            }
        }
    }

    public static function copy($oldGameId, $newGameId, $oldStartTime, $newStartTime)
    {
        $tasks = self::findByGameId($oldGameId);

        foreach ($tasks as $task) {

            $newTask = new Task();
            $newTask->name = $task->name;
            $newTask->description = $task->description;
            $newTask->game_id = $newGameId;
            $newTask->code = $task->code;
            $newTask->type = $task->type;
            $newTask->score = $task->score;
            $newTask->show_number = $task->show_number;
            $newTask->image = $task->image;
            $newTask->mailed = self::NOT_MAILED;
            $newTask->task_owner = $task->task_owner;
            $newTask->time = $task->time;
            $newTask->setTaskDetails();

            $newTaskStartTime = strtotime($task->start_time) - $oldStartTime + $newStartTime;
            $newTaskEndTime = strtotime($task->end_time) - strtotime($task->start_time) + $newTaskStartTime;

            $newTask->start_time = $newTaskStartTime;
            $newTask->end_time = $newTaskEndTime;

            $newTask->validate();
            $newTask->save(true);
        }
    }

    public static function findByGameId($gameId)
    {
        return self::findAll([TaskFields::GAME_ID => $gameId]);
    }

    public function upShowNumber()
    {
        $this->show_number = $this->show_number + 1;
        $this->save(true);
    }

    public function getTypeText()
    {
        switch ($this->type) {
            case self::TYPE_PREMIUM:
                return Yii::t('app', 'Premium');
            case  self::TYPE_TIMED:
                return Yii::t('app', 'Ograniczone czasowo');
            default:
        }
        return Yii::t('app', 'Standardowe');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['code'], 'validateCode'],
            [['code'], 'string', 'max' => 45],

            [['game_id'], 'required'],
            [['game_id'], 'integer'],
            [['game_id'], 'exist', 'skipOnError' => true, 'targetClass' => Game::className(), 'targetAttribute' => ['game_id' => 'id']],

            [['score'], 'integer'],
            [['score'], 'required'],

            [['show_number'], 'integer'],
            [['show_number'], 'required'],

            [['image'], 'string'],

            [['name'], 'string'],
            [['name'], 'required'],

            [['description'], 'required'],
            [['description'], 'string'],

            [['mailed'], 'integer'],

            [['time'], 'integer'],
            [['time'], 'required', 'whenClient' => "function (attribute, value) {
                   return $('#typeTaskDropList').val() == 2;
            }"],

            ['time', 'compare', 'compareValue' => 0, 'operator' => '>=', 'type' => 'number'],

            [['type'], 'integer'],

            [['task_owner'], 'integer'],

            [['addNext'], 'integer'],

            [['status'], 'integer'],

            [['start_time'], 'safe'],
            [['start_time'], 'required', 'whenClient' => "function (attribute, value) {
                   return $('#typeTaskDropList').val() == 1;
            }"],

            [['end_time'], 'safe'],
            [['end_time'], 'required', 'whenClient' => "function (attribute, value) {
                   return $('#typeTaskDropList').val() == 1;
            }"],

            [['end_time'], 'compare', 'compareAttribute' => 'start_time', 'operator' => '>', 'enableClientValidation' => true, 'message' => Yii::t('app', 'Czas deaktywacji musi być większy od czasu aktywacji'),
                'when' => function () {
                    return $this->type == self::TYPE_PREMIUM;
                },
                'whenClient' => "function (attribute, value) {
                   return $('#typeTaskDropList').val() == 1;
                }"],

            [['name', 'description', 'code', 'score', 'type'], 'safe', 'on' => 'search'],

            [['name', 'description', 'game_id', 'code', 'score', 'image', 'time', 'task_owner', 'type', 'mailed',
                'show_number'],
                'filter', 'filter' => '\yii\helpers\HtmlPurifier::process']
        ];
    }

    /**
     * @inheritdoc
     */
    public function validateCode($code)
    {
        $gameByCode = self::findByCodeToGame($this->code, $this->game_id);

        if ($gameByCode && $gameByCode->id != $this->id) {
            $this->addError($code, Yii::t('app', 'Kod musi być unikatowy dla gry.'));
        }
    }

    public static function findByCodeToGame($code, $gameId)
    {
        return self::findOne([TaskFields::CODE => $code, TaskFields::GAME_ID => $gameId]);
    }

    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'game_id' => Yii::t('app', 'Game ID'),
            'name' => Yii::t('app', 'Nazwa'),
            'description' => Yii::t('app', 'Opis'),
            'code' => Yii::t('app', 'Kod'),
            'score' => Yii::t('app', 'Punkty'),
            'image' => Yii::t('app', 'Obrazek'),
            'task_owner' => Yii::t('app', 'Właściel Taska'),
            'file' => Yii::t('app', 'Obrazek'),
            'type' => Yii::t('app', 'Rodzaj zadania'),
            'mailed' => Yii::t('app', 'Wysłano'),
            'time' => Yii::t('app', 'Czas na wykonanie zadania (w minutach)'),
            'start_time' => Yii::t('app', 'Czas aktywacji zadania premium'),
            'end_time' => Yii::t('app', 'Czas deaktywacji zadania  premium'),
            'addNext' => Yii::t('app', ''),
            'status' => Yii::t('app', ''),
            'show_number' => Yii::t('app', 'Kolejność pokazywania'),
        ];
    }

    public function setTaskDetails()
    {
        if (empty($this->code)) {
            $this->code = strval(rand(100000, 999999));
        }

        if ($this->type == self::TYPE_STANDARD) {
            $this->start_time = self::EMPTY_TIME;
            $this->end_time = self::EMPTY_TIME;
            $this->status = self::STATUS_NOT_SET;
            $this->mailed = self::MAILED;
            $this->time = self::TIME_NOT_SET;
        } else if ($this->type == self::TYPE_PREMIUM) {
            $this->time = self::TIME_NOT_SET;
            $this->mailed = self::NOT_MAILED;
            $this->setStatus();
        } else if ($this->type == self::TYPE_TIMED) {
            $this->status = self::STATUS_NOT_SET;
            $this->end_time = self::EMPTY_TIME;
            $this->start_time = self::EMPTY_TIME;
            $this->mailed = self::MAILED;
        }
    }

    public function setStatus()
    {
        $currentDateTime = date("Y-m-d H:i:s");

        if (!$this->isPremium()) {
            return;
        }

        if ($this->start_time < $currentDateTime && $this->end_time > $currentDateTime) {
            $this->status = self::STATUS_CURRENT;
        } else if ($this->start_time > $currentDateTime) {
            $this->status = self::STATUS_FUTURE;
        } else if ($this->end_time < $currentDateTime) {
            $this->status = self::STATUS_FINISHED;
        }
    }

    public function isPremium()
    {
        if ($this->type == self::TYPE_PREMIUM) {
            return true;
        }
        return false;
    }

    public function activateTimedTask($activatorId)
    {
        if ($this->getGame()->isTeamable()) {
            $activatorUserGame = UserGame::findByUserIdAndGameId($activatorId, $this->game_id);
            if ($activatorUserGame->team_id != null) {
                if (!TaskActivator::findByActivatorTeamIdAndTaskId($activatorUserGame->team_id, $this->id)) {
                    $this->createTaskActivator($activatorId, $activatorUserGame->team_id);
                }
            }
        } else {
            if (!TaskActivator::findByActivatorIdAndTaskId($activatorId, $this->id)) {
                $this->createTaskActivator($activatorId);
            }
        }
    }

    public function getGame()
    {
        return Game::findById($this->game_id);
    }

    public function createTaskActivator($activatorId, $activatorTeamId = -1)
    {
        $taskActivator = new TaskActivator();
        $currentTime = date("Y-m-d H:i:s");
        $taskActivator->task_id = $this->id;
        $taskActivator->start_time = strtotime($currentTime);
        $taskActivator->end_time = strtotime($currentTime . " + " . $this->time . " minute");
        $taskActivator->activator_id = $activatorId;
        $taskActivator->activator_team_id = $activatorTeamId;
        $taskActivator->game_id = $this->game_id;
        $taskActivator->save();
    }

    public function getTimedTaskEndTime($activatorId)
    {
        if ($taskActivator = TaskActivator::findByActivatorDetails($activatorId, $this->id)) {
            return $taskActivator->end_time;
        }
        return 0;
    }

    public function isTimedTaskExpired($activatorId)
    {
        if ($taskActivator = TaskActivator::findByActivatorDetails($activatorId, $this->id)) {
            return $taskActivator->isExpired();
        }
        return false;
    }

    public function isActivated($activatorId)
    {
        if ($this->type != self::TYPE_TIMED) {
            return true;
        }

        if (TaskActivator::checkActivation($activatorId, $this->id)) {
            return true;
        }
        return false;
    }

    public function isTimed()
    {
        if ($this->type == self::TYPE_TIMED) {
            return true;
        }
        return false;
    }

    public function isStandard()
    {
        if ($this->type == self::TYPE_STANDARD) {
            return true;
        }
        return false;
    }

    public function getTaskShortDescription($length = 45, $prefix = '...')
    {
        return $this->getShorterText(strip_tags($this->description), $length, $prefix);
    }

    public function getShorterText($text, $length, $prefix)
    {
        if (strlen($text) > $length) {
            $text = mb_substr($text, 0, $length) . $prefix;
        }
        return \yii\helpers\Html::encode($text);
    }

    public function getTaskShortName($length = 16, $prefix = '...')
    {
        return $this->getShorterText($this->name, $length, $prefix);
    }

    public function search($gameId, $params)
    {
        $this->load($params);
        $query = self::find()->where([TaskFields::GAME_ID => $gameId]);

        $query->andFilterWhere(['like', TaskFields::NAME, $this->name]);
        $query->andFilterWhere(['like', TaskFields::DESCRIPTION, $this->description]);
        $query->andFilterWhere(['like', TaskFields::CODE, $this->code]);
        $query->andFilterWhere(['like', TaskFields::TYPE, $this->type]);
        $query->andFilterWhere(['like', TaskFields::SCORE, $this->score]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => [TaskFields::SHOW_NUMBER => SORT_ASC, TaskFields::ID => SORT_ASC]]
        ]);

        return $dataProvider;
    }

    public function getImageSrc()
    {
        return 'http://' . Yii::$app->params['backDomain'] . '/' . $this->image;
    }

    public function removeImage()
    {
        if ($this->haveImage()) {
            unlink($this->image);
        }

        $this->image = "";
        $this->save();
    }

    public function haveImage()
    {
        return boolval($this->image);
    }

    public function delete()
    {
        CompletedTaskTeam::deleteAll([CompletedTaskTeamFields::TASK_ID => $this->id]);
        CompletedTask::deleteAll([CompletedTaskFields::TASK_ID => $this->id]);
        if ($this->haveImage()) {
            unlink($this->image);
        }
        return parent::delete();
    }

    public function updateImage()
    {
        $oldImage = $this->image;
        $this->image = $this->getUploadImage();
        if ($this->save()) {
            if (!empty($oldImage)) {
                unlink($oldImage);
            }
        }
    }

    function getUploadImage()
    {
        $imageName = Yii::$app->security->generateRandomString(32);
        $this->file = UploadedFile::getInstance($this, 'file');
        if ($this->file) {
            if ($this->file->extension == 'jpg' || $this->file->extension == 'png') {
                $this->file->saveAs('uploads/' . $imageName . '.' . $this->file->extension);

                $file = Yii::getAlias('@app/web/' . 'uploads/' . $imageName . '.' . $this->file->extension);
                $image = Yii::$app->image->load($file);
                $resolution = getimagesize($file);

                if ($resolution[0] >= 700) {
                    $x = 700 / $resolution[0];
                    $image->resize(700, $resolution[1] * $x);
                }

                if ($resolution[1] > 2000) {
                    $image->crop($resolution[0], 2000);
                }

                $image->save();

                return 'uploads/' . $imageName . '.' . $this->file->extension;
            } else {
                return '';
            }
        } else {
            return '';
        }
    }

    public function setShowNumber($value)
    {
        $this->show_number = $value;
        $this->save(true);
    }
}
