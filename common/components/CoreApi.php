<?php

namespace common\components;

use common\components\CoreApiResponse;
use common\models\User;
use Yii;
use yii\base\Exception;

class CoreApi
{

    // curl instance
    const REQUEST_TYPE_PUT = 'put';
    const REQUEST_TYPE_POST = 'post';
    const REQUEST_TYPE_DELETE = 'delete';
    private $curl = null;
    private $endpoint = '';
    private $apiVersion = 'v1';

    /**
     * Get new instance of core api with concreete endpoint
     *
     * @param string $endpoint
     */
    public function __construct($endpoint = '')
    {
        if (!isset(Yii::$app->params['apiDomain'])) {
            throw new \Exception("Param apiDomain in module " . Yii::$app->controller->module->id . ", in file  params-local.php is not defined!");
        }

        $user = null;

        $this->curl = new Curl();

        if (isset(Yii::$app->user)) {
            $user = Yii::$app->user->identity;
        }

        $this->curl->setOption(CURLOPT_SSL_VERIFYPEER, false);

        if($user) {
            $this->curl->setOption(CURLOPT_HTTPHEADER, [
                'Authorization: Basic ' . base64_encode($user->getAuthKey() . ':'),
            ]);
        }

        $this->endpoint = $endpoint;
    }

    /**
     * Create new api for tasks endpoint
     * @return \self
     */
    public static function task()
    {
        return new self('task');
    }

    /**
     * Create new api for users endpoint
     * @return \self
     */

    public static function score()
    {
        return new self('score');
    }

    /**
     * Create new api for users endpoint
     * @return \self
     */
    public static function user()
    {
        return new self('user');
    }

    /**
     * Create new api for users endpoint
     * @return \self
     */
    public static function status()
    {
        return new self('status');
    }

    /**
     * Create new api for users endpoint
     * @return \self
     */
    public static function game()
    {
        return new self('game');
    }

    /**
     * Create new api for users endpoint
     * @return \self
     */
    public static function completed()
    {
        return new self('completed');
    }

    /**
     * Create new api for users endpoint
     * @return \self
     */
    public static function usergame()
    {
        return new self('usergame');
    }

    /**
     * Create new api for users endpoint
     * @return \self
     */
    public static function readerusetime()
    {
        return new self('readerusetime');
    }

    /**
     * Create new api for users endpoint
     * @return \self
     */
    public static function team()
    {
        return new self('team');
    }

    /**
     * Create new api for users endpoint
     * @return \self
     */
    public static function userteam()
    {
        return new self('userteam');
    }


    /**
     * Return the endpoint name
     * @return string
     */
    public function getEndpoint()
    {
        return $this->endpoint;
    }

    /**
     * Make get request to core api
     *
     * @param array $params
     * @return array
     */
    public function get($path = '', $params = [])
    {
        $target = implode('/', [Yii::$app->params['apiDomain'], $this->apiVersion, $this->endpoint]);

        if ($path) {
            $target .= '/' . $path;
        }
        if (count($params) > 0) {
            $target .= '?' . http_build_query($params);
        }

        $response = $this->curl->get($target);

        return json_decode($response, true);
    }

    public function delete($path = '', $params = [])
    {
        $target = implode('/', [Yii::$app->params['apiDomain'], $this->apiVersion, $this->endpoint]);

        if ($path) {
            $target .= '/' . $path;
        }
        if (count($params) > 0) {
            $target .= '?' . http_build_query($params);
        }

        $response = $this->curl->delete($target);

        return json_decode($response, true);
    }

    /**
     * Make post request to core api
     *
     * @param array $params
     * @return CoreApiResponse
     */
    public function post($path, $params, $buildHttpQuery = false, $format = null)
    {
        return $this->makeRequest($path, $params, $buildHttpQuery, $format, self::REQUEST_TYPE_POST);
    }

    public function put($path, $params, $buildHttpQuery = false, $format = null)
    {
        return $this->makeRequest($path, $params, $buildHttpQuery, $format, self::REQUEST_TYPE_PUT);
    }

    public function makeRequest($path, $params, $buildHttpQuery, $format, $requestType = self::REQUEST_TYPE_POST)
    {

        $header = $this->curl->getOption(CURLOPT_HTTPHEADER);
        $header[] = 'Content-Type: application/json';
        $this->curl->setOption(CURLOPT_HTTPHEADER, $header);


        if ($buildHttpQuery) {
            $params = http_build_query($params);
        }

        $target = implode('/', [Yii::$app->params['apiDomain'], $this->apiVersion, $this->endpoint]);

        if ($path) {
            $target .= '/' . $path;
        }

        if ($requestType == self::REQUEST_TYPE_POST) {
            $response = $this->curl->setOption(CURLOPT_POSTFIELDS, json_encode($params))->post($target);
        } else if ($requestType == self::REQUEST_TYPE_PUT) {
            $response = $this->curl->setOption(CURLOPT_POSTFIELDS, json_encode($params))->put($target);
        } else {
            throw new Exception('Unsupported request type');
        }
        return json_decode($response);
    }

}

