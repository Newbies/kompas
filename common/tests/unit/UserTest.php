<?php


use \common\models\User;

class UserTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected function _before()
    {
    }

    protected function _after()
    {
    }

    // tests
    public function testIsEmailExisting()
    {
        $user = new User();
        $id = $user->getId();
        $email = 'test'.rand(1, 9999).$this->toString().'@'.'com.pl';
        $user->email = $email;
        $user->setPassword('test123');
        $user->generateAuthKey();
        $user->save();
        $result = User::isEmailExisting($email);

        $this->assertTrue($result);
        User::deleteAll($id);
    }
}