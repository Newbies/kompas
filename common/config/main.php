<?php
return [
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'language' => 'pl-PL',
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'i18n' => [
            'translations' => [
                'app*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@common/messages',
                    'sourceLanguage' => 'pl-PL',
                    'fileMap' => [
                        'app' => 'app.php',
                        'app/error' => 'bad.php',

                    ],
                ],
            ],
        ],
//		'log' => [
//                    'class'=>'CProfileLogRoute',
//                     'routes'=>array(
//                            array(
//                                'class'=>'CFileLogRoute',
//                                'levels'=>'trace, info',
//                                'categories'=>'system.*',
//                            ),
//
//                        ),
//
//
//			'traceLevel' => YII_DEBUG ? 3 : 0,
//			'targets' => [
//				[
//					'class' => 'yii\log\FileTarget',
//					'levels' => ['error', 'warning'],
//				],
//
//			],
//		],
//    ],

        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=localhost;dbname=newbies_kompas',
            'username' => 'newbies_kompas',
            'password' => '!newbies@',
            'charset' => 'utf8',
        ],

        'mailer' => [
            'class' => 'weluse\mailjet\Mailer',
            'apikey' => 'aaf2097f3e37c4732ec8c31d5abe5dc5',
            'secret' => 'e296776983db822f738b9bbcebd84270'
        ],
        'reCaptcha' => [
            'name' => 'reCaptcha',
            'class' => 'himiklab\yii2\recaptcha\ReCaptcha',
            'siteKey' => '6LdadUsUAAAAAHQICCjqZBbRHvrTVOYs6YuYHVom',
            'secret' => '6LdadUsUAAAAACbUzIaFI0J2LtbEhe1b2SBHpxLw',
        ],
    ],
];
