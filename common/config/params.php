<?php
return [
    'supportEmail' => 'newbies@newbies.pl',
    'adminEmail' => 'kompas@newbies.pl',
    'email' => 'kompas@rst.com.pl',
    'user.passwordResetTokenExpire' => 36000,
    'frontDomain' => 'app.kompas.newbies.pl',
    'apiDomain' => 'api.kompas.newbies.pl',
    'backDomain' => 'admin.kompas.newbies.pl',
    'indexDomain' => 'kompas.newbies.pl',
    'trackingId' => 'UA-106066444-1',
];
