KOMPAS v2

===============================
# REQUIREMENTS
- min. php 7.1  
- min. mysql 10.1.38
- min. composer 1.8.0
- cron

# LAUNCHING THE APPLICATION
## DATABASE
- Create database on mysql server
- Import sql command from `database/kompas_Clear.sql`

## COMPOSER
- $php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
- $php -r "if (hash_file('sha384', 'composer-setup.php') === 'a5c698ffe4b8e849a443b120cd5ba38043260d5c4023dbf93e1558871f1f07f58274fc6f4c93bcfd858c6bd0775cd8d1') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
- $php composer-setup.php
- $php -r "unlink('composer-setup.php');"
- $php composer.phar global require "fxp/composer-asset-plugin:1.4.6"

## PARAMS
In file `common/config/params-local.php` apply this configuration:
```
    'supportEmail' => '[contact mail]',
    'adminEmail' => '[administrator mail]',
    'email' => '[application mail]',
    'user.passwordResetTokenExpire' => 36000,
    'frontDomain' => '[frontend domain]',
    'backDomain' => '[backend domain]',
    'indexDomain' => '[index domain]',
    'trackingId' => '[google analitics' key]',
```

## CRON
Recommended CRON configuration
```
* * * * * php [path to project]/yii mail/send-cached-mail
*/1 * * * * php [path to project]/yii game/change-status
*/1 * * * * php [path to project]/yii task/send-mail
0 4 * * * php [path to project]/yii user/delete-unactivated
```

## LUNCHING
1. Clone gitlab repository by `git clone [url]`
2. Enter to directory `cd [dir]`
3. Execute command `php init`
(required composer)
4. Execute command `./composer.phar install`
(required up database)
5. In file `common/config/main-local.php` change configuraction `dsn`
`localhost` to database domain 
`yii2advanced` to database name 
6. Execute command `php yii migrate`
7. Configure params in `common/config/params-local.php`
8. Redirect your domain to `index.php` by vhost/ngix/etc
- [frontend domain] => frontend/web/index.php
- [index domain] => index/web/index.php
- [backend domain] => backend/web/index.php
9. Configure CRON

## CHANGELOG
-------------------

## WDROŻENIA

| DATA 			| OPIS		 |
|---------------|------------|
| 02.08.2017   	| `Poprawa flow na adminie`~`Poprawa wyglądu gracza`~`Dodanie zarządzanie konta gracza`~`UnitTest CI`|
| 13.09.2017   	| `Dodanie edytora tekstu`~`Blokowanie funkcjonalności w zakończonych grach (admin)`~`Mniejsze Fixy`|

## GULP
### INSTALACJA

Zainstaluj node.js pod swój system operacyjny
https://nodejs.org/en/

Następnie z wiersza polecen zainstaluj Gulpa globalnie:
```
npm install -g gulp
```

Teraz w folderze głównym z kompasem z poziomu wiersza polecen zainstaluj gulpa lokalnie:
```
npm install gulp --save-dev
```

Ostatnim krokiem jest pobranie odpowiednich paczek do poprawnego działania komend Gulpa:
```
npm install
```

### KOMENDY
```
gulp
```
Komenda, która obserwuje zmiany dokonywane w plikach w katalogu /frontend i przy zaobserwowaniu modyfikacji odświeża stronę na wszystkich odpalonych przeglądarkach. <br />
**Ważne! Komenda musi być odpalona przed jakimikolwiek zmianami, które będziesz nanosił w plikach w folderze /frontend. :cyclone:**

.php - po każdej zapisanej zmianie odświeża automatycznie strony Kompasa na wszystkich aktualnie odpalonych przeglądarkach. <br />
.js - minifikacja i spłaszczanie plików do jednego pliku wynikowego. <br />
.css - minifikacja, autoprefixer dla atrybutów w CSS.

Obsługuje zmiany na plikach w folderze frontend/web/lib:
```
*.php, *.js, *.css 
```



