<?php

namespace backend\assemblers;

use backend\models\timeline\TimelineSolo;
use backend\models\timeline\TimelineTeam;

class TimelineAssembler
{
    public static function fromCompletedTask($array)
    {
        $timelines = array();
        foreach ($array as $model) {
            array_push($timelines,
                new TimelineSolo($model->time, $model->user_id, $model->task_id));
        }
        return $timelines;
    }

    public static function fromCompletedTaskTeam($array)
    {
        $timelines = array();
        foreach ($array as $model) {
            array_push($timelines,
                new TimelineTeam($model->time, $model->team_id, $model->task_id, $model->activator_id));
        }
        return $timelines;
    }
}