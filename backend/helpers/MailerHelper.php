<?php
/**
 * Created by PhpStorm.
 * User: mBoiler
 * Date: 21.02.2018
 * Time: 21:32
 */

namespace backend\helpers;

use Yii;
use yii\db\Exception;
use yii\helpers\Html;

class MailerHelper
{

    public static function sendInviteToGame($user, $game)
    {
        $frontLink = Yii::$app->params['frontDomain'];
        try {
            Yii::$app->mailer
                ->compose(
                    ['html' => 'inviteToGame-html'],
                    [
                        'game' => $game->name,
                        'frontLink' => $frontLink,
                    ]
                )
                ->setFrom([Yii::$app->params['email'] => 'Kompas'])
                ->setTo($user->email)
                ->setSubject(Yii::t('app','Zostałeś zaproszony do gry w Kompasie!'))
                ->send();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    public static function sendActivationInvitation($user, $game)
    {
        $signupLink = 'http://' . Yii::$app->params['frontDomain'] . '/site/signup' . '?email=' . $user->email;
        try {
            Yii::$app->mailer
                ->compose(
                    ['html' => 'activateAccount-html'],
                    [
                        'game' => $game->name,
                        'signupLink' => $signupLink,
                    ]
                )
                ->setFrom([Yii::$app->params['email'] => 'Kompas'])
                ->setTo($user->email)
                ->setSubject(Yii::t('app','Zostałeś zaproszony do gry w Kompasie!'))
                ->send();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }
}