<?php
/**
 * Created by PhpStorm.
 * User: tycjan
 * Date: 22.02.18
 * Time: 10:08
 */

namespace backend\helpers;

use yii\base\ErrorException;

class CSVReader
{
    public static function getUsersAsArray($csvPath)
    {
        $rows = array_map('str_getcsv', file($csvPath));
        $rows = array_map("unserialize", array_unique(array_map("serialize", $rows)));
        $header = ['email', 'name', 'last_name', 'tag'];
        $usersArray = array();

        foreach ($rows as $row)
        {
            try {
                $row = array_map('trim', $row);
                $usersArray[] = array_combine($header, $row);
            } catch (ErrorException $e) {
                return false;
            }
        }
        return $usersArray;
    }
}
