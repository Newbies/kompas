<?php

namespace backend\helpers;

class ArrayHelper
{
    public static function map($query, $key){
        $array = array();

        foreach ($query as $item){
            $array[] = $item[$key];
        }

        return $array;
    }

}