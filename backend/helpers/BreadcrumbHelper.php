<?php
/**
 * Created by PhpStorm.
 * User: mBoiler
 * Date: 26.08.2017
 * Time: 11:50
 */

namespace backend\helpers;


use Yii;
use yii\helpers\Url;

class BreadcrumbHelper
{
    public function home()
    {
        return '<li>' .
            '<a href="' . Url::to(['/']) . '">' .
            '<i class="fa fa-dashboard"></i>' .
            Yii::t('app', 'Główny panel') . '</a>' . '</li>';
    }

    public function game($game)
    {
        return '<li>' .
            '<a href="' . Url::to(['game/' . $game->id]) . '">' .
            '<i class="fa fa-circle-o"></i>' .
            $game->getGameShortName(19) . '</a>' . '</li>';
    }

    public function task($game, $task)
    {
        return '<li>' .
            '<a href="' . Url::to(['game/' . $game->id . '/task/' . $task->id]) . '">' .
            '<i class="fa fa-tasks"></i>' .
            $task->getTaskShortName(19) . '</a>' . '</li>';
    }

    public function team($game, $team)
    {
        return '<li>' .
            '<a href="' . Url::to(['game/' . $game->id . '/team/' . $team->id]) . '">' .
            '<i class="fa fa-users"></i>' .
            $team->getTeamShortName(19) . '</a>' . '</li>';
    }

    public function organization($org)
    {
        return '<li>' .
            '<a href="' . Url::to(['organization/' . $org->id]) . '">' .
            '<i class="fa fa-bank"></i>' .
            $org->getOrganizationShortName(19) . '</a>' . '</li>';
    }

    public function tasks()
    {
        return '<li class="active">' .
            '<i class="fa fa-tasks"></i>' .
            Yii::t('app', ' Zadania') .
            '</li>';
    }

    public function teams()
    {
        return '<li class="active">' .
            '<i class="fa fa-users"></i>' .
            Yii::t('app', ' Drużyny') .
            '</li>';
    }

    public function users()
    {
        return '<li class="active">' .
            '<i class="fa fa-user"></i>' .
            Yii::t('app', ' Użytkownicy') .
            '</li>';
    }

    public function detail()
    {
        return '<li class="active" >' .
            '<i class="fa fa-navicon"></i>' .
            Yii::t('app', ' Szczegóły') .
            '</li>';
    }

    public function edit()
    {
        return '<li class="active" >' .
            '<i class="fa fa-edit"></i>' .
            Yii::t('app', ' Edycja') .
            '</li>';
    }

    public function addGame()
    {
        return '<li class="active" >' .
            '<i class="fa fa-plus-square"></i>' .
            Yii::t('app', ' Utwórz grę') .
            '</li>';
    }

    public function addTask()
    {
        return '<li class="active" >' .
            '<i class="fa fa-plus-square"></i>' .
            Yii::t('app', ' Utwórz zadanie') .
            '</li>';
    }

    public function uploadImage()
    {
        return '<li class="active" >' .
            '<i class="fa fa-plus-square"></i>' .
            Yii::t('app', ' Dodaj obrazek') .
            '</li>';
    }

    public function addTeam()
    {
        return '<li class="active" >' .
            '<i class="fa fa-plus-square"></i>' .
            Yii::t('app', ' Utwórz drużynę') .
            '</li>';
    }

    public function addUser()
    {
        return '<li class="active" >' .
            '<i class="fa fa-user-plus"></i>' .
            Yii::t('app', ' Zaproś użytkownika') .
            '</li>';
    }

    public function addUserByCSV()
    {
        return '<li class="active" >' .
            '<i class="fa fa-user-plus"></i>' .
            Yii::t('app', ' Dodaj użytkowników do gry') .
            '</li>';
    }

    public function addUserByForm()
    {
        return '<li class="active" >' .
            '<i class="fa fa-user-plus"></i>' .
            Yii::t('app', ' Zaimportuj do gry') .
            '</li>';
    }

    public function settings()
    {
        return '<li class="active" >' .
            '<i class="fa fa-gears"></i>' .
            Yii::t('app', ' Ustawienia') .
            '</li>';
    }

    public function invite()
    {
        return '<li class="active" >' .
            '<i class="glyphicon glyphicon-send"></i>' .
            Yii::t('app', ' Zaproszenie') .
            '</li>';
    }

    public function myOrganization()
    {
        return '<li class="active" >' .
            '<i class="fa fa-bank "></i>' .
            Yii::t('app', ' Moje organizacje') .
            '</li>';
    }

    public function createOrg()
    {
        return '<li class="active" >' .
            '<i class="glyphicon glyphicon-plus "></i>' .
            Yii::t('app', ' Stworz organizację') .
            '</li>';
    }

    public function updateOrg()
    {
        return '<li class="active" >' .
            '<i class="fa fa-pencil "></i>' .
            Yii::t('app', ' Edycja organizacji') .
            '</li>';
    }
}