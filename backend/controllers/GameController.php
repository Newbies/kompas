<?php

namespace backend\controllers;

use backend\converters\CompletedTaskConverter;
use backend\models\CopyForm;
use common\models\CompletedTask;
use common\models\fields\CompletedTaskFields;
use common\models\fields\GameFields;
use common\models\fields\TaskFields;
use common\models\fields\TeamFields;
use common\models\fields\UserFields;
use common\models\fields\UserGameFields;
use common\models\Game;
use common\models\Place;
use common\models\Task;
use common\models\Team;
use common\models\User;
use common\models\UserGame;
use Yii;
use yii\web\NotFoundHttpException;


class GameController extends BaseController
{
    /**
     * Displays a single Game model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        $this->checkAccessByGame($id);
        $this->showEndGameFlash($id);

        $game = $this->findModel($id);
        $place = Place::findById($game->place_id);
        return $this->render('view', [
            'model' => $game,
            'place' => $place
        ]);
    }

    /**
     * Finds the Game model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Game the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        $model = Game::findOne([GameFields::ID => $id]);
        if ($model) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('app', 'Taka strona nie istnieje'));
        }
    }

    /**
     * Creates a new Game model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $user = User::findOne([UserFields::ID => Yii::$app->user->identity->getId()]);

        if (!$user->hasOrganization()) {
            Yii::$app->session->setFlash('error', Yii::t('app', 'Nie możesz dodać gry, nie będąc w żadnej organizacji.'));
            return $this->redirect(['/']);
        }
        $model = new Game();
        $place = new Place();
        if ($model->load(Yii::$app->request->post()) && $place->load(Yii::$app->request->post()) && $place->validate() && $model->validate()) {
            $place->save();
            $model->place_id = $place->id;
            $model->setStatus();
            $model->save();
            Yii::$app->session->setFlash('success', Yii::t('app', 'Gra dodana.'));
            return $this->redirect('/game/' . $model->id);
        }
        return $this->render('create', [
            'model' => $model,
            'place' => $place
        ]);
    }

    /**
     * Updates an existing Game model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $this->checkAccessByGame($id);
        $this->checkIfGameEnded($id);
        $game = $this->findModel($id);
        $place = Place::findByIdToUpdate($game->place_id);
        if ($game->load(Yii::$app->request->post()) && $place->load(Yii::$app->request->post()) && $place->validate() && $game->validate()) {
            $place->save();
            $game->place_id = $place->id;
            $game->setStatus();
            $game->save();
            Yii::$app->session->setFlash('success', Yii::t('app', 'Gra zedytowana.'));
            return $this->redirect(['view', 'id' => $game->id]);
        }
        return $this->render('update', [
            'model' => $game,
            'place' => $place
        ]);
    }

    /**
     * Deletes an existing Game model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->checkAccessByGame($id);

        $tasks = Task::findAll([TaskFields::GAME_ID => $id]);

        foreach ($tasks as $task) {
            CompletedTask::deleteAll(['=', CompletedTaskFields::TASK_ID, $task->id]);
            Task::findById($task->id)->delete();
        }
        Team::deleteAll(['=', TeamFields::GAME_ID, $id]);
        UserGame::deleteAll(['=', UserGameFields::GAME_ID, $id]);
        Game::findOne($id)->delete();
        Yii::$app->session->setFlash('success', Yii::t('app', 'Gra usunięta.'));
        return $this->redirect(['/site/index']);

    }

    public function actionCopy($gameId)
    {
        $model = new CopyForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $oldGame = Game::findById($gameId);
            if($newGame = $oldGame->copyAndSave($model->name,$model->startDate,$model->endDate)){
                return $this->redirect(['game/view','id'=> $newGame->id]);
            }
        }

        return $this->redirect(['game/view','id'=> $gameId]);
    }
}
