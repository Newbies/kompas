<?php

namespace backend\controllers;

use backend\assemblers\TimelineAssembler;
use backend\helpers\ArrayHelper;
use common\models\CompletedTask;
use common\models\CompletedTaskTeam;
use common\models\fields\TaskFields;
use common\models\Game;
use common\models\Task;

class TimelineController extends BaseController
{

    public function actionGame($gameId)
    {
        $this->checkAccessByGame($gameId);

        if (Game::findById($gameId)->isTeamable()) {
            $tasksId = ArrayHelper::map(Task::findByGameId($gameId), TaskFields::ID);
            $timelines = TimelineAssembler::fromCompletedTaskTeam(CompletedTaskTeam::findByTaskIdSortedByTime($tasksId));
            return $this->render('index', [
                'dataProvider' => $timelines
            ]);
        } else {
            $tasksId = ArrayHelper::map(Task::findByGameId($gameId), TaskFields::ID);
            $timelines = TimelineAssembler::fromCompletedTask(CompletedTask::findByTaskIdSortByTime($tasksId));
            return $this->render('index', [
                'dataProvider' => $timelines
            ]);
        }
    }

    public function actionUser($id, $gameId)
    {
        $this->checkAccessByGame($gameId);

        $tasksId = ArrayHelper::map(Task::findByGameId($gameId), TaskFields::ID);
        $timelines = TimelineAssembler::fromCompletedTask(CompletedTask::findByUserIdAndTaskId($id, $tasksId));
        return $this->render('index', [
            'dataProvider' => $timelines
        ]);
    }

    public function actionTeam($id, $gameId)
    {
        $this->checkAccessByGame($gameId);

        $tasksId = ArrayHelper::map(Task::findByGameId($gameId), TaskFields::ID);
        $timelines = TimelineAssembler::fromCompletedTaskTeam(CompletedTaskTeam::findByTeamIdAndTaskId($id, $tasksId));
        return $this->render('index', [
            'dataProvider' => $timelines
        ]);
    }
}
