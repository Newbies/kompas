<?php

namespace backend\controllers;

use backend\helpers\MailerHelper;
use backend\models\AddOneToGameForm;
use backend\models\CSVImportToGameForm;
use common\models\CompletedTask;
use common\models\fields\GameFields;
use common\models\fields\UserFields;
use common\models\fields\UserGameFields;
use common\models\Game;
use common\models\Task;
use common\models\User;
use common\models\UserGame;
use Yii;
use yii\base\ErrorException;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;

/**
 * UserController implements the CRUD actions for User model.
 */
class UserController extends BaseController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex($gameId)
    {
        $this->checkAccessByGame($gameId);
        $this->showEndGameFlash($gameId);

        $game = Game::findOne([GameFields::ID => $gameId]);
        $searchModel = new User(['scenario' => 'search']);
        $dataProvider = $searchModel->search($gameId, \Yii::$app->request->get());

        $canMakeTask = Task::findByGameId($gameId) ? true : false;

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'game' => $game,
            'canMakeTask' => $canMakeTask,
        ]);
    }

    public function actionImport($gameId)
    {
        $this->checkAccessByGame($gameId);
        $this->checkIfGameEnded($gameId);

        $model = new CSVImportToGameForm();
        $game = Game::findOne([GameFields::ID => $gameId]);

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->setUsersArray()) {
                $model->addUsersToGame($gameId);
                Yii::$app->session->setFlash('success', "Dodano użytkowników.");
                return $this->redirect('import');

            } else {
                Yii::$app->session->setFlash('error', "Błąd w strukturze *.csv.");
                return $this->redirect('import');
            }
        }
        return $this->render('import', [
            'model' => $model,
            'game' => $game,
        ]);
    }

    public function actionAdd($gameId)
    {
        $this->checkAccessByGame($gameId);
        $this->checkIfGameEnded($gameId);

        $model = new AddOneToGameForm();
        $game = Game::findOne([GameFields::ID => $gameId]);

        if ($model->load(Yii::$app->request->post())) {
            if ($model->addUserToGame($gameId)) {
                Yii::$app->session->setFlash('success', "Dodano użytkownika.");
                return $this->redirect('add');
            } else {
                Yii::$app->session->setFlash('error', "Uzupełnij dane.");
                return $this->redirect('add');
            }
        }
        return $this->render('add', [
            'model' => $model,
            'game' => $game,
        ]);
    }

    public function actionSelected($gameId)
    {
        $selection = (array)Yii::$app->request->post('selection');

        $type = Yii::$app->request->post('submit');

        if (empty($selection) && $type != 'send-invite-to-all') {
            Yii::$app->session->setFlash('error', "Nikogo nie wybrałeś.");
            return $this->redirect('/game/' . $gameId . '/user');
        }

        switch ($type) {
            case 'send-invited':
                $this->actionSendInvited($selection, $gameId);
                break;
            case 'make-task':
                $this->actionMakeTask($selection, $gameId);
                break;
            case 'send-invite-to-all':
                $this->actionSendInviteToAll($gameId);
                break;
            case 'delete-users':
                $this->actionDeleteFromGame($selection, $gameId);
                break;
        }
    }

    private function actionSendInvited($selectionUserId, $gameId)
    {
        $game = Game::findById($gameId);

        foreach ($selectionUserId as $userId) {
            $user = User::findById($userId);
            if ($user->status == User::STATUS_INACTIVE) {
                MailerHelper::sendActivationInvitation($user, $game);
                UserGame::findByUserIdAndGameId($userId, $gameId)->setToInvited();
            } else {
                if (MailerHelper::sendInviteToGame($user, $game)) {
                    UserGame::findByUserIdAndGameId($userId, $gameId)->setToInvited();
                }
            }

        }

        $this->redirect(['/game/' . $gameId . '/user']);
    }

    private function actionMakeTask($selectionUserId, $gameId)
    {
        $selectedTaskIds = Yii::$app->request->post('task-selecter');

        if (empty($selectedTaskIds)) {
            Yii::$app->session->setFlash('error', "Nie wybrałeś zadania");
            return $this->redirect('/game/' . $gameId . '/user');
        }
        try {
            foreach ($selectionUserId as $userId) {
                foreach ($selectedTaskIds as $taskId) {
                    if (!CompletedTask::findByUserIdAndTaskId($userId, $taskId)) {
                        $task = Task::findById($taskId);
                        CompletedTask::add($userId, $taskId);
                        UserGame::getByUserIdAndGame($userId, $task->game_id)->upScore($task->score);
                        Yii::$app->session->setFlash('success', "Zadania zostało zatwierdzone");
                    }
                }
            }
        } catch (ErrorException $e) {
        }
        $this->redirect(['/game/' . $gameId . '/user/']);
    }

    public function actionSendInviteToAll($gameId)
    {
        $game = Game::findById($gameId);

        $allUnInvitedUserGames = UserGame::find()->where([UserGameFields::GAME_ID => $gameId])->andWhere([UserGameFields::INVITE_STATUS => UserGameFields::INVITE_STATUS_ADD])->all();

        foreach ($allUnInvitedUserGames as $userGame) {

            $user = User::findById($userGame->user_id);

            if ($user->status == User::STATUS_INACTIVE) {
                MailerHelper::sendActivationInvitation($user, $game);
                $userGame->setToInvited();
            } else {
                if (MailerHelper::sendInviteToGame($user, $game)) {
                    $userGame->setToInvited();
                }
            }
        }
        $this->redirect(['/game/' . $gameId . '/user']);
    }

    public function actionDeleteFromGame($selectionsUserId, $gameId)
    {
        foreach ($selectionsUserId as $userId) {
            UserGame::deleteByUserIdAndGameId($userId, $gameId);
        }

        return $this->redirect(['/game/' . $gameId . '/user']);
    }

    protected function findModel($id)
    {
        $model = User::findOne([UserFields::ID => $id]);
        if ($model) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}


