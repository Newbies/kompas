<?php

namespace backend\controllers;

use common\models\Calendar;
use common\models\Game;
use common\models\Task;
use Yii;
use yii\web\NotFoundHttpException;

/**
 * TaskController implements the CRUD actions for Task model.
 */
class TaskController extends BaseController
{
    /**
     * Lists all Task models in Game.
     * @return mixed
     */
    public function actionIndex($gameId)
    {
        $this->checkAccessByGame($gameId);
        $this->showEndGameFlash($gameId);

        $game = Game::findById($gameId);
        $searchModel = new Task(['scenario' => 'search']);
        $dataProvider = $searchModel->search($gameId, \Yii::$app->request->get());

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'game' => $game
        ]);
    }

    /**
     * Displays a single Task model.
     * @param string $id
     * @return mixed
     */
    public function actionView($gameId, $id)
    {
        $this->checkAccessByGame($gameId);
        $game = Game::findById($gameId);

        return $this->render('view', [
            'model' => $this->findModel($id),
            'game' => $game,
        ]);
    }

    /**
     * Finds the Task model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Task the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        $model = Task::findById($id);
        if ($model) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('app', 'Taki obiekt nie istnieje.'));
        }
    }

    /**
     * Creates a new Task model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($gameId)
    {
        $this->checkAccessByGame($gameId);
        $this->checkIfGameEnded($gameId);

        $game = Game::findById($gameId);

        $model = new Task();

        $showNumber = count(Task::findByGameId($game->id)) + 1;
        $model->show_number = $showNumber;

        if ($model->load(Yii::$app->request->post())) {

            $model->show_number++;
            $model->game_id = $gameId;
            $model->task_owner = Yii::$app->user->identity->getId();
            $model->setTaskDetails();

            if ($model->validate()) {
                Task::upShowNumberBelow($gameId, $model->show_number);
                $model->save();
                return $this->redirect(['/game/' . $gameId . '/task/' . 'upload/' . $model->id]);
            }
        }

        return $this->render('create', [
            'model' => $model,
            'game' => $game,
            'showNumber' => $showNumber
        ]);
    }

    /**
     * Updates an existing Task model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */

    public function actionUpload($gameId, $id)
    {
        $this->checkAccessByGame($gameId);
        $this->checkIfGameEnded($gameId);

        $model = Task::findById($id);
        $game = Game::findById($gameId);

        if (Yii::$app->session->get('checkNext') == true) {
            $model->addNext = true;
        }

        if ($model->load(Yii::$app->request->post())) {
            $model->image = $model->getUploadImage();
            if ($model->validate()) {
                if ($model->image) {
                    $model->save();
                }
                Yii::$app->session->setFlash('success', Yii::t('app', 'Zadanie dodane poprawnie'));
                if ($model->addNext == true) {
                    Yii::$app->session->set('checkNext', true);
                    return $this->redirect(['/game/' . $gameId . '/task/create/']);
                } else {
                    Yii::$app->session->set('checkNext', false);
                    return $this->redirect(['/game/' . $gameId . '/task/' . $model->id]);
                }
            }
        }

        return $this->render('uploadImage', [
            'model' => $model,
            'game' => $game
        ]);
    }

    public function actionUpdate($gameId, $id)
    {
        $this->checkAccessByGame($gameId);
        $this->checkIfGameEnded($gameId);

        $game = Game::findById($gameId);
        $model = Task::findById($id);
        $showNumber = count(Task::findByGameId($game->id));

        $model->show_number--;

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {

            $model->show_number++;
            Task::upShowNumberBelow($gameId, $model->show_number);

            $model->setStatus();
            $model->save();

            Yii::$app->session->setFlash('success', Yii::t('app', 'Zadanie zedytowane.'));
            return $this->redirect(['/game/' . $gameId . '/task/view/' . $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
            'game' => $game,
            'showNumber' => $showNumber
        ]);
    }

    /**
     * Deletes an existing Task model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($gameId, $id)
    {
        $this->checkAccessByGame($gameId);
        Task::findOne($id)->delete();
        Yii::$app->session->setFlash('success', Yii::t('app', 'Zadanie usunięte.'));
        return $this->redirect("/game/" . $gameId . "/task");
    }

    public function actionUpdateImage($id, $gameId)
    {
        $model = Task::findById($id);
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->updateImage();
            return $this->redirect('/game/' . $gameId . '/task/' . $id);
        }
        return $this->renderAjax('/task/updateImage', [
            'model' => $model,
        ]);
    }

    public function actionRemoveImage($id, $gameId)
    {
        $task = Task::findById($id);
        $task->removeImage();
        return $this->redirect('/game/' . $gameId . '/task/' . $id);
    }
}
