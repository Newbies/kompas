<?php

namespace backend\controllers;

use backend\helpers\ArrayHelper;
use common\models\CompletedTask;
use common\models\CompletedTaskTeam;
use common\models\fields\GameFields;
use common\models\fields\TeamFields;
use common\models\fields\UserFields;
use common\models\fields\UserGameFields;
use common\models\Game;
use common\models\Task;
use common\models\Team;
use common\models\User;
use common\models\UserGame;
use Yii;
use yii\base\ErrorException;
use yii\data\ActiveDataProvider;
use yii\web\NotFoundHttpException;

class TeamController extends BaseController
{
    /**
     * Lists all Team models.
     * @return mixed
     */
    public function actionIndex($gameId)
    {
        $this->checkAccessByGame($gameId);
        $this->showEndGameFlash($gameId);

        $game = Game::findOne([GameFields::ID => $gameId]);
        $searchModel = new Team(['scenario' => 'search']);
        $dataProvider = $searchModel->search($gameId, \Yii::$app->request->get());

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'game' => $game
        ]);
    }

    /**
     * Displays a single Team model.
     * @param integer $id
     * @param integer $gameId
     * @param string $status
     * @return mixed
     */
    public function actionView($id, $gameId)
    {
        $this->checkAccessByGame($gameId);
        $this->showEndGameFlash($gameId);

        $team = $this->findModel($id);
        $game = Game::findById($gameId);

        $usersIdsInTeam = ArrayHelper::map(
            UserGame::find()
                ->where([UserGameFields::GAME_ID => $gameId])
                ->andWhere([UserGameFields::TEAM_ID => $team->id])
                ->all(),
            UserGameFields::USER_ID);

        $usersIdsInInGame = ArrayHelper::map(UserGame::find()
            ->where([UserGameFields::GAME_ID => $gameId])
            ->andWhere([UserGameFields::TEAM_ID => null])
            ->all(),
            UserGameFields::USER_ID);

        $usersInTeam = new ActiveDataProvider([
            'query' => User::find()
                ->where(['in', UserFields::ID, $usersIdsInTeam]),
        ]);

        $searchModel = new User(['scenario' => 'search']);
        $userparams = array_merge(
            ['in', UserFields::ID, $usersIdsInInGame],
            ['not in', UserFields::ID, $usersIdsInTeam],
            \Yii::$app->request->get());

        $usersInGame = $searchModel->searchToTeam($gameId, $userparams);

        return $this->render('view', [
            'model' => $this->findModel($id),
            'usersInTeam' => $usersInTeam,
            'usersInGame' => $usersInGame,
            'searchModel' => $searchModel,
            'game' => $game,
        ]);
    }

    /**
     * Finds the Team model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Team the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        $model = Team::findOne([TeamFields::ID => $id]);
        if ($model) {
            return $model;
        }
        throw new NotFoundHttpException('Szukany model nie istniej');
    }

    public function actionAdd($gameId, $id)
    {
        $this->checkAccessByGame($gameId);
        $this->checkIfGameEnded($gameId);

        $team = $this->findModel($id);

        $userId = Yii::$app->request->get('user_id');

        UserGame::findByUserIdAndGameId($userId, $gameId)->changeTeam($team->id);
        Yii::$app->session->setFlash('success', Yii::t('app', 'Dodanie gracza do drużyny powiodło się.'));
        return $this->redirect(['/game/' . $gameId . '/team/view', 'id' => $id]);
    }

    public function actionRemove($gameId, $id)
    {
        $this->checkAccessByGame($gameId);
        $this->checkIfGameEnded($gameId);

        $team = $this->findModel($id);

        $userId = Yii::$app->request->get('user_id');

        UserGame::findByUserIdAndGameId($userId, $gameId)->removeTeam();
        Yii::$app->session->setFlash('success', 'Usunięcie z drużyny gracza powiodło się');
        return $this->redirect(['/game/' . $gameId . '/team/view', 'id' => $id]);
    }

    public function actionCreate($gameId)
    {
        $this->checkAccessByGame($gameId);
        $this->checkIfGameEnded($gameId);

        $game = Game::findOne([GameFields::ID => $gameId]);

        $model = new Team();
        $model->game_id = $gameId;
        $model->score = 0;
        $model->last_update_score = time();

        if (Yii::$app->session->getFlash('checkNext')) {
            $model->addNext = true;
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', Yii::t('app', 'Drużyna dodana.'));
            if ($model->addNext) {
                Yii::$app->session->setFlash('checkNext');
                return $this->refresh();
            } else {
                return $this->redirect(['/game/' . $gameId . '/team/view', 'id' => $model->id]);
            }
        } else {
            return $this->render('create', [
                'model' => $model,
                'game' => $game,
            ]);
        }
    }

    public function actionUpdate($gameId, $id)
    {
        $this->checkAccessByGame($gameId);
        $this->checkIfGameEnded($gameId);

        $game = Game::findOne([GameFields::ID => $gameId]);

        $model = $this->findModel($id);
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', Yii::t('app', 'Drużyna zedytowana.'));
            return $this->redirect(['/game/' . $gameId . '/team/view', 'id' => $id]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'game' => $game,
            ]);
        }
    }

    public function actionDelete($id, $gameId)
    {
        $this->checkAccessByGame($gameId);

        UserGame::clearTeam($id);
        $this->findModel($id)->delete();
        Yii::$app->session->setFlash('success', Yii::t('app', 'Drużyna usunięta.'));
        return $this->redirect(['/game/' . $gameId . '/team']);

    }

    public function actionSelected($gameId)
    {
        $selection = (array)Yii::$app->request->post('selection');

        if (empty($selection)) {
            Yii::$app->session->setFlash('error', "Nikogo nie wybrałeś.");
            return $this->redirect('/game/' . $gameId . '/team');
        }

        $type = Yii::$app->request->post('submit');

        switch ($type) {
            case 'make-task':
                $this->actionMakeTask($selection, $gameId);
                break;
        }
    }

    private function actionMakeTask($selection, $gameId)
    {
        $selectedTaskIds = Yii::$app->request->post('task-selecter');

        if (empty($selectedTaskIds)) {
            Yii::$app->session->setFlash('error', "Nie wybrałeś zadania");
            return $this->redirect('/game/' . $gameId . '/team');
        }
        try {
            foreach ($selection as $teamId) {
                $userGames = UserGame::findByTeamId($teamId);
                foreach ($selectedTaskIds as $taskId) {
                    if (!CompletedTaskTeam::findByTeamIdAndTaskId($teamId, $taskId)) {
                        $task = Task::findById($taskId);
                        CompletedTaskTeam::add($taskId, $taskId, Yii::$app->user->id);
                        foreach ($userGames as $userGame) {
                            CompletedTask::add($userGame[UserGameFields::USER_ID], $taskId);
                            $userGame->upScore($task->score);
                        }
                    }
                }
            }
        } catch (ErrorException $e) {
        }
        Yii::$app->session->setFlash('success', "Zadanie zostało wykonane.");
        return $this->redirect('/game/' . $gameId . '/team');
    }
}