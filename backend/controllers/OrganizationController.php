<?php

namespace backend\controllers;

use backend\helpers\ArrayHelper;
use backend\models\InviteToAdminForm;
use common\models\fields\OrganizationFields;
use common\models\fields\UserFields;
use common\models\fields\UserOrgFields;
use common\models\ForceLogout;
use common\models\Organization;
use common\models\User;
use common\models\UserOrg;
use Yii;
use yii\data\ActiveDataProvider;
use yii\web\NotFoundHttpException;

/**
 * OrganizationController implements the CRUD actions for Organization model.
 */
class OrganizationController extends BaseController
{

    public function actionIndex()
    {
        $organizations = new ActiveDataProvider([
            'query' => Organization::find()
                ->where(['in',
                    OrganizationFields::ID,
                    ArrayHelper::map(
                        UserOrg::findAll([UserOrgFields::USER_ID => Yii::$app->user->identity->getId()]),
                        UserOrgFields::ORG_ID)])]);

        return $this->render('index', [
            'organizations' => $organizations,
        ]);
    }

    /**
     * Displays a single Organization model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $this->checkAccessToOrganization($id);

        $adminsId = ArrayHelper::map(UserOrg::findAll([UserOrgFields::ORG_ID => $id]), UserOrgFields::USER_ID);

        $admins = new ActiveDataProvider([
            'query' => User::find()->where(['in', UserFields::ID, $adminsId])
        ]);


        return $this->render('view', [
            'model' => $this->findModel($id),
            'admins' => $admins,
        ]);
    }

    /**
     * Finds the Organization model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Organization the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Organization::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Wybrany element nie istnieje');
        }
    }

    /**
     * Creates a new Organization model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $this->checkAccessToCreateOrganization();
        $user = User::getLogged();

        $model = new Organization();

        if ($model->load(Yii::$app->request->post())) {
            $model->owner_user_id = $user->id;
            if ($model->save()) {
                $user->addToOrganization($model->id);
                Yii::$app->session->setFlash('success', Yii::t('app', 'Stworzenie organizacji 
                    powiodło się i zostałeś automatycznie do niej przydzielony.'));

                return $this->redirect(['view', 'id' => $model->id]);
            }
        }
        return $this->render('create', [
            'model' => $model,
        ]);


    }

    /**
     * Updates an existing Organization model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $this->checkAccessToOrganization($id);

        $model = $this->findModel($id);
        if ($model->load(Yii::$app->request->post())) {
            $model->update();
            return $this->redirect(['view', 'id' => $model->id]);
        }
        return $this->render('update', [
            'model' => $model,
        ]);

    }

    /**
     * Deletes an existing Organization model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->checkAccessToOrganization($id);
        $this->findModel($id)->delete();
        return $this->redirect(['/']);
    }


    public function actionLeave($orgId)
    {
        $this->checkAccessToOrganization($orgId);

        $user = User::getLogged();
        $user->leaveOrganization($orgId);
        $user->downgradeFromAdmin();
        return $this->redirect('/');
    }

    public function actionInvite($orgId)
    {
        $this->checkAccessToOrganization($orgId);

        $org = Organization::findById($orgId);

        $model = new InviteToAdminForm();

        if ($model->load(Yii::$app->request->post()) && $model->invite($orgId)) {
            return $this->refresh();
        }

        return $this->render('invite', [
            'model' => $model,
            'org' => $org
        ]);
    }

    public function actionDiscard($orgId, $userId)
    {
        $org = Organization::findById($orgId);
        $org->discardUser($userId);
        ForceLogout::addUser($userId);
        Yii::$app->session->setFlash('success', Yii::t('app', 'Administrator został usunięty z organizacji.'));
        $this->redirect(['view', 'id' => $orgId]);
    }

    public function actionUpdateLogo($id)
    {
        $model = Organization::findById($id);
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
           $model->updateLogo();
            return $this->redirect('/organization/' . $id);
        }
        return $this->renderAjax('/organization/updateImage', [
            'model' => $model,
        ]);
    }

    public function actionRemoveLogo($id)
    {
        $task = Organization::findById($id);
        $task->removeLogo();
        return $this->redirect('/organization/' . $id);
    }
}
