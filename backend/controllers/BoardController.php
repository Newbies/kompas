<?php
/**
 * Created by PhpStorm.
 * User: janpa
 * Date: 2017-05-21
 * Time: 18:29
 */

namespace backend\controllers;

use common\models\fields\TeamFields;
use common\models\fields\UserGameFields;
use common\models\Game;
use common\models\Team;
use common\models\UserGame;
use yii\data\ActiveDataProvider;


class BoardController extends BaseController
{
    public function actionIndex($gameId)
    {
        $this->checkAccessByGame($gameId);

        $game = Game::findById($gameId);

        if ($game->isTeamable()) {

            $query = Team::find()->where([TeamFields::GAME_ID => $gameId]);

            return $this->render('teamBoard', [
                'dataProvider' => $this->prepareAciveDataProvider($query),
                'game' => $game
            ]);
        }

        $query = UserGame::find()->where([UserGameFields::GAME_ID => $gameId]);

        return $this->render('soloBoard', [
            'dataProvider' => $this->prepareAciveDataProvider($query),
            'game' => $game
        ]);
    }


    private function prepareAciveDataProvider($query)
    {
        return new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 25,
            ],
            'sort' => [
                'defaultOrder' => [
                    'score' => SORT_DESC,
                    'last_update_score' => SORT_ASC,
                    'id' => SORT_DESC,
                ]
            ]
        ]);
    }
}