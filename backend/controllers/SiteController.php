<?php

namespace backend\controllers;

use backend\helpers\ArrayHelper;
use backend\models\LoginForm;
use backend\models\SignupForm;
use common\models\fields\GameFields;
use common\models\fields\UserFields;
use common\models\fields\UserOrgFields;
use common\models\Game;
use common\models\User;
use common\models\UserOrg;
use frontend\models\EmailVerificationForm;
use frontend\models\FinishRegistrationForm;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\BadRequestHttpException;
use yii\web\Controller;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error', 'registration', 'email-verification', 'finish-registration'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index', 'game', 'invite', 'disabled', 'settings'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionRegistration()
    {
        $this->layout = 'login';
        $model = new SignupForm();

        if ($email = Yii::$app->request->get('email')) {
            $user = User::findByEmail($email);
            $model->id = $user->id;
            $model->email = $user->email;
            $model->name = $user->name;
            $model->lastName = $user->last_name;
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $user = $model->signup()) {
            return $this->redirect(['email-verification', 'id' => $user->id]);
        }
        return $this->render('registration', [
            'model' => $model
        ]);
    }

    public function actionEmailVerification($id)
    {
        $this->layout = "login";
        $user = User::findById($id);
        $userBirthDate = Yii::$app->session->get('userBirthDate');

        if ($user == null) {
            throw new BadRequestHttpException(Yii::t('app', 'Błędne ID użytkownika'));
        }

        $model = new EmailVerificationForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->registerByCode($user)) {
            if (User::isUserIndependent($userBirthDate)) {
                return $this->redirect(['site/finish-registration/', 'id' => $user->id]);
            }
            Yii::$app->session->setFlash('error', Yii::t('app', 'Nie możesz założyć konta, nie masz skończone 16 lat'));
            return $this->redirect('registration');
        }
        return $this->render('emailVerification', [
            'model' => $model
        ]);
    }

    public function actionFinishRegistration($id)
    {
        $this->layout = "login";
        $model = new FinishRegistrationForm();
        $user = User::findById($id);

        if($user == null) {
            throw new BadRequestHttpException(Yii::t('app', 'Błędne ID użytkownika'));
        }

        if($user->register_code) {
            throw new BadRequestHttpException(Yii::t('app', 'Konto nie zostało aktywowane'));
        }

        if($model->load(Yii::$app->request->post()) && $model->validate() && $model->setToActive($user))
        {
            Yii::$app->session->setFlash('success', Yii::t('app', 'Założyłeś konto! teraz możesz się zalogować.'));
            return $this->redirect('login');
        }
        return $this->render('finishRegistration', [
            'model' => $model
        ]);
    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goHome();
        } else {

            $this->layout = "login";
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $user = User::findOne([UserFields::ID => Yii::$app->user->identity->getId()]);

        if (!$user->hasOrganization()) {
            return $this->render('noneOrganization');
        }


        $userGames = Game::find()
            ->where(['in', GameFields::OWNER_ORGANIZATION_ID,
                ArrayHelper::map(UserOrg::findAll([UserOrgFields::USER_ID => Yii::$app->user->identity->getId()]), UserOrgFields::ORG_ID)])
            ->all();

        if (!empty($userGames)) {

            $data = [];

            $userOrgsId = ArrayHelper::map(UserOrg::findAll([UserOrgFields::USER_ID => Yii::$app->user->identity->getId()]), UserOrgFields::ORG_ID);

            foreach ($userOrgsId as $userOrgId) {
                $orgStats[0] = $userOrgId;
                $orgStats[1] = Game::find()->where([GameFields::OWNER_ORGANIZATION_ID => $userOrgId])->andWhere([GameFields::STATUS => Game::STATUS_FINISHED])->count();
                $orgStats[2] = Game::find()->where([GameFields::OWNER_ORGANIZATION_ID => $userOrgId])->andWhere([GameFields::STATUS => Game::STATUS_CURRENT])->count();
                $orgStats[3] = Game::find()->where([GameFields::OWNER_ORGANIZATION_ID => $userOrgId])->andWhere([GameFields::STATUS => Game::STATUS_FUTURE])->count();
                array_push($data, $orgStats);
            }
            return $this->render('index', [
                'data' => $data,
            ]);
        }
        return $this->render('emptySite');

    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionDisabled()
    {
        return $this->render('disabledFunctions');
    }

    public function actionSettings()
    {
        return $this->render('settings');
    }
}
