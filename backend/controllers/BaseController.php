<?php

namespace backend\controllers;

use common\models\fields\GameFields;
use common\models\fields\UserOrgFields;
use common\models\ForceLogout;
use common\models\Game;
use common\models\User;
use common\models\UserOrg;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\HttpException;


class BaseController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['add', 'remove', 'index', 'view', 'create', 'update', 'delete', 'list', 'score',
                            'join', 'leave', 'invite', 'discard', 'update-image', 'remove-image', 'update-logo',
                            'remove-logo','timeline','game','user','team','make-manual-task','import','add',
                            'selected', 'send-invite-to-all','copy', 'upload'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function beforeAction($action)
    {
        $user = Yii::$app->user;

        if (ForceLogout::findByUserId($user->id)) {
            ForceLogout::removeUser($user->id);
            $user->logout();
        }
        return parent::beforeAction($action);
    }


    public function checkAccessByGame($gameId)
    {
        $game = Game::findOne([GameFields::ID => $gameId]);


        if (!UserOrg::find()
            ->where([UserOrgFields::USER_ID => Yii::$app->user->identity->getId()])
            ->andWhere([UserOrgFields::ORG_ID => $game->owner_organization_id])
            ->all()) {
            throw new HttpException('403', Yii::t('app', 'Nie masz uprawnień.'));
        }
    }

    public function showEndGameFlash($gameId)
    {
        if (Game::findOne([GameFields::ID => $gameId])->isEnd()) {
            return Yii::$app->session->setFlash('error', Yii::t('app', 'Ta gra jest już zakończona, może to spowodować wyłączenie niektórych funkcji. ') .
                '<a href="' . Url::to(['/site/disabled']) . '">' . Yii::t('app', 'Zobacz których') . '</a>');
        }
    }

    public function checkIfGameEnded($gameId)

    {
        if (Game::findOne([GameFields::ID => $gameId])->isEnd()) {
            throw new HttpException('423', Yii::t('app', 'Ta funkcja dla gier zakończonych jest zablokowana.'));

        }
    }

    public function checkAccessToOrganization($organizationId)
    {
        if (!UserOrg::findAll([UserOrgFields::ORG_ID => $organizationId, UserOrgFields::USER_ID => Yii::$app->user->id])) {
            throw new HttpException('403', Yii::t('app', 'Nie masz uprawnień.'));
        }
    }

    public function checkAccessToCreateOrganization()
    {
        if (User::getLogged()->role != 2) {
            throw new HttpException('403', Yii::t('app', 'Nie masz uprawnień.'));
        }
    }

}