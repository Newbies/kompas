$('#droplist').change(function () {
    $('#search').submit();
});

$('#typeDropList').change(function () {
        if (this.value == 0) {
            $('#typeDiv').addClass('hide-options');
        } else {
            $('#typeDiv').removeClass('hide-options');
        }
    }
);

$('#checkPremium').click(function () {
        if (!this.checked) {
            $('#typeDiv').addClass('hide-options');
        } else {
            $('#typeDiv').removeClass('hide-options');
        }
    }
);

$('table input').on('click, change', function(){
    if ($(this).is(':checked') == true){
        $(this).parent().addClass('active-check');
    } else {
        $(this).parent().removeClass('active-check');
    }
});

$('input.select-on-check-all').on('click, change', function(){
    if ($(this).is(':checked') == true){
        $('table input[type="checkbox"').parent().addClass('active-check');
    } else {
        $('table input[type="checkbox"').parent().removeClass('active-check');
    }
});

$('#game-grid-filters').ready(function(){
  $(this).find('input').parent().addClass('input-search');
});

$('#typeTaskDropList').change(function () {
    if (this.value == 0) {
        $('#premiumDiv').addClass('hide-options');
        $('#timedDiv').addClass('hide-options');
    }
    else if (this.value == 1){
        $('#timedDiv').addClass('hide-options');
        $('#premiumDiv').removeClass('hide-options');
    }
    else if(this.value == 2){
        $('#timedDiv').removeClass('hide-options');
        $('#premiumDiv').addClass('hide-options');
    }

});
