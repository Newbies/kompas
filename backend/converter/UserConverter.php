<?php

namespace backend\converter;

class UserConverter
{
    public static function toArrayIdShowAs($users)
    {
        $array = array();
        foreach ($users as $user) {
            $array[$user->id] = $user->showMe();
        }
        return $array;
    }
}