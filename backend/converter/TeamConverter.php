<?php

namespace backend\converter;

class TeamConverter
{
    public static function toArrayIdName($teams)
    {
        $array = array();
        foreach ($teams as $team) {
            $array[$team->id] = $team->name;
        }
        return $array;
    }
}