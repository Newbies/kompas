<?php

namespace backend\converter;

class TaskConverter
{
    public static function toArrayIdName($tasks)
    {
        if($tasks){
            $array = array();
            foreach ($tasks as $task) {
                $array[$task->id] = $task->name;
            }
            return $array;
        }
        return ['brak zadań'];
    }
}