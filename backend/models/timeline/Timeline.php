<?php
/**
 * Created by PhpStorm.
 * User: mBoiler
 * Date: 18.11.2017
 * Time: 21:13
 */

namespace backend\models\timeline;


use common\models\Task;
use common\models\Team;
use common\models\User;
use Couchbase\Exception;
use Yii;
use yii\base\Model;

interface Timeline
{
    function getName();
    function getContent();
    function getTime();

}