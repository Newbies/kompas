<?php
/**
 * Created by PhpStorm.
 * User: mBoiler
 * Date: 18.11.2017
 * Time: 21:13
 */

namespace backend\models\timeline;


use common\models\Task;
use common\models\User;
use Yii;
use yii\base\Model;

class TimelineSolo extends Model implements Timeline
{
    private $when;
    private $who;
    private $what;
    private $task = null;

    /**
     * Timeline constructor.
     * @param $time
     * @param $who_id
     * @param $task_id
     */
    public function __construct($time, $who_id, $task_id)
    {
        $this->when = $time;
        $this->who = $who_id;
        $this->what = $task_id;
    }

    public function getContent()
    {
        return Yii::t('app', 'Wykonał/a zadanie ')
            . '<b>' . $this->getTaskName() . '</b>'
            . Yii::t('app', ' za ')
            . '<b>' . $this->getTaskScore() . '</b>'
            . Yii::t('app', ' punktów');
    }

    public function getTaskName()
    {
        if ($this->task == null) {
            $this->task = Task::findById($this->what);
        }
        return $this->task ? $this->task->name : Yii::t('app', '[Nieznany]');
    }

    public function getTaskScore()
    {
        if ($this->task == null) {
            $this->task = Task::findById($this->what);
        }
        return $this->task ? $this->task->score : Yii::t('app', '[Nieznany]');
    }

    function getName()
    {
        $user = User::findById($this->who);
        return $user ? $user->showMe() : Yii::t('app', '[Nieznany]');
    }

    function getTime()
    {
        return $this->when ? date("d-m H:i:s", $this->when) : Yii::t('app', '[Nieznany]');
    }
}