<?php

namespace backend\models;

use backend\helpers\CSVReader;
use common\models\fields\UserFields;
use common\models\User;
use common\models\UserGame;
use yii\base\Model;
use yii\web\UploadedFile;

class CSVImportToGameForm extends Model
{
    public $csvFile;
    public $usersArray;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['csvFile'], 'file', 'extensions' => 'csv'],
            [['csvFile'], 'required', 'message' => 'Zaimportuj plik'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'csvFile' => 'Plik *.csv z danymi'
        ];
    }

    public function setUsersArray()
    {
        $this->csvFile = UploadedFile::getInstance($this, 'csvFile');
        $csvPath = 'uploads/' . $this->csvFile->baseName . '.' . $this->csvFile->extension;
        $this->csvFile->saveAs($csvPath);
        $this->usersArray = CSVReader::getUsersAsArray($csvPath);
        unlink($csvPath);

        if ($this->usersArray) {
            return true;
        }
        return false;
    }

    public function addUsersToGame($gameId)
    {
        foreach ($this->usersArray as $csv) {

            if (!self::validateEmail($csv['email'])) {
                continue;
            }
            $user = User::findByEmail($csv['email']);
            if (!$user) {
                $user = User::create($csv['email'], $csv['name'], $csv['last_name']);
            }
            UserGame::addUserToGame($user->id, $gameId, $csv['tag']);;
        }
    }

    public static function validateEmail($email)
    {
        return boolval((filter_var($email, FILTER_VALIDATE_EMAIL)
            && preg_match('/@.+\./', $email)));
    }
}
