<?php
namespace backend\models;

use yii\base\Model;

class SearchForm extends Model
{
    public $sort;

    public function attributeLabels()
    {
        return [
            'sort' => 'Filtry',
            //PURIFIER FILTER
            [['sort'],
                'filter','filter'=>'\yii\helpers\HtmlPurifier::process']
        ];
    }
}
