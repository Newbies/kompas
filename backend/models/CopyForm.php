<?php

namespace backend\models;

use common\models\User;
use Yii;
use yii\base\Model;


class CopyForm extends Model
{
    public $name ;
    public $startDate ;
    public $endDate;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['name'], 'string'],
            [['name'], 'required'],

            [['startDate'], 'required'],
            [['startDate'], 'safe'],
            [['endDate'], 'required'],
            [['endDate'], 'safe'],
            [['endDate'], 'validateDates'],

            [['name','endDate','startDate'],
                'filter', 'filter' => '\yii\helpers\HtmlPurifier::process']
        ];
    }

    public function validateDates()
    {
        if ($this->endDate <= $this->startDate) {
            $this->addError('startDate', Yii::t('app', 'Data zakończenia musi być większa niż data rozpoczęcia.'));
            $this->addError('endDate', Yii::t('app', 'Data zakończenia musi być większa niż data rozpoczęcia.'));
        }
    }

    public function attributeLabels()
    {
        return [
            'name' => Yii::t('app','Nowa nazwa'),
        ];
    }
}
