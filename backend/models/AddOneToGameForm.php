<?php

namespace backend\models;

use common\models\fields\UserFields;
use common\models\User;
use common\models\UserGame;
use Yii;
use yii\base\Model;

class AddOneToGameForm extends Model
{
    public $email;
    public $name;
    public $last_name;
    public $tag;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['email'], 'email'],
            [['email'], 'required'],
            [['name'], 'string', 'max' => 45],
            [['last_name'], 'string', 'max' => 45],
            [['tag'], 'string', 'max' => 45],

            //PURIFIER FILTER
            [['email'],
                'filter', 'filter' => '\yii\helpers\HtmlPurifier::process']
        ];
    }

    public function attributeLabels()
    {
        return [
            'email' => Yii::t('app', 'Email'),
            'name' => Yii::t('app', 'Imię'),
            'last_name' => Yii::t('app', 'Nazwisko'),
            'tag' => Yii::t('app', 'Etykieta'),
        ];
    }

    public function addUserToGame($gameId)
    {
        if ($existingUser = User::findByEmail($this->email)) {
            UserGame::addUserToGame($existingUser->id, $gameId, $this->tag);
            return true;
        } else {
            $newUser = $this->createUser($this->email, $this->name, $this->last_name);
            UserGame::addUserToGame($newUser->id, $gameId, $this->tag);
            return true;
        }
    }

    public static function createUser($email, $name, $last_name)
    {
        return User::create($email, $name, $last_name, User::STATUS_INACTIVE, false);
    }

}
