<?php

namespace backend\models;

use common\models\User;
use DateTime;
use frontend\helpers\MailHelper;
use himiklab\yii2\recaptcha\ReCaptchaValidator;
use Yii;
use yii\base\Model;

/**
 * Signup form
 */
class SignupForm extends Model
{
    public $email;
    public $name;
    public $lastName;
    public $reCaptcha;
    public $status;
    public $id;
    public $birthDay;
    public $birthMonth;
    public $birthYear;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['name', 'trim'],
            ['name', 'required'],
            ['name', 'string', 'max' => 255],
            ['name', 'nameValidation'],

            ['lastName', 'trim'],
            ['lastName', 'required'],
            ['lastName', 'string', 'max' => 255],
            ['lastName', 'lastNameValidation'],

            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email', 'message' => Yii::t('app', 'To nie jest email')],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'when' => function ($model) {
                return !$this->id;
            },
                'targetClass' => '\common\models\User',
                'message' => Yii::t('app', 'Ten email jest już w użytku')],

            [['birthDay', 'birthMonth', 'birthYear'], 'required'],
            [['birthDay'], 'integer', 'min' => 1, 'max' => 31],
            [['birthDay'], 'birthDayValidation'],
            [['birthMonth'], 'integer', 'min' => 1, 'max' => 12],
            [['birthYear'], 'integer', 'min' => 1900, 'max' => date('Y')],


            ['status', 'integer'],
            [['reCaptcha'], ReCaptchaValidator::className(), 'secret' => '6LdadUsUAAAAACbUzIaFI0J2LtbEhe1b2SBHpxLw', 'uncheckedMessage' => 'Potwierdź że jesteś człowiekiem.'],

            //PURIFIER FILTER
            [['email', 'name', 'lastName', 'birthDay', 'birthMonth', 'birthYear'],
                'filter', 'filter' => '\yii\helpers\HtmlPurifier::process']
        ];
    }


    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'name' => Yii::t('app', 'Imię'),
            'email' => Yii::t('app', 'Email'),
            'lastName' => Yii::t('app', 'Nazwisko'),
            'reCaptcha' => Yii::t('app', 'Potwierdź że jesteś człowiekiem'),
            'birthDay' => Yii::t('app', 'Dzień urodzenia'),
            'birthMonth' => Yii::t('app', 'Miesiąc urodzenia'),
            'birthYear' => Yii::t('app', 'Rok urodzenia'),
        ];
    }

    public function birthDayValidation($attribute_name)
    {
        $birthDate = new DateTime("$this->birthYear" . "-" . "$this->birthMonth" . "-" . "$this->birthDay");
        if (User::isUserIndependent($birthDate)) {
            return true;
        }
        $this->addError($attribute_name, Yii::t('app', 'Musisz mieć skończone 16 lat. '));
        $this->addError('birthMonth', Yii::t('app', ''));
        $this->addError('birthYear', Yii::t('app', ''));
    }

    /**
     * Validate user Name by regex
     * @param string
     * @return null
     */
    public function nameValidation($attribute_name)
    {
        if (preg_match('/^[\p{L}-]*$/u', $this->name)) {
            return true;
        }
        $this->addError($attribute_name, Yii::t('app', 'Imię może zawierać tylko litery'));
    }

    /**
     * Validate user Last Name by regex
     * @param string
     * @return null
     */
    public function lastNameValidation($attribute_name)
    {
        if (preg_match('/^[\p{L}-]*$/u', $this->lastName)) {
            return true;
        }
        $this->addError($attribute_name, Yii::t('app', 'Nazwisko może zawierać tylko litery'));
    }

    /**
     * Signs user up
     * @return User
     */
    public function signup(): User
    {
        $birthDate = new DateTime("$this->birthYear" . "-" . "$this->birthMonth" . "-" . "$this->birthDay");
        Yii::$app->session->set('userBirthDate', $birthDate);

        if ($this->id == null) {
            $user = User::create($this->email, $this->name, $this->lastName, User::ROLE_SUPER_ADMIN);
            if (MailHelper::sendRegisterCode($user)) {
                return $user;
            }
        }
        $user = User::findById($this->id);
        return $user;
    }
}
