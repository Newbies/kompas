<?php

namespace backend\models;

use common\models\fields\OrganizationFields;
use common\models\fields\UserFields;
use common\models\fields\UserOrgFields;
use common\models\Organization;
use common\models\User;
use common\models\UserOrg;
use Yii;
use yii\base\Model;

/**
 * Login form
 */
class InviteToAdminForm extends Model
{
    public $email;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['email'], 'email', 'message' => 'To nie jest prawidłowy adres email'],

            //PURIFIER FILTER
            [['email'],
                'filter', 'filter' => '\yii\helpers\HtmlPurifier::process']
        ];
    }

    public function attributeLabels()
    {
        return [
            'email' => Yii::t('app', 'Email'),
        ];
    }

    public function invite($orgId)
    {
        $org = Organization::findOne([OrganizationFields::ID => $orgId]);

        if($org->isFull()){
            Yii::$app->session->setFlash('error',Yii::t('app','Osiągnięto limit administratorów dla tej organizacji.'));
            return false;
        }
        $user = User::findOne([UserFields::EMAIL=>$this->email]);

        if($user == null){
            Yii::$app->session->setFlash('error',Yii::t('app','Nie znaleziono takiego użytkownika.'));
            return false;
        }

        if(UserOrg::findOne([UserOrgFields::USER_ID=>$user->id,UserOrgFields::ORG_ID=>$orgId])){
            Yii::$app->session->setFlash('error',Yii::t('app','Ten administrator jest już w tej organizacji.'));
            return false;
        }

        $user->addToOrganization($orgId);
        $user->upgradeToAdmin();
        Yii::$app->session->setFlash('success',Yii::t('app','Administrator został dodany do organizacji.'));
        return true;
    }

    public function sendMail($email, $game)
    {

    }
}
