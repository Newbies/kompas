<?php
/**
 * Created by PhpStorm.
 * User: mBoiler
 * Date: 11.12.2017
 * Time: 19:40
 */

namespace backend\models;


use backend\converter\TeamConverter;
use backend\converter\UserConverter;
use backend\helpers\ArrayHelper;
use common\models\CompletedTask;
use common\models\CompletedTaskTeam;
use common\models\fields\UserGameFields;
use common\models\Game;
use common\models\Task;
use common\models\Team;
use common\models\User;
use common\models\UserGame;
use Yii;
use yii\base\Model;

class MakeManualTask extends Model
{
    public $selectedValue;
    public $gameId;
    public $taskId;
    public $score;

    public function __construct($gameId, $taskId)
    {
        $this->gameId = $gameId;
        $this->taskId = $taskId;
        $this->score = Task::findById($taskId)->score;
    }

    public function rules()
    {
        return [
            ['selectedValue', 'required']
        ];
    }

    public function getData(): array
    {
        $game = Game::findById($this->gameId);
        if ($game->isTeamable()) {
            return TeamConverter::toArrayIdName(Team::findByGameId($this->gameId));
        } else {
            return UserConverter::toArrayIdShowAs(
                User::findByIds(ArrayHelper::map(UserGame::findByGameId($this->gameId), UserGameFields::USER_ID)));
        }
    }

    public function doTask()
    {
        if(empty($this->selectedValue)){
            return false;
        }

        $game = Game::findById($this->gameId);
        if ($game->isTeamable()) {
            foreach ($this->selectedValue as $value) {
                if ($this->checkCompleteTaskTeam($value, $this->taskId)) {
                    CompletedTaskTeam::add($value, $this->taskId, Yii::$app->user->id);
                    $userGames = UserGame::findByTeamId($value);
                    foreach ($userGames as $userGame) {
                        CompletedTask::add($userGame[UserGameFields::USER_ID], $this->taskId);
                        $userGame->upScore($this->score);
                    }
                }
            }
            return true;
        } else {
            foreach ($this->selectedValue as $value) {
                if ($this->checkCompleteTask($value, $this->taskId)) {
                    CompletedTask::add($value, $this->taskId);
                    $userGame = UserGame::getByUserIdAndGame($value, $this->gameId);
                    $userGame->upScore($this->score);
                }
            }
            return true;
        }
    }

    public function checkCompleteTaskTeam($teamId, $taskId)
    {
        if (CompletedTaskTeam::findByTeamIdAndTaskId($teamId, $taskId)) {
            return false;
        }
        return true;
    }

    public function checkCompleteTask($userId, $taskId)
    {
        if (CompletedTask::findByUserIdAndTaskId($userId, $taskId)) {
            return false;
        }
        return true;
    }
}