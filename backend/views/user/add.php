<?php
use backend\helpers\BreadcrumbHelper;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

$breadcrumb = new BreadcrumbHelper();


?>
<!-- BREADCRUMB -->
<section class="content-header">
    <ol class="breadcrumb">
        <?= $breadcrumb->home(); ?>
        <?= $breadcrumb->game($game); ?>
        <?= $breadcrumb->addUserByForm() ?>
    </ol>
</section>
<!-- /BREADCRUMB -->

<section class="content">

    <div class="site-invite">
        <div class="row">
            <div class="col-lg-5">
                <h1> <?= Yii::t('app', 'Dodajesz do gry') ?>
                    <b><?= $game->name ?></b>
                </h1>

                <?php $form = ActiveForm::begin(['id' => 'form-invite']); ?>

                <?= $form->field($model, 'email')->textInput(['autofocus' => true]) ?>
                <?= $form->field($model, 'name')->textInput(['placeholder' => 'Opcjonalnie','autofocus' => true])?>
                <?= $form->field($model, 'last_name')->textInput(['placeholder' => 'Opcjonalnie','autofocus' => true])?>
                <?= $form->field($model, 'tag')->textInput(['placeholder' => 'Opcjonalnie','autofocus' => true])?>

                <div class="form-group">
                    <?= Html::submitButton(Yii::t('app', 'Dodaj'), ['class' => 'btn btn-primary', 'name' => 'add-by-form']) ?>
                </div>

                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>