<?php

use backend\converter\TaskConverter;
use backend\helpers\BreadcrumbHelper;
use common\models\Task;
use dosamigos\multiselect\MultiSelect;
use yii\bootstrap\Modal;
use yii\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel common\models */
/* @var $dataProvider yii\data\ActiveDataProvider */

$breadcrumb = new BreadcrumbHelper();
$this->params['gameId'] = $game->id;

?>
<!-- BREADCRUMB -->
<section class="content-header">
    <h1><?= Yii::t('app', 'Gracze w grze ') . $game->name ?></h1>
    <ol class="breadcrumb">
        <?= $breadcrumb->home(); ?>
        <?= $breadcrumb->game($game); ?>
        <?= $breadcrumb->users(); ?>
    </ol>
</section>
<!-- /BREADCRUMB -->

<section class="content">
    <div class="user-index box container box-header with-border">
        <a href="<?= \yii\helpers\Url::to(['game/' . $game->id . '/user/import']) ?>"
           class="btn btn-success" <?php if ($game->isEnd()): echo 'disabled'; endif; ?>">
        <?= Yii::t('app', 'Zaimportuj do gry') ?>
        </a>
        <a href="<?= \yii\helpers\Url::to(['game/' . $game->id . '/user/add']) ?>"
           class="btn btn-success" <?php if ($game->isEnd()): echo 'disabled'; endif; ?>">
        <?= Yii::t('app', 'Dodaj gracza') ?>
        </a>
        <?= Html::beginForm(['user/selected', 'gameId' => $game->id], 'post'); ?>
        <?php if (!$game->isTeamable()): ?>
            <a class="btn btn-primary " data-toggle="modal"
               data-target="#make-task"><?= Yii::t('app', 'Wykonaj zadanie') ?></a>
        <?php endif; ?>
        <a class="btn btn-primary" data-toggle="modal"
           data-target="#send-invited"><?= Yii::t('app', 'Wyślij zaproszenie') ?></a>
        <a class="btn btn-warning" data-toggle="modal"
           data-target="#send-invite-to-all"><?= Yii::t('app', 'Wyślij zaproszenie do wszystkich') ?></a>
        <a class="btn btn-danger " data-toggle="modal"
           data-target="#delete-user"><?= Yii::t('app', 'Usuń z gry') ?></a>
        <div class="box-body">
            <?php
            echo GridView::widget([
                'id' => 'game-grid',
                'emptyText' => Yii::t('app', 'Brak użytkowników'),
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'tableOptions' => ['class' => 'table no-margin small-view sortable table-responsive table table-hover task-table'],
                'columns' => [
                    [
                        'class' => 'yii\grid\CheckboxColumn',
                        'contentOptions' => [
                            'class' => 'checkbox-table',
                        ],
                        'headerOptions' => [
                            'class' => 'checkbox-table',
                        ],
                    ],
                    [
                        'attribute' => 'name',
                        'format' => 'text',
                    ],
                    [
                        'attribute' => 'last_name',
                        'format' => 'text',
                    ],
                    [
                        'attribute' => 'email',
                        'format' => 'email',
                    ],
                    [
                        'attribute' => 'tag',
                        'format' => 'text',
                        'value' => function ($model) {
                            $userGame = \common\models\UserGame::findByUserIdAndGameId($model, $this->params['gameId']);
                            return $userGame->tag;
                        },
                        'filter' => HTML::activeInput("text", $searchModel, "tag", ['class' => 'form-control'])
                    ],
                    [
                        'attribute' => 'inviteStatus',
                        'format' => 'html',
                        'label' => "Status",
                        'value' => function ($model) {
                            $userGame = \common\models\UserGame::findByUserIdAndGameId($model, $this->params['gameId']);
                            return '<span class="invite-status-' . Html::encode($userGame->invite_status) . '">' . $userGame->getInvitedStatusText() . '</span>';
                        },
                        'filter' => Html::activeDropDownList($searchModel, 'inviteStatus', [0 => "Dodany", 1 => "Zaproszony", 2 => "Aktywowany"], ['class' => 'form-control', 'prompt' => 'Pokaż wszystko']),

                    ],
                ],
            ]);
            ?>

            <?php
            Modal::begin([
                'id' => 'make-task',
                'header' => Yii::t('app', 'Wybierz zadanie'),
                'size' => 'modal-xs',
            ]);
            ?>
            <p>
                Wybierz zadanie do wykonania.
            </p>
            <div class="text-center">
                <?= MultiSelect::widget([
                    'name' => "task-selecter",
                    'attribute' => 'selectedValue',
                    'data' => TaskConverter::toArrayIdName(Task::findByGameId($game->id)),
                    'options' =>
                        [
                            'multiple' => 'multiple',
                        ],
                    'clientOptions' =>
                        [
                            'includeSelectAllOption' => true,
                            'numberDisplayed' => 6,
                            'enableCaseInsensitiveFiltering' => true,
                            'nonSelectedText' => Yii::t('app', 'Nic nie wybrano'),
                            'allSelectedText' => Yii::t('app', 'Wybrano wszystkich'),
                            'nSelectedText' => Yii::t('app', 'Wybór dla '),
                            'buttonWidth' => '400px',
                            'maxHeight' => '70px',
                            'inheritClass' => true,
                            'enabledHtml' => true,
                            'selectAllText' => Yii::t('app', 'Zaznacz wszystko'),
                            'filterPlaceholder' => Yii::t('app', 'Szukaj')


                        ],
                ]);
                ?>
            </div>
            <br>
            <button class='btn btn-danger' data-dismiss='modal'><?= Yii::t('app', 'Anuluj') ?> </button>
            <button class='btn pull-right btn-info' type="submit" value="make-task"
                    name="submit"><?= Yii::t('app', 'Wybieram') ?> </button>

            <?php
            Modal::end();
            ?>

            <?php
            Modal::begin([
                'id' => 'send-invited',
                'header' => Yii::t('app', 'Potwierdź wysłanie'),
                'size' => 'modal-xs',
            ]);
            ?>

            <p>
                Po wykonaniu tej akcji użytkownicy, dostaną zaproszenia do gry i będą mogli zobaczyć grę na swojej
                tablicy.
            </p>

            <button class='btn btn-danger' data-dismiss='modal'><?= Yii::t('app', 'Anuluj') ?> </button>
            <button class='btn pull-right btn-info' type="submit" value="send-invited"
                    name="submit"><?= Yii::t('app', 'Potwierdzam') ?> </button>

            <?php
            Modal::end();
            ?>

            <?php
            Modal::begin([
                'id' => 'send-invite-to-all',
                'header' => Yii::t('app', 'Potwierdź wysłanie do wszystkich nie zaproszonych'),
                'size' => 'modal-xs',
            ]);
            ?>

            <p>
                Po wykonaniu tej akcji wszyscy nie zaproszeni użytkownicy, dostaną zaproszenia do gry i będą mogli
                zobaczyć grę na swojej
                tablicy.
            </p>

            <button class='btn btn-danger' data-dismiss='modal'><?= Yii::t('app', 'Anuluj') ?> </button>
            <button class='btn pull-right btn-info' type="submit" value="send-invite-to-all"
                    name="submit"><?= Yii::t('app', 'Potwierdzam') ?> </button>

            <?php
            Modal::end();
            ?>

            <?= Html::endForm(); ?>


            <?php
            Modal::begin([
                'id' => 'delete-user',
                'header' => Yii::t('app', 'Potwierdź usunięcie urzytkownika z gry'),
                'size' => 'modal-xs',
            ]);
            ?>

            <p>
                Czy napewno chcesz usunąć z gry tych graczy ?
            </p>

            <button class='btn btn-danger' data-dismiss='modal'><?= Yii::t('app', 'Anuluj') ?> </button>
            <button class='btn pull-right btn-info' type="submit" value="delete-users"
                    name="submit"><?= Yii::t('app', 'Potwierdzam') ?> </button>

            <?php
            Modal::end();
            ?>

            <?= Html::endForm(); ?>
        </div>
    </div>
</section>
