<?php
use backend\helpers\BreadcrumbHelper;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

$breadcrumb = new BreadcrumbHelper();

?>
<!-- BREADCRUMB -->
<section class="content-header">
    <ol class="breadcrumb">
        <?= $breadcrumb->home(); ?>
        <?= $breadcrumb->game($game); ?>
        <?= $breadcrumb->addUserByCSV(); ?>
    </ol>
</section>
<!-- /BREADCRUMB -->

<section class="content">

    <div class="site-invite">
        <div class="row">
            <div class="col-lg-5">
                <h1> <?= Yii::t('app', 'Importujesz graczy do gry') ?>
                    <b><?= $game->name ?></b>
                </h1>

                <?php $form = ActiveForm::begin(['id' => 'form-add-by-csv']); ?>

                <div class="text-info">
                    Dodaj graczy z pliku CSV.  Kolumny w pliku powinny być oddzielone przecinkami i mieć zachowaną następującą kolejność:
                    <span class="text-bold">Mail, Imię, Nazwisko, Etykieta.</span> Przykładowa zawartość pliku:
                    <br><br>
                    <span class="text-red">
                    jan@przyklad.com, Jan, Kowalski, Grupa A
                    tadeusz@przyklad.pl, Tadeusz, Nowak, Grupa B
                    joanna@przyklad.info, Joanna, Iksińska, Grupa B
                    </span>
                    <br><br>
                </div>
                <?= $form->field($model, 'csvFile')->fileInput() ?>
                <div class="form-group">
                    <?= Html::submitButton(Yii::t('app', 'Zaimportuj'), ['class' => 'btn btn-primary', 'name' => 'add-by-csv']) ?>
                </div>

                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>