<script>
    function initMap() {
        var map = new google.maps.Map(document.getElementById('map'), {
            center: {lat: parseFloat('<?php echo $lat;?>'), lng: parseFloat('<?php echo $lng;?>')},
            zoom: 13,
            streetViewControl: false,
            fullscreenControl: false
        });
        var marker = new google.maps.Marker({
            position: {lat: parseFloat('<?php echo $lat;?>'), lng: parseFloat('<?php echo $lng;?>')},
            map: map
        });
    }
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDgetVGiQmyUCPrJGF5J0oVZvHgjVGtYyE&callback=initMap"
       ></script>