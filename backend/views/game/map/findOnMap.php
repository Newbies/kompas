<script type="text/javascript">
    // 1,2,3,4 Steps of failure
    function initMap() {
        var lng = (!isNaN(parseFloat(<?php echo $lng?>))) ? parseFloat(<?php echo $lng?>) : 19.27 ; //1
        var lat = (!isNaN(parseFloat(<?php echo $lat;?>))) ? parseFloat(<?php echo $lat;?>) : 52.03; //1

        var map = new google.maps.Map(document.getElementById('map'), {
            center: {lat: lat, lng: lng}, //1
            zoom: 5, //2
            streetViewControl: false,  //4
            fullscreenControl: false   //4

        });

        var input = document.getElementById('place-city');

        var options = { //3
            types: ['(cities)'], //3
            componentRestrictions: {country: "pl"} //3
        };

        var autocomplete = new google.maps.places.Autocomplete(input, options); //3

        autocomplete.bindTo('bounds', map);

        var infowindow = new google.maps.InfoWindow();
        var infowindowContent = document.getElementById('infowindow-content');
        infowindow.setContent(infowindowContent);
        var marker = new google.maps.Marker({
            map: map,
            anchorPoint: new google.maps.Point(0, -29)
        });

        if (input.value !== '') { //5
            marker.setPosition(new google.maps.LatLng(lat, lng)); //5
        } //5

        autocomplete.addListener('place_changed', function() {
            infowindow.close();
            marker.setVisible(false);
            var place = autocomplete.getPlace();

            var LocationInput = document.getElementById('place-city'); //4
            LocationInput.value = document.getElementById('place-city').value; //4

            document.getElementById('place-lat').value = place.geometry.location.lat(); //4
            document.getElementById('place-lng').value = place.geometry.location.lng(); //4
            document.getElementById('place-voivodeship').value = place.address_components[2].long_name; //4
            document.getElementById('place-postal_code').value = place.address_components[4].short_name; //4

           /* if (!place.geometry) {
                window.alert("No details available for input: '" + place.name + "'");
                return;
            }*/

            if (place.geometry.viewport) {
                map.fitBounds(place.geometry.viewport);
            } else {
                map.setCenter(place.geometry.location);
                map.setZoom(17);
            }
            marker.setPosition(place.geometry.location);
            marker.setVisible(true);

            var address = '';
            if (place.address_components) {
                address = [
                    (place.address_components[0] && place.address_components[0].short_name || ''),
                    (place.address_components[1] && place.address_components[1].short_name || ''),
                    (place.address_components[2] && place.address_components[2].short_name || '')
                ].join(' ');
            }

            infowindowContent.children['place-icon'].src = place.icon;
            infowindowContent.children['place-name'].textContent = place.name;
            infowindowContent.children['place-address'].textContent = address;
            infowindow.open(map, marker);
        });
    }
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAkWTL_wf7ki01gcxjhXY-EUw2Ip_QVIME&libraries=places&callback=initMap"
        async defer></script>

