<?php

use backend\helpers\BreadcrumbHelper;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\Game */

$this->title = Yii::t('app', 'Edycja: ') . $model->name;
$breadcrumb = new BreadcrumbHelper();

?>
<!-- BREADCRUMB -->
<section class="content-header">
    <h1><?= Html::encode(Yii::t('app', 'Edycja: ') . $model->name) ?></h1>
    <ol class="breadcrumb">
        <?= $breadcrumb->home() ?>
        <?= $breadcrumb->game($model) ?>
        <?= $breadcrumb->edit() ?>

    </ol>
</section>
<!-- /BREADCRUMB -->

<section class="content">
    <div class="game-update box container">
        <div class="box-body">
            <?= $this->render('_form', [
                'model' => $model,
                'place' => $place
            ]) ?>
        </div>
    </div>
</section>
