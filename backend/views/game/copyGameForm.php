<?php

use janisto\timepicker\TimePicker;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

echo '<center>';
$form = ActiveForm::begin([
    'options' => ['enctype' => 'multipart/form-data', 'data-pjax' => true],
    'action' => ['/game/copy', 'gameId'=>$gameId],
]);

echo $form->field($model, 'name')->textInput(['maxlength' => true]);
echo $form->field($model, 'startDate')->widget(TimePicker::className(), ['mode' => 'datetime',
    'clientOptions' => [
        'dateFormat' => 'yy-mm-dd',
        'timeFormat' => 'HH:mm:ss',
        'showSecond' => true,
    ],
    'options' => ['style' => 'z-index:1152 !important'],
]);

echo $form->field($model, 'endDate')->widget(TimePicker::className(), ['mode' => 'datetime',
    'clientOptions' => [
        'dateFormat' => 'yy-mm-dd',
        'timeFormat' => 'HH:mm:ss',
        'showSecond' => true,
    ],
    'options' => ['style' => 'z-index:1152 !important'],
]);


echo Html::submitButton(Yii::t('app', 'Skopiuj grę'), ['class' => 'btn btn-success']);

ActiveForm::end();
echo '</center>';
?>