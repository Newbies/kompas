<?php

use backend\helpers\BreadcrumbHelper;
use yii\helpers\Html;
use yii\helpers\Url;


/* @var $this yii\web\View */
/* @var $model common\models\Game */

$breadcrumb = new BreadcrumbHelper();

?>
<!-- BREADCRUMB -->
<section class="content-header">
    <h1><?= Html::encode(Yii::t('app', 'Dodaj grę')) ?></h1>
    <ol class="breadcrumb">
        <?= $breadcrumb->home()?>
        <?= $breadcrumb->addGame()?>
    </ol>
</section>
<!-- /BREADCRUMB -->

<section class="content">
    <div class="game-create box container">
        <div class="box-body">
            <?= $this->render('_form', [
                'model' => $model,
                'place' => $place
            ]) ?>
        </div>
    </div>
</section>
