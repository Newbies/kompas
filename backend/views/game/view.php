<?php

use backend\helpers\BreadcrumbHelper;
use backend\models\CopyForm;
use common\models\fields\LocationFields;
use yii\bootstrap\Modal;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Game */

$breadcrumb = new BreadcrumbHelper();
?>
<!-- BREADCRUMB -->
<section class="content-header">
    <h3><?= Html::encode($model->name) ?></h3>
    <ol class="breadcrumb">
        <?= $breadcrumb->home(); ?>
        <?= $breadcrumb->game($model); ?>
        <?= $breadcrumb->detail(); ?>
    </ol>
</section>
<!-- /BREADCRUMB -->

<section class="content">
    <div class="game-view col-xs-12">
        <div class="container box box-header with-border">
            <div class="col-xs-8 ">
                <h3><?= $model->description ?></h3>
                <div class="col-xs-6">
                    <br>
                    <div><?= Yii::t('app', 'Organizator:') ?></div>
                    <br>
                    <div><?= Yii::t('app', 'Tryb gry:') ?></div>
                    <br>
                    <div><?= Yii::t('app', 'Dostępność:') ?></div>
                    <br>
                    <div><?= Yii::t('app', 'Rozpoczęcie gry:') ?></div>
                    <br>
                    <div><?= Yii::t('app', 'Zakończenie gry:') ?></div>
                    <br>
                </div>
                <div class="col-xs-6">
                    <br>
                    <div><?= $model->getOrganizatorName() ?></div>
                    <br>
                    <div><?= $model->getRankingLikeText() ?></div>
                    <br>
                    <div><?= $model->getTypeLikeText() ?></div>
                    <br>
                    <div><?= $model->start_date ?></div>
                    <br>
                    <div><?= $model->end_date ?></div>
                    <br>
                    <br>
                </div>
                <div>
                    <a href="<?= \yii\helpers\Url::to(['/game/update', 'id' => $model->id]) ?>"
                       class="btn btn-primary <?php if ($model->isEnd()): echo 'disabled'; endif; ?>">
                        <?= Yii::t('app', 'Edytuj grę') ?>
                    </a>
                    <a href="<?= \yii\helpers\Url::to(['/game/delete', 'id' => $model->id]) ?>" data-method="post"
                       data-confirm="<?= Yii::t('app', 'Czy napewno chcesz usunąć tą grę?') ?>"
                       class="btn btn-danger">
                        <?= Yii::t('app', 'Usuń grę') ?>
                    </a>
                    <a class="btn btn-primary" data-toggle="modal"
                       data-target="#copy_game_form"><?= Yii::t('app', 'Skopiuj grę') ?></a>
                </div>
            </div>
            <div class="col-xs-4">
                <?php if (isset($place)): ?>
                    <br>
                    <div style="height: 300px" id="map">
                        <?= $this->render('map/viewOnMap', ['lat' => $place->lat, 'lng' => $place->lng]) ?>
                    </div>
                    <div><?= $place->postal_code . ", " . $place->city . ", " . $place->voivodeship ?></div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</section>

<?php
Modal::begin([
    'id' => 'copy_game_form',
    'header' => Yii::t('app', 'Wczytaj zdjęcie'),
    'size' => 'modal-xs',
]);
echo $this->render('/game/copyGameForm', [
    'model' => new CopyForm(),
    'gameId' => $model->id,
]);
Modal::end();
?>


