<?php


use common\models\fields\OrganizationFields;
use common\models\fields\UserOrgFields;
use common\models\Organization;
use common\models\Place;
use common\models\UserOrg;
use dosamigos\tinymce\TinyMce;
use janisto\timepicker\TimePicker;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Game */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="game-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'owner_organization_id')->dropDownList(ArrayHelper::map(
        Organization::find()
            ->where([
                'in',
                OrganizationFields::ID,
                backend\helpers\ArrayHelper::map(
                    UserOrg::findAll([UserOrgFields::USER_ID => Yii::$app->user->identity->getId()]),
                    UserOrgFields::ORG_ID)])->all(),
        OrganizationFields::ID,
        OrganizationFields::NAME))
        ->label(Yii::t('app', 'Wybierz organizacje')); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->widget(TinyMce::className(), [
        'options' => ['rows' => 6],
        'language' => 'pl',
        'clientOptions' => [
            'plugins' => [
                "advlist autolink lists link charmap print preview anchor",
                "searchreplace visualblocks fullscreen",
                "insertdatetime paste",
            ],
            'toolbar' => "undo redo |  bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
        ]]); ?>


    <div class="row">
        <div class="col-sm-7">

            <?= $form->field($place, 'city')->textInput(['maxlength' => true, 'onChange' => 'document.getElementById("place-postal_code").value=""']) ?>

            <?= html::activeHiddenInput($place, 'lat') ?>
            <?= html::activeHiddenInput($place, 'lng') ?>
            <?= html::activeHiddenInput($place, 'voivodeship') ?>
            <?= html::activeHiddenInput($place, 'postal_code') ?>

            <?= $form->field($model, 'start_date')->widget(TimePicker::className(), ['mode' => 'datetime',
                'clientOptions' => [
                    'dateFormat' => 'yy-mm-dd',
                    'timeFormat' => 'HH:mm:ss',
                    'showSecond' => true,
                ]
            ]) ?>

            <?= $form->field($model, 'end_date')->widget(TimePicker::className(), ['mode' => 'datetime',
                'clientOptions' => [
                    'dateFormat' => 'yy-mm-dd',
                    'timeFormat' => 'HH:mm:ss',
                    'showSecond' => true,
                ]
            ]) ?>
            <div class="<?php if (!$model->isNewRecord): echo 'hide-options'; endif; ?>">
                <?= $form->field($model, 'type')->dropDownList([0 => Yii::t('app', 'Publiczna'), 1 => Yii::t('app', 'Prywatna')], ['id' => 'typeDropList']) ?>
            </div>

            <div id="typeDiv" class="<?php if ($model->type == 0 || !$model->isNewRecord): echo 'hide-options'; endif; ?>">

                <?= $form->field($model, 'ranking')->dropDownList([0 => Yii::t('app', 'Indywidualna'), 1 => Yii::t('app', 'Drużynowa')]) ?>

            </div>
        </div>

        <div class="col-sm-5">
            <br>
            <div style="height: 270px" id="map"></div>
            <br>
        </div>
    </div>

    <div class="form-group text-right">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Dodaj grę') : Yii::t('app', 'Zapisz zmiany'),
            ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
<?= $this->render('map/findOnMap', ['lat' => Place::getLat($model->place_id), 'lng' => Place::getLng($model->place_id)]) ?>
<div id="infowindow-content">
    <img width="16" height="16" id="place-icon">
    <span id="place-name" class="title"></span><br>
    <span id="place-address"></span>
</div>
