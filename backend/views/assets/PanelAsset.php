<?php

namespace backend\modules\panel\assets;

use yii\web\AssetBundle;

/**
 * Main backend application asset bundle.
 */
class PanelAsset extends AssetBundle
{
//    public $basePath = '@webroot';
    public $sourcePath = '@app/modules/panel/web';
    public $baseUrl = '@web';
    public $css = [
        'css/AdminLTE.min.css',
        'css/skins/skin-black.min.css',
        'css/font-awesome.min.css',
        'css/panel.css',
    ];
    public $js = [
        'js/app.min.js',
        'js/adminlte.min.js',
        'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js'
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];

    public function init()
    {
        parent::init();
        if (YII_ENV_DEV) {
            $this->publishOptions['forceCopy'] = true;
        }
    }
}
