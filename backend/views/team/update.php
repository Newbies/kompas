<?php

use backend\helpers\BreadcrumbHelper;

/* @var $this yii\web\View */
/* @var $model common\models\Team */

$breadcrumb = new BreadcrumbHelper();


?>
<!-- BREADCRUMB -->
<section class="content-header">
    <h1><?= Yii::t('app', 'Edycja: ') . $model->name; ?></h1>
    <ol class="breadcrumb">
        <?= $breadcrumb->home(); ?>
        <?= $breadcrumb->game($game); ?>
        <?= $breadcrumb->team($game, $model); ?>
        <?= $breadcrumb->edit(); ?>
    </ol>
</section>
<!-- /BREADCRUMB -->

<section class="content">

    <div class="team-update box container box-header with-border">

        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>

    </div>
