<?php

use backend\helpers\BreadcrumbHelper;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\Team */

$this->params['team_id'] = $model->id;
$this->params['game_id'] = $game->id;
$this->params['game_end'] = $game->isEnd();

$breadcrumb = new BreadcrumbHelper();
?>
<!-- BREADCRUMB -->
<section class="content-header">
    <h3><?= Yii::t('app', 'Szczegóły drużyny') ?></h3>
    <ol class="breadcrumb">
        <?= $breadcrumb->home(); ?>
        <?= $breadcrumb->game($game); ?>
        <?= $breadcrumb->team($game, $model); ?>
        <?= $breadcrumb->detail(); ?>
    </ol>
</section>
<!-- /BREADCRUMB -->

<section class="content">
  <div class="row row-clear">
    <div class="team-view">
        <div class="col-xs-12">
            <div class="box container box-header with-border">
                <div class="box-body">
                    <div class="col-xs-8">
                        <h3><?= $model->name ?></h3>
                    </div>
                    <div class="col-xs-4">
                        <div>
                            <a href="<?= Url::to(['/game/' . $game->id . '/team/update', 'id' => $model->id]) ?>"
                               class="btn btn-block btn-primary <?php if ($game->isEnd()): echo 'disabled'; endif; ?>">
                                <?= Yii::t('app', 'Edytuj drużynę') ?>
                            </a>
                        </div>
                        <div>
                            <a href="<?= Url::to(['/game/' . $game->id . '/team/delete', 'id' => $model->id]) ?>"
                               data-method="post"
                               data-confirm="<?= Yii::t('app', 'Czy napewno chcesz usunąć tą drużynę?') ?>"
                               class="btn btn-block btn-danger">
                                <?= Yii::t('app', 'Usuń drużynę') ?>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xs-6">
            <div class="box container">
                <h1> <?= Yii::t('app', 'Użytkownicy w drużynie') ?> </h1>
                <?= GridView::widget([
                    'dataProvider' => $usersInTeam,
                    'columns' => [
                        'email',
                        'name',
                        'last_name',
                        [
                            'label' => Yii::t('app', ''),
                            'format' => 'raw',
                            'headerOptions' => ['style' => 'width:40px'],
                            'value' => function ($model) {

                                return Html::a("<i class='fa fa-trash' style='padding: -3px;'></i>",
                                    ['/game/' . $this->params['game_id'] . '/team/remove',
                                        'id' => $this->params['team_id'], 'user_id' => $model->id],
                                    ['class' => $this->params['game_end'] ? 'btn btn-primary disabled small-btn' : 'btn btn-primary small-btn']);
                            }
                        ],
                    ],
                    'options' => [
                        'class' => 'grid-view'
                    ],
                ]); ?>
            </div>
        </div>

        <div class="col-xs-6">
            <div class="box container">
                <h1> <?= Yii::t('app', 'Użytkownicy w grze bez drużyny') ?> </h1>
                <?= GridView::widget([
                    'dataProvider' => $usersInGame,
                    'filterModel' => $searchModel,
                    'columns' => [
                        'email',
                        'name',
                        'last_name',
                        [
                            'attribute' => 'tag',
                            'format' => 'text',
                            'value' => function ($model) {
                                $userGame = \common\models\UserGame::findByUserIdAndGameId($model, $this->params['game_id']);
                                return $userGame->tag;
                            },
                            'filter' => HTML::activeInput("text", $searchModel, "tag", ['class' => 'form-control'])
                        ],
                        [
                            'label' => Yii::t('app', ''),
                            'format' => 'raw',
                            'headerOptions' => ['style' => 'width:40px'],
                            'value' => function ($model) {
                                return Html::a("<i class='fa  fa-user-plus'>",
                                    ['/game/' . $this->params['game_id'] . '/team/add',
                                        'id' => $this->params['team_id'], 'user_id' => $model->id],
                                    ['class' => $this->params['game_end'] ? 'btn btn-primary disabled small-btn' : 'btn btn-primary small-btn']);
                            },
                        ],
                    ],
                    'options' => [
                        'class' => 'grid-view'
                    ],
                ]); ?>
            </div>
        </div>
    </div>
  </div>
