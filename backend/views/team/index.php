<?php

use backend\converter\TaskConverter;
use backend\helpers\BreadcrumbHelper;
use common\models\Task;
use dosamigos\multiselect\MultiSelect;
use yii\bootstrap\Modal;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;


/* @var $this yii\web\View */
/* @var $searchModel common\models\TeamSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$breadcrumb = new BreadcrumbHelper();

$this->params['gameId'] = $game->id;
?>

<!-- BREADCRUMB -->
<section class="content-header">
    <h1><?= Yii::t('app', 'Drużyny dla gry ') . $game->name ?></h1>
    <ol class="breadcrumb">
        <?= $breadcrumb->home(); ?>
        <?= $breadcrumb->game($game); ?>
        <?= $breadcrumb->teams(); ?>
    </ol>
</section>
<!-- /BREADCRUMB -->

<section class="content">
    <div class="team-index box container box-header with-border">
        <a href="<?= Url::to(['game/' . $game->id . '/team/create']) ?>"
           class="btn btn-success <?php if ($game->isEnd()): echo 'disabled'; endif; ?>">
            <?= Yii::t('app', 'Dodaj drużynę') ?></a>
        <?= Html::beginForm(['team/selected', 'gameId' => $game->id], 'post'); ?>

        <a class="btn btn-primary" data-toggle="modal"
           data-target="#make-task"><?= Yii::t('app', 'Wykonaj zadanie') ?></a>
        <?= Html::beginForm(['team/selected'], 'post'); ?>
        <div class="box-body">
            <?php

            echo GridView::widget([
                'id' => 'game-grid',
                'emptyText' => Yii::t('app', 'Brak drużyn'),
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'tableOptions' => ['class' => 'table no-margin small-view sortable table-responsive table table-hover task-table'],
                'columns' => [
                    [
                        'class' => 'yii\grid\CheckboxColumn',
                        'contentOptions' => [
                            'class' => 'checkbox-table',
                        ],
                        'headerOptions' => [
                            'class' => 'checkbox-table',
                        ],
                    ],
                    [
                        'attribute' => 'name',
                        'format' => 'html',
                        'value' => function ($model) {
                            return Html::a($model->name,
                                Url::to('/game/' . $this->params['gameId'] . '/team/' . $model->id));
                        },
                    ],
                    [
                        'attribute' => 'score',
                        'format' => 'text',
                    ],
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'contentOptions' => ['style' => 'width:40px'],
                        'template' => $game->isEnd() ? '{view} {delete}' : '{view} {update} {delete}',
                        'buttons' => [
                            'view' => function ($model, $key, $index) {
                                return Html::a('<span class="glyphicon glyphicon - eye - open table - icon"></span>',
                                    Url::to('/game/' . $this->params['gameId'] . '/team/view/' . $index));
                            },
                            'update' => function ($model, $key, $index) {
                                return Html::a('<span class="glyphicon glyphicon - pencil table - icon"></span>',
                                    Url::to('/game/' . $this->params['gameId'] . '/team/update/' . $index));
                            },
                            'delete' => function ($model, $key, $index) {
                                return Html::a('<span class="glyphicon glyphicon - trash text - red table - icon"></span>',
                                    Url::to('/game/' . $this->params['gameId'] . '/team/delete/' . $index),
                                    ['data-method' => 'post', 'data-confirm' => Yii::t('app', 'Czy napewno chcesz usunąć tą drużynę?')]);
                            }
                        ],
                    ],
                    [
                        'label' => Yii::t('app', 'Członkowie'),
                        'format' => 'raw',
                        'value' => function ($model) {
                            return \common\models\Team::getTeamMembersCount($model->game_id, $model->id);
                        }
                    ],
                ],
            ]);
            ?>

            <?php
            Modal::begin([

                'id' => 'make-task',
                'header' => Yii::t('app', 'Wybierz zadanie'),
                'size' => 'modal-xs',
            ]);
            ?>
            <p>
                Wybierz zadanie do wykonania.
            </p>
            <div class="text-center">
                <?= MultiSelect::widget([
                    'name' => "task-selecter",
                    'attribute' => 'selectedValue',
                    'data' => TaskConverter::toArrayIdName(Task::findByGameId($game->id)),
                    'options' =>
                        [
                            'multiple' => 'multiple',
                        ],
                    'clientOptions' =>
                        [
                            'includeSelectAllOption' => true,
                            'numberDisplayed' => 6,
                            'enableCaseInsensitiveFiltering' => true,
                            'nonSelectedText' => Yii::t('app', 'Nic nie wybrano'),
                            'allSelectedText' => Yii::t('app', 'Wybrano wszystkich'),
                            'nSelectedText' => Yii::t('app', 'Wybór dla '),
                            'buttonWidth' => '400px',
                            'maxHeight' => '70px',
                            'inheritClass' => true,
                            'enabledHtml' => true,
                            'selectAllText' => Yii::t('app', 'Zaznacz wszystko'),
                            'filterPlaceholder' => Yii::t('app', 'Szukaj')
                        ],
                ]);
                ?>
            </div>
            <br>

            <button class='btn btn-info' type="submit" value="make-task"
                    name="submit"><?= Yii::t('app', 'Wybieram') ?> </button>
            <button class='btn pull-right btn-danger' data-dismiss='modal'><?= Yii::t('app', 'Anuluj') ?> </button>
            <?php
            Modal::end();
            ?>

            <?= Html::endForm(); ?>

        </div>
    </div>
</section>

