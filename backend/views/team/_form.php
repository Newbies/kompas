<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Team */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="team-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput() ?>

    <div class="form-group text-right col-xs-12">
        <div class="col-xs-9" style="padding-top: 8px;">
            <?php if ($model->isNewRecord) echo $form->field($model, 'addNext')->checkbox()->label(Yii::t('app', 'Zostań na karcie dodawania ')); ?>
        </div>
        <div class="col-xs-3">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Dodaj drużynę') : Yii::t('app', 'Zapisz zmiany'),
                ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
