<ul class="timeline">

    <li class="time-label">
        <span class="bg-red">
           <?=
           Yii::t('app', 'CZAS OBECNY') ?>
        </span>
    </li>

    <?php foreach ($dataProvider as $model) { ?>

        <li>
            <!-- timeline icon -->
            <i class="fa fa-check bg-green"></i>
            <div class="timeline-item">
                <span class="time"><i class="fa fa-clock-o"></i> <?= $model->getTime() ?></span>

                <h4 class="timeline-header"><?= $model->getName() ?></h4>

                <div class="timeline-body"><?= $model->getContent() ?></div>
            </div>
        </li>

    <?php } ?>


    <li class="time-label">
        <span class="bg-red">
           <?= Yii::t('app', 'START GRY') ?>
        </span>
    </li>
</ul>