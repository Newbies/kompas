<?php
/**
 * Created by PhpStorm.
 * User: janpa
 * Date: 2017-05-14
 * Time: 11:10
 */

use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */


$this->params['gameId'] = $game->id;
$this->params['pagination'] = $dataProvider->pagination;
?>
<section>
    <div class="index box container">

        <h1><?= Html::encode($this->title) ?></h1>
        <div class="box-body">
            <?= GridView::widget([
                'id' => 'game-grid',
                'emptyText' => Yii::t('app', 'Brak graczy'),
                'dataProvider' => $dataProvider,
                'tableOptions' => ['class' => 'table no-margin small-view sortable table-responsive table table-hover task-table'],
                'columns' => [
                    [
                        'label' => Yii::t('app', 'Miejsce'),
                        'format' => 'text',
                        'value' => function ($model,$id,$index) {
                            return ($this->params['pagination']->getPage() * $this->params['pagination']->getPageSize()) + $index + 1;

                        }
                    ],
                    [
                        'attribute' => 'name',
                        'label' => Yii::t('app', 'Osoba'),
                        'format' => 'text',
                        'value' => function ($model) {
                            return \common\models\User::findOne(['id' => $model->user_id])->showMe();
                        }
                    ],
                    [
                        'attribute' => 'score',
                        'format' => 'text',
                        'enableSorting' => false
                    ],
                    [
                        'attribute' => 'last_update_score',
                        'format' => 'raw',
                        'enableSorting' => false,
                        'value' => function ($model) {
                            return date("H:i:s d/m/y",$model->last_update_score);
                        }
                    ],
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'contentOptions' => ['style' => 'width:10px'],
                        'template' => '{timeline}',
                        'buttons' => [
                            'timeline' => function ($model, $key, $index) {
                                return Html::a('<span class="fa fa-bar-chart text-green table-icon"></span>',
                                    Url::to(['/timeline/user', 'id' => $key->user_id, 'gameId' => $this->params['gameId']]));
                            }
                        ],
                    ],
                ],
            ]);
            ?>
        </div>
    </div>
</section>