<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */

/* @var $model \frontend\models\SignupForm */

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

?>
<div class="container">
    <br>
    <br>
    <br>
    <div class="login-logo">
        <img src="/images/logo_kompas.png">
    </div>
    <br>
    <div class="login-box-body">

        <h2><?= Yii::t('app', 'Zarejestruj się jako administrator') ?></h2>
        <?php $form = ActiveForm::begin(['id' => 'signup-form']); ?>

        <h3><?= Yii::t('app', 'Podaj email') ?></h3>
        <div class="">
            <?= $form->field($model, 'email')->textInput(['autofocus' => true, 'type' => 'email', 'disabled' => $model->id, 'placeholder' => \Yii::t('app', 'Email'), 'class' => 'signup-input form-control'])->label(false) ?>
        </div>
        <h3><?= Yii::t('app', 'Podaj imię') ?></h3>
        <div class=" ">
            <?= $form->field($model, 'name')->textInput(['autofocus' => true, 'placeholder' => \Yii::t('app', 'Imie'), 'class' => 'signup-input form-control'])->label(false) ?>
        </div>
        <h3><?= Yii::t('app', 'Podaj nazwisko') ?></h3>
        <div class=" ">
            <?= $form->field($model, 'lastName')->textInput(['autofocus' => true, 'placeholder' => \Yii::t('app', 'Nazwisko'), 'class' => 'signup-input form-control'])->label(false) ?>
        </div>
        <h3><?= Yii::t('app', 'Data urodzenia') ?></h3>

        <div class="row">
            <div class=" col-xs-4">
                <?= $form->field($model, 'birthDay')->textInput(['type' => 'number', 'autofocus' => true, 'placeholder' => \Yii::t('app', 'DD'), 'class' => 'signup-input form-control'])->label(false) ?>
            </div>
            <div class=" col-xs-4 small-padding">
                <?= $form->field($model, 'birthMonth')->textInput(['type' => 'number', 'autofocus' => true, 'placeholder' => \Yii::t('app', 'MM'), 'class' => 'signup-input form-control'])->label(false) ?>
            </div>
            <div class=" col-xs-4">
                <?= $form->field($model, 'birthYear')->textInput(['type' => 'number', 'autofocus' => true, 'placeholder' => \Yii::t('app', 'RRRR'), 'class' => 'signup-input form-control'])->label(false) ?>
            </div>
        </div>
        <div class=" ">
            <?= $form->field($model, 'reCaptcha')->widget(\himiklab\yii2\recaptcha\ReCaptcha::className()) ?>
        </div>

        <div class=" row ">
            <div class="col-xs-4">
                <a class="btn btn-primary btn-block btn-flat"
                   href="<?php echo yii\helpers\Url::to(['/site/login']); ?>">
                    <?= Yii::t('app', 'Powrót') ?>
                </a>
            </div>
            <div class="col-xs-4"></div>
            <div class="col-xs-4">
                <?= Html::submitButton('Dalej', ['class' => 'btn btn-success btn-block btn-flat', 'name' => 'signup-button']) ?>
            </div>
        </div>
        <p style="color:#ffffff">chuj</p>

    </div>

    <?php ActiveForm::end(); ?>
</div>

<br>
<br>
<br>
<br>
<br>


