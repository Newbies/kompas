<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */

/* @var $model \common\models\LoginForm */

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

$this->title = Yii::t('app', 'Login');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="login-box">
    <div class="login-box">
        <div class="login-logo">
            <img src="/images/logo_kompas.png">
        </div>
        <div class="login-box-body">
            <p class="login-box-msg"><?= Yii::t('app', 'Zaloguj się do panelu administratora') ?></p>
            <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>

            <div class="form-group has-feedback">
                <?= $form->field($model, 'email')
                    ->textInput(['autofocus' => true, 'class' => "form-control", "placeholder" => Yii::t('app', 'Email')])
                    ->label(false) ?>
                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
            </div>

            <div class="form-group has-feedback">
                <?= $form->field($model, 'password')
                    ->textInput(['autofocus' => true, 'class' => "form-control", 'type' => 'password', "placeholder" => Yii::t('app', 'Hasło')])
                    ->label(false) ?>
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            </div>
            <div class="row">

                <div class="col-xs-8">
                    <?= $form->field($model, 'rememberMe')
                        ->checkbox(['template' => '<div style="padding-top: 7px;padding-left: 4px;">{input}' . Yii::t('app', ' Zapamiętaj mnie') . '</div>'])
                    ?>
                </div>
                <div class="col-xs-4">
                    <?= Html::submitButton(Yii::t('app', 'Zaloguj'), ['class' => 'btn btn-success btn-block btn-flat', 'name' => 'login-button']) ?>
                </div>
            </div>
            <?php ActiveForm::end(); ?>
        </div>

        <div class="login-box">
            <div class="login-box-body">
                <div class="row">
                    <div class="col-xs-1"></div>
                    <div class="col-xs-11">
                        <p>
                            <?= Yii::t('app', 'Nie jesteś jeszcze administratorem ?') ?>
                        </p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-3"></div>
                    <div class="col-xs-9">
                        <a href="<?= \yii\helpers\Url::to(['/site/registration']) ?>"
                           class="btn btn-warning btn-block btn-flat">
                            <?= Yii::t('app', 'Dołącz do grona administratorów') ?>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>