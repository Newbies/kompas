<?php

use common\models\Organization;

?>


<section class="content-header">
    <div class="row row-clear">
        <?php
        foreach ($data as $dat) {
            ?>
            <div class="col-xs-12">
            <h3><?= Yii::t('app', 'Statystyki dla organizacji ') ?>  <?= Organization::findOne([\common\models\fields\OrganizationFields::ID => $dat[0]])->name ?></h3>

            <!-- End Game counter-->
            <div class="col-xs-4">
                <div class="small-box bg-red">
                    <div class="inner">
                        <h3><?= $dat[1] ?></h3>
                        <p><?= Yii::t('app', 'Ilość twoich gier zakończonych') ?></p>
                    </div>
                    <div class="icon">
                        <i class="fa  fa-stop"></i>
                    </div>
                </div>
            </div>
            <!-- /End game counter-->

            <!-- Now game counter-->
            <div class="col-xs-4">
                <div class="small-box bg-green">
                    <div class="inner">
                        <h3><?= $dat[2] ?></h3>
                        <p><?= Yii::t('app', 'Ilość twoich gier trwających') ?></p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-play"></i>
                    </div>
                </div>
            </div>
            <!-- /Now game counter-->

            <!-- Be game counter-->
            <div class="col-xs-4">
                <div class="small-box bg-yellow">
                    <div class="inner">
                        <h3><?= $dat[3] ?></h3>
                        <p><?= Yii::t('app', 'Ilość twoich gier przyszłych') ?></p>
                    </div>
                    <div class="icon">
                        <i class="fa  fa-step-forward"></i>
                    </div>
                </div>
            </div>
            <!-- /be game counter-->
            </div>
            <?php
        }
        ?>
    </div>
</section>
