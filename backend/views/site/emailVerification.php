<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\EmailVerificationForm */

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

?>

<div class="container">
    <br>
    <br>
    <br>
    <div class="login-logo">
        <img src="/images/logo_kompas.png">
    </div>
    <br>

    <div class="login-box-body col-xs-12">
        <div class="col-xs-12 text-center">
            <h1><?= \Yii::t('app', 'Zweryfikuj email') ?></h1>
        </div>
        <div class="col-xs-12 text-justify">
            <h4><?= \Yii::t('app', 'Na podany przez Ciebie adres email został wysłany kod werfikacyjny. Wpisz go w polu poniżej aby potwierdzić poprawność adresu email.') ?>  </h4>
            <br>
            <h4><?= \Yii::t('app', 'Jeśli nie podasz kodu do dnia ') . "<b>" . date('Y-m-d', strtotime("+10 day")) . "</b>" . \Yii::t('app', ' twoje dane podane w poprzednim kroku zostaną usunięte.') ?> </h4>
            <br>
            <h3><?= \Yii::t('app', 'Podaj kod werifikacyjny') ?></h3>
            <?php $form = ActiveForm::begin(['id' => 'email-verification-form']); ?>
            <div class="no-padding col-xs-12">
                <?= $form->field($model, 'registerCode')->textInput(['autofocus' => true, 'placeholder' => \Yii::t('app', 'Kod weryfikacyjny'), 'class' => 'signup-input form-control'])->label(false) ?>
            </div>
            <?= Html::submitButton(Yii::t('app', 'Dalej'), ['class' => 'btn btn-primary signup-button', 'name' => 'signup-button']) ?>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>

