<?php

?>

<div class="container text-center">
    <center>
        <b><h2>Nie podlegasz jeszcze żadnej organizacji.</h2>
            <h1>Utwórz nową aby dołaczyć do zabawy.</h1></b>
            <br/>
            <br/>
            <br/>
            <a href="<?= \yii\helpers\Url::to(['/organization/create']) ?>"
               class="btn btn-block btn-primary">
                <?= Yii::t('app', 'Stwórz nową organizację') ?>
            </a>
        </div>
    </center>
</div>
