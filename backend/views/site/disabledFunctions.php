<?php

?>

<section class="content-header">
    <h1><?= Yii::t('app', 'Wyłączone funkcje dla zakończonej gry') ?></h1>
</section>
<section class="content">
    <div class="box container box-header with-border">
        <ul>
            <li><?=Yii::t('app','Gra')?></li>
            <ul>
                <li><?=Yii::t('app','Edycja obecnej gry')?></li>
            </ul>
            <li><?=Yii::t('app','Gracze')?></li>
            <ul>
                <li><?=Yii::t('app','Zapraszanie nowych graczy do gry')?></li>
            </ul>
            <li><?=Yii::t('app','Zadania')?></li>
            <ul>
                <li><?=Yii::t('app','Dodawanie nowych zadań do gry')?></li>
                <li><?=Yii::t('app','Edycja obecnych zadań w grze')?></li>
            </ul>
            <li><?=Yii::t('app','Drużyny')?></li>
            <ul>
                <li><?=Yii::t('app','Dodawanie nowych drużyn do gry')?></li>
                <li><?=Yii::t('app','Edycja nowych drużyn w grze')?></li>
                <li><?=Yii::t('app','Dodawanie graczy do drużyny')?></li>
                <li><?=Yii::t('app','Usuwanie graczy z drużyny')?></li>
            </ul>
        </ul>
    </div>
</section>
