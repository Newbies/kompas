<?php

use backend\helpers\BreadcrumbHelper;
use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Organization */

$breadcrumb = new BreadcrumbHelper();
?>

<section class="content-header">
    <h1><?= Html::encode(Yii::t('app', 'Edytuj organizację')) ?></h1>
    <ol class="breadcrumb">
        <?= $breadcrumb->home() ?>
        <?= $breadcrumb->updateOrg() ?>
    </ol>
</section>

<section class="content">
    <div class="organization-update box container">
        <div class="box-body">
            <?= $this->render('_form', [
                'model' => $model,
            ]) ?>
        </div>
    </div>
</section>
