<?php

use dosamigos\tinymce\TinyMce;
use kartik\file\FileInput;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Organization */
/* @var $form yii\widgets\ActiveForm */


if (!$model->hasLogo()) {
    $imageInputOptions = [];
} else {
    $imageInputOptions = ['initialPreview' => [
        $model->getLogoSrc(),
    ],
        'initialPreviewAsData' => true,
        'initialPreviewConfig' => [
            ['caption' => Yii::t('app', 'Logo organizacji ')],
        ],
        'overwriteInitial' => true,
        'maxFileSize' => 2800];
}
?>

<div class="organization-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->textarea(); ?>

    <?= $form->field($model, 'contact_email')->textInput(['maxlength' => true]) ?>

    <div class="form-group text-right">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Dodaj') : Yii::t('app', 'Zapisz zmiany'),
            ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
