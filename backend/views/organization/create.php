<?php

use backend\helpers\BreadcrumbHelper;
use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Organization */

$breadcrumb = new BreadcrumbHelper();

?>
<section class="content-header">
    <h1><?= Html::encode(Yii::t('app', 'Stwórz organizacje')) ?></h1>
    <ol class="breadcrumb">
        <?= $breadcrumb->home() ?>
        <?= $breadcrumb->createOrg() ?>
    </ol>
</section>

<section class="content">
    <div class="organization-create box container">
        <div class="box-body">
            <?= $this->render('_form', [
                'model' => $model,
            ]) ?>
        </div>
    </div>
</section>
