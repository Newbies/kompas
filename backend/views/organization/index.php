<?php

use backend\helpers\BreadcrumbHelper;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\Organization */

$breadcrumb = new BreadcrumbHelper()
?>
<section class="content-header">
    <h1><?= Yii::t('app', 'Moje Organizacje') ?></h1>
    <ol class="breadcrumb">
        <?= $breadcrumb->home(); ?>
        <?= $breadcrumb->myOrganization(); ?>
    </ol>
</section>

<section class="content">
    <div class="game-view col-xs-12">
        <div class="container box box-header with-border">
            <?= GridView::widget([
                'dataProvider' => $organizations,
                'columns' => [
                    [
                        'attribute' => 'name',
                        'format' => 'html',
                        'value' => function ($model) {
                            return Html::a($model->name,
                                Url::to('/organization/' . $model->id));
                        },
                    ],
                ]]) ?>
        </div>
    </div>
</section>
