<?php

use backend\helpers\BreadcrumbHelper;
use yii\bootstrap\Modal;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model common\models\Organization */

$breadcrumb = new BreadcrumbHelper();
$this->params['orgId'] = $model->id;
$this->params['orgOwnerId'] = $model->owner_user_id;
?>
    <section class="content-header">
        <h1><?= Html::encode($model->name) ?></h1>
        <ol class="breadcrumb">
            <?= $breadcrumb->home(); ?>
            <?= $breadcrumb->organization($model); ?>
        </ol>
    </section>

    <section class="content-header">
        <div class="row row-clear">
            <div class="game-view col-xs-12">
                <div class="col-xs-12">
                    <div class="container box box-header with-border">
                        <div class="col-xs-8">
                            <div class="col-xs-6">
                                <?= Yii::t('app', 'Email kontaktowy:') ?>
                                <br>
                                <?= Yii::t('app', 'Opis:') ?>
                            </div>
                            <div class="col-xs-6">
                                <?= $model->contact_email ?>
                                <br>
                                <?= $model->description ?>
                            </div>
                        </div>
                        <div class="col-xs-4">
                            <?php if ($model->hasLogo()): ?>
                                <div class="col-xs-12">
                                    <div class="col-xs-6">
                                        <a class="btn btn-primary" data-toggle="modal"
                                           data-target="#update_task_photo"><?= Yii::t('app', 'Zmień logo') ?></a>
                                    </div>
                                    <div class="col-xs-6">
                                        <a class="btn btn-danger"
                                           href="<?= \yii\helpers\Url::to(['/organization/remove-logo', 'id' => $model->id]) ?>">
                                            <?= Yii::t('app', 'Usuń logo') ?></a>
                                    </div>
                                </div>
                                <br>
                                <br>
                                <img src="<?= $model->getLogoSrc() ?>" class="img-responsive">
                            <?php else: ?>
                                <a class="btn btn-primary" data-toggle="modal" data-target="#update_task_photo">
                                    <?= Yii::t('app', 'Dodaj logo') ?>
                                </a>
                            <?php endif; ?>
                        </div>

                    </div>

                    <div class="container box box-header with-border col-xs-5">
                        <?= Html::a(Yii::t('app', 'Zaproś do administrowania'), ['/organization/invite', 'orgId' => $model->id], ['class' => 'btn btn-success']) ?>
                        <?= Html::a(Yii::t('app', 'Edytuj'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                        <a href="<?= \yii\helpers\Url::to(['/organization/leave', 'orgId' => $model->id]) ?>"
                           data-confirm="<?= Yii::t('app', 'Czy napewno chcesz opuścić tą organizację?') ?>"
                           class="btn btn-danger">
                            <?= Yii::t('app', 'Opuść tą organizację') ?>
                        </a>
                    </div>

                    <h4><?= Yii::t('app', 'Administratorzy organizacji') ?></h4>
                    <div class="container box box-header with-border">
                        <?= GridView::widget([
                            'dataProvider' => $admins,
                            'columns' => [
                                [
                                    'class' => 'yii\grid\ActionColumn',
                                    'contentOptions' => ['style' => 'width:10px'],
                                    'template' => '{status}',
                                    'buttons' => [
                                        'status' => function ($model, $key, $index) {
                                            if ($this->params['orgOwnerId'] == $index) {
                                                return '<span class="glyphicon glyphicon-star text-yellow table-icon"/>';
                                            }
                                        }
                                    ],
                                ],
                                'email',
                                [
                                    'class' => 'yii\grid\ActionColumn',
                                    'contentOptions' => ['style' => 'width:10px'],
                                    'template' => '{delete}',
                                    'buttons' => [
                                        'delete' => function ($model, $key, $index) {
                                            if (Yii::$app->user->id != $index && $this->params['orgOwnerId'] != $index) {
                                                return Html::a('<span class="glyphicon glyphicon-trash text-red table-icon"></span>',
                                                    Url::to(['/organization/discard', 'userId' => $index, 'orgId' => $this->params['orgId']]),
                                                    ['data-method' => 'post', 'data-confirm' => Yii::t('app', 'Czy napewno chcesz wyrzucić tego administratora?')]);
                                            }
                                        }
                                    ],
                                ],
                            ],
                        ]) ?>
                        <div class="text-right">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php
Modal::begin([
    'id' => 'update_task_photo',
    'header' => Yii::t('app', 'Wczytaj zdjęcie'),
    'size' => 'modal-xs',
]);
Pjax::begin(['id' => 'organization_update_image', 'enableReplaceState' => false, 'timeout' => 5000, 'enablePushState' => false]);
echo $this->render('/organization/updateLogo', [
    'model' => $model,
]);
Pjax::end();
Modal::end();

?>