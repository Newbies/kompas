<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;


$breadcrumb = new \backend\helpers\BreadcrumbHelper();
?>
<section class="content-header">
    <h1><?= Html::encode(Yii::t('app', 'Zaproś do administrowania')) ?></h1>
    <ol class="breadcrumb">
        <?= $breadcrumb->home() ?>
        <?= $breadcrumb->organization($org) ?>
        <?= $breadcrumb->invite() ?>
    </ol>
</section>

<section class="content">
    <div class="organization-create box container">
        <div class="box-body">

            <?php $form = ActiveForm::begin(); ?>

            <?= $form->field($model, 'email')->textInput(); ?>


            <div class="form-group text-right">
                <?= Html::submitButton(Yii::t('app', 'Zaproś'),
                    ['class' => 'btn btn-success']) ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</section>