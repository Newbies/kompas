<?php

use common\models\Task;
use dosamigos\tinymce\TinyMce;
use janisto\timepicker\TimePicker;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Task */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="task-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->widget(TinyMce::className(), [
        'options' => ['rows' => 6],
        'language' => 'pl',
        'clientOptions' => [
            'plugins' => [
                "advlist autolink lists link charmap print preview anchor",
                "searchreplace visualblocks fullscreen",
                "insertdatetime paste",
            ],
            'toolbar' => "undo redo |  bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
        ]]); ?>

    <?= $form->field($model, 'code')->textInput(['maxlength' => true])->label($model->isNewRecord ? Yii::t('app', 'Kod zadania (pozostaw to pole puste jeśli chcesz aby kod został wygenerowany automatycznie)') : Yii::t('app', 'Kod zadania')) ?>

    <?= $form->field($model, 'score')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'type')->dropDownList([0 => Yii::t('app', 'Standard'), 1 => Yii::t('app', 'Premium'), 2 => Yii::t('app', 'Ograniczone czasowo')], ['id' => 'typeTaskDropList', 'disabled' => $model->isNewRecord ? false : true]) ?>

    <?= $form->field($model, 'show_number')->dropDownList(range(1, $showNumber)) ?>

    <div id="premiumDiv" class="<?php if ($model->type != Task::TYPE_PREMIUM): echo 'hide-options';endif; ?>">

        <?= $form->field($model, 'start_time')->widget(TimePicker::className(), [
            'clientOptions' => [
                'dateFormat' => 'yy-mm-dd',
                'timeFormat' => 'HH:mm:ss',
                'showSecond' => true,
            ]]) ?>

        <?= $form->field($model, 'end_time')->widget(TimePicker::className(), [
            'clientOptions' => [
                'dateFormat' => 'yy-mm-dd',
                'timeFormat' => 'HH:mm:ss',
                'showSecond' => true,
            ]]) ?>

    </div>

    <div id="timedDiv" class="<?php if ($model->type != Task::TYPE_TIMED): echo 'hide-options';endif; ?>">

        <?= $form->field($model, 'time')->textInput(['maxlength' => true]) ?>

    </div>

    <div class="form-group">
        <div class=" text-right">
            <br>
            <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Dalej') : Yii::t('app', 'Zapisz zmiany'),
                ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-success']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>
</div>
