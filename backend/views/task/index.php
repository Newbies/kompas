<?php

use backend\helpers\BreadcrumbHelper;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$breadcrumb = new BreadcrumbHelper();
$this->params['gameId'] = $game->id;
?>
<!-- BREADCRUMB -->
<section class="content-header">
    <h1><?= Yii::t('app', 'Zadania dla gry ') . $game->name; ?></h1>
    <ol class="breadcrumb">
        <?= $breadcrumb->home() ?>
        <?= $breadcrumb->game($game) ?>
        <?= $breadcrumb->tasks() ?>
    </ol>
</section>
<!-- /BREADCRUMB -->


<section class="content">
    <div class="task-index box container box-header with-border">
        <a href="<?= \yii\helpers\Url::to(['game/' . $game->id . '/task/create']) ?>"
           class="btn btn-success <?php if ($game->isEnd()): echo 'disabled'; endif; ?>">
            <?= Yii::t('app', 'Dodaj zadanie') ?>
        </a>
        <div class="box-body">
            <?php
            echo GridView::widget([
                'id' => 'game-grid',
                'emptyText' => Yii::t('app', 'Brak zadań'),
                'dataProvider' => $dataProvider,
                //'filterModel' => $searchModel,
                'tableOptions' => ['class' => 'table no-margin small-view sortable table-responsive table table-hover task-table'],
                'columns' => [
                    [
                        'attribute' => 'show_number',
                        'format' => 'text',
                    ],
                    [
                        'attribute' => 'name',
                        'format' => 'html',
                        'value' => function ($model) {
                            return Html::a($model->getTaskShortName(50),
                                Url::to('/game/' . $this->params['gameId'] . '/task/' . $model->id));
                        },
                    ],
                    [
                        'attribute' => 'description',
                        'format' => 'text',
                        'value' => function ($model) {
                            return $model->getTaskShortDescription(55);
                        },
                    ],
                    [
                        'attribute' => 'code',
                        'format' => 'text',
                    ],
                    [
                        'attribute' => 'score',
                        'format' => 'text',
                    ],
                    [
                        'attribute' => 'type',
                        'format' => 'raw',
                        'value' => function ($model) {
                            return $model->getTypeText();
                        },
                        'filter' => [
                            \common\models\Task::TYPE_PREMIUM => Yii::t('app', 'Premium'),
                            \common\models\Task::TYPE_STANDARD => Yii::t('app', 'Standardowe'),
                            \common\models\Task::TYPE_TIMED => Yii::t('app', 'Czasowe'),
                        ],
                    ],
                    [
                        'attribute' => 'image',
                        'format' => 'raw',
                        'value' => function ($model) {
                            return Html::img('http://' . Yii::$app->params['backDomain'] . "/" . $model->image, ['width' => '30', 'height' => '30']);
                        },
                        'filter' => false

                    ],
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'contentOptions' => ['style' => 'width:10px'],
                        'template' => $game->isEnd() ? '{view} {delete}' : '{view} {update} {delete}',
                        'buttons' => [
                            'view' => function ($model, $key, $index) {
                                return Html::a('<span class="glyphicon glyphicon-eye-open table-icon"></span>',
                                    Url::to('/game/' . $this->params['gameId'] . '/task/view/' . $index));
                            },
                            'update' => function ($model, $key, $index) {
                                return Html::a('<span class="glyphicon glyphicon-pencil table-icon"></span>',
                                    Url::to('/game/' . $this->params['gameId'] . '/task/update/' . $index));
                            },
                            'delete' => function ($model, $key, $index) {
                                return Html::a('<span class="glyphicon glyphicon-trash text-red table-icon"></span>',
                                    Url::to('/game/' . $this->params['gameId'] . '/task/delete/' . $index),
                                    ['data-method' => 'post', 'data-confirm' => Yii::t('app', 'Czy napewno chcesz usunąć to zadanie?')]);
                            }
                        ],
                    ]
                ],
            ]);
            ?>
        </div>
    </div>
</section>


