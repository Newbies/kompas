<?php

use kartik\file\FileInput;
use yii\widgets\ActiveForm;

echo '<center>';
$form = ActiveForm::begin([
    'options' => ['enctype' => 'multipart/form-data', 'data-pjax' => true],
    'action' => ['/task/update-image', 'id' => $model->id, 'gameId' => $model->game_id],
]);

echo FileInput::widget([
    'name' => 'kartiks_file',
    'model' => $model,
    'attribute' => 'file',
    'options' => [
        'multiple' => false,
        'accept' => 'image/*',
    ],
    'pluginOptions' => [
        'showCaption' => false,
        'browseClass' => 'btn btn-primary btn-block',
        'removeClass' => 'btn btn-danger btn-block',
        'uploadClass' => 'btn btn-success btn-block',
        'showRemove' => true,
        'showUpload' => true,
    ]

]);
ActiveForm::end();
echo '</center>';
?>