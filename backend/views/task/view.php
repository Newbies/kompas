<?php

use backend\helpers\BreadcrumbHelper;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model common\models\Task */

$this->title = $model->name;
$breadcrumb = new BreadcrumbHelper();

?>
    <!-- BREADCRUMB -->
    <section class="content-header">
        <h3><?= Html::encode($model->name) ?></h3>
        <ol class="breadcrumb">
            <?= $breadcrumb->home() ?>
            <?= $breadcrumb->game($game) ?>
            <?= $breadcrumb->task($game, $model) ?>
            <?= $breadcrumb->detail() ?>
        </ol>
    </section>
    <!-- /BREADCRUMB -->

    <section class="content-header">
        <div class="row row-clear">

            <div class="task-view col-xs-12">
                <div class="box container box-header with-border">
                    <div class="box-body">
                        <div class="col-xs-8">
                            <h3><?= $model->description ?></h3>
                            <div class="col-xs-6">
                                <br>
                                <div><?= Yii::t('app', 'Numer wyświetlania:') ?></div>
                                <br>
                                <div><?= Yii::t('app', 'Kod zadania:') ?></div>
                                <br>
                                <div><?= Yii::t('app', 'Punkty za zadanie:') ?></div>
                                <br>
                                <div><?= Yii::t('app', 'Rodzaj zadania: ') ?></div>
                                <?php if ($model->isPremium()): ?>
                                    <br>
                                    <div><?= Yii::t('app', 'Czas aktywacji :') ?></div>
                                    <br>
                                    <div><?= Yii::t('app', 'Czas deaktywacji :') ?></div>
                                <?php endif; ?>
                                <?php if ($model->isTimed()): ?>
                                    <br>
                                    <div><?= Yii::t('app', 'Czas na wykonanie zadania (w minutach) :') ?></div>
                                    <br>
                                <?php endif; ?>
                            </div>
                            <div class="col-xs-6">
                                <div class="col-xs-6">
                                    <br>
                                    <div><?= $model->show_number?></div>
                                    <br>
                                    <div><?= $model->code ?></div>
                                    <br>
                                    <div><?= $model->score ?></div>
                                    <br>
                                    <div><?= $model->getTypeText() ?></div>
                                    <?php if ($model->isPremium()): ?>
                                        <br>
                                        <div><?= $model->start_time ?></div>
                                        <br>
                                        <div><?= $model->end_time ?></div>
                                    <?php endif; ?>
                                    <?php if ($model->isTimed()): ?>
                                        <br>
                                        <div><?= $model->time ?></div>
                                        <br>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-4">

                            <?php if ($model->haveImage()): ?>
                                        <a class="btn btn-primary" data-toggle="modal"
                                           data-target="#update_task_photo"><?= Yii::t('app', 'Zmień zdjęcie') ?></a>
                                        <a class="btn btn-danger"
                                           href="<?= \yii\helpers\Url::to(['/task/remove-image', 'id' => $model->id, 'gameId' => $model->game_id]) ?>">
                                            <?= Yii::t('app', 'Usuń zdjęcie') ?></a>
                                <img src="<?= $model->getImageSrc() ?>" class="img-responsive">
                            <?php else: ?>
                                <br>
                                <br>
                                <br>
                                <div><?= Yii::t('app', 'Nie posiada obrazka') ?></div>
                                <a class="btn btn-primary" data-toggle="modal"
                                   data-target="#update_task_photo"><?= Yii::t('app', 'Dodaj obrazek') ?></a>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php
Modal::begin([
    'id' => 'update_task_photo',
    'header' => Yii::t('app', 'Wczytaj zdjęcie'),
    'size' => 'modal-xs',
]);
Pjax::begin(['id' => 'task_update_image', 'enableReplaceState' => false, 'timeout' => 5000, 'enablePushState' => false]);
echo $this->render('/task/updateImage', [
    'model' => $model,
]);
Pjax::end();
Modal::end();
?>
