<?php

use backend\helpers\BreadcrumbHelper;
use yii\helpers\Url;


/* @var $this yii\web\View */
/* @var $model common\models\Task */

$breadcrumb = new BreadcrumbHelper();

?>
<!-- BREADCRUMB -->
<section class="content-header">
    <h1><?= Yii::t('app', 'Dodaj zadanie do gry ') . $game->name ?></h1>
    <ol class="breadcrumb">
        <?= $breadcrumb->home()?>
        <?= $breadcrumb->game($game)?>
        <?= $breadcrumb->addTask()?>
    </ol>
</section>
<!-- /BREADCRUMB -->

<section class="content">
    <div class="task-create box container box-header with-border">
        <div class="box-body">
            <?= $this->render('_form', [
                'model' => $model,
                'calendar' => $model,
                'showNumber' => $showNumber
            ]) ?>
        </div>
    </div>
</section>