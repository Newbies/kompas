<?php

use dosamigos\multiselect\MultiSelect;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>
<div class="container-fluid site-center">
    <div>
        <h3><?= Yii::t('app', 'Wykonaj zadanie ') . $taskName . Yii::t('app', ' dla ... ') ?></h3>
    </div>
    <br>
    <div>
        <?php
        $form = ActiveForm::begin([
            'options' => ['enctype' => 'multipart/form-data', 'data-pjax' => true],
            'action' => ['/task/make-manual-task', 'id' => $model->taskId, 'gameId' => $model->gameId],
        ]);
        ?>
        <?= MultiSelect::widget([
            'model' => $model,
            'attribute' => 'selectedValue',
            'data' => $model->getData(),
            'options' =>
                [
                    'multiple' => 'multiple',
                ],
            'clientOptions' =>
                [
                    'includeSelectAllOption' => true,
                    'numberDisplayed' => 6,
                    'enableCaseInsensitiveFiltering' => true,
                    'nonSelectedText' => Yii::t('app', 'Nic nie wybrano'),
                    'allSelectedText' => Yii::t('app', 'Wybrano wszystkich'),
                    'nSelectedText' => Yii::t('app', 'Wybór dla '),
                    'buttonWidth' => '400px',
                    'maxHeight' => '70px',
                    'inheritClass' => true,
                    'enabledHtml' => true,
                    'selectAllText' => Yii::t('app', 'Zaznacz wszystko'),
                    'filterPlaceholder' => Yii::t('app', 'Szukaj')


                ],
        ]);
        ?>
    </div>
    <br>
    <div>
        <div class="col-xs-6">
            <?= Html::button(Yii::t('app', 'Anuluj'), ['class' => 'btn btn-danger', 'data-dismiss' => 'modal']); ?>
        </div>
        <div class="col-xs-6">
            <?= Html::submitButton(Yii::t('app', 'Wykonaj'), ['class' => 'btn btn-success']); ?>
        </div>
    </div>
    <?php
    ActiveForm::end();
    ?>
</div>
