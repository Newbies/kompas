<?php

use backend\helpers\BreadcrumbHelper;
use kartik\file\FileInput;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = $model->name;
$breadcrumb = new BreadcrumbHelper();

/* @var $this yii\web\View */
/* @var $model common\models\Task */
/* @var $form yii\widgets\ActiveForm */

if (!$model->haveImage()) {
    $imageInputOptions = [
        'showUpload' => false
    ];
} else {
    $imageInputOptions = ['initialPreview' => [
        $model->getImageSrc(),
    ],
        'initialPreviewAsData' => true,
        'initialPreviewConfig' => [
            ['caption' => Yii::t('app', 'Obrazek do zadania')],
        ],
        'overwriteInitial' => true,
        'maxFileSize' => 2800,
        'showUpload' => false
    ];
}
?>
<section class="content-header">
    <h1><?= Yii::t('app', 'Dodaj obrazek do zadania ') . $model->name ?></h1>
    <ol class="breadcrumb">
        <?= $breadcrumb->home() ?>
        <?= $breadcrumb->game($game) ?>
        <?= $breadcrumb->addTask() ?>
        <?= $breadcrumb->uploadImage() ?>
    </ol>
    <br>
</section>

<div class="box box-header with-border">
    <div class="container">
        <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
        <br>
        <label class="control-label"> <?= Yii::t('app', 'Obrazek pomocniczy (krok opcjonalny)') ?> </label>
        <?= FileInput::widget([
            'model' => $model,
            'attribute' => 'file',
            'options' => [

                'multiple' => false,
            ],
            'pluginOptions' => $imageInputOptions
        ]); ?>

        <div class="form-group text-right">
            <div class="row">
                <div class="col-xs-9" style="padding-top: 8px;">
                    <?= $form->field($model, 'addNext')->checkbox()->label(Yii::t('app', 'Zostań na karcie dodawania ')); ?>
                </div>
                <div class="col-xs-3">
                    <?= Html::submitButton(Yii::t('app', 'Dodaj zadanie'),
                        ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                </div>
            </div>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
</div>