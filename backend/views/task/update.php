<?php

use backend\helpers\BreadcrumbHelper;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Task */

$breadcrumb = new BreadcrumbHelper();


?>
<!-- BREADCRUMB -->
<section class="content-header">
    <h1><?= Yii::t('app', 'Edycja: ') . $model->name ?></h1>
    <ol class="breadcrumb">
        <?= $breadcrumb->home(); ?>
        <?= $breadcrumb->game($game); ?>
        <?= $breadcrumb->task($game, $model); ?>
        <?= $breadcrumb->edit(); ?>
    </ol>
</section>
<!-- /BREADCRUMB -->

<section class="content">
    <div class="task-update box container">

        <h1><?= Html::encode($this->title) ?></h1>
        <div class="box-body">
            <?= $this->render('_form', [
                'model' => $model,
                'showNumber' => $showNumber
                ]) ?>
        </div>
    </div>
</section>
