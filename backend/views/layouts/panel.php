<?php

use backend\helpers\ArrayHelper;
use common\models\fields\GameFields;
use common\models\fields\UserOrgFields;
use common\models\Game;
use common\models\User;
use common\models\UserOrg;
use common\widgets\Alert;
use kartik\social\GoogleAnalytics;
use yii\helpers\Html;
use yii\helpers\Url;

backend\assets\PanelAsset::register($this);

$user = User::findOne(['id' => Yii::$app->user->getId()]);

$organizationId = ArrayHelper::map(UserOrg::findAll([UserOrgFields::USER_ID => $user->id]), UserOrgFields::ORG_ID);

$futureGames = Game::find()->where(['in', GameFields::OWNER_ORGANIZATION_ID, $organizationId])
    ->andWhere([GameFields::STATUS => Game::STATUS_FUTURE])
    ->orderBy([GameFields::START_DATE => SORT_DESC])
    ->all();

$currentGames = Game::find()->where(['in', GameFields::OWNER_ORGANIZATION_ID, $organizationId])
    ->andWhere([GameFields::STATUS => Game::STATUS_CURRENT])
    ->orderBy([GameFields::START_DATE => SORT_DESC])
    ->all();

$finishGames = Game::find()->where(['in', GameFields::OWNER_ORGANIZATION_ID, $organizationId])
    ->andWhere([GameFields::STATUS => Game::STATUS_FINISHED])
    ->orderBy([GameFields::START_DATE => SORT_DESC])
    ->all();

$games = array_merge($futureGames, $currentGames, $finishGames);

$selectedGameId = Yii::$app->request->getQueryParam('gameId');
if (Yii::$app->controller->action->controller->id == 'game') {
    $selectedGameId = Yii::$app->request->getQueryParam('id');
}
?>

<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title>Kompas</title>

    <!-- Google Font -->
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

    <?php $this->head() ?>
    <?= GoogleAnalytics::widget([]); ?>

</head>
<body class="skin-black sidebar-mini">
<?php $this->beginBody() ?>
<div class="wrapper">

    <header class="main-header">
        <!-- Logo -->
        <a href="/" class="logo">
            <!-- mini logo for sidebar mini 50x50 pixels -->
            <span class="logo-mini"><b>K</b></span>
            <!-- logo for regular state and mobile devices -->
            <span class="logo-lg"><b>Kompas</b></span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top">
            <!-- Sidebar toggle button-->
            <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                <span class="sr-only">Toggle navigation</span>
            </a>

            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">
                    <?php if ($user->hasOrganization()): ?>
                        <li>
                            <a href="<?= Url::to(['/organization/']) ?>"
                               aria-expanded="false">
                                <i class="fa fa-bank "></i>
                                Moje organizacje
                            </a>
                        </li>
                    <?php endif; ?>
                    <li class="dropdown user user-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <span class="hidden-xs"><?= $user->showMe() ?></span>
                        </a>
                        <ul class="dropdown-menu">
                            <!-- User image -->
                            <li class="user-header">
                                <p>
                                    <?= $user->name . " " . $user->last_name ?>
                                    <small><?= $user->email ?></small>
                                </p>
                            </li>
                            <!-- Menu Footer-->
                            <li class="user-footer">
                                <div class="pull-left">
                                    <!--                                    <a href="-->
                                    <? //= \yii\helpers\Url::to(['/site/settings']) ?><!--"-->
                                    <!--                                       class="btn btn-default btn-flat">-->
                                    <!--                                        <i class="fa fa-gear"></i>-->
                                    <!--                                    </a>-->
                                </div>
                                <div class="pull-right">
                                    <a href="<?= \yii\helpers\Url::to(['site/logout']) ?>"
                                       class="btn btn-default btn-flat"
                                       data-method="post"> <?= Yii::t('app', 'Wyloguj') ?></a>
                                </div>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </nav>
    </header>

    <!-- SLIDE BAR -->
    <aside class="main-sidebar">
        <section class="sidebar">

            <div class="user-panel">
                <div class="text-center profile">
                    <p><?= $user->showMe() ?></p>
                </div>
            </div>

            <div class="sidebar-add-btn">
                <a href="<?= \yii\helpers\Url::to(['/game/create']) ?>"
                   class="btn btn-success btn-block sidebar-add-btn-text">
                    <span><?= Yii::t('app', 'Dodaj grę') ?></span>
                </a>
            </div>

            <ul class="sidebar-menu tree" data-widget="tree">
                <li class="header"><?= Yii::t('app', 'Gry twojej organizacji') ?></li>

                <?php foreach ($games as $game): ?>

                    <li class="treeview <?= $game->id == $selectedGameId ? 'menu-open' : '' ?>">
                        <a href="">
                            <i class="fa fa-circle-o <?= $game->getCircleColor() ?>"></i>
                            <span><?= $game->getGameShortName(15) ?></span>
                            <span class="pull-right-container"> <i class="fa fa-angle-left pull-right"></i><div></div></span>
                        </a>
                        <ul class="treeview-menu" style="<?php if ($game->id == $selectedGameId): echo 'display: block';
                        else: 'display: none'; endif; ?>">
                            <li>
                                <a href="<?= \yii\helpers\Url::to(['/game/view', 'id' => $game->id]) ?>">
                                    <i class="fa fa-navicon"></i>
                                    <?= Yii::t('app', 'Szczegóły') ?>
                                </a>
                            </li>
                            <li>
                                <a href="<?= \yii\helpers\Url::to(['/game/' . $game->id . '/task']) ?>">
                                    <i class="fa fa-tasks"></i>
                                    <?= Yii::t('app', 'Zadania') ?>
                                    <span class="pull-right-container">
                                    <span class="label label-info pull-right">
                                        <?= $game->getTaskCount() ?>
                                    </span>
                                    </span>
                                </a>
                            </li>
                            <li>
                                <a href="<?= \yii\helpers\Url::to(['/game/' . $game->id . '/user']) ?>">
                                    <i class="fa fa-user"></i>
                                    <?= Yii::t('app', 'Gracze') ?>
                                    <span class="pull-right-container">
                                    <span class="label label-info pull-right">
                                        <?= $game->getUserCount() ?>
                                    </span>
                                    </span>
                                </a>
                            </li>
                            <li>
                                <a href="<?= \yii\helpers\Url::to(['/timeline/game','gameId'=>$game->id]) ?>">
                                    <i class="fa fa-bar-chart"></i>
                                    <?= Yii::t('app', 'Postęp') ?>
                                </a>
                            </li>
                            <?php if ($game->isIndividual()): ?>
                                <li>
                                    <a href="<?= \yii\helpers\Url::to(['/game/' . $game->id . '/team']) ?>">
                                        <i class="fa fa-group"></i>
                                        <?= Yii::t('app', 'Drużyny') ?>
                                        <span class="pull-right-container">
                                        <span class="label label-info pull-right">
                                            <?= $game->getTeamCount() ?>
                                        </span>
                                        </span>
                                    </a>
                                </li>
                            <?php endif; ?>
                            <li><a href="<?= \yii\helpers\Url::to(['/game/' . $game->id . '/board']) ?>">
                                    <i class="fa fa-line-chart">
                                    </i> <?= Yii::t('app', 'Ranking ') ?>
                                </a></li>
                        </ul>
                    </li>
                <?php endforeach; ?>
            </ul>
        </section>
    </aside>

    <!-- MAIN CONTENT -->
    <div class="content-wrapper">
        <section class="content">
            <?= Alert::widget() ?>
            <?= $content ?>
        </section>
    </div>


    <!-- FOOTER -->
    <!--    <footer class="main-footer">-->
    <!--        <div class="pull-right hidden-xs">-->
    <!--            <b>Version</b> 1.0.0-->
    <!--        </div>-->
    <!--        <strong>Copyright &copy; 2016---><?php //echo date("Y");?><!--. </strong> All rights-->
    <!--        reserved.-->
    <!--    </footer>-->
</div>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>

