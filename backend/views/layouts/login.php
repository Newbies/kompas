<?php

/* @var $this \yii\web\View */

/* @var $content string */

use common\widgets\Alert;
use kartik\social\GoogleAnalytics;
use yii\helpers\Html;

backend\assets\LoginAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title>Kompas</title>
    <?php $this->head() ?>
    <?= GoogleAnalytics::widget([]); ?>

</head>
<body class="hold-transition login-page">
<?php $this->beginBody() ?>
<?= Alert::widget() ?>
<?= $content ?>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
