<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-backend',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'backend\controllers',
    'bootstrap' => ['log'],
    'modules' => [
        'redactor' => 'yii\redactor\RedactorModule',
        'social' => [
            'class' => 'kartik\social\Module',
            'googleAnalytics' => [
                'id' => 'UA-106066444-2',
                'domain' => 'admin.kompas.newbies.pl'
            ],
        ],
    ],
    'layout' => 'panel',
    'components' => [
//        'request' => [
//            'csrfParam' => '_csrf-backend',
//        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
            // 'identityCookie' => ['name' => '_identity-backend', 'httpOnly' => true],
        ],
        'image' => [
            'class' => 'yii\image\ImageDriver',
            'driver' => 'GD',  //GD or Imagick
        ],
//        'session' => [
//            // this is the name of the session cookie used for login on the backend
//            'name' => 'advanced-backend',
//        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
                'email' => [
                    'class' => 'yii\log\EmailTarget',
                    'except' => ['yii\web\HttpException:404', 'yii\web\HttpException:401'],
                    'levels' => ['error'],
                    'message' => ['from' => 'kompas@newbies.pl', 'to' => ['newbies@rst.com.pl']],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'urlManager' => [
            'class' => 'yii\web\UrlManager',
            // Disable index.php
            'showScriptName' => false,
            // Disable r= routes
            'enablePrettyUrl' => true,
            'rules' => array(
                '<controller:\w+>/<id:\d+>' => '<controller>/view',
                '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
                '<controller:\w+>/<action:\w+>' => '<controller>/<action>',

                'game/<gameId:\d+>/<controller:\w+>' => '<controller>/index',
                'game/<gameId:\d+>/<controller:\w+>/<id:\d+>' => '<controller>/view',
                'game/<gameId:\d+>/<controller:\w+>/<action:\w+>' => '<controller>/<action>',
                'game/<gameId:\d+>/<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
            ),
        ],
        'i18n' => [
            'translations' => [
                'app*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    //'basePath' => '@app/messages',
                    //'sourceLanguage' => 'en-US',
                    'fileMap' => [
                        'app' => 'app.php',
                        'app/error' => 'bad.php',
                    ],
                ],
            ],
        ],
    ],
    'params' => $params,
];
