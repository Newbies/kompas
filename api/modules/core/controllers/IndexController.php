<?php
namespace api\modules\core\controllers;

use  yii\web\Controller;

class IndexController extends Controller {
	
	
	public function __construct($id, $module, $config = array()) {
        parent::__construct($id, $module, $config);
    }

	/**
     * Authentication bahaviors
     *
     * @return array
     */
    public function behaviors()
    {
	    return  parent::behaviors();
    }

	/**
	 * Set the logged user
	 *
	 * @param type $action
	 * @return boolean
	 */
	public function beforeAction($action) {
        return parent::beforeAction($action);
    }
	
	public function actionIndex(){
		return [
			'core' => 'v.1',
			'status' => 'running',
			'avaliableApis' => ['v1']];
	}
}