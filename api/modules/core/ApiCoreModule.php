<?php
namespace api\modules\core;

class ApiCoreModule extends \yii\base\Module {
	
	 public $controllerNamespace = 'api\modules\core\controllers';
	 
    public function init() {
        parent::init();  

        $this->modules = [
            'v1' => [
                'class' => 'api\modules\core\modules\v1\Module',
            ],
        ];      
    }
}