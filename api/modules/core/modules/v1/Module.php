<?php
namespace api\modules\core\modules\v1;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'api\modules\core\modules\v1\controllers';

    public function init()
    {
        parent::init();        
    }
}