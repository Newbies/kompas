<?php

namespace api\modules\core\modules\v1\controllers;

use common\models\User;
use Yii;

class UserController extends BaseController
{

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => \yii\filters\auth\HttpBasicAuth::className(),
            'auth' => [User::className(), 'apiAuth'],
            'except' =>
                [
                    'post',
                    'put',
                ],
        ];
        return $behaviors;
    }


    public function actionIndex()
    {
        return ['status' => 'running'];
    }

    public function actionGet()
    {
        $params = Yii::$app->request->get();
        if (empty($params)) {
            return User::find()->all();
        }
        $users = User::findAll($params);

        if (count($users) == 1) {
            return $users[0];
        }
        return $users;
    }

    public function actionPut()
    {
        $params = Yii::$app->request->post();

        if (empty($params)) {
            Yii::$app->response->statusCode = 412;
            return ['message' => 'empty params'];
        }

        $user = User::findOne($params['id']);

        $user->setScenario('apiPut');
        $user->setAttributes($params);

        if ($user->update(true)) {
            Yii::$app->response->statusCode = 201;
            return $user;
        }
        Yii::$app->response->statusCode = 422;
        return $user->getErrors();

    }

    public function actionPost()
    {
        $params = Yii::$app->request->post();

        if (empty($params)) {
            Yii::$app->response->statusCode = 412;
            return ['message' => 'empty params'];
        }

        $user = new User();

        //must have variable
        $user->role = 0;
        $user->email = $params['email'];
        $user->alias = $params['alias'];
        $user->setPassword($params['password']);
        $user->generatePasswordResetToken();
        $user->generateAuthKey();

        //If variable exists
        if ($params['name']) {
            $user->name = $params['name'];
        }
        if ($params['last_name']) {
            $user->last_name = $params['last_name'];
        }

        if ($user->save()) {
            Yii::$app->response->statusCode = 201;
            return $user;
        } else {
            Yii::$app->response->statusCode = 422;
            return $user->getErrors();
        }
    }

    public function actionDelete()
    {
        return null;
    }
}