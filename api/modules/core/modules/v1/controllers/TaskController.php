<?php

namespace api\modules\core\modules\v1\controllers;


use common\models\Task;
use Yii;

/**
 * Endpoint for managing tasks
 */
class TaskController extends BaseController
{

    public function actionIndex()
    {
        return ['status' => 'running'];
    }

    public function actionGet()
    {
        $params = Yii::$app->request->get();

        if (empty($params)) {
            return Task::find()->all();
        }

        $tasks = Task::findAll($params);

        if (count($tasks) == 1) {
            return $tasks[0];
        }
        return $tasks;
    }

    public function actionPut()
    {
        return null;
    }

    public function actionPost()
    {
        return null;
    }

    public function actionDelete()
    {
        return null;
    }
}	

