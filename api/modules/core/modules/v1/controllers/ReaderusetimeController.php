<?php

namespace api\modules\core\modules\v1\controllers;

use common\models\ReaderUseTime;
use Yii;

class ReaderusetimeController extends BaseController
{
    public function actionIndex()
    {
        return ['status' => 'running'];
    }

    public function actionGet()
    {
        $params = Yii::$app->request->get();

        if (empty($params)) {
            return ReaderUseTime::find()->all();
        }

        $readerUseTimes = ReaderUseTime::findAll($params);

        if (count($readerUseTimes) == 1) {
            return $readerUseTimes[0];
        }
        return $readerUseTimes;
    }

    public function actionPost()
    {
        $params = Yii::$app->request->post();

        if (empty($params)) {
            Yii::$app->response->statusCode = 412;
            return ['message' => 'empty params'];
        }

        $readerUseTime = new ReaderUseTime();
        $readerUseTime->user_id = $params['user_id'];
        $readerUseTime->timestamp = time() + 5;

        if ($readerUseTime->save()) {
            Yii::$app->response->statusCode = 201;
            return $readerUseTime;
        } else {
            Yii::$app->response->statusCode = 422;
            return $readerUseTime->getErrors();
        }
    }

    public function actionPut()
    {
        $params = Yii::$app->request->post();

        if (empty($params)) {
            Yii::$app->response->statusCode = 412;
            return ['message' => 'empty params'];
        }
        $readerUseTime = ReaderUseTime::findOne(['user_id' => $params['user_id']]);
        $readerUseTime->timestamp = time() + 5;
        if ($readerUseTime->update(false)) {
            Yii::$app->response->statusCode = 204;
            return $readerUseTime;
        } else {
            Yii::$app->response->statusCode = 422;
            return $readerUseTime->getErrors();
        }
    }

    public function actionDelete()
    {
        return null;
    }

}