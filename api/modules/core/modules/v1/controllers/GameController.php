<?php

namespace api\modules\core\modules\v1\controllers;

use common\models\Game;
use Yii;

/**
 * Endpoint for managing tasks
 */
class GameController extends BaseController
{

    public function actionIndex()
    {
        return ['status' => 'running'];
    }

    public function actionGet()
    {
        $params = Yii::$app->request->get();
        if (empty($params)) {
            return Game::find()->all();
        }

        $games = Game::findAll($params);

        if (count($games) == 1) {
            return $games[0];
        }
        return $games;
    }

    public function actionPut()
    {
        return null;
    }

    public function actionPost()
    {
        return null;
    }

    public function actionDelete()
    {
        return null;
    }
}