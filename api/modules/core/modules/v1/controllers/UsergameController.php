<?php

namespace api\modules\core\modules\v1\controllers;

use common\models\UserGame;
use Yii;

class UsergameController extends BaseController
{
    public function actionIndex()
    {
        return ['status' => 'running'];
    }

    public function actionGet()
    {
        $params = Yii::$app->request->get();

        if (empty($params)) {
            return UserGame::find()->all();
        }

        $userGames = UserGame::findAll($params);

        if (count($userGames) == 1) {
            return $userGames[0];
        }
        return $userGames;
    }

    public function actionPut()
    {
        $params = Yii::$app->request->post();

        if (empty($params)) {
            Yii::$app->response->statusCode = 412;
            return ['message' => 'empty params'];
        }
        $userGame = UserGame::findOne(['user_id' => $params['user_id'], 'game_id' => $params['game_id']]);
        $userGame->score += $params['score'];
        if ($userGame->update(false)) {
            Yii::$app->response->statusCode = 200;
            return $userGame;
        } else {
            Yii::$app->response->statusCode = 422;
            return $userGame->getErrors();
        }
    }

    public function actionPost()
    {
        $params = Yii::$app->request->post();

        if (empty($params)) {
            Yii::$app->response->statusCode = 412;
            return ['message' => 'empty params'];
        }

        $userGame = new UserGame();
        $userGame->user_id = $params['user_id'];
        $userGame->game_id = $params['game_id'];
        $userGame->score = 0;

        if ($userGame->save()) {
            Yii::$app->response->statusCode = 201;
            return $userGame;
        } else {
            Yii::$app->response->statusCode = 422;
            return $userGame->getErrors();
        }
    }

    public function actionDelete()
    {

        $params = Yii::$app->request->get();

        if (empty($params)) {
            Yii::$app->response->statusCode = 412;
            return ['message' => 'empty params'];
        }

        $userGame = UserGame::findOne($params);

        if(!$userGame){
            Yii::$app->response->statusCode = 204;
            return ['message' => 'no content'];
        }

        if ($userGame->delete()) {
            Yii::$app->response->statusCode = 200;
            return ['message' => 'Deleted'];
        } else {
            Yii::$app->response->statusCode = 422;
            return $userGame->getErrors();
        }
    }
}