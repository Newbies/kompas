<?php

namespace api\modules\core\modules\v1\controllers;

use common\models\User;
use yii\web\Controller;

/**
 * Index Controller
 *
 */
class BaseController extends Controller
{
    /* @var $account common\models\User */
    protected $user = null;

    public function __construct($id, $module, $config = array())
    {
        parent::__construct($id, $module, $config);
    }

    /**
     * Authentication bahaviors
     *
     * @return array
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => \yii\filters\auth\HttpBasicAuth::className(),
            'auth' => [User::className(), 'apiAuth'],
        ];
        return $behaviors;
    }

    /**
     * Set the logged user
     *
     * @param type $action
     * @return boolean
     */
    /**
    public function beforeAction($action)
    {
        parent::beforeAction($action);
        $this->user = User::find()
            ->where(['id' => \Yii::$app->user->id])->one();
        return true;
    }
     */

}