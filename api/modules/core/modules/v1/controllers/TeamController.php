<?php

namespace api\modules\core\modules\v1\controllers;

use common\models\Team;
use Yii;

/**
 * Endpoint for managing tasks
 */
class TeamController extends BaseController
{

    public function actionIndex()
    {
        return ['status' => 'running'];
    }

    public function actionGet()
    {
        $params = Yii::$app->request->get();

        if (empty($params)) {
            return Team::find()->all();
        }

        $teams = Team::findAll($params);

        if (count($teams) == 1) {
            return $teams[0];
        }
        return $teams;
    }

    public function actionPut()
    {
       // Scores is counted by UserGameController on Put
    }

    public function actionPost()
    {
        return null;
    }

    public function actionDelete()
    {
        return null;
    }
}