<?php

namespace api\modules\core\modules\v1\controllers;

use common\models\CompletedTask;
use Yii;

class CompletedController extends BaseController
{
    public function actionIndex()
    {
        return ['status' => 'running'];
    }

    public function actionGet()
    {
        $params = Yii::$app->request->get();
        if (empty($params)) {
            return CompletedTask::find()->all();
        }

        $completeTasks = CompletedTask::findAll($params);
        if (count($completeTasks) == 1) {
            return $completeTasks[0];
        }
        return $completeTasks;

    }

    public function actionPost()
    {
        $params = Yii::$app->request->post();

        if (empty($params)) {
            Yii::$app->response->statusCode = 412;
            return ['message' => 'empty params'];
        }
        $completedTask = new CompletedTask();
        $completedTask->user_id = $params['user_id'];
        $completedTask->task_id = $params['task_id'];
        $completedTask->team_id = $params['team_id'];

        if ($completedTask->save()) {
            Yii::$app->response->statusCode = 201;
            return $completedTask;
        } else {
            Yii::$app->response->statusCode = 422;
            return $completedTask->getErrors();
        }
    }

    public function actionPut()
    {
        return null;
    }

    public function actionDelete()
    {
        return null;
    }
}