<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);


return [
    'id' => 'app-api',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'modules' => [
        'core' => [
            'class' => 'api\modules\core\ApiCoreModule'
        ],

    ],

    'components' => [
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => false,
            'enableSession' => false,
        ],

        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
                'email' => [
                    'class' => 'yii\log\EmailTarget',
                    'except' => ['yii\web\HttpException:404','yii\web\HttpException:401'],
                    'levels' => ['error'],
                    'message' => ['from' => 'kompas@newbies.pl', 'to' => ['newbies@rst.com.pl']],
                ],
            ],
        ],
//		'urlManagerBackend' => [
//			'class' => 'yii\web\UrlManager',
//			'enablePrettyUrl' => true,
//			'showScriptName' => false,
//			'enableStrictParsing' => false,
//		],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'enableStrictParsing' => false,
            'showScriptName' => false,
            'rules' => [
                //Users
                [
                    'verb' => ['GET'],
                    'pattern' => 'v1/user/',
                    'route' => 'core/v1/user/get',
                ],
                [
                    'verb' => ['PUT'],
                    'pattern' => 'v1/user/',
                    'route' => 'core/v1/user/put',
                ],
                [
                    'verb' => ['POST'],
                    'pattern' => 'v1/user/',
                    'route' => 'core/v1/user/post',
                ],
                [
                    'verb' => ['DELETE'],
                    'pattern' => 'v1/user/',
                    'route' => 'core/v1/user/delete',
                ],
                //Game
                [
                    'verb' => ['GET'],
                    'pattern' => 'v1/game/',
                    'route' => 'core/v1/game/get',
                ],
                [
                    'verb' => ['PUT'],
                    'pattern' => 'v1/game/',
                    'route' => 'core/v1/game/put',
                ],
                [
                    'verb' => ['POST'],
                    'pattern' => 'v1/game/',
                    'route' => 'core/v1/game/post',
                ],
                [
                    'verb' => ['DELETE'],
                    'pattern' => 'v1/game/',
                    'route' => 'core/v1/game/delete',
                ],

                //Task
                [
                    'verb' => ['GET'],
                    'pattern' => 'v1/task',
                    'route' => 'core/v1/task/get',
                ],
                [
                    'verb' => ['PUT'],
                    'pattern' => 'v1/task/',
                    'route' => 'core/v1/task/put',
                ],
                [
                    'verb' => ['POST'],
                    'pattern' => 'v1/task/',
                    'route' => 'core/v1/task/post',
                ],
                [
                    'verb' => ['DELETE'],
                    'pattern' => 'v1/task/',
                    'route' => 'core/v1/task/delete',
                ],

                //CompletedTask
                [
                    'verb' => ['GET'],
                    'pattern' => 'v1/completed/',
                    'route' => 'core/v1/completed/get',
                ],
                [
                    'verb' => ['POST'],
                    'pattern' => 'v1/completed/',
                    'route' => 'core/v1/completed/post',
                ],
                [
                    'verb' => ['PUT'],
                    'pattern' => 'v1/completed/',
                    'route' => 'core/v1/completed/put',
                ],
                [
                    'verb' => ['DELETE'],
                    'pattern' => 'v1/completed/',
                    'route' => 'core/v1/completed/delete',
                ],

                //UserGame
                [
                    'verb' => ['GET'],
                    'pattern' => 'v1/usergame/',
                    'route' => 'core/v1/usergame/get',
                ],
                [
                    'verb' => ['POST'],
                    'pattern' => 'v1/usergame/',
                    'route' => 'core/v1/usergame/post',
                ],
                [
                    'verb' => ['PUT'],
                    'pattern' => 'v1/usergame/',
                    'route' => 'core/v1/usergame/put',
                ],
                [
                    'verb' => ['DELETE'],
                    'pattern' => 'v1/usergame/',
                    'route' => 'core/v1/usergame/delete',
                ],
                //ReaderUseTime
                [
                    'verb' => ['GET'],
                    'pattern' => 'v1/readerusetime/',
                    'route' => 'core/v1/readerusetime/get',
                ],
                [
                    'verb' => ['POST'],
                    'pattern' => 'v1/readerusetime/',
                    'route' => 'core/v1/readerusetime/post',
                ],
                [
                    'verb' => ['PUT'],
                    'pattern' => 'v1/readerusetime/',
                    'route' => 'core/v1/readerusetime/put',
                ],
                [
                    'verb' => ['DELETE'],
                    'pattern' => 'v1/readerusetime/',
                    'route' => 'core/v1/readerusetime/delete',
                ],
                //Team
                [
                    'verb' => ['GET'],
                    'pattern' => 'v1/team/',
                    'route' => 'core/v1/team/get',
                ],
                [
                    'verb' => ['POST'],
                    'pattern' => 'v1/team/',
                    'route' => 'core/v1/team/post',
                ],
                [
                    'verb' => ['PUT'],
                    'pattern' => 'v1/team/',
                    'route' => 'core/v1/team/put',
                ],
                [
                    'verb' => ['DELETE'],
                    'pattern' => 'v1/team/',
                    'route' => 'core/v1/team/delete',
                ],
                //Other
                [
                    'verb' => ['GET'],
                    'pattern' => '/',
                    'route' => 'core/index/index',
                ],
            ],
        ],
        'request' => [
            'parsers' => [
                'application/json' => 'yii\web\JsonParser',
            ],
            'enableCookieValidation' => false,
            'enableCsrfCookie' => false,
            'enableCsrfValidation' => false
        ],
        'response' => [
            'format' => yii\web\Response::FORMAT_JSON,
        ],
    ],
    'timeZone' => 'Europe/Warsaw',
    'params' => $params,
];
